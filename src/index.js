import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import GlobalStyle from './styles/GlobalStyle';
import 'font-awesome/css/font-awesome.css'


ReactDOM.render(
  <React.StrictMode>
      <GlobalStyle />
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  
  </React.StrictMode>,
  document.getElementById('root')
);

