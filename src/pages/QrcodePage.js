import React, {
  useEffect,
  useState
} from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import QrReader from "react-qr-reader";
import GridBackground from '../img/QRCode.svg';
import { QRCodeStyled } from '../styles/QRCodeStyle';
import {
  Button,
  Modal, DropdownButton, Dropdown, ListGroup, Card
} from "react-bootstrap";
import styled from 'styled-components';
import NotificationSystem from "react-notification-system";
import Swal from 'sweetalert2';
import moment from 'moment';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';

let morningToTime = '';
let morningFromTimeG = '';
let afternoonToTime = '';
let afternoonFromTime = '';
let userMealRecord;

function AttendancePage() {
  const [result, setresult] = useState('No result');
  const [attendance, setattendance] = React.useState(false);
  const [time, setTime] = useState(new Date());

  const [firstname, setfirstname] = useState('');
  const [lastname, setlastname] = useState('');
  const [address, setaddress] = useState('');
  const [department, setdepartment] = useState('');
  const [email, setemail] = useState('');
  const [cpnumber, setcpnumber] = useState('');
  const [id, setid] = useState('');
  const [capturedDate, setcapturedDate] = useState(new Date());

  const [mealEmployeeQr, setmealEmployeeQr] = useState(false);
  const [meal, setmeal] = useState(false);
  const [mealType, setmealType] = useState('breakfast');
  const [mealId, setmealId] = useState('');
  const [mealSelected, setmealSelected] = useState('Menu');
  const [mealOption, setmealOption] = useState('breakfast');
  const [mealServed, setmealServed] = useState(0);
  const [estimation, setestimation] = useState(0);
  const [allAttendance, setallAttendance] = useState([]);
  const [allMealRecords, setallMealRecords] = useState([]);
  const [allMeals, setallMeals] = useState([]);
  const [mealToday, setmealToday] = useState([]);
  const [morningFrom, setmorningFrom] = React.useState('');
  const [morningTo, setmorningTo] = React.useState('');
  const [afternoonFrom, setafternoonFrom] = React.useState('');
  const [afternoonTo, setafternoonTo] = React.useState('');
  const [morningAfternoon, setmorningAfternoon] = React.useState('');
  const [morningAfternoonTo, setmorningAfternoonTo] = React.useState('');
  const [attendanceToday, setattendanceToday] = React.useState('');
  const [remarks, setremarks] = React.useState('');
  const [theTimeIn, settheTimeIn] = React.useState(false);
  const [theTimeOut, settheTimeOut] = React.useState(false);
  const [show, setshow] = React.useState(false);
  const [earlyOut, setearlyOut] = React.useState('');

  const [morningPresent, setmorningPresent] = React.useState('');
  const [afternoonPresent, setafternoonPresent] = React.useState('');

  const [morningFromIO, setmorningFromIO] = React.useState('');
  const [morningToIO, setmorningToIO] = React.useState('');
  const [afternoonFromIO, setafternoonFromIO] = React.useState('');
  const [afternoonToIO, setafternoonToIO] = React.useState('');

  const [morningFromTime, setmorningFromTime] = React.useState('');
  const [morningToTime2, setmorningToTime] = React.useState('');
  const [afternoonFromTime2, setafternoonFromTime] = React.useState('');
  const [afternoonToTime2, setafternoonToTime] = React.useState('');
  const [lateMorningOut, setlateMorningOut] = React.useState(false);
  const [morningAbsent, setmorningAbsent] = React.useState(false);

  const [breakfastCheck, setbreakfastCheck] = React.useState(false);
  const [lunchCheck, setlunchCheck] = React.useState(false);
  const [dinnerCheck, setdinnerCheck] = React.useState(false);
  const [attendanceExist, setattendanceExist] = React.useState(false);
  const [employeeMealRecord, setemployeeMealRecord] = React.useState([]);

  let addMorningIN = [];
  let addMorningOUT = [];
  let addAfternoonIN = [];
  let addAfternoonOUT = [];
  let startDate = new Date();
  const formatAMPM = (date) => {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }


  useEffect(() => {


    axios.get(process.env.REACT_APP_BASE_URL + '/employee/attall')
      .then(res => {
        setallAttendance(res.data)
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecordsall')
      .then(res => {
        setallMealRecords(res.data);
      })
      .catch(error => {
        console.log(error);
      })

    const startDate = new Date;
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        setallMeals(res.data);

        let theMealToday = res.data.filter(function (pilot) {
          return pilot.mealDate === moment(new Date()).format('MM-DD-YYYY');
        });
        const mapping = theMealToday.map(year => [{
          name: year.mealName,
          id: year._id,
          type: year.mealType,
          estimation: year.mealServedEst,
          mealServed: year.mealServed,
        }]);

        setmealToday(Object.keys(mapping).map(function (key) {
          return {
            'name': mapping[key][0].name, 'id': mapping[key][0].id, 'type': mapping[key][0].type,
            'estimation': mapping[key][0].estimation, 'mealServed': mapping[key][0].mealServed
          };
        }))

      })
      .catch(error => {
        console.log(error);
      })


    //Get Time Schedule set
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
      .then(res => {
        setmorningFrom(res.data[0].morningFrom);
        setmorningTo(res.data[0].morningTo);
        morningToTime = res.data[0].morningTo;
        morningFromTimeG = res.data[0].morningFrom;
        afternoonToTime = res.data[0].afternoonTo;
        afternoonFromTime = res.data[0].afternoonFrom;
        setafternoonFrom(res.data[0].afternoonFrom);
        setafternoonTo(res.data[0].afternoonTo);
        setmorningAfternoon(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(res.data[0].afternoonFrom, 'h:mma')) ? res.data[0].afternoonFrom : res.data[0].afternoonFrom);
        setmorningAfternoonTo(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(res.data[0].afternoonFrom, 'h:mma')) ? res.data[0].afternoonFrom : res.data[0].afternoonTo);
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear()
      }
    })
      .then(res => {
        setattendanceToday(res.data);

        const ids = res.data.map(o => o.user?._id);
        let presentEmployees = res.data?.filter(({ user }, index) => !ids.includes(user?._id, index + 1));
        // setTotalWorkersPresent(presentEmployees.length);
        let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });

        const idsMorning = presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFrom, 'h:mma')) || v.includes("lateOUT")) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
        const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

        setmorningPresent(presentInMorning?.filter(el => (-1 == idsMorning.indexOf(el.user._id)))?.length);
        setafternoonPresent(presentInAfternoon?.filter(el => (-1 == idsAfternoon.indexOf(el.user._id)))?.length);

        setmorningFromIO(presentInMorning.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; }));
        setmorningToIO(presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0 }));
        setafternoonFromIO(presentInAfternoon.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; }));
        setafternoonToIO(presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonToTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }));


      })
      .catch(error => {
        console.log(error);
      })

    return () => {
      setresult('No result');
      setTime(new Date());
      setcapturedDate(new Date());
      setfirstname('');
      setlastname('')
      setaddress('');
      setdepartment('');
      setemail('');
      setcpnumber('');
      setid('');
      setshow(false);
      setallAttendance([]);
      setattendance(false);
      setmeal(false);
      settheTimeIn(false);
      settheTimeOut(false);
      setmealSelected('Menu');
      setmealId('');
      setallMealRecords([]);
      setmealType('breakfast');
      setmealOption('breakfast');
      setmealEmployeeQr(false);
      setattendanceToday([]);
      setestimation(0);
      setmealServed(0);
      setallMeals([]);
      setmorningFrom('');
      setmorningTo('');
      setafternoonFrom('');
      setafternoonTo('');
      setremarks('');
      setmorningAfternoon('');
      setmorningAfternoonTo('');
      // setmorningFromIO('');
      // setmorningToIO('');
      // setafternoonFromIO('');
      // setafternoonToIO('');

    }

  }, [])


  const handleClose = () => {
    setresult('No result');
    setTime(new Date());
    setcapturedDate(new Date());
    setfirstname('');
    setlastname('')
    setaddress('');
    setdepartment('');
    setemail('');
    setcpnumber('');
    setid('');
    setshow(false);
    setallAttendance([]);
    setattendance(false);
    setmeal(false);
    settheTimeIn(false);
    settheTimeOut(false);
    setmealSelected('Menu');
    setmealId('');
    setallMealRecords([]);
    setmealType('breakfast');
    setmealOption('breakfast');
    setmealEmployeeQr(false);
    setattendanceToday([]);
    setestimation(0);
    setmealServed(0);
    setallMeals([]);
    setmorningFrom('');
    setmorningTo('');
    setafternoonFrom('');
    setafternoonTo('');
    setremarks('');
    setmorningAfternoon('');
    setmorningAfternoonTo('');

    window.location = "/qrcode";

  }

  const handleError = err => {
    console.error(err);
  };

  const handleScan = data => {
    let existing = allAttendance?.filter(el => el.user === id && el.dateRecorded === new Date().toLocaleDateString());
    if (existing.length !== 0) {
      setattendanceExist(true);
    } else {
      setattendanceExist(false);
    }

    let userID;
    if (data) {
      if (!data.includes(',')) {
        setresult(data);

        userID = data;
      } else {
        let generatedMeal = allMeals?.filter(x =>
          x._id === data.split(',')[1]
        );
        setresult(data.split(',')[0]);
        setmealEmployeeQr(true);
        setmealType(data.split(',')[2]);
        setmealId(data.split(',')[1]);
        setmealSelected(data.split(',')[3]);
        setestimation(generatedMeal[0].mealServedEst);
        setmealServed(generatedMeal[0].mealServed);
        setmealOption(data.split(',')[2]);
        userID = data.split(',')[0];
      }

      console.log("userID.length", userID, data)

      if (userID.length !== 0) {
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/' + userID)
          .then(res => {
            if (res.data) {
              console.log('dataaaaaaaaaaaaa', userID, mealId)

              setfirstname(res.data.firstname);
              setlastname(res.data.lastname);
              setaddress(res.data.address);
              setdepartment(res.data.department);
              setemail(res.data.email);
              setcpnumber(res.data.cpnumber);
              setid(res.data._id);
              setcapturedDate(time.toString());

            } else {
              Swal.fire({
                title: "We can't find you in the System",
                text: `If you think this is wrong please go see the Admin`,
                icon: 'warning',
                confirmButtonText: 'Okay'
              }).then((result) => {
                if (result.isConfirmed) {
                  window.location = "/qrcode"
                }
              })
            }


          })
          .catch(error => {
            console.log(error);
          })


        axios.get(process.env.REACT_APP_BASE_URL + '/employee/get/getAttendance', {
          params: {
            date: new Date().toLocaleDateString(),
            id: userID
          }
        })
          .then(res => {
            if (res.data) {
              console.log('ate2222', res.data, res.data[0].breakfast)

              setbreakfastCheck(res.data[0]?.breakfast !== undefined ? res.data[0]?.breakfast === "true" ? true : false : false);
              setlunchCheck(res.data[0]?.lunch !== undefined ? res.data[0]?.lunch === "true" ? true : false : false);
              setdinnerCheck(res.data[0]?.dinner !== undefined ? res.data[0]?.dinner === "true" ? true : false : false);

              console.log('breakfastCheck', breakfastCheck, lunchCheck, dinnerCheck)
            } else {
              Swal.fire({
                title: "We can't find you in the System",
                text: `If you think this is wrong please go see the Admin`,
                icon: 'warning',
                confirmButtonText: 'Okay'
              }).then((result) => {
                if (result.isConfirmed) {
                  window.location = "/qrcode"
                }
              })
            }


          })
          .catch(error => {
            console.log(error);
          })
      } else {
        Swal.fire({
          title: 'That didnt go well',
          text: `Please re-scan again`,
          icon: 'warning',
          confirmButtonText: 'Okay'
        }).then((result) => {
          if (result.isConfirmed) {
            window.location = "/qrcode"
          }
        })
      }



    }
    const startDate = new Date;
    if (result !== "No result" && !result.includes(',')) {
      axios.get(process.env.REACT_APP_BASE_URL + '/employee/getuserattendance', {
        params: {
          id: result,
          startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear()
        }
      })
        .then(res => {
          let restheTimeIn = res.data[0]?.timeIn?.length !== undefined ? res.data[0]?.timeIn?.length : 0;
          let restheTimeOut = res.data[0]?.timeOut?.length !== undefined ? res.data[0]?.timeOut?.length : 0;

          if (restheTimeIn === 0 || restheTimeIn === restheTimeOut) {
            settheTimeIn(false);
            settheTimeOut(true);
          } else {
            settheTimeIn(true);
            settheTimeOut(false);
          }

        })
        .catch(error => {
          console.log(error);
        })
    } else if (result.includes(',')) {
    }

  }

  const handleSelect = (e, state, thetype) => {
    setmealSelected(e.target.name);
    setmealId(e.target.id);
    setmealOption(state.type);
    setmealType(state.type);
    setestimation(state.estimation);
    setmealServed(state.mealServed);
  }

  const onSelectChange = (e) => {
    setmealType(e.target.value);
  }

  const handleAttendanceButton = () => {
    setattendance(true);
  }

  const handleMealButton = () => {
    setmeal(true);
  }

  const onValueChange = (e) => {
    setremarks(e.target.value);
  }

  useEffect(() => {
    let mIN;
    let mOUT;

    let aIN;
    let aOUT;





    axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
      .then(res => {
        morningToTime = res.data[0].morningTo;
        afternoonToTime = res.data[0].afternoonTo;
        afternoonFromTime = res.data[0].afternoonFrom;

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear()
      }
    })
      .then(res => {
        setattendanceToday(res.data);

        const ids = [id];
        let presentEmployees = res.data?.filter(({ user }, index) => user?._id === id);
        // setTotalWorkersPresent(presentEmployees.length);
        let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });


        addMorningIN = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        addMorningOUT = presentEmployees.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFromTime, 'h:mma')) || v.includes('lateOUT')) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0 });
        addAfternoonIN = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        addAfternoonOUT = presentEmployees.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma'))) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; });
        setmorningFromTime(addMorningIN);
        setmorningToTime(addMorningOUT);
        setafternoonFromTime(addAfternoonIN);
        setafternoonToTime(addAfternoonOUT);

        if (addMorningOUT?.length === 0 &&
          moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(afternoonFromTime, 'h:mma'))
          // (!moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(morningToTime, 'h:mma'))
          //   || !moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(afternoonFromTime, 'h:mma')))
        ) {
          setlateMorningOut(true);
        }
        // console.log('outIn', addMorningOUT.length === 0, afternoonFromTime, 'uuu', morningToTime.length, moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(morningToTime, 'h:mma'))
        //   , moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(afternoonFromTime, 'h:mma')))
        if (addMorningIN?.length === 0) {
          setmorningAbsent(true);
          setlateMorningOut(false);
        }

      })
      .catch(error => {
        console.log(error);
      })

  }, [id])

  const addTimeIn = (result, time, e, InorOUT) => {
    const attendance = {};
    let theRemarks = remarks;
    let remarkOut = 'none';



    if (moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningFrom : afternoonFrom, 'h:mma').isBefore(moment(formatAMPM(new Date), 'h:mma')) && !InorOUT && !lateMorningOut) {
      theRemarks = 'late';
    }
    else if (lateMorningOut &&
      (!moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(morningToTime, 'h:mma'))
        || !moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(afternoonFromTime, 'h:mma')))) {
      theRemarks = 'lateOUT';
    }


    let theTimeIn = '';
    let theTimeOut = '';
    attendance['user'] = id;
    attendance['dateRecorded'] = new Date().toLocaleDateString();
    if (e.target.id === 'timeIn') {
      theTimeIn = time;
      if (theRemarks) {
        attendance['timeIn'] = [time + '/' + theRemarks];
      } else {
        attendance['timeIn'] = [time];
      }
    } else {
      theTimeOut = time
      if (theRemarks) {
        attendance['timeOut'] = [time + '/' + theRemarks];
      } else {
        attendance['timeOut'] = [time];
      }
    }
    let existing = allAttendance?.filter(el => el.user === id && el.dateRecorded === new Date().toLocaleDateString());

    let outFor = moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningToTime : afternoonToTime;

    // if ((!moment(morningAfternoonTo, 'h:mma').isBefore(moment(formatAMPM(new Date), 'h:mma'))
    //   && (moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningFromTimeG : afternoonFromTime, 'h:mma'))
    //     && moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningToTime : afternoonToTime, 'h:mma')))
    //   && !theTimeOut && !lateMorningOut && morningAbsent) === false) {
    //   if (
    //     moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(outFor, 'h:mma')
    //     )
    //   ) {
    //     if (theRemarks.length === 0) {
    //       remarkOut = 'required';
    //     }
    //   }

    //   console.log('remarkoutttt', theRemarks.length, theRemarks, remarkOut, lateMorningOut, afternoonFromTime,
    //     moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(outFor, 'h:mma'))
    //   )
    // }

    var earlyOUTID = document.getElementById("early-out");

    if (earlyOUTID) {
      if (theRemarks.length === 0) {
        remarkOut = 'required';
      }
    }

    let InMorning = morningFromIO?.filter(el => el.user._id === id);
    let OutMorning = morningToIO?.filter(el => el.user._id === id);


    let InAfternoon = afternoonFromIO?.filter(el => el.user._id === id);
    let OutAfternoon = afternoonToIO?.filter(el => el.user._id === id);

    let morAf = moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? 'morning' : 'afternoon';
    let checkBaseonInOut = morAf === 'afternoon' ?
      InAfternoon.length !== 0 && OutAfternoon.length !== 0
      : InMorning.length !== 0 && OutMorning.length !== 0;

    // console.log('2222222222', id, morAf, InMorning.length !== 0 && OutMorning.length !== 0, checkBaseonInOut, InMorning, OutMorning)
    // attendance['breakfast'] = breakfastCheck;
    // attendance['lunch'] = lunchCheck;
    // attendance['dinner'] = dinnerCheck;

    console.log('atttt', attendance)
    if (checkBaseonInOut) {
      Swal.fire({
        title: 'Repeated Time-In',
        text: `You are not allowed to time in again for this ${morAf.toUpperCase()}`,
        icon: 'warning',
        confirmButtonText: 'Okay'
      }).then((result) => {
        if (result.isConfirmed) {
          handleClose();
        }
      })
    } else {
      if (remarkOut === 'required' && theRemarks.length === 0) {
        Swal.fire(
          'Early Out?',
          'Please Indicate a Reason.',
          'question'
        )
      } else {
        // console.log('SAVING TO DB!!!!!!!!!!!!!')
        if (existing.length !== 0) {
          if (existing[0][e.target.id]) {
            attendance[e.target.id] = [theRemarks ? time + '/' + theRemarks : time, ...existing[0][e.target.id]];
          }
          let setOther = e.target.id === 'timeIn' ? 'timeOut' : 'timeIn';
          if (existing[0][setOther]) {
            attendance[setOther] = [...existing[0][setOther]];
          }



          axios.post(process.env.REACT_APP_BASE_URL + '/employee/updateAttendance/' + existing[0]?._id, attendance)
            .then(res =>
              [
                Swal.fire({
                  title: 'Attendance Monitored!',
                  text: "Have a Good day!",
                  icon: 'success',
                  confirmButtonText: 'Okay'
                }).then((result) => {
                  if (result.isConfirmed) {
                    window.location = "/qrcode"
                  }
                })]
              // window.location = "/qrcode", 
              // handleClose]
            )
            .catch(err => console.log('Error :' + err));

        }
        else {
          axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/attendance', attendance)
            .then(res =>
              [
                Swal.fire({
                  title: 'Attendance Monitored!',
                  text: "Have a Good day!",
                  icon: 'success',
                  confirmButtonText: 'Okay'
                }).then((result) => {
                  if (result.isConfirmed) {
                    window.location = "/qrcode"
                  }
                })]
            )
            .catch(err => console.log('Error :' + err));
        }
      }
    }



  }

  useEffect(() => {
    if (id && mealId) {
      axios.get(process.env.REACT_APP_BASE_URL + '/employee/usermealRecords', {
        params: {
          userID: id,
          mealID: mealId,
        }
      })
        .then(res => [
          userMealRecord = res.data,
          setemployeeMealRecord(res.data),
          // settheRecord(res.data),
          // setmealSelected(meal),
          console.log('meal.mealCategory', res.data),]

        )
        .catch(err => console.log('Error :' + err));

    }
  }, [id, mealId])

  const saveMeal = () => {
    let existing = allMealRecords?.filter(el => el.user._id === id &&
      el.mealType === mealType &&
      moment(new Date()).format('MM-DD-YYYY') === moment(el.createdAt).format('MM-DD-YYYY'));
    let isPresentToday = attendanceToday?.filter(el => el.user._id === id);

    let InMorning = morningFromIO?.filter(el => el.user._id === id);
    let OutMorning = morningToIO?.filter(el => el.user._id === id);


    let InAfternoon = afternoonFromIO?.filter(el => el.user._id === id);
    let OutAfternoon = afternoonToIO?.filter(el => el.user._id === id);





    let checkBaseonMealType = mealType === 'dinner' ?
      (InAfternoon?.length !== 0) === false && (OutAfternoon?.length !== 0) === false ? true : InAfternoon?.length !== 0 && OutAfternoon?.length !== 0
      : (InMorning?.length !== 0) === false && (OutMorning?.length !== 0) === false ? true : InMorning?.length !== 0 && OutMorning?.length !== 0;
    let check = mealType === 'dinner' ? dinnerCheck : mealType === 'lunch' ? lunchCheck : breakfastCheck;
    console.log('yyyyy22', employeeMealRecord, userMealRecord, userMealRecord?.[0]?.availing, userMealRecord?.[0]?.availing !== 'true')

    if (userMealRecord?.[0]?.availed === 'true') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Taking the Meal Twice',
        confirmButtonText: 'Okay',
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/qrcode"
        }
      })
      // }

      setmealEmployeeQr(false);
      setresult('No result');
    }
    else if (checkBaseonMealType) {
      Swal.fire({
        icon: 'error',
        title: 'You dont seem to be present today',
        text: 'Please make you sure to have Attendance',
        confirmButtonText: 'Okay',
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/qrcode"
        }
      })
    }
    else if (userMealRecord?.[0]?.availing !== 'true') {
      Swal.fire({
        icon: 'error',
        title: `You seem to be not included for this ${mealType.toUpperCase()} `,
        text: 'Please make sure to check the AVAIL option in your Employee Account',
        confirmButtonText: 'Okay',
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/qrcode"
        }
      })
    } else {
      console.log('saving to DB',)
      axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/updatemealRecord', { mealRecordId: userMealRecord?.[0]?._id })
        .then(res =>
          axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/mealRecord2', { user: id, meal: mealId, mealRecordId: userMealRecord?.[0]?._id, mealType: mealType })
            .then(res =>
              // window.location = "/qrcode"
              Swal.fire({
                icon: 'info',
                title: 'Meal Saved',
                // text: 'Please make you sure to have Attendance',
                confirmButtonText: 'Okay',
              }).then((result) => {
                if (result.isConfirmed) {
                  window.location = "/qrcode"
                }
              })
            )
            .catch(err => console.log('Error :' + err)))
        .catch(err => console.log('Error :' + err));
    }
  }

  return (
    <QRCodeStyled>
      <div className="whole" style={{ backgroundImage: `url(${GridBackground})`, backgroundSize: 'auto', backgroundRepeat: 'no-repeat', backgroundPosition: 'bottom' }}>
        <div className="right">
          <h2 className="time"> {time.toString()}</h2>
          <h1>Scan your QR CODE</h1>
          <p>Make your QR code readable and steady</p>
        </div>
        <div className="left">
          <div className="qrCode-card">
            <h2 className="time"> {''}</h2>
            <QrReader className="qrCODE"
              delay={300}
              onError={handleError}
              onScan={handleScan}

            />
            <span className="result">{firstname ? firstname + " " + lastname : 'No Result'}</span>
            {/* <p>{result}</p> */}

            {/* ATTENDANCE */}
            {
              result !== 'No result' && attendance ?
                <Modal show={result !== 'No result' && attendance} onHide={handleClose} animation={false}>
                  <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                    <Modal.Title >Attendance Monitoring</Modal.Title>

                  </Modal.Header>
                  <Modal.Body>
                    <>
                      <h1 style={{ textAlign: 'center' }}>{moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? 'Good Morning' : 'Good Afternoon'}</h1>
                      <p style={{ whiteSpace: 'pre-wrap', textAlign: 'center' }}>{moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ?
                        `Morning Start From: ${morningFrom} \nMorning Start To: ${morningTo}`
                        :
                        `Afternoon Start From: ${afternoonFrom} \nAfternoon Start To: ${afternoonTo}`
                      }
                      </p>
                      {
                        moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningFrom : afternoonFrom, 'h:mma').isBefore(moment(formatAMPM(new Date), 'h:mma')) && !theTimeIn ?
                          <div className="form-group">
                            <p style={{ whiteSpace: 'pre-wrap', textAlign: 'center' }}>You are Late</p>
                            {/* <label>Remark-Late</label>
                          <input type="text" defaultValue='late' className="form-control" data-name="remark" required onChange={onValueChange} /> */}
                          </div>
                          : !moment(morningAfternoonTo, 'h:mma').isBefore(moment(formatAMPM(new Date), 'h:mma'))
                            && (moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningFromTimeG : afternoonFromTime, 'h:mma'))
                              && moment(moment(formatAMPM(new Date), 'h:mma')).isBefore(moment(moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? morningToTime : afternoonToTime, 'h:mma')))
                            && !theTimeOut && !lateMorningOut && morningAbsent ?

                            <div className="form-group">
                              <label id="early-out">Remark-Early Out</label>
                              <input type="text" className="form-control" data-name="remark" required onChange={onValueChange} />
                            </div>
                            : lateMorningOut &&
                              moment(moment(formatAMPM(new Date), 'h:mma')).isAfter(moment(afternoonFromTime, 'h:mma'))
                              ?
                              <>
                                <p style={{ whiteSpace: 'pre-wrap', textAlign: 'center' }}>Late Morning Out</p>
                              </>
                              :
                              null
                      }
                      {/* {
                        !attendanceExist ?
                          <>
                            <Divider style={{ marginBottom: '3%' }}>
                              <Chip label="Let us know if you are availing the following:" />
                            </Divider>
                            <div className="form-group" style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '1.2rem' }}>

                              <>
                                <label className="checkbox-inline" style={{ margin: '5%' }}>
                                  <input className="form-control" type="checkbox" checked={breakfastCheck} onChange={(e) => setbreakfastCheck(e.target.checked)} style={{ height: '30px' }} />Breakfast
                                </label>

                                <label className="checkbox-inline" style={{ margin: '5%' }}>
                                  <input className="form-control" type="checkbox" checked={lunchCheck} onChange={(e) => setlunchCheck(e.target.checked)} style={{ height: '30px' }} />Lunch
                                </label></>
                              <label className="checkbox-inline" style={{ margin: '5%' }}>
                                <input className="form-control" type="checkbox" checked={dinnerCheck} onChange={(e) => setdinnerCheck(e.target.checked)} style={{ height: '30px' }} />Dinner
                              </label>
                            </div>
                          </>
                          : null

                      } */}


                      <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                        <Button id='timeIn' disabled={theTimeIn} onClick={(e) => { addTimeIn(result, new Date().toLocaleTimeString(), e, theTimeIn) }} variant={!theTimeIn ? "outline-primary" : "outline-secondary"}>Time IN</Button>{' '}
                        <Button disabled={theTimeOut} id='timeOut' onClick={(e) => { addTimeIn(result, new Date().toLocaleTimeString(), e, theTimeIn) }} variant={!theTimeOut ? "outline-danger" : "outline-secondary"}>Time OUT</Button>{' '}
                      </div>
                    </>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
                :
                <p></p>
            }

            {/* MEALS */}
            {
              (result !== 'No result' && meal) || mealEmployeeQr ?
                <Modal show={result !== 'No result' && meal || mealEmployeeQr} onHide={handleClose} animation={false} >
                  <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                    <Modal.Title>Meal Selection</Modal.Title>
                  </Modal.Header>

                  {mealToday || mealEmployeeQr ?
                    <Modal.Body>
                      <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                        <p>Meal Today</p>
                        <DropdownButton id="dropdown-item-button" title={mealSelected}>
                          {mealToday.map((state, key) => {
                            return <Dropdown.Item style={{ zIndex: 999 }} onClick={(e) => handleSelect(e, state, mealType)} id={state.id} name={state.name} as="button">{state.name}</Dropdown.Item>;
                          })}
                        </DropdownButton><br />
                        {
                          mealSelected !== 'Menu' ?
                            <div>
                              <p><strong>Estimation: </strong>{estimation}</p>
                              <p><strong>Served: </strong>{mealServed}</p>
                              <p style={estimation - mealServed <= 0 ? { color: 'red' } : {}}><strong>Available: </strong>{estimation - mealServed}</p>
                            </div>

                            :
                            <p style={{ color: 'red' }}>Please Select from the Dropdown Menu</p>
                        }

                        {
                          mealSelected !== 'Menu' ?
                            <>
                              <select value={mealType} className="form-control" data-name="mealType" required onChange={onSelectChange}>
                                <option value={mealOption}>{mealOption}</option>
                              </select> <br />
                            </>

                            : null
                        }
                        <Button disabled={mealId === ''} id='timeIn' onClick={saveMeal} variant={mealId !== '' ? "outline-primary" : "outline-secondary"}>Save</Button>{' '}
                      </div>

                    </Modal.Body> :
                    <Modal.Body>
                      <p >No Available Meal Today.</p>
                    </Modal.Body>
                  }



                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
                :
                <p></p>
            }
            {/*  */}
            <Modal show={result !== 'No result' && !attendance && !meal && !mealEmployeeQr} onHide={handleClose} animation={''}>
              <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                <Modal.Title>QR Code Selection</Modal.Title>
              </Modal.Header>
              <Modal.Body style={{ alignItems: 'center', textAlign: 'center' }}>
                <Button id='timeIn' onClick={handleAttendanceButton} variant="outline-primary" style={{ borderRadius: '10px' }}>Attendance</Button>{' '}
                <Button id='timeOut' onClick={handleMealButton} variant="outline-danger" style={{ borderRadius: '10px' }}> Meal</Button>{' '}
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose} style={{ background: '#203354', border: 'none', color: 'white', padding: '0.5rem 2rem' }}>
                  Close
                </Button>
              </Modal.Footer>
            </Modal>

          </div>
        </div>
      </div>
    </QRCodeStyled>

  )
}



export default AttendancePage;

