import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../action/authAction";
import { LogInStyle } from "../styles/LogInStyle.js"
import classnames from "classnames";
import styled from 'styled-components';
import TextField from '@mui/material/TextField';
import { Button } from "@mui/material";
import logo from '../img/Logo1.png';
import logo1 from '../img/LogoText.png';
import wave from '../img/wave.png';
import BG from '../img/bgLogIn.png';
import Swal from 'sweetalert2';

let theLocalStorageUser;
class Login extends Component {
    constructor() {
        super()
        this.state = {
            email: "",
            password: "",
            errors: {}
        }
    }
    componentDidMount() {
        theLocalStorageUser = JSON.parse(localStorage.getItem('user'));
        if (this.props.auth.isAuthenticated) {
            localStorage.removeItem("invalid");
            if (theLocalStorageUser?.role === 'Admin') {
                window.location.href = "/dashboard";
            } else {
                window.location.href = "/meals";
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        theLocalStorageUser = JSON.parse(localStorage.getItem('user'));
        if (nextProps.auth.isAuthenticated) {
            localStorage.removeItem("invalid");
            if (theLocalStorageUser?.role === 'Admin') {
                Swal.fire({
                    title: 'How would you like to login?',
                    showDenyButton: true,
                    confirmButtonText: 'Admin',
                    denyButtonText: `My Account`,
                  }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location.href = "/dashboard";
                    } else if (result.isDenied) {
                      window.location.href = "/meals";
                      localStorage.setItem('manage', 'manage')
                    }
                  })
            } else if (theLocalStorageUser?.role === 'Canteen') {
                // window.location.href = "/qrcode";
                Swal.fire({
                    title: 'How would you like to login?',
                    showDenyButton: true,
                    confirmButtonText: 'Canteen',
                    denyButtonText: `My Account`,
                  }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location.href = "/dashboard";
                    } else if (result.isDenied) {
                      window.location.href = "/meals";
                      localStorage.setItem('manage', 'manage')
                    }
                  })
            }else if (theLocalStorageUser?.role === 'Frontdesk') {
                // window.location.href = "/qrcode";
                Swal.fire({
                    title: 'How would you like to login?',
                    showDenyButton: true,
                    confirmButtonText: 'Frontdesk',
                    denyButtonText: `My Account`,
                  }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                      window.location.href = "/qrcode";
                    } else if (result.isDenied) {
                      window.location.href = "/meals";
                      localStorage.setItem('manage', 'manage')
                    }
                  })
            } else {
                window.location.href = "/meals";
            }
        }

        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value })
    }

    onSubmit = e => {
        e.preventDefault();

        const userData = {
            email: this.state?.email,
            password: this.state.password,
        }
        this.props.loginUser(userData);
    }
    render() {
        const { email, password, errors } = this.state;
        return (
            <LogInStyle>

                <div className="whole">

                    <img src={BG} id="wave"></img>


                    <div className="container">

                        <div className="img">
                            <img src={logo1}></img>
                        </div>

                        <div className="logIn-card">
                            {/* <h2>Monitor Meal and Attendance</h2> */}
                            <form className="login-form" onSubmit={this.onSubmit}>
                                <div className="logIn">
                                    <div className="logoText">
                                        <img src={logo} id="logo"></img>
                                        <h2>SIGN IN</h2>
                                        <p>Monitor your Employees Attendance and Meal</p>
                                        <hr></hr>
                                    </div>
                                    <div className="email">
                                        <div className="form-group">
                                            <i className="fa fa-user fa-2x" style={{ marginRight: '0.5rem' }}></i>
                                            <input type="email"
                                                id="email"
                                                placeholder="Email Address"
                                                value={email}
                                                error={errors}
                                                onChange={this.onChange}
                                                className={classnames("form-control", {
                                                    invalid: errors.email || errors.emailnotfound
                                                })} />

                                        </div>
                                    </div>
                                    <span className="red-text">
                                        {errors.email}
                                        {errors.emailnotfound}
                                    </span>
                                    <div className="password">
                                        <div className="form-group">
                                            <i className="fa fa-unlock-alt fa-2x" style={{ marginRight: '0.5rem' }}></i>
                                            <input type="password"
                                                id="password"
                                                placeholder="Password"
                                                value={password}
                                                error={errors}
                                                onChange={this.onChange}
                                                className={classnames("form-control", {
                                                    invalid: errors.password || errors.passwordincorrect
                                                })}
                                            />
                                        </div>
                                    </div>
                                    <span className="red-text">
                                        {errors.password}
                                        {errors.passwordincorrect}
                                    </span>
                                    <div className="button">  <button type="submit" className="btn btn-primary btn-block btn-lg" style={{ backgroundColor: '#0039AE', border: 'none', borderRadius: '2rem', fontWeight: 'bold', width: '70%', }}>LOGIN</button></div>

                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </LogInStyle>
        )
    }
}


Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { loginUser }
)(Login);