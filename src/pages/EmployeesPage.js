import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { FiSave } from "react-icons/fi";
import { FiPrinter } from "react-icons/fi";
import { CgImport } from "react-icons/cg";
// import QRCode from 'react-qr-code';
import QRCode2 from "qrcode.react";
// ES6 Modules or TypeScript
import styled from 'styled-components';
import { Line, MainLayout, TableLayout } from '../styles/TableLayout';
import { BiEdit } from "react-icons/bi";
import { RiDeleteBin5Line } from "react-icons/ri";
import { RiQrCodeLine } from "react-icons/ri";
import QrCodeIcon from '@mui/icons-material/QrCode';
import Swal from 'sweetalert2';
import {
    Button,
    Modal,
} from "react-bootstrap";
import ReactTable from "react-table-6";
import { EmployeeTable } from '../styles/LayoutAttendance';
import { exportExcel } from './AttendancePage';
import * as XLSX from "xlsx";
import { printdiv } from "../globalFunctions/gFunction";
export default class EmpList extends Component {

    constructor(props) {
        super(props)

        this.deleteEmployee = this.deleteEmployee.bind(this);
        this.generateQr = this.generateQr.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.pagination = this.pagination.bind(this);
        this.download = this.download.bind(this);
        this.state = { employee: [], toShow: false, toGenerate: '', generateFor: [], page: 1, data: [], columns: [], theRow: [], toShowImport: false, dataImported: [] }
        this.handleImport = this.handleImport.bind(this);
        this.readExcel = this.readExcel.bind(this);
        this.bulkInsert = this.bulkInsert.bind(this);
        this.filterCaseInsensitive = this.filterCaseInsensitive.bind(this);
    }

    componentDidMount() {

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/', { params: { page: this.state.page } })
            .then(res => {
                this.setState({ employee: res.data })
                this.setState({ data: res.data })
                this.setState({
                    columns: [
                        {
                            id: 'firstname',
                            Header: 'Firstname',
                            accessor: 'firstname',
                            headerClassName: 'headerTable'
                        },
                        {
                            id: 'lastname',
                            Header: 'Lastname',
                            accessor: 'lastname',
                            headerClassName: 'headerTable',
                        },
                        {
                            id: 'address',
                            Header: 'Address',
                            accessor: 'address',
                            headerClassName: 'headerTable'
                        },
                        {
                            id: 'department',
                            Header: 'Department',
                            accessor: 'department',
                            headerClassName: 'headerTable'
                        },
                        {
                            id: 'email',
                            Header: 'Email',
                            accessor: 'email',
                            headerClassName: 'headerTable'
                        },
                        {
                            id: 'cpnumber',
                            Header: 'Contact',
                            accessor: 'cpnumber',
                            headerClassName: 'headerTable'
                        },
                        {
                            id: 'role',
                            Header: 'Role',
                            accessor: 'role',
                            headerClassName: 'headerTable'
                        },
                        {
                            Header: 'Actions',
                            filterable: false,
                            headerClassName: 'headerTable',
                            Cell: row => (
                                <div>
                                    <Link to={'/edit/' + row.original._id} style={{ background: '#00C2DE', marginLeft: '15%', padding: '3%', paddingTop: '1%' }} className="btn"><BiEdit style={{ fontSize: 20, color: 'white' }} /></Link>
                                    <a href="#" onClick={() => {
                                        // props.deleteEmployee(props.employee._id)
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "You won't be able to revert this!",
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Yes, delete it!'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                // Swal.fire(
                                                //   'Deleted!',
                                                //   'Your file has been deleted.',
                                                //   'success'
                                                // )
                                                this.deleteEmployee(row.original._id);
                                            }
                                        })
                                    }} style={{ background: 'red', marginLeft: '2%', padding: '3%', paddingTop: '1%' }} className="btn"><RiDeleteBin5Line style={{ fontSize: 20, color: 'white' }} /></a>
                                    <a href="#" onClick={() => { this.generateQr(row.original._id) }
                                    } style={{ background: 'gray', marginLeft: '1.9%', padding: '3%', paddingTop: '1%' }} className="btn"><RiQrCodeLine style={{ fontSize: 20, color: 'white' }} /></a>
                                </div>
                            )
                        }
                    ]
                })
            })
            .catch(error => {
                console.log(error);
            })


    }

    componentDidUpdate(prevProps, prevState) {
        // Typical usage (don't forget to compare props):
        if (prevState.page !== this.state.page) {
            axios.get(process.env.REACT_APP_BASE_URL + '/employee/', { params: { page: this.state.page } })
                .then(res => {
                    this.setState({ employee: res.data })
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    deleteEmployee(id) {
        axios.delete(process.env.REACT_APP_BASE_URL + '/employee/' + id)
            .then(res => window.location.href = "/employees")
        this.setState({
            employee: this.state.employee.filter(el => el._id !== id)
        })
    }

    handleImport() {
        this.setState({ toShowImport: true })
    }

    filterCaseInsensitive = (filter, row) => {
        const id = filter.pivotId || filter.id;
        const content = row[id];
        if (typeof content !== 'undefined') {
            // filter by text in the table or if it's a object, filter by key
            if (typeof content === 'object' && content !== null && content.key) {
                return String(content.key).toLowerCase().includes(filter.value.toLowerCase());
            } else {
                return String(content).toLowerCase().includes(filter.value.toLowerCase());
            }
        }

        return true;
    };

    readExcel(file) {
        const promise = new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsArrayBuffer(file);

            fileReader.onload = (e) => {
                const bufferArray = e.target.result;

                const wb = XLSX.read(bufferArray, { type: "buffer" });

                const wsname = wb.SheetNames[0];

                const ws = wb.Sheets[wsname];

                const data = XLSX.utils.sheet_to_json(ws);

                resolve(data);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });

        promise.then((d) => {
            this.setState({
                dataImported: d.map(element => ({ password: element.Email, ...element })).map(name => Object.keys(name).reduce((acc, key) => {
                    acc[key.toLowerCase() === 'contact' ? 'cpnumber' : key.toLowerCase()] = name[key];
                    return acc;
                }, {}))
            });
        });
    };

    bulkInsert() {
        let theMessage = 0;
        axios.post(process.env.REACT_APP_BASE_URL + '/employee/bulkInsert', this.state.dataImported)
            .then(res => {
                if (res.data.message === 'success') {
                    theMessage = res.data.data?.length;
                } else if (res.data.message === 'duplicate') {
                    theMessage = res.data.data.insertedDocs?.length;
                }

                Swal.fire({
                    icon: 'info',
                    title: `Added ${theMessage} record`,
                    text: 'Duplicate records are skip',
                    confirmButtonText: 'Okay',
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/employees"
                    }
                }
                )
            }
            )
            .catch(err => console.log('Error :' + err));
    }

    generateQr(id) {
        let generatingFor = this.state.employee.filter(user => user._id === id);
        this.setState({ toShow: true, toGenerate: id, generateFor: generatingFor })
        let str = id
    }
    handleClose() {
        this.setState({ toShow: false, toShowImport: false, dataImported: [] });
    }
    download = function () {
        const link = document.createElement("a");
        link.download = "filename.png";
        link.href = document.getElementById("canvas").toDataURL();
        link.click();
    };

    pagination(e, page) {
        if (e.target.id === 'previous' && page !== 1) {
            this.setState({ page: page - 1 });
        } else if (e.target.id === 'next') {
            this.setState({ page: this.state.page + 1 });
        }

    }


    render() {
        return (
            <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: 'auto' }}>
                <MainLayout>
                    <div style={{ padding: '0%' }} className="container">
                        <div id="print_employees" style={{ paddingBottom: '0.5rem' }}>

                            <TableLayout>
                                <div className="upperPart" >
                                    <div className="textTitle employee"><h1>Employee List</h1> </div>
                                    {/*Buttons*/}
                                    <div style={{ display: 'flex' }} className="meal-up-con">
                                        <div className="createEmployees" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                                            <Link to={'/create'} style={{ display: 'flex', color: 'white' }} className="btn btn-create" ><i className="fa fa-plus" style={{ fontSize: '25px', marginRight: '5%' }}></i> CREATE</Link>
                                        </div>
                                        <div className="exportEmployees" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                                            <button style={{ display: 'flex', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(this.state.data, this.state.columns.filter(function (e) {
                                                return e.Header !== 'Actions';
                                            }), `Employees-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                                        </div>
                                        <div className="importEmployees" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                                            <button style={{ display: 'flex', background: '#1C6A41', color: 'white' }} className="btn" onClick={() => this.handleImport()}><CgImport style={{ fontSize: '25px', paddingRight: '2%' }} />IMPORT</button>
                                        </div>
                                        <div className="printMeal" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                                            <button style={{ display: 'flex', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_employees', '/employees')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                                        </div>
                                    </div>


                                </div>
                                <Modal show={this.state.toShowImport} onHide={this.handleClose} animation={false} size='lg'>
                                    <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                                        <Modal.Title>Import Excel</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                                            <input
                                                type="file"
                                                onChange={(e) => {
                                                    const file = e.target.files[0];
                                                    this.readExcel(file);
                                                }}
                                            /> <br /> </div>
                                        <ReactTable
                                            data={this.state.dataImported}
                                            filterable
                                            defaultFilterMethod={this.state.filterCaseInsensitive}
                                            minRows={1}
                                            columns={this.state.columns.filter(function (e) {
                                                return e.Header !== 'Actions';
                                            })}
                                            defaultPageSize={5}
                                            className="-striped -highlight"
                                            getTheadThProps={(columnIndex) => ({
                                                style: { textAlign: "left", color: "black" }
                                            })}
                                            getTdProps={() => ({
                                                style: { fontSize: "0.8rem", border: "none" }
                                            })}
                                            style={{ textAlign: 'left', border: "None" }}
                                        />
                                        {
                                            this.state.dataImported.length !== 0 ?
                                                <>
                                                    <br />
                                                    <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                                                        <Button type="button" className="mt-30" onClick={() => this.bulkInsert()}>
                                                            Import
                                                        </Button>
                                                    </div>
                                                </>

                                                : null
                                        }

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={this.handleClose}>
                                            Close
                                        </Button>
                                    </Modal.Footer>
                                </Modal>


                                <Line></Line>
                                <div align="center">
                                    <Modal show={this.state.toShow} onHide={this.handleClose} animation={false}>
                                        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                                            <Modal.Title>QR Code Generator</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            {/* <canvas id="canvas" width={600} height={500} style={{maxWidth: '100%', zIndex: 999}} /> */}
                                            <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                                                <QRCode2 style={{ width: '40%', height: '40%', }} value={this.state.toGenerate} id="canvas" />
                                                <p style={{ textTransform: 'uppercase' }}>{this.state.generateFor[0]?.firstname + " " + this.state.generateFor[0]?.lastname}</p>
                                                <Button type="button" className="mt-30" onClick={this.download}>
                                                    Download QR
                                                </Button>
                                            </div>

                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.handleClose}>
                                                Close
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                    {/* <button onClick={this.generateQr}>
                        Generate QR!
                    </button> */}
                                    <ReactTable
                                        data={this.state.data}
                                        filterable
                                        defaultFilterMethod={this.filterCaseInsensitive}
                                        minRows={1}
                                        columns={this.state.columns}
                                        defaultPageSize={10}
                                        className="-striped -highlight"
                                        getTheadThProps={(columnIndex) => ({
                                            style: { textAlign: "left", color: "black" }
                                        })}
                                        getTdProps={() => ({
                                            style: { fontSize: "0.8rem", border: "none" }
                                        })}
                                        style={{ textAlign: 'left', border: "None" }}
                                    />
                                </div>

                            </TableLayout>
                        </div>
                    </div>


                </MainLayout>
            </div>
        )
    }
}

