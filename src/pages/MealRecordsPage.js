import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import { Line, MainLayout, TableLayout, AttendanceStyled } from '../styles/TableLayout';
import { Link } from 'react-router-dom';
import { FiSave } from "react-icons/fi";
import { FiPrinter } from "react-icons/fi";
import { QRcode, CustomDatePickDiv } from '../styles/LayoutAttendance';
import nothing from '../img/nothing.png';
import breakfastIcon from '../img/Breakfast.png';
import lunchIcon from '../img/Lunch.png';
import dinnerIcon from '../img/Dinner.png';
import snackIcon from '../img/Snack.png';
import TotalAttendanceIcon from '../img/icon_totalEmployees.png';
import peopleServed from '../img/icon_peopleServed.png'
import icon_peopleToBeServedv2 from '../img/icon_peopleToBeServedv2.png';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Fab from "@mui/material/Fab";
import records from '../img/records.svg';
import axios from 'axios';
// Import React Table
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import moment from 'moment';
import { exportExcel } from './AttendancePage';
import { filterCaseInsensitive } from './Recommendation';
import { printdiv } from "../globalFunctions/gFunction";

const Input = styled('input')({
  display: 'none',
  width: '5vw'
});


function MealRecordsPage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [columns, setColumns] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [toLoad, setToLoad] = React.useState(0);
  const [noOfPeopleServed, setNoOfPeopleServed] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [theRow, settheRow] = React.useState([]);
  const [breakfast, setbreakfast] = React.useState(0);
  const [lunch, setlunch] = React.useState(0);
  const [dinner, setdinner] = React.useState(0);
  const [snack, setsnack] = React.useState(0);
  const [totalBreakfasts, settotalBreakfasts] = React.useState(0);

  const selectDateHandler = (d) => {
    setDate(d)
  }

  const setDateRange = (d) => {
    setEndDate(d);
  }

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        setToLoad(res.data.length);
        const ids = res.data.map(o => o.user?._id);
        let noOfServed = res.data.filter(({ user }, index) => !ids.includes(user?._id, index + 1));
        setNoOfPeopleServed(noOfServed.length);
        setbreakfast(res.data.filter(x => x.mealType === 'breakfast').length);

        setlunch(res.data.filter(x => x.mealType === 'lunch').length);
        setdinner(res.data.filter(x => x.mealType === 'dinner').length);
        setsnack(res.data.filter(x => x.mealType === 'snack').length);

        if (res.data.length > 0) {
          setColumns([
            {
              id: 'user',
              Header: 'Name',
              headerClassName: 'headerTable',
              accessor: d => d.user?.firstname + " " + d.user?.lastname
            },
            {
              id: 'mealName',
              Header: 'Meal Name',
              headerClassName: 'headerTable',
              accessor: d => d.meal?.mealName
            }, {
              id: 'mealType',
              Header: 'Meal Type',
              headerClassName: 'headerTable',
              accessor: d => d.meal?.mealType
            }, {
              id: 'availed',
              Header: 'Consumed',
              headerClassName: 'headerTable',
              accessor: d => d.availed
            }, {
              id: 'rating',
              Header: 'Meal Rating',
              headerClassName: 'headerTable',
              accessor: d => d?.rating
            }, {
              id: 'feedback',
              Header: 'Meal Feedback',
              headerClassName: 'headerTable',
              accessor: d => d?.feedback
            }, {
              id: 'ingToRemove',
              Header: 'Ing to Remove',
              headerClassName: 'headerTable',
              accessor: d => d.ingToRemove
            }, {
              id: 'ingToAdd',
              Header: 'Ing to Add',
              headerClassName: 'headerTable',
              accessor: d => d.ingToAdd
            }]);
          setData(...[res.data]);

          const mappingHistory = res.data.map(year => [{
            user: year.user.firstname + ' ' + year.user.lastname,
            department: year.user.department,
            mealName: year.meal.mealName,
            mealDesc: year.meal.mealDesc,
            mealType: year.meal.mealType,
            rating: year.rating,

          }]);

          settheRow([].concat.apply([], mappingHistory));
        }

      })
      .catch(error => {
        console.log(error);
      })

    let morningToTime = '';
    let afternoonToTime = '';
    let afternoonFromTime = '';
    //Get Time Schedule set
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
      .then(res => {
        morningToTime = res.data[0].morningTo;
        afternoonToTime = res.data[0].afternoonTo;
        afternoonFromTime = res.data[0].afternoonFrom;
      })
      .catch(error => {
        console.log(error);
      })
  }, [startDate, endDate]);

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const ids = res.data.map(o => o.meal?._id)
      })
      .catch(error => {
        console.log(error);
      })
  });

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon className="date-icon" icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });


  return (
    <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: 'auto' }}>
      <MainLayout>
        {/* 
        <QRcode>
          <Fab arial-label='qrcode' style={{ transform: 'scale(1.6)' }} >
            <Input accept="image/*" id="icon-button-file" type="file" />
            <IconButton color="primary" aria-label="upload picture" component="span" >
              <QrCodeScannerIcon fontSize="large" />
            </IconButton>
          </Fab>
        </QRcode> */}


        <AttendanceStyled>

          <div className="container">
            <div className="upperLayout">
              <div className="card totalEmployees">
                <div className="text">
                  <h2>Total Employees</h2>
                  <h3>{totalEmployees}</h3>
                </div>
                <div className="icon">
                  <img src={TotalAttendanceIcon} alt="totalAttendance" />
                </div>
              </div>

              <div className="card present">
                <div className="text">
                  <h2 style={{ color: '#F88101' }}>People Served</h2>
                  <h3 style={{ color: '#F88101' }}>{noOfPeopleServed}</h3>
                </div>
                <div className="icon">
                  <img src={peopleServed} id="peopleServed" />
                </div>
              </div>

              <div className="card absent">
                <div className="text">
                  <h2 style={{ color: '#5e5e5e' }}>People to be Serve</h2>
                  <h3 style={{ color: '#5e5e5e' }}>{totalEmployees - noOfPeopleServed}</h3>
                </div>
                <div className="icon">
                  <img src={icon_peopleToBeServedv2} id="peopleToBeServed" />
                </div>
              </div>
            </div>
            {/* In and Out */}

            <div className="upperLayout">
              <div className="card totalEmployees">
                <div className="text">
                  <h2>Breakfast</h2>
                  <h3>{breakfast}</h3>
                </div>
                <div className="icon">
                  <img src={breakfastIcon} alt="totalAttendance" />
                </div>
              </div>

              <div className="card present">
                <div className="text">
                  <h2 style={{ color: '#F88101' }}>Lunch</h2>
                  <h3 style={{ color: '#F88101' }}>{lunch}</h3>
                </div>
                <div className="icon">
                  <img src={lunchIcon} id="peopleServed" />
                </div>
              </div>

              <div className="card absent">
                <div className="text">
                  <h2 style={{ color: '#5e5e5e' }}>Dinner</h2>
                  <h3 style={{ color: '#5e5e5e' }}>{dinner}</h3>
                </div>
                <div className="icon">
                  <img src={dinnerIcon} id="peopleToBeServed" />
                </div>
              </div>

              <div className="card absent">
                <div className="text">
                  <h2 style={{ color: '#2b8400' }}>Snack</h2>
                  <h3 style={{ color: '#2b8400' }}>{snack}</h3>
                </div>
                <div className="icon">
                  <img src={snackIcon} id="peopleToBeServed" />
                </div>
              </div>
            </div>

            <div className="lowerLayout">
              <div id="print_mealrecords">

                <TableLayout>
                  <div className="upperPart">
                    {/* <div className="textTitle attendance"> <h1 style={{ marginTop: '2%' }}>Meal Records</h1></div>
                    <div style={{ display: 'flex', width: '150%' }} className="up-con">

                      <div id="btn" className="datePicker" style={{ left: '0', flexDirection: 'row' }}>
                        <label className="start-date" style={{ padding: '0' }}>Start Date:
                          <div>
                            <DatePicker
                              className="form-control"
                              dateFormat="yyyy/MM/dd"
                              selected={startDate}
                              onChange={selectDateHandler}
                              // minDate={today}
                              todayButton={"Today"}
                              customInput={<CustomInput />}
                            />
                          </div>
                        </label>

                        <label className="end-date" style={{ padding: '0', paddingTop: '0' }}>End Date:
                          <div>
                            <DatePicker
                              className="form-control"
                              dateFormat="yyyy/MM/dd"
                              selected={endDate}
                              onChange={setDateRange}
                              minDate={startDate}
                              todayButton={"Today"}
                              customInput={<CustomInput />}
                            />
                          </div>
                        </label>

                      </div>

                      <div className="up-con" style={{ width: '19.5vw', padding: '1%', display: 'flex', flexDirection: 'row', justifyContent: 'right' }}>
                        <div className="exportMealRecords" style={{ width: 'auto', padding: '1%', display: 'flex' }}>
                          <button style={{ display: 'flex', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns, `MealRecords-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                        </div>
                        <div className="printMeal" style={{ width: 'auto', padding: '1%', display: 'flex' }}>
                          <button style={{ display: 'flex', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_mealrecords', '/mealrecords')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                        </div>
                      </div>
                    </div> */}
                  </div>

                  <div id="btn" class="flex-container2" >
                    <div style={{ marginRight: '8%' }}> <div className="textTitle attendance"> <h1 style={{ marginTop: '2%', width: '16vw' }}>Meal Records</h1></div></div>

                    <div style={{ display: 'inherit', marginLeft: '1%' }}>
                      <div><label className="start-date wordings" style={{ padding: '0', width: '6vw' }}>Start Date:  </label> </div>
                      <div className="start-date" style={{ width: '11vw' }}> <DatePicker
                        className=""
                        dateFormat="yyyy/MM/dd"
                        selected={startDate}
                        onChange={selectDateHandler}
                        maxDate={endDate}
                        todayButton={"Today"}
                        customInput={<CustomInput />}
                      /></div>
                      <div><label className="end-date wordings" style={{ padding: '0', paddingTop: '0', width: '6vw' }}>End Date:  </label> </div>
                      <div className="start-date" style={{ width: '10vw', padding: '0' }}>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy/MM/dd"
                          selected={endDate}
                          onChange={setDateRange}
                          minDate={startDate}
                          todayButton={"Today"}
                          customInput={<CustomInput />}
                        />
                      </div>
                    </div>

                    <div style={{ width: '20vw', marginLeft: '1%' }}>
                      <div className="exportAttendance" style={{ width: 'auto', padding: '1%', display: 'flex' }}>
                        <button style={{ display: 'flex', margin: '1%', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns, `MealRecords-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                        <button style={{ display: 'flex', margin: '1%', marginLeft: '5%', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_mealrecords', '/mealrecords')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                      </div>
                    </div>
                  </div>

                  <Line></Line>
                  <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>

                    {
                      toLoad !== 0 ?
                        <ReactTable
                          data={data}
                          filterable
                          defaultFilterMethod={filterCaseInsensitive}
                          minRows={1}
                          columns={columns}
                          defaultPageSize={10}
                          className="-striped -highlight"
                          getTheadThProps={(columnIndex) => ({
                            style: { textAlign: "left", color: "black" }
                          })}
                          getTdProps={() => ({
                            style: { fontSize: "0.8rem", border: "none" }
                          })}
                          style={{ textAlign: 'left', border: "None" }}
                        />
                        :
                        <div style={{ margin: 'auto', marginTop: '5%', textAlign: 'center' }}><img style={{ height: '30%', width: '30%', opacity: '80%' }} src={nothing} alt="React Logo" /></div>
                    }

                  </div>
                </TableLayout>
              </div>
            </div>

          </div>
        </AttendanceStyled>
        <zoom in={true} timeout={{ enter: 500, exit: 500 }}>
        </zoom>
      </MainLayout>
    </div>
  )
}



export default MealRecordsPage;
