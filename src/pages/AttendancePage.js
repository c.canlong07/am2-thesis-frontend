import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import { Line, MainLayout, TableLayout, AttendanceStyled } from '../styles/TableLayout';
import { FiSave } from "react-icons/fi";
import { FiPrinter } from "react-icons/fi";
import { Link } from 'react-router-dom';
import { QRcode, CustomDatePickDiv } from '../styles/LayoutAttendance';
import TotalAttendanceIcon from '../img/icon_totalEmployees.png';
import TotalPresentIcon from '../img/icon_totalPresent.png';
import TotalAbsentIcon from '../img/icon_totalAbsent.png';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import nothing from '../img/nothing.png';
import Fab from "@mui/material/Fab";
import afternoonIconAttendance from '../img/afternoonIconAttendance.png';
import morningIconAttendance from '../img/morningIconAttendance.png';
import time from '../img/time.svg';
import axios from 'axios';
// Import React Table
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import moment from 'moment';
import Excel from 'exceljs';
import { saveAs } from 'file-saver';
import { filterCaseInsensitive } from './Recommendation';
import { printdiv } from "../globalFunctions/gFunction";

export const exportExcel = (rows, newHeader, title) => {
  //Excel Exporting
  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet('Sheet1');

  //Columns
  worksheet.columns = newHeader.map(i => {
    const settings = {
      header: i.Header,
      key: i.id,
      width: 15,
      style: {
        font: {
          name: 'Arial',
          size: 10
        },
      }
    };

    // if (i.numFmt) { settings.style.numFmt = i.numFmt }

    return settings;
  });

  worksheet.getRow(1).font = { name: 'Arial', size: 10, bold: true };
  worksheet.addRows(rows);

  workbook.xlsx.writeBuffer().then(function (data) {
    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    saveAs(blob, `${title}.xlsx`);
  });
};
const Input = styled('input')({
  display: 'none',
  width: '5vw'
});


function AttendancePage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [columns, setColumns] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [toLoad, setToLoad] = React.useState(0);
  const [totalWorkersPresent, setTotalWorkersPresent] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [theRow, settheRow] = React.useState([]);
  const [morningFrom, setmorningFrom] = React.useState('');
  const [morningTo, setmorningTo] = React.useState('');
  const [afternoonFrom, setafternoonFrom] = React.useState('');
  const [afternoonTo, setafternoonTo] = React.useState('');

  const [morningPresent, setmorningPresent] = React.useState('');
  const [afternoonPresent, setafternoonPresent] = React.useState('');

  const [morningFromIO, setmorningFromIO] = React.useState('');
  const [morningToIO, setmorningToIO] = React.useState('');
  const [afternoonFromIO, setafternoonFromIO] = React.useState('');
  const [afternoonToIO, setafternoonToIO] = React.useState('');

  const selectDateHandler = (d) => {
    setDate(d)
  }

  const setDateRange = (d) => {
    setEndDate(d);
  }

  const formatAMPM = (date) => {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      })
  });

  useEffect(() => {

    let morningToTime = '';
    let afternoonToTime = '';
    let afternoonFromTime = '';
    //Get Time Schedule set
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
      .then(res => {
        setmorningFrom(res.data[0].morningFrom);
        setmorningTo(res.data[0].morningTo);
        morningToTime = res.data[0].morningTo;
        afternoonToTime = res.data[0].afternoonTo;
        afternoonFromTime = res.data[0].afternoonFrom;

        setafternoonFrom(res.data[0].afternoonFrom);
        setafternoonTo(res.data[0].afternoonTo);
      })
      .catch(error => {
        console.log(error);
      })

  })

  useEffect(() => {

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
      params: {
        startDate: moment(startDate).format("M/DD/YYYY"),
        endDate: moment(endDate).format("M/DD/YYYY")
      }
    })
      .then(res => {
        let theRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(startDate).subtract(1, "days").format("M/DD/YYYY"))) &&
          moment(new Date(el.dateRecorded)).isBefore(new Date(moment(endDate).add(1, "days").format("M/DD/YYYY"))));
        const ids = theRes.map(o => o.user?._id);
        setToLoad(theRes?.length);
        let presentEmployees = theRes?.filter(({ user }, index) => !ids.includes(user?._id, index + 1));
        setTotalWorkersPresent(presentEmployees?.length);
        let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningTo, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningTo, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; });
        const idsMorning = presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT")) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
        const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

        setmorningPresent(presentInMorning?.filter(el => (-1 == idsMorning.indexOf(el.user._id)))?.length);
        setafternoonPresent(presentInAfternoon?.filter(el => (-1 == idsAfternoon.indexOf(el.user._id)))?.length);

        setmorningFromIO(presentInMorning.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningTo, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; })?.length);
        setmorningToIO(presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFrom, 'h:mma')) || v.includes("lateOUT")) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; })?.length);

        setafternoonFromIO(presentInAfternoon.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningTo, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; })?.length);
        setafternoonToIO(presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; })?.length);
        // console.log('heyyyyyyyyyyyy', presentInAfternoon, presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).length, presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFrom, 'h:mma')) && v.includes("lateOUT") === false) } : null)))

        if (theRes?.length > 0) {
          setColumns([
            {
              id: 'user',
              Header: 'Name',
              headerClassName: 'headerTable',
              accessor: d => d.user?.firstname + " " + d.user?.lastname
            }, {
              id: 'dateRecorded',
              Header: 'Date Recorded',
              headerClassName: 'headerTable',
              accessor: 'dateRecorded'
            },
            // {
            //   id: 'breakfast',
            //   Header: 'Breakfast',
            //   headerClassName: 'headerTable',
            //   accessor: d => d?.breakfast !== undefined ? d.breakfast : 'false'
            // }, {
            //   id: 'lunch',
            //   Header: 'Lunch',
            //   headerClassName: 'headerTable',
            //   accessor: d => d?.lunch !== undefined ? d.lunch : 'false'
            // }, {
            //   id: 'dinner',
            //   Header: 'Dinner',
            //   headerClassName: 'headerTable',
            //   accessor: d => d?.dinner !== undefined ? d.dinner : 'false'
            // }, 
            {
              id: 'timeIn',
              headerClassName: 'headerTable',
              Header: 'Time In',
              filterable: false,
              accessor: data =>
                data.timeIn?.sort(function (a, b) {
                  return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
                }).map(item => (
                  <div key={item._id}>
                    <span style={{ marginRight: "10px", fontSize: "0.8rem" }}>{item}</span>
                    <span>{item.value}</span>
                  </div>
                ))
            }, {
              id: 'timeOut',
              headerClassName: 'headerTable',
              Header: 'Time Out',
              filterable: false,
              accessor: data =>
                data.timeOut?.sort(function (a, b) {
                  return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
                }).map(item => (
                  <div>
                    <span style={{ marginRight: "10px", fontSize: "0.8rem" }}>{item !== undefined ? item : 'none'}</span>
                    <span>{item.value}</span>
                  </div>
                ))
            }]);
          setData(...[theRes]);
        }

        const mappingHistory = res.data.map(year => [{
          user: year.user?.firstname !== undefined ? year.user.firstname + " " + year.user.lastname : 'deleted',
          dateRecorded: year.dateRecorded,
          timeIn: year.timeIn,
          timeOut: year.timeOut,
        }]);

        settheRow([].concat.apply([], mappingHistory));

      })
      .catch(error => {
        console.log(error);
      })
  }, [startDate, endDate, morningTo, afternoonFrom]);



  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon className="date-icon" icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });


  return (
    <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: 'auto' }}>
      <MainLayout>
        {/*    
        <QRcode>
              <Fab arial-label='qrcode' style={{ transform: 'scale(1.6)' }} >
                <Input accept="image/*" id="icon-button-file" type="file" />
                <IconButton color="primary"  aria-label="upload picture" component="span" >
                <QrCodeScannerIcon fontSize="large"/>
                </IconButton>
              </Fab>
        </QRcode>     */}


        <AttendanceStyled>

          <div className="container">
            <div className="upperLayout">
              <div style={{ height: '30vh' }} className="card totalEmployees">
                <div className="text">
                  <h2>Total Employees</h2>
                  <h3>{totalEmployees}</h3>
                </div>
                <div className="icon">
                  <img src={TotalAttendanceIcon} alt="totalAttendance" />
                </div>
              </div>

              {/* <div className="card present">
                  <div className="text">
                    <h2>Present</h2>
                    <h3>{totalWorkersPresent}</h3>
                  </div>
                  <div className="icon">
                  <img src={TotalPresentIcon} alt="totalPresent" />
                  </div>
                </div> */}

              <div style={{ height: '30vh' }} className="card absent">
                <div className="text">
                  <h2>Absent</h2>
                  <h3>{totalEmployees - totalWorkersPresent}</h3>
                </div>
                <div className="icon">
                  <img src={TotalAbsentIcon} alt="logo" />
                </div>
              </div>
            </div>

            <div className="upperLayout">
              <div style={{ height: '30vh' }} className="card present" >
                <div className="text">
                  <h2>Morning Present</h2>
                  <h3>{morningFromIO - morningToIO}</h3>
                  <p>In: {morningFromIO}  &nbsp; Out: {morningToIO}</p>
                </div>
                <div className="icon">
                  <img src={morningIconAttendance} alt="totalPresent" />
                </div>
              </div>

              <div style={{ height: '30vh' }} className="card present">
                <div className="text">
                  <h2>Afternoon Present</h2>
                  <h3>{afternoonFromIO - afternoonToIO}</h3>
                  <p>In: {afternoonFromIO}  &nbsp; Out: {afternoonToIO}</p>
                </div>
                <div className="icon">
                  <img src={afternoonIconAttendance} alt="totalPresent" />
                </div>
              </div>
              {/* <div className="card totalEmployees">
                  <div className="text">
                    <h2>Morning</h2>
                    <h1>{morningPresent}</h1>
                  </div>
                  <div className="icon">
                  <h5>{morningFrom}</h5>
                  </div>
                </div> */}
            </div>

            <div className="lowerLayout">
              <div id="print_attendance">
                <TableLayout>
                  <div className="upperPart">
                    {/* <div className="textTitle attendance"> <h1 style={{ marginTop: '2%' }}>Attendances</h1></div>
                    <div style={{ display: 'flex', width: '190%', marginRight: '1vw' }} className="up-con">

                      <div id="btn" className="datePicker" style={{ left: '0', flexDirection: 'row' }}>
                        <label className="start-date" style={{ padding: '0' }}>Start Date:
                          <div>
                            <DatePicker
                              className="form-control"
                              dateFormat="yyyy/MM/dd"
                              selected={startDate}
                              onChange={selectDateHandler}
                              // minDate={today}
                              maxDate={endDate}
                              todayButton={"Today"}
                              customInput={<CustomInput />}
                            />
                          </div>
                        </label>

                        <label className="end-date" style={{ padding: '0', paddingTop: '0' }}>End Date:
                          <div>
                            <DatePicker
                              className="form-control"
                              dateFormat="yyyy/MM/dd"
                              selected={endDate}
                              onChange={setDateRange}
                              minDate={startDate}
                              todayButton={"Today"}
                              customInput={<CustomInput />}
                            />
                          </div>
                        </label>

                      </div>

                      <div className="exportAttendance" style={{ width: 'auto', padding: '1%', display: 'flex' }}>
                        <button style={{ display: 'flex', margin: '1%', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns, `Attendance-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                        <button style={{ display: 'flex', margin: '1%', marginLeft: '5%', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_attendance', '/attendance')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                      </div>

                    </div> */}

                  </div>

                  <div id="btn" className="flex-container2">
                    <div style={{ marginRight: '8%' }}> <div className="textTitle attendance"> <h1 style={{ marginTop: '2%' }}>Attendances</h1></div></div>

                    <div style={{ display: 'inherit', marginLeft: '6%' }}>
                      <div><label className="start-date wordings" style={{ padding: '0', width: '6vw' }}>Start Date:</label> </div>
                      <div className="start-date" style={{ width: '11vw' }}> <DatePicker
                        className=""
                        dateFormat="yyyy/MM/dd"
                        selected={startDate}
                        onChange={selectDateHandler}
                        maxDate={endDate}
                        todayButton={"Today"}
                        customInput={<CustomInput />}
                      /></div>
                      <div><label className="end-date wordings" style={{ padding: '0', paddingTop: '0', width: '6vw' }}>End Date:  </label> </div>
                      <div className="start-date" style={{ width: '10vw', padding: '0' }}>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy/MM/dd"
                          selected={endDate}
                          onChange={setDateRange}
                          minDate={startDate}
                          todayButton={"Today"}
                          customInput={<CustomInput />}
                        />
                      </div>
                    </div>

                    <div style={{ width: '20vw', marginLeft: '1%' }}>
                      <div className="exportAttendance" style={{ width: 'auto', padding: '1%', display: 'flex' }}>
                        <button style={{ display: 'flex', margin: '1%', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns, `Attendance-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                        <button style={{ display: 'flex', margin: '1%', marginLeft: '5%', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_attendance', '/attendance')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                      </div>
                    </div>
                  </div>

                  <Line></Line>
                  <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>

                    {
                      toLoad !== 0 ?
                        <ReactTable
                          data={data}
                          filterable
                          minRows={1}
                          defaultFilterMethod={filterCaseInsensitive}
                          columns={columns}
                          defaultPageSize={10}
                          className="-striped -highlight"
                          getTheadThProps={(columnIndex) => ({
                            style: { textAlign: "left", color: "black" }
                          })}
                          getTdProps={() => ({
                            style: { fontSize: "0.8rem", border: "none" }
                          })}
                          getPaginationProps={() => ({
                            style: {}
                          })}
                          style={{ textAlign: 'left', border: "None" }}
                        />
                        :
                        <div style={{ margin: 'auto', marginTop: '5%', textAlign: 'center' }}><img style={{ height: '50%', width: '50%', opacity: '80%' }} src={nothing} alt="React Logo" /></div>
                    }

                  </div>
                </TableLayout>
              </div>
            </div>

          </div>
        </AttendanceStyled>
        <zoom in={true} timeout={{ enter: 500, exit: 500 }}>
        </zoom>

      </MainLayout>
    </div>
  )
}



export default AttendancePage;
