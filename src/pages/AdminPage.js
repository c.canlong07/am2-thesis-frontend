import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import { CustomDatePickDiv } from '../styles/LayoutAttendance';
import "react-datepicker/dist/react-datepicker.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Fab from "@mui/material/Fab";
import time from '../img/time.svg';
import axios from 'axios';
// Import React Table
import {
  Button,
  Modal,
} from "react-bootstrap";
import QRCode2 from "qrcode.react";
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import moment from 'moment';
import { TimePicker } from 'antd';
import 'antd/dist/antd.css';
import Swal from 'sweetalert2';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';

const Input = styled('input')({
  display: 'none',
  width: '5vw'
});


function UsersPage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [toGenerateFor, setToGenerateFor] = React.useState([]);
  const [toShow, setToShow] = React.useState(false);
  const [toGenerate, setToGenerate] = React.useState(0);
  const [totalWorkersPresent, setTotalWorkersPresent] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  const [userDB, setUserDB] = useState([]);
  const [morningTimeFrom, setmorningTimeFrom] = useState('');
  const [morningTimeTo, setmorningTimeTo] = useState('');
  const [afternoonTimeFrom, setafternoonTimeFrom] = useState('');
  const [afternoonTimeTo, setafternoonTimeTo] = useState('');

  const selectDateHandler = (d) => {
    setDate(d)
  }

  const setDateRange = (d) => {
    setEndDate(d);
  }

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/' + user?._id)
      .then(res => {
        setUserDB(res.data)
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
      .then(res => {
        setmorningTimeFrom(res.data[0].morningFrom);
        setmorningTimeTo(res.data[0].morningTo);
        setafternoonTimeFrom(res.data[0].afternoonFrom);
        setafternoonTimeTo(res.data[0].afternoonTo);
      })
      .catch(error => {
        console.log(error);
      })


  }, []);

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      })
  });

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });

  const generateQr = (id) => {
    // let generatingFor= this.state.employee.filter(user => user._id === id);
    // this.setState({toShow: true, toGenerate: id, generateFor: generatingFor})
    // let str = id

    setToShow(true);
    setToGenerate(id);
  }
  const handleClose = () => {
    setToShow(false);
  }

  const download = function () {
    const link = document.createElement("a");
    link.download = "filename.png";
    link.href = document.getElementById("canvas").toDataURL();
    link.click();
  };

  const saveTime = () => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/add/time', {
      params: {
        morningFrom: morningTimeFrom, morningTo: morningTimeTo,
        afternoonFrom: afternoonTimeFrom, afternoonTo: afternoonTimeTo
      }
    })
      .then(res => 
        Swal.fire({
          title: 'Time has been Updated',
          text: "Successfully Updated the Time",
          icon: 'success',
          confirmButtonText: 'Okay'
      }).then((result) => {
          if (result.isConfirmed) {
            window.location = "/admin"
          }
      })
        )
      .catch(err => console.log('Error :' + err));
    // setToShow(false);
  }

  let mFrom = morningTimeFrom.split(' ')[0].toString();

  return (
    <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: '100vh' }}>
      <UsersStyled>
        <h1> Manage Settings </h1>
        <div className="whole" >
          <div className="card text-center">
            <div className="card-header text-uppercase">
              Time
            </div>
            <div className="card-body">
              <Divider style={{ marginBottom: '3%' }}>
                <Chip label="Morning Time" />
              </Divider>
              <p>{morningTimeFrom + ' - ' + morningTimeTo}</p>
              <TimePicker
                format='hh:mm A'
                value={moment(`2021-01-01 ${morningTimeFrom}`)}
                onChange={(time) => 
                  [new Date(time._d) > new Date(moment(`2021-01-01 ${morningTimeTo}`))
                ? Swal.fire(
                  `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                  `Time should be less than the Morning End Time : ${morningTimeTo} `,
                  'warning'
                ) :
                setmorningTimeFrom(moment(time?._d).format('hh:mm A'))]} />

              <TimePicker
                format='hh:mm A'
                value={moment(`2021-01-01 ${morningTimeTo}`)}
                onChange={(time) =>
                  [new Date(time._d) < new Date(moment(`2021-01-01 ${morningTimeFrom}`))
                  ? Swal.fire(
                    `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                    `Time should be greater than the Morning Start Time : ${morningTimeFrom} `,
                    'warning'
                  ) :
                  new Date(time._d) > new Date(moment(`2021-01-01 ${afternoonTimeFrom}`))
                  ? Swal.fire(
                    `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                    `Time should be less than the Afternoon Start Time : ${afternoonTimeFrom} `,
                    'warning'
                  ) :
                  
                  setmorningTimeTo(moment(time._d).format('hh:mm A'))]
                } />
            {/* <div className="card-header text-uppercase">
              Afternoon
            </div> */}
            <Divider style={{ marginBottom: '3%', marginTop: '3%' }}>
              <Chip label="Afternoon Time" />
            </Divider>
              <p>{afternoonTimeFrom + ' - ' + afternoonTimeTo}</p>
              <TimePicker
                format='hh:mm A'
                value={moment(`2021-01-01 ${afternoonTimeFrom}`)}
                onChange={(time) =>
                  [new Date(time._d) < new Date(moment(`2021-01-01 ${morningTimeTo}`))
                  ? Swal.fire(
                    `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                    `Time should be greater than the Morning End Time : ${morningTimeTo} `,
                    'warning'
                  ) :
                  new Date(time._d) > new Date(moment(`2021-01-01 ${afternoonTimeTo}`))
                  ? Swal.fire(
                    `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                    `Time should be less than the Afternoon End Time : ${afternoonTimeTo} `,
                    'warning'
                  ) :
                  setafternoonTimeFrom(moment(time?._d).format('hh:mm A'))]
                 } />

              <TimePicker
                format='hh:mm A'
                value={moment(`2021-01-01 ${afternoonTimeTo}`)}
                onChange={(time) => 
                  [new Date(time._d) < new Date(moment(`2021-01-01 ${afternoonTimeFrom}`))
                  ? Swal.fire(
                    `Invalid Time (${moment(time?._d).format('hh:mm A')})?`,
                    `Time should be greater than the Afternoon Start Time : ${afternoonTimeFrom} `,
                    'warning'
                  ) :setafternoonTimeTo(moment(time._d).format('hh:mm A'))]
                } />
            
          </div>
          <div className="card-footer text-muted">
              <button type="submit" onClick={() => saveTime()} style={{ marginRight: 10 }} className="btn btn-primary">Save time</button>
            </div>
        </div>
        </div>


      </UsersStyled>
    </div>



  )
}
const UsersStyled = styled.div`
  text-align: center;
  width: auto;

  h1{
    padding-top: 2rem;
  }

  button{
    background: #203354;
    border-color: #203354;
    color: white;
    width: 25%;
  }

  .card-body{
    background: #E6E6E6;
  }

  .list-group-item{
    border-bottom: 3px solid #203354;
  }

  .card-header{
    background: #203354;
    color: white;
  
  }

  .whole{
    width: 60%;
    margin-left: auto;
    margin-right: auto;
    float: center;
  }
  
  
  @media only screen and (max-width: 500px){
    h1{
      font-size: 2rem;
      padding-top: 2rem;
    }

    .whole{
      width: 80%;
    }

  }
`;
export default UsersPage;
