import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import { Line, MainLayout, TableLayout, AttendanceStyled } from '../styles/TableLayout';
import { ButtonLayout } from '../styles/LayoutForm';
import { Link } from 'react-router-dom';
import { BiEdit } from "react-icons/bi";
import { QRcode, CustomDatePickDiv } from '../styles/LayoutAttendance';
import TotalAttendanceIcon from '../img/icon_totalEmployees.png';
import TotalPresentIcon from '../img/icon_totalPresent.png';
import TotalAbsentIcon from '../img/icon_totalAbsent.png';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Fab from "@mui/material/Fab";
import time from '../img/time.svg';
import axios from 'axios';
// Import React Table
import {
  Button,
  Modal,
} from "react-bootstrap";
import QRCode2 from "qrcode.react";
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import moment from 'moment';
const Input = styled('input')({
  display: 'none',
  width: '5vw'
});


function UsersPage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [toGenerateFor, setToGenerateFor] = React.useState([]);
  const [toShow, setToShow] = React.useState(false);
  const [toGenerate, setToGenerate] = React.useState(0);
  const [totalWorkersPresent, setTotalWorkersPresent] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  const [userDB, setUserDB] = useState([]);

  const selectDateHandler = (d) => {
    setDate(d)
  }

  const setDateRange = (d) => {
    setEndDate(d);
  }

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/' + user?._id)
      .then(res => {
        setUserDB(res.data)
      })
      .catch(error => {
        console.log(error);
      })
  }, []);

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      })
  });

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });

  const generateQr = (id) => {
    // let generatingFor= this.state.employee.filter(user => user._id === id);
    // this.setState({toShow: true, toGenerate: id, generateFor: generatingFor})
    // let str = id

    setToShow(true);
    setToGenerate(id);
  }
  const handleClose = () => {
    setToShow(false);
  }

  const download = function () {
    const link = document.createElement("a");
    link.download = "filename.png";
    link.href = document.getElementById("canvas").toDataURL();
    link.click();
  };



  return (
    <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: 'auto', paddingBottom: '2rem' }}>
      <UsersStyled>

        <div className="whole">
          <h4 style={{ fontSize: '2rem', textAlign: 'center', backgroundColor: '#203354', color: 'white', fontWeight: 'bold', borderRadius: '4px' }} className="title"> Manage Profile </h4>
          <div className="card text-center" style={{ backgroundColor: '#white', border: '15', borderColor: '#203354', padding: '1rem', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div className=" text-uppercase" style={{ paddingTop: '1rem', backgroundColor: 'none', alignContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
              <div className="circle" style={{ boxShadow: '0px 0px 4px rgba(0,0,0,1)', height: '10rem', width: '10rem', background: '#203354', borderRadius: '50%', marginBottom: '0.5rem' }}>
                <h2 style={{ color: 'white', textAlign: 'center', paddingTop: '2rem', marginBottom: '0px !important', fontSize: '90px', }}>
                  {userDB?.firstname?.charAt(userDB?.firstname.length0)}
                  {userDB?.lastname?.charAt(userDB?.lastname.length0)}
                </h2>
              </div>
              <h3>{userDB?.firstname + ' ' + userDB?.lastname}</h3>
            </div>
            <div className="card-body">

              <div className="form" style={{ display: 'flex', background: 'white' }}>
                <div className="right">
                  <label>Deparment:</label>
                </div>
                <div className="left">
                  <label style={{ color: 'gray' }}>{userDB?.department}</label>

                </div>
              </div>

              <div className="form" style={{ display: 'flex', background: 'white' }}>
                <div className="right">
                  <label>Email:</label>
                </div>
                <div className="left">
                  <label style={{ color: 'gray' }}>{userDB?.email}</label>
                </div>
              </div>

              <div className="form" style={{ display: 'flex', background: 'white' }}>
                <div className="right">
                  <label>Address:</label>
                </div>
                <div className="left">
                  <label style={{ color: 'gray' }}>{userDB?.address}</label>
                </div>
              </div>

              <div className="form" style={{ display: 'flex', background: 'white' }}>
                <div className="right">
                  <label>Contact Number:</label>
                </div>
                <div className="left">
                  <label style={{ color: 'gray' }}>{userDB?.cpnumber}</label>
                </div>
              </div>

            </div>
            <div className="card-footer text-muted" style={{ display: 'flex', gap: '1rem' }}>
              <Link to={'/editemp/' + userDB?._id}><button type="submit" style={{ background: 'green', border: 'none' }} className="btn btn-primary update">Update</button></Link>
              <button type="submit" onClick={() => generateQr(userDB?._id)} style={{ border: 'none' }} className="btn btn-primary">Generate QR Code</button>
            </div>
          </div>
        </div>

        <Modal show={toShow} onHide={handleClose} animation={false}>
          <Modal.Header style={{justifyContent:'center', background: ' rgb(32, 51, 84)', color: 'white', border: 'none' }}>
            <Modal.Title style={{}}>User QR Code</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{padding: '0px 0px 0px 0px', justifyContent: 'center'}}>
            <div style={{margin: 'auto', display: 'block', textAlign: 'center'}}>
            <div className="qrCode" style={{paddingTop: '1rem',}}>
              <QRCode2 className="qrCodeBorder" style={{width: '40%', height: '40%', padding: '0.9rem 0.9rem',
              background: 'linear-gradient(to right, black 4px, transparent 4px) 0 0, linear-gradient(to right, black 4px, transparent 4px) 0 100%, linear-gradient(to left, black 4px, transparent 4px) 100% 0,     linear-gradient(to left, black 4px, transparent 4px) 100% 100%,  linear-gradient(to bottom, black 4px, transparent 4px) 0 0, linear-gradient(to bottom, black 4px, transparent 4px) 100% 0, linear-gradient(to top, black 4px, transparent 4px) 0 100%, linear-gradient(to top, black 4px, transparent 4px) 100% 100%',
              backgroundRepeat: 'no-repeat', backgroundSize: '20% 20%'
              }}
              value={toGenerate} id="canvas" /><br />
              </div>  
              <p style={{marginTop: '2vh',marginRight: 'auto', marginLeft: 'auto', padding: '0.5vw 0.5vw', borderRadius: '10px', width: '60%', background: 'rgb(32, 51, 84)', color: 'white', textTransform: 'uppercase' }}>{userDB?.firstname + " " + userDB?.lastname}</p>


            </div>

          </Modal.Body>
          <Modal.Footer>

            <ButtonLayout>
              <Button style={{ marginRight: 10, }} type="button" className="mt-30" onClick={download}>
                Download QR
              </Button>
              <Link to='/users' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark" onClick={handleClose}> Close </Link>
            </ButtonLayout>
          </Modal.Footer>
        </Modal>
      </UsersStyled>
    </div>


  )
}
const UsersStyled = styled.header`
  

  .whole{
    text-align: left;
    padding-top: 1.5rem;

    label{
      font-family: 'Montserrat', sans-serif;
      font-weight: 700;
      margin-bottom: 0px;
      font-size: 20px;
    }

  
    .form{
      box-shadow: 0px 0px 5px rgba(0,0,0,0.5);
      border: 2px solid;
      margin-bottom: 1rem;
      justify-content: center;
      text-align: center;
      border-radius: 8px;
      padding: 0.5rem 2rem;
    }

    .right{
      flex: 50%;
      text-align: left;
      justify-content: center;
    }
    .left{
      flex: 50%;
      text-align: right;
      justify-content: center;
    }

    .card-footer{
      width: 50%;
    }

  }

  h4{
    padding: 0.5rem 2rem;
    background-color: white;
    border-radius: 8px;
    box-shadow: 0px 0px 10px rgba (0,0,0,1)
    text-size: 20px;
  }

  h3{
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
  }

  h5{
    margin-bottom: 0;
    text-align: right;
    font-size: 20px;
  }

  .text-center{
    background-color: none;
    
  }
  button{
    background: #203354;
    border-color: #203354;
    color: white;
    width: 25%;
  }

  .card-body{
    background: white;
    border-radius: 8px;
    width: 70%;
    padding: 0;
  }

  .list-group-item{
    border-radius: 8px;
    margin-bottom: 0.5rem;
    padding: 0.2rem;
    
  }

  .card-header{
    background: #203354;
    color: white;
  
  }

  .whole{
    width: 60%;
    margin-left: auto;
    margin-right: auto;
    float: center;
    
  }

  .btn{
    width: 100%;
  }

  
  @media only screen and (max-width: 500px){
    h1{
      font-size: 2rem;
      padding-top: 0.5rem;
    }

    .whole{
      width: 80%;
      text-align: center;

      .form{
        padding: 0.5rem 1rem;
      }

      label{
        font-size: 13px;
      }
      .card-footer{
        width: 100%;
      }
  
    }

    .card-body{
      width: 95%;
    }
    
    

  }
`;
export default UsersPage;
