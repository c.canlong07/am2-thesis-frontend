import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import ReactPaginate from 'react-paginate'
import { Link } from 'react-router-dom';
import { ModalLayout, ButtonLayout } from '../styles/LayoutForm';
import { MainLayout, TableLayout } from '../styles/TableLayout';
import { Line } from "../styles/TableLayout";
import nothing from '../img/nothing.png';
import axios from 'axios';
import ReactTable from "react-table-6";
import Moment from 'moment';
import Swal from 'sweetalert2'
import { BiEdit } from "react-icons/bi";
import { RiDeleteBin5Line } from "react-icons/ri";
import { FiSave } from "react-icons/fi";
import { FiPrinter } from "react-icons/fi";
import {
  Card, Button, Modal, Col, Row, Spinner
} from "react-bootstrap";
import food from '../img/food.svg';
import budget from '../img/budget.svg';
import QRCode2 from "qrcode.react";
import Rating from 'material-ui-rating';

import { exportExcel } from './AttendancePage';
import TextareaAutosize from 'react-textarea-autosize';
import { filterCaseInsensitive } from './Recommendation';
import { printdiv } from "../globalFunctions/gFunction";
import WeeklyMealPlan from "../dataVisualization/WeeklyMealPlan";
import Calendar2 from "../components/calendar2.js";

var thisMonday;
var NextMonday;

var thisFriday;
var NextFriday;
function MealPage() {
  const [foodState, setFoodState] = useState();
  const [columns, setColumns] = React.useState([]);
  const [budgetcolumns, setBudgetColumns] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [budgetdata, setBudgetData] = React.useState([]);
  const [toLoad, setToLoad] = React.useState(0);
  const [mealToday, setmealToday] = React.useState([]);
  const [Title, setTitle] = React.useState('Menus');
  const [MenuSelected, setMenuSelected] = React.useState();
  const [menuToDisplay, setmenuToDisplay] = React.useState([]);
  const [mapping, setmapping] = React.useState([]);
  const [toShow, setToShow] = React.useState(false);
  const [toGenerate, setToGenerate] = React.useState('');
  const [toUser, setToUser] = React.useState();
  const [toOverall, setToOverall] = React.useState();
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  const [history, sethistory] = React.useState([]);
  const [ratingID, setratingID] = React.useState();
  const [allTimeFav, setallTimeFav] = React.useState([]);
  const [theRow, settheRow] = React.useState([]);
  const [relatedhistory, setrelatedhistory] = React.useState([]);
  const [toShowFeedback, setToShowFeedback] = React.useState(false);
  const [userFeedback, setUserFeedback] = React.useState('');
  const [userFeedbackID, setUserFeedbackID] = React.useState('');
  const [res, setRes] = React.useState([]);
  const [editMeal, setEditMeal] = React.useState([]);
  const [editRating, setEditRating] = React.useState('');
  const [checked, setChecked] = useState([]);
  const [checked2, setChecked2] = useState([]);
  const [weekChange, setweekChange] = useState(false);
  const checkList = [];
  const checkList2 = [];

  const [currentFiveHistory, setCurrentFiveHistory] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  const [currentAllHistory, setCurrentAllHistory] = useState([]);
  const [pageCountAll, setPageCountAll] = useState(0);
  const [itemOffsetAll, setItemOffsetAll] = useState(0);
  const [thisWeek, setthisWeek] = useState([]);
  const [nextWeek, setnextWeek] = useState([]);
  const [theAllMeals, settheAllMeals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isMealToday, setIsMealToday] = useState(true);


  let itemsPerPage = 6;
  const getColumnWidth = (rows, accessor, headerText) => {
    const maxWidth = 400
    const magicSpacing = 10
    const cellLength = Math.max(
      ...rows.map(row => (`${row[accessor]}` || '').length),
      headerText.length,
    )
    return Math.min(maxWidth, cellLength * magicSpacing)
  }

  // Add/Remove checked item from list
  const handleCheck = (event) => {
    var updatedList = [...checked];
    if (event.target.checked) {
      updatedList = [...checked, event.target.value];
    } else {
      updatedList.splice(checked.indexOf(event.target.value), 1);
    }
    setChecked(updatedList);
  };

  // Add/Remove checked item from list
  const handleCheck2 = (event) => {
    var updatedList = [...checked2];
    if (event.target.checked) {
      updatedList = [...checked2, event.target.value];
    } else {
      updatedList.splice(checked2.indexOf(event.target.value), 1);
    }
    setChecked2(updatedList);
  };

  const deleteEmployee = (id) => {
    axios.delete(process.env.REACT_APP_BASE_URL + '/employee/deleteMeal/' + id)
      .then(res => window.location.href = "/meals")
    // this.setState({
    //     employee: this.state.employee.filter(el => el._id !== id)
    // })

  }

  const deleteBudget = (id) => {
    axios.delete(process.env.REACT_APP_BASE_URL + '/employee/deleteBudget/' + id)
      .then(res => window.location.href = "/meals")
    // this.setState({
    //     employee: this.state.employee.filter(el => el._id !== id)
    // })

  }

  useEffect(() => {
    var now = new Date(Moment().subtract('days', 7).format('DD-MMM-YYYY'));
    var now2 = new Date(Moment().subtract('days', 7).format('DD-MMM-YYYY'));
    thisMonday = now.getNextWeekDay(1); // 0 = Sunday, 1 = Monday, ...
    NextMonday = new Date(thisMonday).getNextWeekDay(1);

    thisFriday = now2.getNextWeekDay(5); // 0 = Sunday, 5 = Friday, ...
    NextFriday = new Date(thisFriday).getNextWeekDay(5);

    setthisWeek([Moment(thisMonday).format('MM/DD/YYYY'), Moment(thisFriday).format('MM/DD/YYYY')]);
    setnextWeek([Moment(NextMonday).format('MM/DD/YYYY'), Moment(NextFriday).format('MM/DD/YYYY')]);


    axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget', {
      params: {
        startDate: Moment(thisMonday).format('MM/DD/YYYY'),
        endDate: Moment(thisFriday).format('MM/DD/YYYY')
      }
    })
      .then(res => {
        console.log('budget this week', res)
      })
      .catch(error => {
        console.log(error);
      })
  }, []);


  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        console.log('WEEK MEALSSSSSSSSSSS', res.data);
        setToLoad(res.data.length);

        if (res.data.length > 0) {
          setColumns([
            {
              id: 'mealName',
              Header: 'Name',
              className: 'table-Name',
              headerClassName: 'headerTable',
              accessor: 'mealName'
            }, {
              id: 'mealDesc',
              className: 'table-meal',
              headerClassName: 'headerTable',
              Header: 'Descriptions',
              accessor: data =>
                <div>
                  <p style={{ marginRight: "10px", fontSize: "0.8rem" }}>{data.mealDesc}</p>
                </div>,
            }, {
              id: 'mealIng',
              Header: 'Ingredients',
              className: 'table-ingredients',
              headerClassName: 'headerTable',
              accessor: 'mealIng'
            }, {
              id: 'mealType',
              Header: 'Meal Type',
              className: 'table-Name',
              headerClassName: 'headerTable',
              accessor: 'mealType'
            }, {
              id: 'mealCategory',
              Header: 'Meal Category',
              className: 'table-Name',
              headerClassName: 'headerTable',
              accessor: data => data.mealCategory
            }, {
              id: 'mealDate',
              className: 'table-data',
              headerClassName: 'headerTable',
              Header: 'Date',
              headerClassName: 'headerTable',
              accessor: data =>
                // data.timeIn?.map(item => (
                <div>
                  <span style={{ marginRight: "10px", fontSize: "0.8rem" }}>{Moment(data.mealDate).format('ddd, MMM DD YYYY')}</span>
                </div>
              // ))  .format('MMMM Do YYYY
              // Cell: (props) => {Moment(props.value).format('dd/MM/yyyy')}  
            }, {
              id: 'mealServedEst',
              Header: 'Serve Estimation',
              accessor: 'mealServedEst',
              headerClassName: 'headerTable'
            }, {
              id: 'mealTotalPrice',
              Header: 'Overall Meal Price Estimation',
              accessor: 'mealTotalPrice',
              headerClassName: 'headerTable'
            }, {
              id: 'mealServed',
              Header: 'Meal Served',
              accessor: 'mealServed',
              headerClassName: 'headerTable'
            }, {
              Header: 'Default',
              id: 'defaultMeal',
              className: 'table-Name',
              headerClassName: 'headerTable',
              accessor: data => data.defaultMeal ? data.defaultMeal : 'false'
            }, {
              Header: 'Actions',
              filterable: false,
              headerClassName: 'headerTable',
              Cell: row => (
                <div >
                  <Link to={'/editMeal/' + row.original._id + '/' + row.original.mealDate + '/' + row.original.budget} style={{ background: '#00C2DE', marginLeft: '24%', padding: '3%', paddingTop: '1%' }} className="btn "><BiEdit style={{ fontSize: 20, color: 'white' }} /></Link>

                  <a href="#" onClick={() => {
                    Swal.fire({
                      title: 'Are you sure?',
                      text: "You won't be able to revert this!",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                      if (result.isConfirmed) {
                        deleteEmployee(row.original._id)
                      }
                    })
                  }} style={row.original.mealServed ? { background: '#7a3b3b', marginLeft: '3%', padding: '3%', paddingTop: '1%', pointerEvents: 'none' }
                    : { background: 'red', marginLeft: '3%', padding: '3%', paddingTop: '1%' }} className="btn "><RiDeleteBin5Line style={{ fontSize: 20, color: 'white' }} /></a>

                </div>
              )
            },]);
          setData(...[res.data]);
          const mappingHistory = res.data.map(year => [{
            mealName: year.mealName,
            mealDesc: year.mealDesc,
            mealIng: year.mealIng,
            mealType: year.mealType,
            mealDate: year.mealDate,
            mealServedEst: year.mealServedEst,
            mealTotalPrice: year.mealTotalPrice,
            mealServed: year.mealServed,
            defaultMeal: year.defaultMeal,
          }]);

          settheRow([].concat.apply([], mappingHistory));
        }



        let theMealToday = res.data.filter(function (pilot) {
          return pilot.mealDate === Moment(new Date()).format('MM-DD-YYYY');
        });

        const mapping = theMealToday.map(year => [{
          mealName: year.mealName,
          id: year._id,
          mealDesc: year.mealDesc,
          mealIng: year.mealIng,
          mealType: year.mealType,
        }]);

        setmealToday(Object.keys(mapping).map(function (key) {
          return { 'mealName': mapping[key][0].mealName, 'id': mapping[key][0].id, 'mealType': mapping[key][0].mealType };
        }));

        setmapping(mapping);
        setIsMealToday(false);

        let theThisWeeklyMeal = res.data.filter(function (weeklyDate) {
          return Moment(new Date(weeklyDate.mealDate)).isAfter(new Date(Moment(thisMonday).subtract("days", 1).format("MM-DD-YYYY")));
        });



        // const AllMealsMapping = res.data.map(meal => [{
        // mealName: meal.mealName,
        //   id: meal._id,
        //     mealDesc: meal.mealDesc,
        //       mealIng: meal.mealIng,
        //         mealType: meal.mealType,
        //           mealServedEst: meal.mealServedEst,
        //             mealServed: meal.mealServed,
        //               mealTotalPrice: meal.mealTotalPrice,
        //                 budget: meal.budget,
        //                   mealCategory: meal.mealCategory,
        //                     mealDate: meal.mealDate,
        //                       mealDay: Moment(meal.mealDate).format('dddd'),
        // }]);

        let theNextWeeklyMeal = res.data.filter(function (weeklyDate) {
          return Moment(new Date(weeklyDate.mealDate)).isAfter(new Date(Moment(NextMonday).subtract("days", 1).format("MM-DD-YYYY"))) &&
            Moment(new Date(weeklyDate.mealDate)).isBefore(new Date(Moment(NextFriday).add("days", 1).format("MM-DD-YYYY")));
        });


        settheAllMeals(theNextWeeklyMeal);
        setIsLoading(false);

      })
      .catch(error => {
        console.log(error);
      })


    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allbudgets')
      .then(res => {

        setToLoad(res.data.length);

        if (res.data.length > 0) {
          setBudgetColumns([
            {
              id: 'budgetNote',
              className: 'table-meal',
              headerClassName: 'headerTable',
              Header: 'Budget Note',
              accessor: data =>
                <div>
                  <p style={{ marginRight: "10px", fontSize: "0.8rem" }}>{data.budgetNote}</p>
                </div>,
            }, {
              id: 'budgetDate',
              className: 'table-data',
              headerClassName: 'headerTable',
              Header: 'Date',
              headerClassName: 'headerTable',
              accessor: data =>
                // data.timeIn?.map(item => (
                <div>
                  <span style={{ marginRight: "10px", fontSize: "0.8rem" }}>{Moment(data.budgetDate).format('ddd, MMM DD YYYY')}</span>
                </div>
              // ))  .format('MMMM Do YYYY
              // Cell: (props) => {Moment(props.value).format('dd/MM/yyyy')}  
            }, {
              id: 'budget',
              Header: 'Budget',
              accessor: 'budget',
              headerClassName: 'headerTable'
            },
            {
              Header: 'Actions',
              filterable: false,
              headerClassName: 'headerTable',
              Cell: row => (
                <div >
                  <Link to={'/editBudget/' + row.original._id} style={{ background: '#00C2DE', marginLeft: '24%', padding: '3%', paddingTop: '1%' }} className="btn "><BiEdit style={{ fontSize: 20, color: 'white' }} /></Link>
                  <a href="#" onClick={() => {
                    Swal.fire({
                      title: 'Are you sure?',
                      text: "You won't be able to revert this!",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                      if (result.isConfirmed) {
                        deleteBudget(row.original._id)
                      }
                    })
                  }} style={{ background: 'red', marginLeft: '3%', padding: '3%', paddingTop: '1%' }} className="btn "><RiDeleteBin5Line style={{ fontSize: 20, color: 'white' }} /></a>
                  <Link to={'/createMeal/' + row.original._id + '/' + row.original.budgetDate} style={{ background: '#00C2DE', marginLeft: '4%', paddingTop: '1%' }} className="btn "><i className="fa fa-plus" style={{ fontSize: 20, color: 'white' }}></i></Link>

                </div>
              )
            },]);
          setBudgetData(res.data.sort((a, b) => b.budgetDate.localeCompare(a.budgetDate)));
        }

      })
      .catch(error => {
        console.log(error);
      })


  }, []);

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        let theNextWeeklyMeal = res.data.filter(function (weeklyDate) {
          return Moment(new Date(weeklyDate.mealDate)).isAfter(new Date(Moment(NextMonday).subtract("days", 1).format("MM-DD-YYYY"))) &&
            Moment(new Date(weeklyDate.mealDate)).isBefore(new Date(Moment(NextFriday).add("days", 1).format("MM-DD-YYYY")));
        });
        console.log('WEEK MEALS',
          theNextWeeklyMeal
          , NextMonday, NextFriday);

        settheAllMeals(theNextWeeklyMeal);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      })


  }, [weekChange, NextMonday, NextFriday]);

  // useEffect(() => {

  //   const startDate = new Date;
  //   axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealToday', {
  //     params: {
  //       startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
  //       endDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear()
  //     }
  //   })
  //     .then(res => {
  //       const mapping = res.data.map(year => [{
  //         mealName: year.mealName,
  //         id: year._id,
  //         mealDesc: year.mealDesc,
  //         mealIng: year.mealIng,
  //         mealType: year.mealType,
  //       }]);

  //       setmealToday(Object.keys(mapping).map(function (key) {
  //         return { 'mealName': mapping[key][0].mealName, 'id': mapping[key][0].id, 'mealType': mapping[key][0].mealType };
  //       }));

  //       localStorage.setItem('mealToday', JSON.stringify(Object.keys(mapping).map(function (key) {
  //         return { 'name': mapping[key][0].name, 'id': mapping[key][0].id };
  //       })));


  //       setmapping(mapping);

  //     })
  //     .catch(error => {
  //       console.log(error);
  //     })


  // }, [])

  useEffect(() => {
  }, [editRating])

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeemealhistory', { params: { id: user?._id } })
      .then(res => {
        //  let breakF = res.data.filter(function (pilot) {
        //   return pilot.mealType === "breakfast";
        // });
        //   breakF = breakF.filter(function (pilot) {
        //   return pilot.rating === Math.max(...breakF.map(e => e.rating ? e.rating : 0))
        // });

        // setallTimeFav(obj);


        const mappingHistory = res.data.map(year => [{
          mealName: year.meal.mealName,
          _id: year._id,
          rating: year?.rating,
          // date: Moment(year.createdAt).format('MMMM DD YYYY HH:mm:ss'),
          date: year.meal.mealDate,
          mealDesc: year.meal.mealDesc,
          mealIng: year.meal.mealIng,
          mealType: year.meal.mealType,
          feedback: year.feedback,
          availed: year?.availed
        }]);

        sethistory(mappingHistory);
        setPageCountAll(Math.ceil(mappingHistory.length / itemsPerPage))
        setRes(res.data);
        let theArray = mappingHistory.filter(function (pilot) {
          return pilot[0].rating === 5;
        });
        setallTimeFav(theArray);
        setPageCount(Math.ceil(theArray.length / itemsPerPage))
      })
      .catch(error => {
        console.log(error);
      })
  }, [ratingID]);

  useEffect(() => {

    const startDate = new Date;
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealTodaySelected', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear(),
        id: MenuSelected
      }
    })
      .then(res => {
        setmenuToDisplay(res.data);
      })
      .catch(error => {
        console.log(error);
      })

  }, [MenuSelected, Title])

  const handleSelect = (e) => {
    setTitle(e.target.name);
    setMenuSelected(e.target.id);
  }

  useEffect(() => {
    let toEditMeal = res.filter(function (el) {
      return el._id === userFeedbackID  // Changed this so a home would match
    });
    setEditMeal(toEditMeal);
    setUserFeedback(toEditMeal[0]?.feedback2);
    setEditRating(toEditMeal[0]?.rating !== undefined && toEditMeal[0]?.rating !== null ? toEditMeal[0]?.rating : 0)
    setChecked(toEditMeal[0]?.ingToRemove ? toEditMeal[0]?.ingToRemove : []);
    setChecked2(toEditMeal[0]?.ingToAdd ? toEditMeal[0]?.ingToAdd : []);
    return () => {
      setEditMeal([]);

    }
  }, [toShowFeedback])

  const generateQr = (id, type, name, overall) => {
    let user = JSON.parse(localStorage.getItem("user"));
    setToUser(user);
    setToShow(true);
    setToOverall(overall);
    setToGenerate(user._id + ',' + id + ',' + type + ',' + name);

  }

  const handleClose = () => {
    setToShow(false);
    setToShowFeedback(false);
    setEditMeal([])
    setUserFeedback('');
    setChecked([]);
    setChecked2([]);
  }

  const download = () => {
    const link = document.createElement("a");
    link.download = "filename.png";
    link.href = document.getElementById("canvas").toDataURL();
    link.click();
  };

  const ratingChanged = (newValue) => {
    if (ratingID !== undefined) {
      axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeemealrating', { params: { id: ratingID, rating: newValue } })
        .then(res => {

          // window.location.href = "/meals";
        })
        .catch(error => {
          console.log(error);
        })
    }

  };


  const feedbackChanged = (ratingID) => {
    // if(ratingID !== undefined){
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeemealratingfeedback', {
      params: {
        id: ratingID, feedback2: userFeedback, rating: editRating,
        // ingToRemove: checked, ingToAdd: checked2 
      }
    })
      .then(res => {

        Swal.fire({
          title: 'Feedback has been Send',
          text: "Successfully send a Feedback",
          icon: 'success',
          confirmButtonText: 'Okay'
        }).then((result) => {
          if (result.isConfirmed) {
            window.location = "/meals"
          }
        })
      })
      .catch(error => {
        console.log(error);
      })
    // }

  };

  const onValueChange = (e) => {
    setUserFeedback(e.target.value)
  }


  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentFiveHistory(allTimeFav.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(allTimeFav.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, allTimeFav]);

  useEffect(() => {
    const endOffset = itemOffsetAll + itemsPerPage;
    setCurrentAllHistory(history.slice(itemOffsetAll, endOffset));
    setPageCount(Math.ceil(history.length / itemsPerPage));
  }, [itemOffsetAll, itemsPerPage, history]);

  // Invoke when user click to request another page.
  const handlePageClick = (event, card) => {
    if (card === 'five') {
      const newOffset = event.selected * itemsPerPage % allTimeFav.length;
      setItemOffset(newOffset);
    } else {
      const newOffsetAll = event.selected * itemsPerPage % history.length;
      setItemOffsetAll(newOffsetAll);

    }
  };
  Date.prototype.getNextWeekDay = function (d) {
    if (d) {
      var next = this;
      next.setDate(this.getDate() - this.getDay() + 7 + d);
      return next;
    }
  }

  Date.prototype.getPrevWeekDay = function (d) {
    if (d) {
      var next = this;
      next.setDate(this.getDate() - this.getDay() - 7 + d);
      return next;
    }
  }


  const onDefaultChange = (data, btn) => {
    if (btn === 'next') {
      var now = new Date(Moment(data).subtract('days', 7).format('DD-MMM-YYYY'));
      var now2 = new Date(Moment(data).subtract('days', 7).format('DD-MMM-YYYY'));
      thisMonday = now.getNextWeekDay(1); // 0 = Sunday, 1 = Monday, ...
      NextMonday = new Date(thisMonday).getNextWeekDay(1);

      thisFriday = now2.getNextWeekDay(5); // 0 = Sunday, 5 = Friday, ...
      NextFriday = new Date(thisFriday).getNextWeekDay(5);
      setweekChange(!weekChange)
    } else {
      var now = new Date(Moment(data).format('DD-MMM-YYYY'));
      var now2 = new Date(Moment(data).format('DD-MMM-YYYY'));
      NextMonday = now.getPrevWeekDay(1); // 0 = Sunday, 1 = Monday, ...
      // NextMonday = new Date(thisMonday).getPrevWeekDay(1);
      // NextMonday = thisMonday;

      NextFriday = now2.getPrevWeekDay(5); // 0 = Sunday, 5 = Friday, ...
      // NextFriday = new Date(thisFriday).getPrevWeekDay(5);
      // NextFriday = thisFriday;
      setweekChange(!weekChange)
    }

  }


  return (

    <div style={{ backgroundColor: '#f1f1f1', width: 'auto', height: 'auto' }}>
      <Modal show={toShowFeedback} onHide={handleClose} centered >
        <ModalLayout>

          <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
            <Modal.Title style={{ color: 'white' }}>Feedback</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
              <Rating
                id={editMeal[0]?.mealType}
                name={editMeal[0]?.mealType}
                // name="simple-controlled"
                value={editRating}
                // onChange={(e) => setEditRating(e)}
                onChange={(event, newValue) => {
                  setEditRating(event);
                }}
              // onClick={(e) => setEditRating(e)}
              />

              <p className="text-uppercase"><strong>{editMeal[0]?.meal.mealName}</strong></p>
              <p className="text-uppercase"><strong>{editMeal[0]?.mealType}</strong></p>
              <div className="form-group">
                <label>Feedback</label>
                <TextareaAutosize
                  className="textArea form-control" data-name="mealIng" value={userFeedback !== undefined && userFeedback !== null ? userFeedback : ''} required onChange={onValueChange}
                  minRows={3}
                  maxRows={6}
                  placeholder="Your feedback..."
                />
              </div>
              {/* 
              <div className="form-group" >
                <label>Check Ingredients you want to <strong>REMOVE</strong> from this meal</label>
                <MealStyled>
                  {editMeal[0]?.meal.mealIng.split("\n").map((item, index) => (
                    <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{item}
                      <input value={item} type="checkbox" checked={checked.includes(item) ? 'checked' : ''} onChange={handleCheck} />
                      <span className="checkmark"></span>
                    </label>
                  ))}

                </MealStyled>
              </div> */}

              {/* <div className="form-group" >
                <label>Check Ingredients you want to be <strong>ADDED</strong> more</label>
                <MealStyled>
                  {editMeal[0]?.meal.mealIng.split("\n").map((item, index) => (
                    <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{item}
                      <input value={item} type="checkbox" checked={checked2.includes(item) ? 'checked' : ''} onChange={handleCheck2} />
                      <span className="checkmark"></span>
                    </label>
                  ))}

                </MealStyled>
              </div> */}
            </div>

          </Modal.Body>
          <Modal.Footer>
            <ButtonLayout>
              <button type="submit" style={{ marginRight: 10 }} className="btn btn-primary" onClick={() => feedbackChanged(userFeedbackID)}> Save </button>
              <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark" onClick={handleClose}> Close </Link>
            </ButtonLayout>
          </Modal.Footer>
          <br />
        </ModalLayout>
      </Modal>
      <MainLayout>
        <div className="container">
          <div className="mealToday">
            <TableLayout style={{ height: 'auto', marginBottom: '2rem' }}>
              <p style={{ fontWeight: 'bold', marginTop: '5%', fontSize: '1.8rem', textAlign: 'center' }}>Meal Today</p>
              {
                isMealToday ?
                  <div style={{ textAlign: 'center' }}>
                    <Spinner animation="grow" variant="secondary" />
                  </div>
                  :
                  <>
                    {
                      mapping.length !== 0 ?
                        <>
                          <Row xs={3} className="mealGroup">
                            {Object.keys(mapping).map(function (key) {
                              return (
                                // <MealTodayCards>
                                <Col className='mt-2'>
                                  <Card className="mealCards" style={{ height: '65vh' }}>
                                    <Card.Body className="cardBody" >
                                      <Card.Title tag="h5" className="mealTitle">{mapping[key][0].mealName}</Card.Title>
                                      <Card.Text className="mealDescription">{mapping[key][0].mealDesc}</Card.Text>
                                      <Card.Text className="mealIngredients" style={{ whiteSpace: 'pre-wrap', overflowX: 'hidden', overflowY: 'scroll', scrollbarWidth: 'normal' }}>{mapping[key][0].mealIng}</Card.Text>
                                      <Card.Text className="mealType">{mapping[key][0].mealType}</Card.Text>
                                      <Button className="mealQR" onClick={() => generateQr(mapping[key][0].id, mapping[key][0].mealType, mapping[key][0].mealName, mapping[key][0])}>Generate Qr for this Meal</Button>
                                    </Card.Body>
                                  </Card>
                                </Col>

                                // </MealTodayCards>
                              )
                            })}
                          </Row>
                        </>
                        :
                        <p style={{ textAlign: 'center' }}>No Meal {user?.role === 'Admin' ? 'Created' : 'Found'}</p>
                    }
                  </>
              }

              <Modal show={toShow} onHide={handleClose} animation={false}>
                <Modal.Header style={{ justifyContent: 'center', background: ' rgb(32, 51, 84)', color: 'white' }}>
                  <Modal.Title>Meal QR Code Generator</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                    <div className="qrCode" style={{ paddingTop: '0.5rem', }}>
                      <QRCode2 style={{
                        width: '50%', height: '50%', padding: '0.9rem 0.9rem',
                        background: 'linear-gradient(to right, black 4px, transparent 4px) 0 0, linear-gradient(to right, black 4px, transparent 4px) 0 100%, linear-gradient(to left, black 4px, transparent 4px) 100% 0,     linear-gradient(to left, black 4px, transparent 4px) 100% 100%,  linear-gradient(to bottom, black 4px, transparent 4px) 0 0, linear-gradient(to bottom, black 4px, transparent 4px) 100% 0, linear-gradient(to top, black 4px, transparent 4px) 0 100%, linear-gradient(to top, black 4px, transparent 4px) 100% 100%',
                        backgroundRepeat: 'no-repeat', backgroundSize: '20% 20%'
                      }} value={toGenerate} id="canvas" /><br />
                    </div>
                    <p style={{ marginTop: '2vh', marginBottom: '0.5vh', marginRight: 'auto', marginLeft: 'auto', padding: '0.5vw 0.5vw', borderRadius: '10px', width: '60%', background: 'rgb(32, 51, 84)', color: 'white', textTransform: 'uppercase' }}>{toUser?.firstname + " " + toUser?.lastname}</p>
                    <p style={{ marginTop: '0.5vh', marginBottom: '0.5vh', marginRight: 'auto', marginLeft: 'auto', padding: '0.5vw 0.5vw', borderRadius: '10px', width: '60%', border: '2px solid rgb(32, 51, 84)', color: 'rgb(32, 51, 84)', textTransform: 'uppercase' }}>{toOverall?.mealName}</p>
                    <p style={{ marginTop: '0.5vh', marginRight: 'auto', marginLeft: 'auto', padding: '0.5vw 0.5vw', borderRadius: '10px', width: '60%', border: '2px solid rgb(32, 51, 84)', color: 'rgb(32, 51, 84)', textTransform: 'uppercase' }}>{toOverall?.mealType}</p>

                  </div>

                </Modal.Body>
                <Modal.Footer>
                  <ButtonLayout>
                    <Button style={{ marginRight: 10 }} type="button" className="mt-30" onClick={download}>
                      Download QR
                    </Button>

                    <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark" onClick={handleClose}> Close </Link>
                  </ButtonLayout>
                </Modal.Footer>
              </Modal>
            </TableLayout>

            <WeeklyMealPlan allMeals={theAllMeals} week={[NextMonday, NextFriday]} isLoading={isLoading} handleOnClick={onDefaultChange} />
          </div>

          {(user?.role === 'Canteen' || user?.role === 'Admin') && localStorage.getItem("manage") === null ?
            <TableLayout style={{ height: 'auto', marginBottom: '2rem' }}>
              <p style={{ fontWeight: 'bold', marginTop: '5%', fontSize: '1.8rem', textAlign: 'center' }}>Monthly Meal Plan</p>
              <Calendar2 />
            </TableLayout>
            : null}

          {user?.role === 'Employee' || localStorage.getItem("manage") !== null ?
            <div className="mealToday">
              {/* RECOMMENDATION */}
              <TableLayout style={{ height: 'auto', marginBottom: '2rem', }}>

                <p style={{ fontWeight: 'bold', marginTop: '2%', fontSize: '1.8rem', textAlign: 'center' }}>Five Star Meal</p>

                {currentFiveHistory.length !== 0 ?
                  <>
                    <Row xs={3} className="mealGroup">
                      {Object.keys(currentFiveHistory).map(function (key) {
                        return (
                          // <MealTodayCards>
                          <Col className='mt-2'>
                            <Card className="mealCards">
                              <Card.Body className="cardBody" >
                                <Card.Title tag="h5" className="mealTitle">{currentFiveHistory[key][0].mealName}</Card.Title>
                                {/* <Card.Text className="mealDescription">{currentFiveHistory[key][0].mealDesc}</Card.Text> */}
                                <Card.Text className="mealType">{currentFiveHistory[key][0].mealType}</Card.Text>
                                <Card.Text className="mealType">{currentFiveHistory[key][0].date}</Card.Text>
                                <Rating
                                  id={currentFiveHistory[key][0].mealType}
                                  name={currentFiveHistory[key][0].mealType}
                                  value={currentFiveHistory[key][0].rating ? currentFiveHistory[key][0].rating : 0}
                                  onChange={ratingChanged}
                                  onClick={() => setratingID(currentFiveHistory[key][0]._id)}
                                />
                              </Card.Body>
                            </Card>
                          </Col>
                          // </MealTodayCards>
                        )
                      })}
                    </Row>
                    <ReactPaginate
                      previousLabel={"previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCount}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={3}
                      onPageChange={(e) => handlePageClick(e, 'five')}
                      containerClassName={"pagination justify-content-center"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    /></>

                  :
                  <p style={{ textAlign: 'center' }}>No Five Star Meal Found</p>
                }

              </TableLayout>

              <TableLayout>

                <p style={{ fontWeight: 'bold', marginTop: '2%', fontSize: '1.8rem', textAlign: 'center' }}>Meal History</p>
                {currentAllHistory.length !== 0 ?
                  <>
                    <Row xs={3} className="mealGroup">
                      {Object.keys(currentAllHistory).map(function (key) {

                        return (
                          // <MealTodayCards>
                          <Col className='mt-2' style={currentAllHistory[key][0].availed !== 'true' ? { display: 'none' } : {}}>
                            <Card className="mealCards">
                              <Card.Body className="cardBody" >
                                <Card.Title tag="h5" className="mealTitle">{currentAllHistory[key][0].mealName}

                                </Card.Title>
                                {/* <Card.Text className="mealDescription">{currentAllHistory[key][0].mealDesc}</Card.Text> */}
                                <Card.Text className="mealType">{currentAllHistory[key][0].mealType}</Card.Text>
                                <Card.Text className="mealType">{currentAllHistory[key][0].date}</Card.Text>
                                <Rating
                                  id={currentAllHistory[key][0].mealType}
                                  name={currentAllHistory[key][0].mealType}
                                  value={currentAllHistory[key][0].rating ? currentAllHistory[key][0].rating : 0}
                                  readOnly
                                // onChange={ ratingChanged}
                                // onClick={() => setratingID(history[key][0]._id)}
                                />
                                <ButtonLayout>
                                  <button type="button" style={{ float: 'right', width: '100%', textTransform: 'uppercase' }} className="btn btn-primary" onClick={() => [setToShowFeedback(true), setUserFeedbackID(history[key][0]._id)]}>Edit</button>
                                </ButtonLayout>
                              </Card.Body>
                            </Card>
                          </Col>

                          // }

                          // </MealTodayCards>
                        )
                      })}
                    </Row>

                    <ReactPaginate
                      previousLabel={"previous"}
                      nextLabel={"next"}
                      breakLabel={"..."}
                      pageCount={pageCountAll}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={3}
                      onPageChange={(e) => handlePageClick(e, 'all')}
                      containerClassName={"pagination justify-content-center"}
                      pageClassName={"page-item"}
                      pageLinkClassName={"page-link"}
                      previousClassName={"page-item"}
                      previousLinkClassName={"page-link"}
                      nextClassName={"page-item"}
                      nextLinkClassName={"page-link"}
                      breakClassName={"page-item"}
                      breakLinkClassName={"page-link"}
                      activeClassName={"active"}
                    />

                  </>

                  :
                  <p style={{ textAlign: 'center' }}>No Meal History Found</p>
                }

              </TableLayout>
            </div>






            : null}

          {(user?.role === 'Canteen' || user?.role === 'Admin') && localStorage.getItem("manage") === null ?
            <div className="mealTable" >
              <div id="print_budget">

                <TableLayout>
                  <div className="upperPart meal">
                    <div className="textTitle meal"><h1>Budget</h1></div>

                    {/*Buttons*/}
                    <div className="up-con">
                      <div className="createBudget" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <Link to={'/budgetMeal'} style={{ display: 'flex', color: 'white' }} className="btn btn-create" ><i className="fa fa-plus" style={{ fontSize: '25px', marginRight: '5%' }}></i> CREATE</Link>
                      </div>
                      <div className="exportBudget" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <button style={{ display: 'flex', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns.filter(function (e) {
                          return e.Header !== 'Actions';
                        }), `Budget-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                      </div>
                      <div className="printBudget" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <button style={{ display: 'flex', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_budget', '/meals')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                      </div>
                    </div>

                  </div>
                  <Line></Line>
                  <div >

                    {
                      toLoad !== 0 ?
                        <ReactTable
                          data={budgetdata}
                          filterable
                          defaultFilterMethod={filterCaseInsensitive}
                          columns={budgetcolumns}
                          minRows={1}
                          defaultPageSize={10}
                          className="-striped -highlight"
                          getTheadThProps={(columnIndex) => ({
                            style: { textAlign: "left", color: "black" }
                          })}
                          getTdProps={() => ({
                            style: { fontSize: "0.8rem", border: "none" }
                          })}
                          style={{ textAlign: 'left', border: "None" }}
                        />

                        :
                        <div style={{ margin: 'auto', marginTop: '5%', textAlign: 'center' }}><img style={{ height: '30%', width: '30%', opacity: '80%' }} src={nothing} alt="React Logo" /></div>
                    }
                  </div>
                </TableLayout>
              </div>

              <div id="print_meal" style={{ paddingBottom: '0.5rem' }}>
                <TableLayout>
                  <div className="upperPart meal">
                    <div className="textTitle meal"><h1>All Meals</h1></div>

                    <div style={{ justifyContent: 'right' }} className="up-con">
                      {/* <div className="createMeal" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <Link to={'/createMeal'} style={{ display: 'flex', color: 'white' }} className="btn btn-create" ><i className="fa fa-plus" style={{ fontSize: '25px', marginRight: '5%' }}></i> CREATE</Link>
                      </div> */}
                      <div className="exportMeal" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <button style={{ display: 'flex', background: '#1561BE', color: 'white' }} className="btn" onClick={() => exportExcel(theRow, columns.filter(function (e) {
                          return e.Header !== 'Actions';
                        }), `Meals-${new Date().toLocaleDateString()}`)}><FiSave style={{ fontSize: '25px', paddingRight: '2%' }} />EXPORT</button>
                      </div>
                      <div className="printMeal" style={{ display: 'flex', width: 'auto', padding: '2%' }}>
                        <button style={{ display: 'flex', background: '#2D508F', color: 'white' }} className="btn" onClick={() => printdiv('print_meal', '/meals')}><FiPrinter style={{ fontSize: '25px', paddingRight: '2%' }} />PRINT</button>
                      </div>
                    </div>

                  </div>
                  <Line></Line>
                  <div >
                    {
                      toLoad !== 0 ?
                        <ReactTable
                          data={data}
                          filterable
                          defaultFilterMethod={filterCaseInsensitive}
                          columns={columns}
                          minRows={1}
                          defaultPageSize={10}
                          className="-striped -highlight"
                          getTheadThProps={(columnIndex) => ({
                            style: { textAlign: "left", color: "black" }
                          })}
                          getTdProps={() => ({
                            style: { fontSize: "0.8rem", border: "none" }
                          })}
                          style={{ textAlign: 'left', border: "None" }}
                        />
                        :
                        <div style={{ margin: 'auto', marginTop: '5%', textAlign: 'center' }}><img style={{ height: '30%', width: '30%', opacity: '80%' }} src={nothing} alt="React Logo" /></div>
                    }
                  </div>
                </TableLayout>
              </div>

            </div> : null}
        </div>
      </MainLayout >
    </div >
  )
}

const MealStyled = styled.header`
/* The container2 */
.container2 {
  display: block;
  position: relative;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;


}

/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  margin-left: 0px !important;
}



/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.modal-dialog{
  transform: translateY(30px) !important;
}

`;



export default MealPage;
