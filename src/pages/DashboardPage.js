import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { MainLayout, CustomDatePickDiv } from '../styles/DashboardLayout';
import { Container } from '../styles/DashboardLayout';
import { LeftLayout } from '../styles/DashboardLayout';
import { RightLayout } from '../styles/DashboardLayout';
import { Charts } from '../styles/DashboardLayout';
import { Line, Lines } from '../styles/DashboardLayout'
import { Card, Modal, Col, ListGroup } from "react-bootstrap";
import earlybird from '../img/earlybird.png';
import noData2 from '../img/noData2.svg';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend, ArcElement
} from 'chart.js';
import { Pie, Doughnut, Bar } from 'react-chartjs-2';
import axios from 'axios';
import moment from 'moment';
// import { Carousel,  } from '@trendyol-js/react-carousel';
import Moment from 'moment';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Rating from '@mui/material/Rating';



ChartJS.register(ArcElement, Tooltip, Legend, CategoryScale,
  LinearScale,
  BarElement,
  Title);
var retrievedObject = JSON.parse(localStorage.getItem('employee'));


function DashboardPage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [totalWorkersPresent, setTotalWorkersPresent] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [earlyBird, setEarlyBird] = React.useState([]);
  const [MealNames, setMealNames] = React.useState([]);
  const [NoOfServes, setNoOfServes] = React.useState([]);
  const [NoOfAvailableServes, setNoOfAvailableServes] = React.useState([]);
  const [MostServed, setMostServed] = React.useState([]);
  const [noOfPeopleServed, setNoOfPeopleServed] = React.useState(0);
  const [breakfast, setbreakfast] = React.useState(0);
  const [lunch, setlunch] = React.useState(0);
  const [snack, setSnack] = React.useState(0);
  const [dinner, setdinner] = React.useState(0);
  const [carouseldata, setcarousel] = useState([]);
  const [history, sethistory] = useState([]);
  const [morningFromIO, setmorningFromIO] = React.useState('');
  const [morningToIO, setmorningToIO] = React.useState('');
  const [afternoonFromIO, setafternoonFromIO] = React.useState('');
  const [afternoonToIO, setafternoonToIO] = React.useState('');
  const [breakfastEST, setbreakfastEST] = React.useState(0);
  const [lunchEST, setlunchEST] = React.useState(0);
  const [dinnerEST, setdinnerEST] = React.useState(0);
  const [snackEST, setsnackEST] = React.useState(0);

  const selectDateHandler = (d) => {
    setDate(d)
  }

  const setDateRange = (d) => {
    setEndDate(d);
  }

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon className="date-icon" style={{ height: '16px' }} icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });

  useEffect(() => {
    let myEle = document.getElementById("date-con");
    if (myEle) {
      window.addEventListener("scroll", () => setTimeout(function () {
        if (window.pageYOffset !== 0 && myEle && window.location.pathname === '/dashboard' || window.location.pathname === '/recommendation') {
          document.getElementById('date-con').style.marginTop = "-10%"
        } else if (window.pageYOffset === 0 && myEle && window.location.pathname === '/dashboard' || window.location.pathname === '/recommendation') {
          document.getElementById('date-con').style.marginTop = "unset"

        }
      }));
    }

  }, [])

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      });

    let theDate = new Date;

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
      params: {
        startDate: moment(startDate).format("M/DD/YYYY"),
        endDate: moment(endDate).format("M/DD/YYYY")
      }
    })
      .then(res => {
        let theRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(startDate).subtract(1, "days").format("M/DD/YYYY"))) &&
        moment(new Date(el.dateRecorded)).isBefore(new Date(moment(endDate).add(1, "days").format("M/DD/YYYY"))));
        const ids = theRes.map(o => o.user._id);
        console.log('theRes',ids, theRes, theRes.length,
        moment(startDate).subtract(1, "days").format("M/DD/YYYY"),
        moment(endDate).add(1, "days").format("M/DD/YYYY"),
        theRes)
        setEarlyBird(theRes[theRes.length - 1]);
        let presentToday = theRes.filter(({ user }, index) => !ids.includes(user._id, index + 1)).length;
        setTotalWorkersPresent(presentToday);
        console.log('ppppppppppp', presentToday)

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const ids = res.data.map(o => o.user._id);
        setNoOfPeopleServed(res.data.filter(({ user }, index) => !ids.includes(user._id, index + 1)).length);

        setbreakfast(res.data.filter(x => x.mealType === 'breakfast').length);
        setlunch(res.data.filter(x => x.mealType === 'lunch').length);
        setdinner(res.data.filter(x => x.mealType === 'dinner').length);
        setSnack(res.data.filter(x => x.mealType === 'snack').length);

        let mealIds = res.data.map(o => o.meal._id).filter(function (item, index, inputArray) {
          return inputArray.indexOf(item) == index;
        });

        let breaklunchdinnersnack = res.data.filter((v, i, a) => a.findIndex(v2 => (v2.meal._id === v.meal._id)) === i);

        setbreakfastEST(breaklunchdinnersnack.filter(x => x.mealType === 'breakfast').reduce(function (acc, obj) { return acc + obj.meal.mealServedEst; }, 0))
        setlunchEST(breaklunchdinnersnack.filter(x => x.mealType === 'lunch').reduce(function (acc, obj) { return acc + obj.meal.mealServedEst; }, 0))
        setdinnerEST(breaklunchdinnersnack.filter(x => x.mealType === 'dinner').reduce(function (acc, obj) { return acc + obj.meal.mealServedEst; }, 0))
        setsnackEST(breaklunchdinnersnack.filter(x => x.mealType === 'snack').reduce(function (acc, obj) { return acc + obj.meal.mealServedEst; }, 0))
      })
      .catch(error => {
        console.log(error);
      })

    // const startDate = new Date;
    // const endDate = new Date;
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealToday2', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const mealName = res.data.map(element => element.mealName);
        const mealServed = res.data.map(element => element.mealServed);
        const mealServedEst = res.data.map(element => element.mealServedEst);
        var maxVotes = Math.max(...res.data.map(e => e.mealServed));
        var obj = res.data.find(game => game.mealServed === maxVotes);
        setMealNames(mealName);
        setNoOfServes(mealServed);
        setNoOfAvailableServes(mealServedEst);
        setMostServed(obj);

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        setcarousel(res.data);

        const mappingHistory = res.data.map(year => [{
          mealName: year.meal.mealName,
          _id: year._id,
          rating: year?.rating,
          date: Moment(year.createdAt).format('MMMM DD YYYY HH:mm:ss'),
          mealDesc: year.meal.mealDesc,
          mealIng: year.meal.mealIng,
          mealType: year.meal.mealType,
          feedback: year.feedback,
          user: year.user.firstname + " " + year.user.lastname
        }]);

        sethistory(mappingHistory)
      })
      .catch(error => {
        console.log(error);
      });

    return () => {
      setMealNames([]);
      setNoOfServes([]);
      setMostServed([]);
      setNoOfPeopleServed([]);
      setTotalEmployees([]);
      setEarlyBird([]);
      setTotalWorkersPresent(0);
      sethistory([]);
      setcarousel([])

    }

  }, [startDate, endDate]);

  const data = {
    labels: ['Total Workers', 'Present', 'Absent'],
    datasets: [
      {
        label: '# of Votes',
        data: [totalEmployees, totalWorkersPresent, totalEmployees - totalWorkersPresent],
        backgroundColor: [
          '#3b127d',
          '#1bc041',
          '#e91b2f',
        ],
        borderColor: [
          '#3b127d',
          '#1bc041',
          '#e91b2f',

        ],
        borderWidth: 1,
        // hoverOffset: 4
      },
    ],
  };

  const data2 = {
    labels: MealNames,
    datasets: [
      {
        label: '# of Votes',
        data: NoOfServes,
        backgroundColor: [
          '#FFBF00',
          '#6495ED',
          '#CCCCFF',
          '#DE3163',
          '#DFFF00',
          '#40E0D0',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          '#FFBF00',
          '#6495ED',
          '#CCCCFF',
          '#DE3163',
          '#DFFF00',
          '#40E0D0',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderWidth: 1,
      },
    ],
  };

  const lunchBreakDinner = {
    labels: ['BreakFast', 'Lunch', 'Dinner', 'Snack'],
    datasets: [
      {
        label: '# of Votes',
        data: [breakfast, lunch, dinner, snack],
        backgroundColor: [
          '#D83864',
          '#53B9D0',
          '#3C978A',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          '#D83864',
          '#53B9D0',
          '#3C978A',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderWidth: 1,
      },
    ],
  };

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Meal Served and Estimation',
      },
    },
  };

  //   const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  const barData = {
    labels: MealNames,
    datasets: [
      {
        label: 'Meal Served',
        data: NoOfServes,
        backgroundColor: '#376A99',
      },
      {
        label: 'Meal Serve Estimation',
        data: NoOfAvailableServes,
        backgroundColor: '#F27B2B',
      },
    ],
  };

  return (
    <DashboardStyled>
      <MainLayout>
        <div className="dashBoardText" style={{ display: 'flex', alignItems: 'center', marginBottom: '1%' }}>
          <div style={{ flex: '40%' }}>
            <h2 style={{ paddingTop: '2rem', marginBottom: '0', }}>Dashboard</h2>
          </div>

          <div className="date-con" id="date-con">
            <>
            <div id="btn" class="flex-container2" style={{ paddingLeft: '3%'}}>
                    <div><label className="start-date wordings" style={{ padding: '0'}}>Start Date:  </label> </div>
                    <div className="start-date" style={{ width: '11vw' }}> <DatePicker
                      className=""
                      dateFormat="yyyy/MM/dd"
                      selected={startDate}
                      onChange={selectDateHandler}
                      maxDate={endDate}
                      todayButton={"Today"}
                      customInput={<CustomInput />}
                    /></div>
                    <div><label className="end-date wordings" style={{ padding: '0', paddingTop: '0' }}>End Date:  </label> </div>
                    <div className="start-date" style={{ width: '8vw' }}>
                      <DatePicker
                        className="form-control"
                        dateFormat="yyyy/MM/dd"
                        selected={endDate}
                        onChange={setDateRange}
                        minDate={startDate}
                        todayButton={"Today"}
                        customInput={<CustomInput />}
                      />

                    </div>

                  </div>

            </>
          </div>

        </div>
        <Line style={{ marginBottom: '1rem' }}></Line>



        {/* Dashboard */}
        <div className="dash-con" >
          {/*Upper
          <div className="upper">
          </div>
          */}

          {/* Left Container*/}

          <div className="left-con">
            {/* <div className="admin-card">
                  <div className="text">
                    <h4>Welcome</h4>
                    <h1>Admin</h1>
                    <Lines></Lines>
                    <h5 className="text2">There are 32 present employees and 0 absent, for the meal today there are 4 and a total of 500 feedback</h5>
                  </div>
                </div> */}

            <div className="attendance-card" style={{ zIndex: 700 }} >
              {/*left*/}
              <div className="left">
                <div className="text">
                  <h1 style={{cursor: 'pointer'}} onClick={() => window.location = "/attendance"}>Attendance Monitoring Board</h1>
                  <div className="attendance-con">

                    <Card className="text-center card" style={{cursor: 'pointer'}} onClick={() => window.location = "/attendance"}>
                      <Card.Img variant="top" src={earlybird} />
                      <Card.Body>
                        <Card.Title >Early Bird</Card.Title>
                        <Card.Text style={{textTransform: 'capitalize'}}>
                          {earlyBird?.user !== undefined ? earlyBird?.user?.firstname : 'Seems like no one'} {earlyBird?.user !== undefined ? earlyBird?.user?.lastname : 'is present'}
                        </Card.Text>
                        <Card.Text>
                          {moment(earlyBird?.createdAt).format('lll')}
                        </Card.Text>
                      </Card.Body>
                    </Card>

                    <Card className="text-center card" style={{cursor: 'pointer'}} onClick={() => window.location = "/attendance"}>
                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Total Workers</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{totalEmployees}</Card.Title>
                      </Card.Body>

                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Present</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{totalWorkersPresent}</Card.Title>
                      </Card.Body>


                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Absent</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{totalEmployees - totalWorkersPresent}</Card.Title>
                      </Card.Body>
                    </Card>
                  </div>
                </div>
              </div>

              {/*right*/}
              <div className="right" style={{ zIndex: 999 }} onClick={() => console.log('noneee')}>
                <div className="chart-con">
                  <Pie
                    width={50}
                    height={50}
                    options={{ maintainAspectRatio: false }} data={data} />
                </div>
              </div>
            </div>

            <div className="meal-card" >
              {/*left*/}
              <div className="left">
                <div className="text">
                  <h1 style={{cursor: 'pointer'}} onClick={() => window.location = "/mealrecords"}>Meal Consumption Board</h1>
                  <div className="meal-con">

                    <Card style={{ marginRight: '1%',cursor: 'pointer' }} className="text-center card" onClick={() => window.location = "/mealrecords"}>
                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Breakfast Served</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{breakfast}</Card.Title>
                      </Card.Body>

                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Lunch Served</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{lunch}</Card.Title>
                      </Card.Body>


                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Dinner Served</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{dinner}</Card.Title>
                      </Card.Body>

                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Snack Served</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{snack}</Card.Title>
                      </Card.Body>
                    </Card>

                    <Card className="text-center card" style={{cursor: 'pointer'}} onClick={() => window.location = "/mealrecords"}>
                      <Card.Header style={{ background: '#0072A7', color: 'white', fontSize: '99%' }}>Breakfast to Serve</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{breakfastEST - breakfast}</Card.Title>
                      </Card.Body>

                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Lunch to Serve</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{lunchEST - lunch}</Card.Title>
                      </Card.Body>


                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Dinner to Serve</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{dinnerEST - dinner}</Card.Title>
                      </Card.Body>

                      <Card.Header style={{ background: '#0072A7', color: 'white' }}>Snack to Serve</Card.Header>
                      <Card.Body style={{ background: '#EBE9EA' }}>
                        <Card.Title>{snackEST - snack}</Card.Title>
                      </Card.Body>
                    </Card>
                  </div>
                </div>
              </div>

              {/*right*/}
              <div className="right">
                <div className="chart-con">
                  {noOfPeopleServed !== 0 ?
                    <Doughnut width={60}
                      height={60}
                      options={{ maintainAspectRatio: false }} data={lunchBreakDinner} />
                    : <p style={{ margin: 'auto', textAlign: 'center', paddingTop: '25%' }}><img style={{ all: 'unset', height: '150px !important', width: '290px', marginTop: '-15%' }} src={noData2} alt="noDataFound" /><br />No Data Found</p>}
                </div>
              </div>
            </div>

            <div className="bar-card">
              <h1 style={{cursor: 'pointer'}} onClick={() => window.location = "/mealrecords"}>Meal Serve Estimation Board</h1>
              <div className="bar-chart">
                <Bar options={options} data={barData} />
              </div>
            </div>

          </div>



          {/* Right Container*/}
          <div className="right-con">
            {/* <div className="date-card">
              <div className="date-con">
                        <label className="start-date">Start Date:
                          <div>
                        <DatePicker
                            className="form-control"
                            dateFormat="yyyy/MM/dd"    
                            selected={startDate}
                            onChange={selectDateHandler} 
                            // minDate={today}
                            todayButton={"Today"}
                            customInput={<CustomInput />}
                          />
                          </div>
                        </label>  
                        
                        <label className="end-date">End Date:
                          <div>
                          <DatePicker
                            className="form-control"
                            dateFormat="yyyy/MM/dd"
                            selected={endDate}
                            onChange={setDateRange} 
                            minDate={startDate} 
                            todayButton={"Today"}
                            customInput={<CustomInput/>}
                            />
                            </div>
                          </label>                
                        
                      </div>
                      
              </div>   */}

            <div className="feedback-card">
              <h1 style={{ color: 'white' }}>Feedbacks</h1>
              <Lines></Lines>
              {Object.keys(history).map(function (key) {
                return (
                  <div key={history[key][0]._id} className="feed-card">
                    <h2>{history[key][0].mealName}</h2>
                    <Line></Line>
                    <h3 style={{ textTransform: 'capitalize', }}>{history[key][0].user}</h3>
                    <h2 style={{ textTransform: 'capitalize', }}>{history[key][0].mealType}</h2>
                    <h3>{Moment(history[key][0].date).format('ddd, MMM DD YYYY')}</h3>
                    <h5>{history[key][0].feedback}</h5>
                    <Rating
                      value={history[key][0].rating}
                      readOnly
                    />
                  </div>
                )
              })}
            </div>
          </div>


          {/*Lower*/}
          <div className="lower">

          </div>

        </div>

      </MainLayout>

    </DashboardStyled>

  )
}
const DashboardStyled = styled.header`
    background-color: #f1f1f1;
    margin-top: 0;
    padding-top: 0;
    
 
    .dashBoardText{
        padding-top: 1rem;
        margin-bottom: 2%;
    }
    .dashBoardText h2{
        font-size: 5vh;
    }

        .workers-card{
            height: 30vh;
            width: 22vw;
            background-color: red;
            border-radius: 20px;
        }

        .chart{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius: 50px 0px 0px 50px;
        }

        .chart2{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 50px 50px 0px;
        }

        .chart3{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 0px 0px 0px;
        }

        @media (max-width: 450px){
            .chart{
                padding: 1rem;
            }
        }
`;

export default DashboardPage;
