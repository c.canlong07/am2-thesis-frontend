import React, {
  useEffect,
  useState
} from "react";
import styled from 'styled-components';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { MainLayout } from '../styles/DashboardLayout';
import { Container } from '../styles/DashboardLayout';
import { LeftLayout } from '../styles/DashboardLayout';
import { RightLayout } from '../styles/DashboardLayout';
import { Charts } from '../styles/DashboardLayout';
import { Line } from '../styles/DashboardLayout'
import { Card, Table, ListGroup } from "react-bootstrap";
import earlybird from '../img/earlybird.png';
import hotmeal from '../img/hotmeal.png';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend, ArcElement
} from 'chart.js';
import { Pie, Doughnut, Bar } from 'react-chartjs-2';
import axios from 'axios';
import moment from 'moment';
// import { Carousel,  } from '@trendyol-js/react-carousel';
import Moment from 'moment';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { QRcode, CustomDatePickDiv } from '../styles/LayoutAttendance';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Rating from '@mui/material/Rating';
// Import React Table
import ReactTable from "react-table-6";
import "react-table-6/react-table.css";
import records from '../img/records.svg';
import Swal from 'sweetalert2';
import Select from "react-select"; // v1
import noDataFound from '../img/no-data-found.png';

import BestGreatOkWorst from '../dataVisualization/BestGreatOkWorst';
import WeeklyMeals from '../dataVisualization/WeeklyMeals';
import LastWeekInRecommendation from '../dataVisualization/LastWeekInRecommendation';
// import "react-select/dist/react-select.css";
ChartJS.register(ArcElement, Tooltip, Legend, CategoryScale,
  LinearScale,
  BarElement,
  Title);
var retrievedObject = JSON.parse(localStorage.getItem('employee'));

export const filterCaseInsensitive = (filter, row) => {
  const id = filter.pivotId || filter.id;
  const content = row[id];
  if (typeof content !== 'undefined') {
    // filter by text in the table or if it's a object, filter by key
    if (typeof content === 'object' && content !== null && content.key) {
      return String(content.key).toLowerCase().includes(filter.value.toLowerCase());
    } else {
      return String(content).toLowerCase().includes(filter.value.toLowerCase());
    }
  }

  return true;
};

function DashboardPage() {
  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(1, "month")._d);
  const [endDate, setEndDate] = React.useState(new Date);
  const [totalWorkersPresent, setTotalWorkersPresent] = React.useState(0);
  const [totalEmployees, setTotalEmployees] = React.useState(0);
  const [earlyBird, setEarlyBird] = React.useState([]);
  const [MealNames, setMealNames] = React.useState([]);
  const [NoOfServes, setNoOfServes] = React.useState([]);
  const [NoOfAvailableServes, setNoOfAvailableServes] = React.useState([]);
  const [MostServed, setMostServed] = React.useState([]);
  const [noOfPeopleServed, setNoOfPeopleServed] = React.useState(0);
  const [breakfast, setbreakfast] = React.useState(0);
  const [lunch, setlunch] = React.useState(0);
  const [snack, setsnack] = React.useState(0);
  const [dinner, setdinner] = React.useState(0);
  const [carouseldata, setcarousel] = useState([]);
  const [history, sethistory] = useState([]);
  const [allTimeFav, setallTimeFav] = React.useState([]);
  const [columns, setColumns] = React.useState([]);
  const [dataTable, setData] = React.useState([]);
  const [toLoad, setToLoad] = React.useState(0);
  const [theRow, settheRow] = React.useState([]);
  const [select2, setselect2] = React.useState(undefined);
  const [filtered, setfiltered] = React.useState([]);
  const [overallRatings, setoverallRatings] = React.useState([]);
  const [allMeals, setallMeals] = React.useState([]);
  const [perUser, setperUser] = React.useState([]);
  const [theWeeks, settheWeeks] = React.useState([]);
  const [theMeals, settheMeals] = React.useState([]);
  const [modalMeals, setmodalMeals] = React.useState([]);
  const [removeDuplicates, setremoveDuplicates] = React.useState([]);


  const selectDateHandler = (d) => {
    setDate(d)
  }

  const onFilteredChangeCustom = (value, accessor) => {
    let filtered2 = filtered;
    let insertNewFilter = 1;
    if (filtered2.length) {
      filtered2.forEach((filter, i) => {
        if (filter["id"] === accessor) {
          if (value === "" || !value.length) filtered2.splice(i, 1);
          else filter["value"] = value;

          insertNewFilter = 0;
        }
      });
    }

    if (insertNewFilter) {
      filtered2.push({ id: accessor, value: value });
    }

    setfiltered(filtered2);
  };

  const setDateRange = (d) => {
    setEndDate(d);
  }

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon className="date-icon" icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });

  useEffect(() => {
    let myEle = document.getElementById("date-con");
    if (myEle) {
      window.addEventListener("scroll", () => setTimeout(function () {
        if (window.pageYOffset !== 0 && myEle && window.location.pathname === '/recommendation') {
          document.getElementById('date-con').style.marginTop = "-10%"
        } else if (window.pageYOffset === 0 && myEle && window.location.pathname === '/recommendation') {
          document.getElementById('date-con').style.marginTop = "unset"

        }
      }));
    }

  }, [])

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        setmodalMeals(res.data);
        setToLoad(res.data.length);
        const ids = res.data.map(o => o.user?._id)
        setNoOfPeopleServed(res.data.filter(({ user }, index) => !ids.includes(user?._id, index + 1)).length);
        if (res.data.length > 0) {
          setColumns([
            {
              id: 'user',
              Header: 'Name',
              headerClassName: 'headerTable',
              accessor: d => d.user?.firstname
                + " " + d.user?.lastname,

            },
            {
              id: 'mealName',
              Header: 'Meal Name',
              headerClassName: 'headerTable',
              accessor: d => d.meal?.mealName
            },
            {
              id: 'mealIng',
              Header: 'Meal Ingredients',
              headerClassName: 'headerTable',
              accessor: d => d.meal?.mealIng
            }, {
              id: 'mealType',
              Header: 'Meal Type',
              headerClassName: 'headerTable',
              accessor: d => d.meal?.mealType,
            }, {
              id: 'rating',
              Header: 'Meal Rating',
              headerClassName: 'headerTable',
              // accessor: d => d?.rating
              accessor: data =>
                <Rating
                  value={data.rating ? data.rating : 0}
                  readOnly
                />
            }, {
              id: 'feedback',
              Header: 'Meal Feedback',
              headerClassName: 'headerTable',
              accessor: d => d?.feedback
            }]);
          setData(...[res.data]);

          const mappingHistory = res.data.map(year => [{
            user: year.user.firstname + ' ' + year.user.lastname,
            department: year.user.department,
            mealName: year.meal.mealName,
            mealDesc: year.meal.mealDesc,
            mealCategory: year.meal.mealCategory,
            mealType: year.meal.mealType,
            rating: year.rating,

          }]);

          settheRow([].concat.apply([], mappingHistory));
        }

      })
      .catch(error => {
        console.log(error);
      })
  }, [startDate, endDate]);

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const ids = res.data.map(o => o.meal?._id)
      })
      .catch(error => {
        console.log(error);
      })
  });

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
      .then(res => {
        setTotalEmployees(res.data.length);
      })
      .catch(error => {
        console.log(error);
      });

    let theDate = new Date;

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const ids = res.data.map(o => o.user._id);
        setEarlyBird(res.data[0]);
        let presentToday = res.data.filter(({ user }, index) => !ids.includes(user._id, index + 1)).length;
        setTotalWorkersPresent(presentToday);
      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const ids = res.data.map(o => o.user._id);
        setNoOfPeopleServed(res.data.filter(({ user }, index) => !ids.includes(user._id, index + 1)).length);

        setbreakfast(res.data.filter(x => x.mealType === 'breakfast').length);
        setlunch(res.data.filter(x => x.mealType === 'lunch').length);
        setdinner(res.data.filter(x => x.mealType === 'dinner').length);
        setsnack(res.data.filter(x => x.mealType === 'snack').length);
      })
      .catch(error => {
        console.log(error);
      })



    // const startDate = new Date;
    // const endDate = new Date;
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealToday', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        const mealName = res.data.map(element => element.mealName);
        const mealServed = res.data.map(element => element.mealServed);
        const mealServedEst = res.data.map(element => element.mealServedEst);
        var maxVotes = Math.max(...res.data.map(e => e.mealServed));
        var obj = res.data.find(game => game.mealServed === maxVotes);
        setMealNames(mealName);
        setNoOfServes(mealServed);
        setNoOfAvailableServes(mealServedEst);
        setMostServed(obj);

      })
      .catch(error => {
        console.log(error);
      })
    //weekly
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealWeekly', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: moment(moment(startDate, "MM/DD/yyyy").add(7, 'days')._d).format('MM/DD/yyyy')
      }
    })
      .then(res => {
        let theWeeklyData = res.data.map(val => ({ ...val, mealDate: moment(val.mealDate).format('MM/DD/yyyy') }));
        let weeklyData = {};
        for (let element of theWeeklyData) {
          if (weeklyData[element.mealDate]) {
            weeklyData[element.mealDate].sum = weeklyData[element.mealDate].sum + (element.rating !== undefined ? element.rating : 0);
            weeklyData[element.mealDate].n++;
            weeklyData[element.mealDate].mealDate = element.mealDate;
            weeklyData[element.mealDate].mealName = element.mealName;
            weeklyData[element.mealDate].mealDesc = element.mealDesc;
            weeklyData[element.mealDate].mealCategory = element.mealCategory;

          } else {
            weeklyData[element.mealDate] = {
              sum: element.rating ? element.rating : 0,
              n: 1,
              mealDate: element.mealDate,
              mealName: element.mealName,
              mealDesc: element.mealDesc,
              mealCategory: element.mealCategory,
            };
          }
        }

        let weeklyOverall = [];

        for (let element of Object.keys(weeklyData)) {
          weeklyOverall.push({
            name: element,
            users: weeklyData[element].n,
            rating: weeklyData[element].sum / weeklyData[element].n,
            mealDate: weeklyData[element].mealDate,
            mealName: weeklyData[element].mealName,
            mealDesc: weeklyData[element].mealDesc,
            mealCategory: weeklyData[element].mealCategory,

          });
        }

        settheWeeks(weeklyOverall);

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        settheMeals(res.data)

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
      params: {
        startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
        endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      }
    })
      .then(res => {
        setcarousel(res.data);
        setallMeals(res.data);

        const mappingHistory = res.data.map(year => [{
          mealName: year.meal.mealName,
          _id: year._id,
          rating: year?.rating,
          date: Moment(year.meal.mealDate).format('MM/DD/YYYY'),
          mealDesc: year.meal.mealDesc,
          mealCategory: year.meal.mealCategory,
          mealIng: year.meal.mealIng,
          mealType: year.meal.mealType,
          feedback: year.feedback,
          user: year.user.firstname + " " + year.user.lastname
        }]);


        sethistory(mappingHistory);
        let theArray = mappingHistory.filter(function (pilot) {
          return pilot[0].rating === 5;
        });


        let sumData = {};
        let ratingData = res.data;
        var newArray = res.data.filter(function (el) {
          return el.mealType === "breakfast"  // Changed this so a home would match
        });

        const mappingMeal = res.data.map(year => [{
          mealName: year.meal.mealName,
          mealDesc: year.meal.mealDesc,
          mealCategory: year.meal.mealCategory,
          _id: year.user._id,
          mealType: year.mealType,
          rating: year.rating ? year.rating : 0,
          feedback: year.feedback ? year.feedback : 'none',
          feedback2: year.feedback2 ? year.feedback2 : 'none',
          firstname: year.user.firstname,
          lastname: year.user.lastname,
        }]);



        let sumData2 = {};
        for (let element of ratingData) {
          if (sumData2[element.user._id]) {
            sumData2[element.user._id].sum = sumData2[element.user._id].sum + (element.rating !== undefined ? element.rating : 0);
            sumData2[element.user._id].n++;
            sumData2[element.user._id].department = element.user.department;
            sumData2[element.user._id].firstname = element.user.firstname;
            sumData2[element.user._id].lastname = element.user.lastname;
            sumData2[element.user._id].feed = element.feedback;
            sumData2[element.user._id].feed2 = element.feedback2;
            sumData2[element.user._id]._id = element._id;
          } else {
            sumData2[element.user._id] = {
              sum: element.rating ? element.rating : 0,
              n: 1,
              department: element.user.department,
              firstname: element.user.firstname,
              lastname: element.user.lastname,
              feed: element.feedback,
              feed2: element.feedback2,
              _id: element._id
            };
          }
        }

        let averageData2 = [];

        for (let element of Object.keys(sumData2)) {
          averageData2.push({
            name: element,
            users: sumData2[element].n,
            rating: sumData2[element].sum / sumData2[element].n,
            department: sumData2[element].department,
            firstname: sumData2[element].firstname,
            lastname: sumData2[element].lastname,
            feed: sumData2[element].feed,
            feed2: sumData2[element].feed2,
            _id: sumData2[element]._id,
          });
        }


        setperUser(averageData2);

        const out = mappingMeal.flat(1).reduce((a, v) => {

          if (a[v.mealName]) {
            a[v.mealName].allfeedback = [a[v.mealName].feedback ? a[v.mealName].feedback : 'none', v.feedback ? v.feedback : 'none'].join(',');
            a[v.mealName].allUser = [a[v.mealName].firstname + " " + a[v.mealName].lastname, v.firstname + " " + v.lastname].join(',');
            a[v.mealName].addedRating = a[v.mealName].rating + v.rating;
            a[v.mealName].ratings = [a[v.mealName].rating ? a[v.mealName].rating : '0', v.rating ? v.rating : '0'].join(',');

            // a[v.mealName].count= count++;
          } else {
            a[v.mealName] = v
          }
          return a
        }, {})

        for (let element of ratingData) {
          if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
            sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
            sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
            sumData[element.meal.mealName + '+' + element.meal.mealType].type = element.meal.mealType;
            sumData[element.meal.mealName + '+' + element.meal.mealType].budget = element.meal.mealTotalPrice;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealID = element.meal._id;
            sumData[element.meal.mealName + '+' + element.meal.mealType].ingToAdd = element.ingToAdd;
            sumData[element.meal.mealName + '+' + element.meal.mealType].ingToRemove = element.ingToRemove;
            sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
            sumData[element.meal.mealName + '+' + element.meal.mealType].feed2 = element.feedback2;
            sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
          } else {
            sumData[element.meal.mealName + '+' + element.meal.mealType] = {
              sum: element.rating ? element.rating : 0,
              n: 1,
              type: element.meal.mealType,
              budget: element.meal.mealTotalPrice,
              mealID: element.meal._id,
              date: element.meal.mealDate,
              day: element.meal.mealDate,
              feed: element.feedback,
              feed2: element.feedback2,
              _id: element._id,
              ingToAdd: element.ingToAdd,
              ingToRemove: element.ingToRemove,
            };
          }
        }

        let averageData = [];

        for (let element of Object.keys(sumData)) {
          averageData.push({
            name: element,
            users: sumData[element].n,
            rating: sumData[element].sum / sumData[element].n,
            type: sumData[element].type,
            budget: sumData[element].budget,
            mealID: sumData[element].mealID,
            date: moment(sumData[element].date).format('MM/DD/YYYY'),
            day: moment(sumData[element].date).format('dddd'),
            feed: sumData[element].feed,
            feed2: sumData[element].feed2,
            _id: sumData[element]._id,
            ingToAdd: sumData[element].ingToAdd,
            ingToRemove: sumData[element].ingToRemove,
          });
        }


        let unique = [];
        averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)).map(x => unique.filter(a => a.name.split('+')[0] == x.name.split('+')[0] && a.day === x.day).length > 0 ? null : unique.push(x));

        setremoveDuplicates(unique);
        setallTimeFav(averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)));

      })
      .catch(error => {
        console.log(error);
      });

    return () => {
      setMealNames([]);
      setNoOfServes([]);
      setMostServed([]);
      setNoOfPeopleServed([]);
      setTotalEmployees([]);
      setEarlyBird([]);
      setTotalWorkersPresent(0);
      sethistory([]);
      setcarousel([]);
      setallTimeFav([]);
      settheWeeks([])

    }

  }, [startDate, endDate]);

  const columnsAllTime = [
    {
      Header: "Name",
      accessor: "name",
      Cell: (row) => {
        const removeType = row.original.name
        if (!removeType) return "";
        return removeType.split('+')[0];
      }
    },
    {
      Header: "Users",
      id: "users",
      accessor: "users"
    },
    {
      Header: "Meal Type",
      accessor: "type"
    },
    {
      Header: "Rating in #",
      id: "rating#",
      accessor: "rating",
      Cell: (row) => {
        return row.original.rating.toFixed(2);
      }
    },
    {
      Header: "Rating",
      id: "rating",
      filterable: false,
      accessor: data =>
        <Rating
          value={data.rating}
          readOnly
        />
    },
  ];

  const columnsPerUser = [
    {
      Header: "Name",
      accessor: "name",
      Cell: (row) => {
        // const removeType = row.original.name
        // if (!removeType) return "";
        return row.original.firstname.replace(/\b(\w)/g, s => s.toUpperCase()) + ' ' + row.original.lastname.replace(/\b(\w)/g, s => s.toUpperCase());
      }
    },
    {
      Header: "Meal Taken",
      id: "users",
      accessor: "users"
    },
    {
      Header: "Department",
      accessor: "department"
    },
    // {
    //   Header: "Rating in #",
    //   id: "rating#",
    //   accessor: "rating"
    // },
    // {
    //   Header: "Rating",
    //   id: "rating",
    //   filterable: false,
    //   accessor: data =>
    //         <Rating
    //                         value={data.rating}
    //                         readOnly
    //                       />
    // },
  ];

  const overalls = [
    {
      Header: "Name",
      accessor: "name"
    },
    {
      Header: "Users",
      id: "users",
      accessor: "users"
    },
    {
      Header: "Meal Type",
      accessor: "type"
    },
    {
      Header: "rating",
      accessor: "rating"
    },
  ];

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }

  return (
    <DashboardStyled>
      <MainLayout>
        <div className="dashBoardText" style={{ display: 'flex', alignItems: 'center', marginBottom: '1%' }}>
          <div style={{ flex: '40%' }}>
            <h2 style={{ paddingTop: '2rem', marginBottom: '0', }}>Recommendation</h2>
          </div>

          <div className="date-con" id="date-con">
            <>
              <div id="btn" class="flex-container2" style={{ paddingLeft: '3%' }}>
                <div><label className="start-date wordings" style={{ padding: '0' }}>Start Date:  </label> </div>
                <div className="start-date" style={{ width: '11vw' }}> <DatePicker
                  className=""
                  dateFormat="yyyy/MM/dd"
                  selected={startDate}
                  onChange={selectDateHandler}
                  maxDate={endDate}
                  todayButton={"Today"}
                  customInput={<CustomInput />}
                /></div>
                <div><label className="end-date wordings" style={{ padding: '0', paddingTop: '0' }}>End Date:  </label> </div>
                <div className="start-date" style={{ width: '8vw' }}>
                  <DatePicker
                    className="form-control"
                    dateFormat="yyyy/MM/dd"
                    selected={endDate}
                    onChange={setDateRange}
                    minDate={startDate}
                    todayButton={"Today"}
                    customInput={<CustomInput />}
                  />

                </div>

              </div>
            </>
          </div>






        </div>
        <Line style={{ marginBottom: '1rem' }}></Line>



        <div className="wholePage" style={{ marginRight: '4rem', display: 'flex', flexDirection: 'column' }}>

          <div className="container" style={{ padding: '0' }}>
            {/* Series of cards */}
            {/* <div className="totals" style={{alignItems: 'center', justifyContent: 'center'}}>
       
            <Card className="totalRec cardtotal">
              <Card.Text >
                 <h1>00,000</h1>
              </Card.Text>
              <Card.Text>
                <p>Total Feedbacks</p>
              </Card.Text>
            </Card>

            <Card className="totalRec cardtotal" style={{background: 'linear-gradient(90deg, rgba(0,85,255,1) 0%, rgba(0,43,128,1) 100%)', height: '24vh'}}>
              <Card.Title style={{color: 'white'}}>
                 <h2 style={{fontSize: 'auto', marginBottom: '0', textTransform:'uppercase'}}>Adobong Manok</h2>
              </Card.Title>
              <Card.Text style={{margin: '0', color: 'white'}}>
                <p>Most Consumed Meal</p>
              </Card.Text>
            
            </Card>

            <Card className="totalRec cardtotal">
              <Card.Title>
                 <h1>00,000</h1>
              </Card.Title>
              <Card.Text>
                <p>Total Consumed Meal</p>
              </Card.Text>
            </Card>

            <Card className="totalRec cardtotal">
              <Card.Title>
                 <h1>00,000</h1>
              </Card.Title>
              <Card.Text>
                <p>Total Employees</p>
              </Card.Text>
            </Card>

      </div> */}



            <LastWeekInRecommendation key="lastweekInrecomendation" allTimeFav={removeDuplicates} allMeals={allMeals} modalMeals={modalMeals} />
            <WeeklyMeals allTimeFav={allTimeFav} allMeals={allMeals} modalMeals={modalMeals} />
            <BestGreatOkWorst allTimeFav={allTimeFav} allMeals={allMeals} modalMeals={modalMeals} />


            {/* <div className="recommendationRating">
              <div className="tableRating">
                <h1>Weekly Consumed Meals</h1>
                <Line style={{ marginBottom: '1rem' }}></Line>
                <ReactTable
                  data={theWeeks}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  columns={[
                    {
                      Header: "Week",
                      accessor: "mealDate",
                      filterable: false
                    }
                  ]}
                  minRows={1}
                  defaultPageSize={10}
                  className="-striped -highlight"
                  getTheadThProps={(columnIndex) => ({
                    style: { textAlign: "left", color: "black" }
                  })}
                  getTdProps={() => ({
                    style: { fontSize: "0.8rem", border: "none" }
                  })}
                  style={{ textAlign: 'left', border: "None" }}
                  SubComponent={(row) => {
                    return (
                      <div style={{ padding: "5px 20px 5px 20px" }}>
                        <div className="slider" style={{ marginTop: '2%', marginBottom: '2%' }}>
                          <div className="slides">
                            {theMeals.filter(v => moment(v.mealDate).format('MM/DD/yyyy') === row.original.mealDate).map(station =>
                              <Card style={{ width: '18rem', height: 'auto', marginBottom: '3%' }}>
                                <Card.Body>
                                  <Card.Title style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{station.mealName}</Card.Title>
                                  <Card.Subtitle className="mb-2 text-muted"> {station.mealType}</Card.Subtitle>
                                  <Card.Text>
                                    {station.mealType}

                                  </Card.Text>
                                  {allTimeFav.filter(v => v.name === station.mealName + '+' + station.mealType).map(station2 =>
                                    <>
                                      <Card.Subtitle className="mb-2 text-muted"> <Rating
                                        value={station2.rating}
                                        readOnly
                                      /></Card.Subtitle>
                                      <Card.Text></Card.Text>
                                    </>


                                  )}

                                  <Card.Text style={{ whiteSpace: 'pre-wrap', color: 'green' }}>
                                    {station.ingToAdd ? station.ingToAdd.map(txt =>
                                      <label className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                                        <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                        <span className="checkmark"></span>
                                      </label>
                                    )
                                      : null}
                                  </Card.Text>
                                </Card.Body>
                              </Card>
                            )
                            }


                          </div>
                        </div>
                      </div>

                    );
                  }}
                />
              </div>
            </div> */}

            <div className="recommendationRating">
              <div className="tableRating">
                <h1>Recommendation Based on Computed Ratings</h1>
                <Line style={{ marginBottom: '1rem' }}></Line>
                <ReactTable
                  data={allTimeFav}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  columns={columnsAllTime}
                  minRows={1}
                  defaultPageSize={10}
                  className="-striped -highlight"
                  getTheadThProps={(columnIndex) => ({
                    style: { textAlign: "left", color: "black" }
                  })}
                  getTdProps={() => ({
                    style: { fontSize: "0.8rem", border: "none" }
                  })}
                  style={{ textAlign: 'left', border: "None" }}
                  SubComponent={(row) => {
                    return (
                      <div style={{ padding: "5px 20px 5px 20px" }}>

                        {/* //SLIDER */}
                        <div className="slider" style={{ marginTop: '2%', marginBottom: '2%' }}>
                          <div className="slides">
                            {allMeals.filter(v => v.meal.mealName === row.original.name.split('+')[0] && v.mealType === row.original.type).map(station =>
                              // <div key={station.mealType}> {station.mealType} </div>
                              <Card style={{ width: '18rem', height: 'auto', marginBottom: '3%' }}>
                                <Card.Body>
                                  <Card.Title style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{station.user.firstname} {station.user.lastname}</Card.Title>
                                  <Card.Subtitle className="mb-2 text-muted"> <Rating
                                    value={station.rating}
                                    readOnly
                                  /></Card.Subtitle>
                                  <Card.Text>{station.meal.mealName}</Card.Text>
                                  <Card.Text>
                                    <mark style={{ color: 'white', backgroundColor: '#134881', borderRadius: '5px', padding: '3%' }}>{station.feedback ? station.feedback : 'No Feedback'}</mark>
                                  </Card.Text>
                                  <Card.Text style={{ whiteSpace: 'pre-wrap', color: 'red' }}>
                                    {station.ingToRemove ? station.ingToRemove.map(txt =>
                                      <label className="container2" style={{ textAlign: 'left', color: 'red' }}>{txt}
                                        <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                        <span className="checkmark"></span>
                                      </label>
                                    )
                                      : null}

                                  </Card.Text>

                                  <Card.Text style={{ whiteSpace: 'pre-wrap', color: 'green' }}>
                                    {station.ingToAdd ? station.ingToAdd.map(txt =>
                                      <label className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                                        <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                        <span className="checkmark"></span>
                                      </label>
                                    )
                                      : null}
                                  </Card.Text>
                                </Card.Body>
                              </Card>
                            )
                            }


                          </div>
                        </div>
                      </div>

                    );
                  }}
                />
              </div>
            </div>

            <div className="recommendationRating">
              <div className="tableRating">
                <h1>Feedback per User</h1>
                <Line style={{ marginBottom: '1rem' }}></Line>
                <ReactTable
                  data={perUser}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  columns={columnsPerUser}
                  minRows={1}
                  defaultPageSize={10}
                  className="-striped -highlight"
                  getTheadThProps={(columnIndex) => ({
                    style: { textAlign: "left", color: "black" }
                  })}
                  getTdProps={() => ({
                    style: { fontSize: "0.8rem", border: "none" }
                  })}
                  style={{ textAlign: 'left', border: "None" }}
                  SubComponent={(row) => {
                    return (
                      <div style={{ padding: "5px 20px 5px 20px" }}>

                        {/* //SLIDER */}
                        <div className="slider" style={{ marginTop: '2%', marginBottom: '2%' }}>
                          <div className="slides">
                            {allMeals.filter(v => v.user._id === row.original.name).sort(compareRating).map(station =>
                              // <div key={station.mealType}> {station.mealType} </div>
                              <Card style={{ width: '18rem', height: 'auto', marginBottom: '3%' }}>
                                <Card.Body>
                                  <Card.Title style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{station.user.firstname} {station.user.lastname}</Card.Title>
                                  <Card.Subtitle className="mb-2 text-muted"> <Rating
                                    value={station.rating}
                                    readOnly
                                  /></Card.Subtitle>
                                  <Card.Text>{station.meal.mealName}</Card.Text>
                                  <Card.Text>
                                    <mark style={{ color: 'white', backgroundColor: '#134881', borderRadius: '5px', padding: '3%' }}>{station.feedback ? station.feedback : 'No Feedback'}</mark>

                                  </Card.Text>
                                  <Card.Text>
                                    {station.mealType}

                                  </Card.Text>
                                  <Card.Text style={{ whiteSpace: 'pre-wrap', color: 'red' }}>
                                    {station.ingToRemove ? station.ingToRemove.map(txt =>
                                      <label className="container2" style={{ textAlign: 'left', color: 'red' }}>{txt}
                                        <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                        <span className="checkmark"></span>
                                      </label>
                                    )
                                      : null}

                                  </Card.Text>

                                  <Card.Text style={{ whiteSpace: 'pre-wrap', color: 'green' }}>
                                    {station.ingToAdd ? station.ingToAdd.map(txt =>
                                      <label className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                                        <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                        <span className="checkmark"></span>
                                      </label>
                                    )
                                      : null}
                                  </Card.Text>
                                </Card.Body>
                              </Card>
                            )
                            }


                          </div>
                        </div>
                      </div>

                    );
                  }}
                />
              </div>
            </div>

            <div className="allFeedbacks">
              <div style={{ paddingBottom: '2rem' }} >
                <h1>Feedbacks</h1>
                <Line></Line>
              </div>


              <ReactTable
                data={dataTable}
                filterable
                defaultFilterMethod={filterCaseInsensitive}
                getTdProps={(state, rowInfo, col, instance) => {
                  return {
                    style: { fontSize: "0.8rem", border: "none" },
                    onClick: (e) => {
                      if (col.Header === undefined) {
                        const { expanded } = state;
                        const path = rowInfo.nestingPath[0];
                        const diff = { [path]: expanded[path] ? false : true };
                        instance.setState({
                          expanded: {
                            ...expanded,
                            ...diff
                          }
                        });
                      } else {
                        Swal.fire(
                          `${rowInfo.original.user.firstname} ${rowInfo.original.user.firstname}  ${rowInfo.original.rating !== undefined ? `rated ${rowInfo.original.meal.mealName} ${rowInfo.original.rating} star` : `still not rating ${rowInfo.original.meal.mealName.toUpperCase()}`}  and said  "${rowInfo.original.feedback !== undefined ? rowInfo.original.feedback : 'nothing'}"`,
                        )
                      }
                    }
                  };
                }}
                columns={columns}
                defaultPageSize={10}
                className="-striped -highlight"
                getTheadThProps={(columnIndex) => ({
                  style: { textAlign: "left", color: "black" }
                })}
                style={{ textAlign: 'left', border: "None" }}

              />
            </div>



          </div>
        </div>
      </MainLayout>

    </DashboardStyled>

  )
}
const DashboardStyled = styled.div`
    background-color: #f1f1f1;
    margin-top: 0;
    padding-top: 0;
    

  
 
    .dashBoardText{
        padding-top: 1rem;
        margin-bottom: 2%;
    }
    .dashBoardText h2{
        font-size: 5vh;
    }

        .workers-card{
            height: 30vh;
            width: 22vw;
            background-color: red;
            border-radius: 20px;
        }

        .chart{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius: 50px 0px 0px 50px;
        }

        .chart2{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 50px 50px 0px;
        }

        .chart3{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 0px 0px 0px;
        }

        @media (max-width: 450px){
            .chart{
                padding: 1rem;
            }
        }

        .attribution { 
          font-size: 11px; text-align: center; 
      }
      .attribution a { 
          color: hsl(228, 45%, 44%); 
      }
      
      h1:first-of-type {
          font-weight: var(--weight1);
          color: var(--varyDarkBlue);
      
      }
      
      h1:last-of-type {
          color: var(--varyDarkBlue);
      }
      
      @media (max-width: 400px) {
          h1 {
              font-size: 1.5rem;
          }
      }
      
      .header {
          text-align: center;
          line-height: 0.8;
          margin-bottom: 50px;
          margin-top: 100px;
      }
      
      .header p {
          margin: 0 auto;
          line-height: 2;
          color: var(--grayishBlue);
      }
      
      
      .box p {
          color: var(--grayishBlue);
      }
      
      .box {
          border-radius: 5px;
          box-shadow: 0px 30px 40px -20px var(--grayishBlue);
          padding: 30px;
          margin: 20px;  
      }
      
      img {
          float: right;
      }
      
      @media (max-width: 450px) {
          .box {
              height: 200px;
          }
      }
      
      @media (max-width: 950px) and (min-width: 450px) {
          .box {
              text-align: center;
              height: 180px;
          }
      }
      
      .cyan {
          border-top: 3px solid var(--cyan);
          background-color: white;
      }
      .red {
          border-top: 3px solid var(--red);
          background-color: white;
      }
      .blue {
          border-top: 3px solid var(--blue);
          background-color: white;
      }
      .orange {
          border-top: 3px solid var(--orange);
      }
      
      h2 {
          color: var(--varyDarkBlue);
          font-weight: var(--weight3);
      }
      
      
      @media (min-width: 950px) {
          .row1-container {
              display: flex;
              justify-content: center;
              align-items: center;
          }
          
          .row2-container {
              display: flex;
              justify-content: center;
              align-items: center;
          }
          .box-down {
              position: relative;
              top: 150px;
          }
          .box {
              width: 28%;
           
          }
          .header p {
              width: 30%;
          }
          
      }
      .header {
        display: flex;
      }
      
      .item {
        width: 100%;
      }
      
      
      /* Demo styles, for aesthetics. */
      
      .demo {
        margin: 3rem;
      }
      
      .demo .item {
        text-align: center;
        padding: 3rem;
        background-color: #eee;
        margin: 0 1.5rem;
      }
    
    .slider {
      width: 100%;
      text-align: center;
      overflow: hidden;
    }
    
    .slides {
      display: flex;
      
      overflow-x: auto;
      scroll-snap-type: x mandatory;
      
      
      
      scroll-behavior: smooth;
      -webkit-overflow-scrolling: touch;
      
      /*
      scroll-snap-points-x: repeat(300px);
      scroll-snap-type: mandatory;
      */
    }
    .slides::-webkit-scrollbar {
      width: 10px;
      height: 10px;
    }
    .slides::-webkit-scrollbar-thumb {
      background: black;
      border-radius: 10px;
    }
    .slides::-webkit-scrollbar-track {
      background: transparent;
    }
    .slides > div {
      scroll-snap-align: start;
      flex-shrink: 0;
      width: 300px;
      height: 300px;
      margin-right: 50px;
      border-radius: 10px;
      background: #eee;
      transform-origin: center center;
      transform: scale(1);
      transition: transform 0.5s;
      position: relative;
      
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 100px;
    }
    .slides > div:target {
    /*   transform: scale(0.8); */
    }
    .author-info {
      background: rgba(0, 0, 0, 0.75);
      color: white;
      padding: 0.75rem;
      text-align: center;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      margin: 0;
    }
    .author-info a {
      color: white;
    }
    img {
      object-fit: cover;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    
    .slider > a {
      display: inline-flex;
      width: 1.5rem;
      height: 1.5rem;
      background: white;
      text-decoration: none;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      margin: 0 0 0.5rem 0;
      position: relative;
    }
    .slider > a:active {
      top: 1px;
    }
    .slider > a:focus {
      background: #000;
    }
    
    /* Don't need button navigation */
    @supports (scroll-snap-type) {
      .slider > a {
        display: none;
      }
    }
    
    html, body {
      height: 100%;
      overflow: hidden;

    }
    body {
      display: flex;
      align-items: center;
      justify-content: center;
      background: linear-gradient(to bottom, #74ABE2, #5563DE);
      font-family: 'Ropa Sans', sans-serif;
      
    }

    /* The container2 */
.container2 {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.modal-dialog{
  transform: translateY(30px) !important;
}
`;

export default DashboardPage;
