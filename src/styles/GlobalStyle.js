import { createGlobalStyle } from 'styled-components';


const GlobalStyle = createGlobalStyle`
/* :root{
    --primary-color: #007bff;
    --primary-color-light: #057FFF;
    --secondary-color: #6c757d;
    --background-dark-color: #10121A;
    --background-dark-grey: #191D2B;
    --border-color: #2e344e;
    --background-light-color: #F1F1F1;
    --background-light-color-2: rgba(3,127,255,.3);
    --white-color: #FFF;
    --font-light-color: #a4acc4;
    --font-dark-color: #313131;
    --font-dark-color-2: #151515;
    --sidebar-dark-color: #191D2B;
    --scrollbar-bg-color: #383838;
    --scrollbar-thump-color: #6b6b6b;
    --scrollbar-track-color: #383838;

} */

.light-theme{
    --primary-color: #007bff;
    --primary-color-light: #057FFF;
    --secondary-color: #ff7675;
    --background-dark-color: #F1F1F1;
    --background-dark-grey: #e4e4e4;
    --border-color: #cbced8;
    --background-light-color: #F1F1F1;
    --background-light-color-2: rgba(3,127,255,.3);
    --white-color: #151515;
    --font-light-color: #313131;
    --font-dark-color: #313131;
    --font-dark-color-2: #151515;
    --sidebar-dark-color: #E4E4E4;
    --scrollbar-bg-color: #383838;
    --scrollbar-thump-color: #6b6b6b;
    --scrollbar-track-color: #383838;
}
.dark-theme{
    --primary-color: #007bff;
    --primary-color-light: #057FFF;
    --secondary-color: #6c757d;
    --background-dark-color: #10121A;
    --background-dark-grey: #191D2B;
    --border-color: #2e344e;
    --background-light-color: #F1F1F1;
    --background-light-color-2: rgba(3,127,255,.3);
    --white-color: #FFF;
    --font-light-color: #a4acc4;
    --font-dark-color: #313131;
    --font-dark-color-2: #151515;
    --sidebar-dark-color: #191D2B;
    --scrollbar-bg-color: #383838;
    --scrollbar-thump-color: #6b6b6b;
    --scrollbar-track-color: #383838;
}

*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    list-style: none;
    text-decoration: none;
    font-family: 'Nunito', sans-serif;
    font-size: 1.1rem; 
    
}

.date-picker-holder{
    padding: 0px;

}


.react-datepicker__input-container{
    width: auto;
}

.css-mbfek{
    width: 40vw;
}

.flex-container2 {
    display: flex;
  }
  
  .flex-container2 > div {
    margin: 0px;
    padding: 0px;
  }

  .wordings {
    font-size: 0.8rem;
    color: rgb(32, 51, 84);
    font-weight: bold;
    padding-top: 9px !important;
  }

  .btn-label{
    font-size: 0.9rem;
    color: rgb(32, 51, 84);
    font-weight: bold;
  }

  .start-date, .end-date  {
      margin-top: 9px !important;
  }



@media print{
    html, body{
        background-image: none;
        height: initial !important;
    overflow: initial !important;
    -webkit-print-color-adjust: exact;
    }
    @page {
         size: landscape; 
         margin: 20mm;
    }

    .pagination-bottom{
        display: none !important;
    }

    #btn{
        display: none !important;
     }

     .btn{
        display: none !important;
     }
}

.grid-container {
    display: grid;
    grid-template-columns: auto auto auto;
    padding: 10px;
   
  }
  
  .grid-item {
    background-color: rgba(255, 255, 255, 0.8);
    border: 8px outset rgba(0, 0, 0, 0.8);
    border-color: rgb(32, 51, 84);
    padding: 10px;
    font-size: 20px;
    text-align: center;
  }



.ReactTable .-pagination .-btn {
    -webkit-appearance: none;
    appearance: none;
    display: block;
    width: 100%;
    height: 100%;
    border: 0;
    border-radius: 3px;
    padding: 6px;
    font-size: 1em;
    color: white;
    background-color: #203354 !important;
    background: rgba(0,0,0,0.1);
    transition: all .1s ease;
    cursor: pointer;
    outline-width: 0;
    border: 1px solid rgba(0, 0, 0, 0.8);
    border-radius: 3px;
    border-color: #203354;
}

.ReactTable .-pagination .-btn:not([disabled]):hover {
    background: rgba(0,0,0,0.3);
    color: white;
}

//Global Media Queries

`;

export default GlobalStyle;