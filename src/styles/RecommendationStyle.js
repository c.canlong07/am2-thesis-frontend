import styled from "styled-components";


export const Cards = styled.div`
    width: auto;
    height: auto;
    background: white;
    box-shadow: 0px 0px 5px rgba(0,0,0,0.3);
    border-radius: 5px;
    padding: 10px;
    margin-bottom: 0.7rem;

    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        background-color: #203354 !important;
    }

    h5{
        padding: 0.5rem 0.9rem;
        background: #ffbb00;
        border-radius: 10px;
    }

    .cardTitle{
        font-family: 'Montserrat', sans-serif;
        /* font-weight: 700; */
        font-size: 1rem;


    }
`;


export const BudgetCards = styled.div`
    width: auto;
    height: auto;
    background: white;
    box-shadow: 0px 0px 5px rgba(0,0,0,0.3);
    border-radius: 5px;
    padding: 10px;
    margin-bottom: 0.7rem;
    .css-1mcnwpj-MuiList-root{
      max-width: 100vw;
    }
    // p{
    //   font-size: 1.2rem;
    //   font-weight: bold;
    //   color: #322f3a;
    //   text-transform: uppercase;
    // }

    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        background-color: #203354 !important;
    }

    .css-10hburv-MuiTypography-root{
      margin-bottom: 3px;
      font-size: 1.4rem;
      font-weight:bold;
    }

    h5{
        padding: 0.5rem 0.9rem;
        background: #ffbb00;
        border-radius: 10px;
    }

    .cardTitle{
        font-family: 'Montserrat', sans-serif;
        /* font-weight: 700; */
        font-size: 1rem;


    }
`;

export const CardTitle = styled.div`


    h1{
        background-color: #f3ec78;
        background-image: linear-gradient(45deg, #f5d020 , #f53803);
        background-size: 100%;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        -webkit-text-fill-color: transparent; 
        -moz-text-fill-color: transparent;
        font-size: 34px;
        font-weight: bold !important;
        font-family: 'Nunito',sans-serif;
        }

`;

export const CheckLists = styled.div`


    
/* The container2 */
.container2 {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

h5{
    font-weight:bold;
    margin-left: 5%;
}

h4{
    text-transform: uppercase;
    font-weight:bold;
}

u{
    font-weight:bold;
    color:gray;
}

`;



