import styled from "styled-components";


export const LogInStyle = styled.div`

.container{
     width: 100%;
     padding: 0;
     margin: 0;
     max-width: 1920px;
     
}

.container h2{
    text-align: center;
    
}

.whole{
    width: 100%;
    display: flex;
    overflow: hidden;
}
#wave{
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    z-index: -1;
}
.container{
    width: 100vw;
    height: 100vh;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap :7rem;
}

.img{
    display: flex;
	justify-content: center;
	align-items: center;
}

.img img{
    width: 30rem;
    margin-top: 3rem;
}



 .logIn-card{
     margin-top: 4rem;
     display: flex;
	 justify-content: flex-start;
	 align-items: center;  
     width: auto;
     height: auto;
     border-radius: 1rem;
     text-align: center;
     margin-left: 1rem;
 }

#logo{
    width: 5rem;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    display: none;
}
 .login-form{
     width: 32vw;
     padding: 2rem 1rem 2rem 1rem;

 }

 .form-group{
     display: flex;
     align-items: center;
     padding: .5rem;
     padding-left: 1rem;
     margin-bottom: 0rem;
     margin-top: 0;
     color: white;
     background-color: #f1f1f1;
     border-radius: 2rem;
     
 }

 ::placeholder{
     color: black;
 }

 .fa{
     font-size: 1.5rem;
     color: #003798;
 }

 .form-group input{
     background: none;
     border: none;
     outline: none;
     border-radius: 2rem;
     margin-bottom: 0;
     color: #3d3d3d;
     font-size: 0.9rem;
     font-weight: bold;
 }

.logoText {
    height: auto;
    text-align: center;
    justify-content: center;
    h2{
    font-size: 2rem;
    text-transform: uppercase;
    font-weight: bold;
    color: #3d3d3d;
    margin-bottom: 0;
    display: flex;
    flex-direction: column; 
    }
    p{
        font-size: 0.9rem;
        text-transform: uppercase;
        font-weight: 600;
        color: #0039AE;
    }
}

.form-group input:-webkit-autofill,
.form-group  input:-webkit-autofill:focus,
.form-group  input:-webkit-autofill:hover {
   background-color: black;
}


 .form-group input:focus{
    background-color: none;
    box-shadow: none;
    border: none;
}


.logIn{
    display: flex;
    flex-direction: column;
    height: 60vh;
}

.email{
    margin-bottom: 0.7rem;
    height: 10%;
}

.password{
    margin-bottom: 0.7rem;
    height: 10%;
}

.red-text{
    color: red;
    font-size: 0.9rem;
    height: 9%;
}

.button{
    display: flex;
    width: 100%;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
}

#email::placeholder, #password::placeholder{
    color: #c4c4c4;
    font-size: 0.9rem;
    text-transform: uppercase;
    font-weight: bold;
}


@media (max-width: 900px) {


    .container{
		grid-template-columns: 1fr;
	}

    .logIn-card{
        margin-top: 1rem;
    }

    .img{
		display: none;
	}

	#wave{
		display: none;
	}
    .logIn-card{
     
        justify-content: center;
        margin-left: 0;
    }

    .login-form{
        width: 50vw;
    }

    #logo{
        display: inline;
        width: 10rem;
    }

    .email{
        margin-bottom: 0.5rem;
        height: 8%
    }
    
    .password{
        margin-bottom: 0.5rem;
        height: 8%
    }

    .red-text{
      
        height: 7%;
    }
   
  } 
  @media (max-width: 500px) {

    .login-form{
        width: 70vw;
    }
   
  } 


  @media (max-height: 670px) {


    .logIn{
        height: 80vh;
    }

    .logIn-card{
      margin-top: 5rem;
    }

    .login-form{
        padding-top: 0;
        padding-bottom: 0;
    }

    #logo{
        margin-bottom: 0;
        width: 9rem;
    }

    .logoText{
        h2{
            font-size: 1.7rem;
        }
        p{
            font-size: 0.7rem;
            margin-bottom: 0;
        }
        hr{
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }
    }

    .email{
        margin-bottom: 0.5rem;
        height: 9%
    }
    
    .password{
        margin-bottom: 0.5rem;
        height: 9%
    }

    .red-text{
        height: 5%;
    }
   
  } 

`;