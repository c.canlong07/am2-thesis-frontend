import styled from "styled-components";

export const MealPlan = styled.div`


.board{
    display: flex;
    flex-direction: column;
}

.Cards{
   
   display: flex;   
   padding: 0.5rem;
   border-bottom: 2px solid black;
    
   .labels{
       flex: 15%;
       border-right: 5px solid black;
       height: 20vh;
       margin-right: 1rem;
       background-color: #ff2b2b;
       border-radius: 10px 0px 0px 10px;
   }

   .labels h2{
       color: white;
       font-size: 1.5rem;
       align-items: center;
       text-align: center;
       margin-top: 30%;
       margin-bottom: 50%;
    
   }


.mealCards{
    display: flex;
    gap: 1vw;
    background-color: white;
    flex: 80%;
   }
}
`;

export const Cards = styled.div`
    height: 20vh;
    width: 11vw;
    background-color: white;
    box-shadow: 0px 0px 5px rgba(0,0,0, 0.5);
    border-radius: 2px;

    .withClick{
        height: 20vh;
    width: 11vw;
    background-color: white;
    box-shadow: 0px 0px 5px rgba(0,0,0, 0.5);
    border-radius: 2px;
    }

    .firstList {
        background-color: #ad7885 !important;
        padding: 0px 4px 14px 6px !important;
        color: white !important;
        overflow: hidden;
        text-overflow: ellipsis;
        height: 38%;
        text-align: center;
        margin: 0px;
    }
    .faUtensils{
        color: #2c2b36;
        margin-top: 10%;
        height: 65px;
        display: inline-block;
        width: 100% ;
    }

    .faExclamation{
        margin-top: 25%;
        color: gray;
        display: inline-block;
        width: 100% ;
        height: 80px;
    }

    .secondList {
        background-color: #ad7885 !important;
        padding: 0px 0px 0px 6px !important;
        color: white !important;
    }
`;

export const Days = styled.div`
    height: 3vh;
    width: 11vw;
    padding-bottom: 0;

    p{
        font-size: 1.3rem;
        font-weight: bold;
    }
`;

export const Naming = styled.div`

/* The container2 */
.container2 {
display: block;
position: relative;
padding-left: 35px;
margin-bottom: 12px;
cursor: pointer;
font-size: 22px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
}

/* Hide the browser's default checkbox */
.container2 input {
position: absolute;
opacity: 0;
cursor: pointer;
height: 0;
width: 0;
}

/* Create a custom checkbox */
.checkmark {
position: absolute;
top: 0;
left: 0;
height: 25px;
width: 25px;
background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
content: "";
position: absolute;
display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
left: 9px;
top: 5px;
width: 5px;
height: 10px;
border: solid white;
border-width: 0 3px 3px 0;
-webkit-transform: rotate(45deg);
-ms-transform: rotate(45deg);
transform: rotate(45deg);
}

h5{
    font-weight:bold;
    margin-left: 5%;
}

h4{
    text-transform: uppercase;
    font-weight:bold;
}

h3{
    text-transform: uppercase;
    font-weight:bold;
}


u{
    font-weight:bold;
    color:gray;
}

`;


