import styled from "styled-components";

export const CustomDatePickDiv = styled.div`
    border: solid 0.1em #203354;
    border-radius: 0.25em;
    width: 135%;
    margin-left: 4%;
    padding-bottom: 2%;
    padding-right: 2%;
   
    @media (max-width: 450px){
        width: 100%;
  
        .btn-label{
            font-size: 2px;
            //margin-right: 9%;
            //margin-left: 9%;
            
            margin-bottom: 1%;
        }
        
    }
    
    .btn-label{
    font-size: 0.9rem;
    margin-right: 6%;
    margin-left: 9%;
    margin-bottom: 2%;  
    
    }

    .date-icon{
        color: #203354;
        
    }

    @media print{
        @page {
             size: landscape; 
        }
    
        button {
            display: none;
          }
    
          body {
            zoom:165%;
        }
    }
`;







export const QRcode = styled.div`
    position: fixed;
    bottom: 0;
    margin-bottom: 3.8%;
    margin-left: 78%;
    border: 2px solid;
    border: inherit;
    align-items: center;
    padding: 0;
    z-index: 999;
    width: 100%;
    
`;




export const Card = styled.div`
background-color: white;
border-radius: 0.5rem;
color: white;
padding-top: 2%;
width: 24vw;
height: 20vh;
box-shadow: 0px 0px 10px rgba(0,0,0 ,0.1);
display: flex;
flex-direction: row;
`;

export const Text = styled.div`
margin-left: 10%;
text-align: left;
width: 60%;

h3{
    font-size:4vw;
    margin-bottom: 0;

}
h2{
    text-align: left;
    font-size:1.3vw;
    

}
`;
export const Icon = styled.div`
width: 40%;
align-items: center;
justify-content: center;
text-align: center;

img{
    width: 45%;
    margin-top: 20%;
    margin-bottom: 20%;
    }

`;

