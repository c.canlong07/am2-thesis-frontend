import styled from "styled-components";


export const MainLayout = styled.div`
margin-left: 5rem;
width: auto;
padding-top: 2%;
margin-right: 1rem;
.container
{
  padding-left: 0;
  padding-right: 0;
  padding-top: 2rem;
  padding-bottom: 1rem;
 

}
@media (max-width: 900px){
  .mealToday{
    padding: 3%;
  }

}


.upperPart{
    width: 100%;
    display: flex;
    margin-top: 2%;
}

.textTitle{
    width: 85%;
}

.textTitle h1{
    font-weight: bold;
    font-size: 1.8rem;
}

.headerTable
    {
        background-color: #e8edf2;
        font-weight: 550;
    }

.btn-create {
        background-color: DodgerBlue; 
        border: none; 
        color: white; 
        padding: 10px 18px; 
        font-size: 16px; 
        cursor: pointer; 
    }
.btn-create:hover {
        background-color: RoyalBlue;
    }
    
    
    
    
.ReactTable .rt-thead .rt-th.-sort-asc,.ReactTable .rt-thead .rt-td.-sort-asc {
        div:first-child:after {
          content:  "▴";
        }
      }
.ReactTable .rt-thead .rt-th.-sort-desc,.ReactTable .rt-thead .rt-td.-sort-desc {
        div:first-child:after {
          content: "▾";
        }
      }

      @media (max-width: 900px){
        margin: 0;
      
        .container{
          margin: 0;
          padding: 5%;
        }

       
        .upperPart{
          flex-direction: column;
          text-align: center;
        }
        .textTitle{
          width: 100%;
        }
        .textTitle h1{
          font-size: 2rem;
        }


  }
  //Meal

      .mealGroup{
        padding-bottom: 1rem;
      }
      .mealCards{
        border: none;
        background-color: #f1f1f1;
        border-radius: 0;
      }
      .cardBody{
        display: flex;
        flex-direction: column;
      }
      .mealTitle{
        height: 10%;
        font-size: 1.3rem;
        font-weight: bold;
        margin-bottom: 0.4rem;
        border-bottom: 2px solid black;
        white-space: nowrap;
        text-overflow: ellipsis;
      }
      .mealDescription{
        height: 50%;
        font-size: 0.8rem;
      }

      .mealIngredients{
        font-size: 0.8rem;
        text-overflow: hidden;

        ::-webkit-scrollbar {
          width: 10px;
        }
        
        /* Track */
        ::-webkit-scrollbar-track {
          background: #f1f1f1;
        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
          background: #888;
        }
        
        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
          background: #555;
        }
       
      }
      .mealType{
        font-size: 1rem;
        font-weight: bold;
        text-transform: uppercase;

      }
      .mealQR{
        height: auto;
        background-color: #f54e42;
        border: none;
      }
      .btn-primary:focus{
        box-shadow: 0 0 0 0.2rem rgb(255 164 164 / 50%)
      }

      .mealQR:not(:disabled):not(:disabled):active {
        background-color:#b83228 ;
      }
//meal responsive

      @media (max-width: 900px){
        margin: 0;
      
        .container{
          margin: 0;
          padding: 5%;
        }

      
        .upperPart{
          flex-direction: column;
          text-align: center;
        }
        
        .textTitle{
          width: 100%;
        }
        .textTitle h1{
          font-size: 2rem;
        }

        .table{
          overflow-x: hidden;
        }

        .row{
          flex-direction: column;
        }

        .row-cols-3 > *{
          width: 100%;
          max-width: 100%;
          padding: 0 0.2rem 0.2rem 0.2rem;
        }

        .mealCards{
          width: 100%;
        }


      }
      
`;

export const TableLayout = styled.div`
/*padding: 1vh 2vw;
height: auto;
background-color: white;
max-height: 200vh;
margin-bottom: 1rem;
padding-bottom: 1rem;*/
  padding: 1rem;
  width: 100%;
  height: auto;
  background-color: white;
  border-radius: 10px;
  box-shadow: 0px 5px 10px #bfbfbf;
  margin-bottom: 2%;

  .prev-next-div{
    .btn-prev{
      color: #fff;
      background-color: #2C3E50;
      border-color: #2C3E50;
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
      border: 1px solid transparent;
      padding: 0.4em 0.65em;
      font-size: 1em;
      line-height: 1.5;
     
    }
    .btn-next{
      color: #fff;
      background-color: #2C3E50;
      border-color: #2C3E50;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
      border: 1px solid transparent;
      padding: 0.4em 0.65em;
      font-size: 1em;
      line-height: 1.5;
    
    }
  }

  .up-con{
    width: 35vw;
    display: flex;
    
  }

  .meal-up-con{
    width: 75vw;
  }

  
  .start-date, .end-date .react-datepicker-wrapper {
    margin-top: 13px;
}

@media (max-width: 900px){

  

  .up-con{
    width: auto;
    display: flex;
    flex-direction: column; 

    .exportMealRecords{
      margin-right: 50%;
      margin-left: 20%;
    }

    .datePicker{
      width: 53%;
      padding-left: 0;
      margin: 0;
      padding: 0;
      justify-content: space-between;
     
  }
  }
  
  .start-date{
    width: auto;
    font-size: 10px;
    margin: 0;
    padding: 0;
  }
  .end-date{
    width: auto;
    font-size: 10px;
    margin: 0;
    padding: 0;
  }  
}


    
`;





export const Line = styled.div`
  margin-top: 2%;
  width: 100%;
  border-bottom: 1px solid #2f2f31;
  margin-bottom: 2%;
`;

export const AttendanceStyled = styled.div`
.upperLayout
{
  display: flex;
  flex-direction: row;
  width: auto;
  margin-bottom: 2%;
  gap: 2rem;
}


.card{
background: white;
border-radius: 0.5rem;
color: white;
padding-top: 2%;
box-shadow: 0px 0px 10px rgba(0,0,0 ,0.1);
display: flex;
flex-direction: row;
width: 50vw;

}

.text{
margin-left: 10%;
text-align: left;
width: 60%;

h3{
  font-size:4vw;
  margin-bottom: 0;
}
h2{
  text-align: left;
  font-size:1.3vw;
}
}


.icon{
width: 50%;
align-items: center;
justify-content: center;
text-align: center;

img{
    width: 50%;
    margin-top: 20%;
    margin-bottom: 20%;
    }
}


.totalEmployees{
  background-color:white;
  color:#203354;

}

.present
{
 color:#00ad11;

}

.absent
{
 color:#db0000;

}

.datePicker{
        //margin-right: 0%;
        display: inherit;
        //margin-top:2%;
        margin: 0;
        padding: 0;
        background-color: white;
        //padding-top: 0.5%;
        //padding-left: 2%;
        //padding-bottom: 0.5%;
        //box-shadow: 0px 0px 10px rgba(0,0,0 ,0.2);
        width: 38vw;
        background: green;
        
        
        .form-control{
            font-size: 1rem;
            text-align: center;
        }   
        
        .start-date{
            display: flex;
            align-items: center;
            font-size: .8rem;
            font-weight: bold;
            margin: 0 ;
            color: #203354;
            width: 6vw;
            padding: 2%;
            
        }
        
        
        .end-date{
            display: flex;
            align-items: center;
            font-size: 0.8rem;
            font-weight: bold;
            margin: 0;
            color: #203354;
            width: 6vw;
            padding-left: 20% !important;
           
           
            
        }

}



@media (max-width: 900px){
        .upperLayout
            {
              display: flex;
              flex-direction: column;
              width: 100%;
            }

          .card{
            width: 100%;
            height: 15vh;
            flex-direction: row;
            text-align: center;
            position: block;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 0.2rem;
            padding: 0;
            padding-top:3%;
          }

          .text{
            text-align: left;
            width: 50%;
            margin-left: 2rem;

            h2{
              text-align: left;
              font-size: 0.7rem;
              margin-bottom: 0;
            }
            h3{
              font-size: 2.5rem;
              margin-top: 0;
            }
          }

      
          .icon{
            width: 50%;

            img{
              width: auto;
              height: 7.5vh;
              margin: 0;
            }
          }

          


          

          .react-datepicker__input-container{

          }

}

`;








