import styled from "styled-components";


export const QRCodeStyled = styled.div`
    background-color: #f1f1f1;
    margin: 0;
    padding: 0;
    
    }

  
    .container {
      display: flex;
      flex-direction: column;
      position: relative;
      width: 100%;
      margin: 100px auto 0;
      word-break: break-all;
      border: 1px solid rgba(0, 0, 0, 0.274);
    }
  
    .whole{
      display:flex;
      height: 100vh;
      //background-color: #black;
    }
  
    .right{
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding-bottom: 8rem;
      text-align: center;
      width: 60%;
  
    }
  
    .right h1{
      font-weight: 900;
      margin-bottom: 0;
      font-family: 'Montserrat', sans-serif;
      color: #203354;
    }
  
    .left{
      display:flex;
      width: 40%;
      align-items: center;
    }
  
    .qrCode-card{
      color: white;
      display: flex;
      flex-direction: column;
      padding: 2rem;
      border-radius: 20px;
      width: 30vw;
      height: 70vh;
      background-color: #203354;
      text-align:center;
      align-items: center;
  
    }
  
    .time{
      font-size: 3vh;
    }
  
    .qrCODE {
      width: 20vw;
      margin: auto;
    }
  
    .result{
      margin-top: 1.2rem;
      border-radius: 0.5rem;
      padding: 0.5rem 6rem;
      background: #FF9C05;
      text-transform: uppercase;
    }

    @media (max-width: 900px){
   
        .container{
            position: none;
        }
        .whole{
            width: 100%;
            flex-direction: column;
            height: 90vh;
        }

        .right{
            width: 100%;
            flex-direction: column;
            padding-top: 2rem;
            padding-bottom: 2rem;

            h1{
                font-size: 2.5rem;
                margin-bottom: 0;
            }
            p{
                font-size: 1rem;
            }
        }
        

        .left{
            width: 100%;
            flex-direction: column;
            margin: 0;
            padding: 0;
            padding-bottom: 2rem;
        }

        .qrCode-card{
          width: 80vw;
          padding: 1.5rem;
          padding-top: 1rem;
          height: auto;
        }

        .time{
            font-size: 1rem;
        }

        .qrCODE{
            width: 65vw;
            margin-left: auto;
            margin-right: auto;
        }

        .result{
            margin-top: 1rem;
            padding: 0.3rem 4rem;
        }
   
    
`;



