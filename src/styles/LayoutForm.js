import styled from "styled-components";

export const ModalLayout = styled.div`

   .form-group{
        border-bottom: 3px solid #203354;
        transition: 0.2s;
        color: black;
        
        
    }

    .form-group label{
        font-size: 1rem;
        color: #919191;
        text-transform: uppercase;
        margin-bottom: 0;
    }

    .form-group input{
        background-color: #E6E6E6;
        height: 2rem;
        border-radius: 0.4rem 0.4rem 0 0;
    }

    .form-group input:focus{
    background-color: #f5f5f5;
    box-shadow: none;
    border: none;
       
    }

    .form-group:hover{
        border-bottom: 3px solid #00AD11;
    }

    .form-group select{
        background-color: #E6E6E6;
    }

    .textArea{
        background-color: #E6E6E6;
    }

    .textArea:focus{
        background-color: #f5f5f5;
        box-shadow: none;
        border: none;
    }

    .footer-form{

    }

`;


export const LayoutForm = styled.div`
    margin-top: 2%;
    margin-left:5rem;
    height: 100vh;
    
    .grid-container {
        display: grid;
        grid-template-columns: 50% 50%;
        background-color: #203354;
        padding: 10px;
      }
      .grid-item {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid rgba(0, 0, 0, 0.8);
        padding: 20px;
        font-size: 30px;
        text-align: center;
      }
      
    .container{
        margin-top: 5%;
        background-color: white;
        padding-bottom: 2rem;
        width: 60rem;
        padding-left: 0;
        padding-right: 0;
        box-shadow: 0 0 2rem rgba(0,0,0, 0.2);
    }


    .container h1{
        font-size: 1.8rem;
        font-weight: bold;
        text-align: center;
        background-color: #203354;
        padding: 1rem 0rem;
        color: white;
    }

    .wholeForm{
        padding-left: 2rem;
        padding-right: 2rem;
    }
    .form{
        margin-top: 3%;
        display: flex;
        justify-content: space-evenly;
    }

    .leftSide{
        width: 50%;
        margin-right: 3%;
        margin-left: 4%;
    }
    .rightSide{
        width: 50%;
        margin-right: 3%;
        margin-left: 5%;
      
    }

    .form-group{
        border-bottom: 3px solid #203354;
        transition: 0.2s;
        color: black;
        
    }

    .form-group label{
        font-size: 1rem;
        color: #919191;
        text-transform: uppercase;
        margin-bottom: 0;
    }

    .form-group input{
        background-color: #E6E6E6;
        height: 2rem;
        border-radius: 0.4rem 0.4rem 0 0;
    }

    .form-group input:focus{
    background-color: #f5f5f5;
    box-shadow: none;
    border: none;
       
    }

    .form-group:hover{
        border-bottom: 3px solid #00AD11;
    }

    .form-group select{
        background-color: #E6E6E6;
    }

    .textArea{
        background-color: #E6E6E6;
    }

    .textArea:focus{
        background-color: #f5f5f5;
        box-shadow: none;
        border: none;
    }

    .mealType:focus{
        background-color: #f5f5f5;
        box-shadow: none;
        border: none;
    }

    .mealGroup{
        justify-content: center;
    }

 

    @media (max-width: 900px){
        margin-left: 0;
        height: auto;
        padding: 1.5rem;


        .container{
            width: auto;
        }
        .form{
            display: flex;
            flex-direction: column;
        }
        .wholeForm{
            padding-left: 1rem;
            padding-right: 1rem;
            width: 100%;
            align-items: center;
            text-align: center;
        }

        .leftSide{
            width: 100%;
            margin:0;
        }
        .rightSide{
            width: 100%;
            margin: 0;
        }

    }
    
`;


export const ButtonLayout = styled.div`
    margin-top: 2%;
    
    

    button{
        background-color: #203354;
        border-color: #203354; 
    }

    .btn-primary:hover {
        color: #fff;
        background-color: #1E90FF;
    }

    .btn-outline-dark{
        
    }

    .btn-outline-dark:hover {
        color: #203354;
        background-color: #1E90FF;
        border-color: #007BFF; 
    }
`;

export const Buttons = styled.div`
    margin-top: 2%;
    text-align: right;
    margin-right: 5%;
    

    button{
        background-color: #203354;
        border-color: #203354;
        
    }

    .btn-primary:hover {
        color: #fff;
        background-color: #1E90FF;
    }

    .btn-outline-dark:hover {
        color: #203354;
        background-color: #1E90FF;
        border-color: #007BFF; 
    }

    @media (max-width: 900px){
       
    }
    
    
`;



