import styled from "styled-components";


export const MainLayout = styled.div`
    margin-left:5rem;
    width: auto ;
   


    .wholePage{
        display: flex;
        flex-direction: column;
    
        
        
    }
    .card {
        width: 15vw;
        height: auto;
    }

    @media (max-width: 450px){
        margin-left: 1%;
        

        .card{
            width: 100%;
        }
    } 


    //RECOMMENDATION
   .containerRec{
        width: 100%;
        height: auto;
        padding-bottom: 1rem;
        margin-right: 1rem;
        diplay: flex;
        flex-direction: column;
      }

    .totals{
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        padding-bottom: 2rem;
        gap: 2rem;
        
    }

    .cardtotal{
        width: 21vw;
        box-shadow: 0px 5px 10px #bfbfbf;
        border: none;
        padding: 1rem;
        height: 20vh;
    }
    
    .recommendationRating{
        display: flex;
        flex-direction: column;
        text-align: Left;
        align-items: center;
        padding-bottom: 2rem;
    }

    .tableRating{
        padding: 1rem;
        width: 100%;
        height: auto;
        background-color: white;
        border-radius: 10px;
        box-shadow: 0px 5px 10px #bfbfbf;
    }

    .feedBack{
        display: flex;
        padding-bottom: 2rem;
    }

    .recentFeedback{
        flex: 40%;
        width: auto;
        height: auto;
        padding-right: 2rem;
    }

        .feedBackCarousel{
            background-color: #C7C7C7;
            border-radius: 10px;
            height: 40vh;
            width: auto;
    
   
        }


    .feedBackGraph{
        flex: 60%;
    }
        .context{
            display: flex;
            background-color: white;
            box-shadow: 0px 5px 10px #bfbfbf;
            border-radius: 10px;
            width: 100%;
            height: 40vh;
        }

        .text{
            flex: 40%;
            padding-left: 2rem;
            padding-top: 1rem;
            text-align: left;
        }

        .graph{
            flex: 60%;
        }
    
    .allFeedbacks{
        padding: 1.5rem;
        background-color: white;
        box-shadow: 0px 5px 10px #bfbfbf;
        width: 100%;
        height: auto;
        border-radius: 10px;

    }
    //MEAL RANKING
    .mealrank-con{
        // text-align: center;
        padding: 1.5rem;
        background: white;
        box-shadow: 0px 5px 10px #bfbfbf;
        width: 100%;
        height: auto;
        border-radius: 10px;
        margin-top: 3%;
        margin-bottom: 4%;
        .choices-con{
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            padding-left: 20%;
            align-text: center;
            height: 7vh;
           
            .con1{
                text-align: center;
                margin: 1%;
                margin-left: 3%;
                margin-right: 2%;
        
            }
            .con2, .con3, .con4{
                text-align: center;
                padding: 1%;
                width: 50vw;
                
              
            }
        }
        .best-con, .great-con, .ok-con, .worst-con, .notrated-con{
            display: flex;
            flex-direction: row;
            padding: 0px;
            margin-bottom: 1rem;
            height: auto;
            background: white;
            box-shadow: 0px 0px 8px rgba(0,0,0,0.4);
           
          

           
            .con1, .con2, .con3, .con4, .con5{
                text-align: center;
                width: 80vw;
                height: auto;
                padding: 0.5rem;
                padding-top: 2rem;
            }

            .con1{
                border-right: 3px solid black;
            
            }

           
        }

    
    }
    //DAILY RANKKING
    .dailyrank-con{
        padding: 1.5rem;
        background: white;
        box-shadow: 0px 5px 10px #bfbfbf;
        width: 100%;
        height: auto;
        border-radius: 10px;
        margin-top: 3%;
  
        .choices-con{
            display: flex;
            flex-direction: row;
            padding-left: 20%;
           
 
           
            /*.con1{
                text-align: center;
                margin: 1%;
                margin-left: 3%;
                margin-right: 1%;
                padding: 1%;
                width: 50vw;
                background: blue;
            }*/
            .con1, .con2, .con3, .con4, .con5{
                text-align: center;
                height: auto;
                width: auto;
                float: left;
                margin: 1%;
                /* padding: 1%; */
                width: 50vw;
              
                h2{
                    font-size: 23px;
                    font-weight: bold;
                   
                }  
                
              
            }
        }
        .breakfast-con, .lunch-con, .snacks-con, .dinner-con{
            display: flex;
            flex-direction: row;
            padding: 0px;
            margin-bottom: 1rem;
            height: auto;
            /* background: maroon; */
            box-shadow: 0px 0px 8px rgba(0,0,0,0.4);
          
         
            .con1{
                text-align: center;
                //margin: 1%;
                //margin-right: 4%;
                //padding: 1%;
                height: auto;
                width: 40vw;
                flex-flow: wrap;
                border-right: 3px solid black;
          
                h2{
                    font-size: 23px;
                    font-weight: bold;
                    padding-top: 15%;
                    
                }
            }


            .con2, .con3, .con4, .con5, .con6{
                text-align: center;
                margin: 1%;
                /* padding: 1%; */
                height: auto;
                width: 30%;
                flex-flow: wrap;
              
                h3, h4{
                    font-size: 23px;
                    margin-top: 5%;
                }      
            }
        }
    }

  

    @media (max-width: 450px){

        .dashBoardText{
            flex-direction: column;

            .datePicker{
                flex-direction: column;
                gap:0;
            }
        }
        

        

        .totals{
            flex-direction: column;
            gap: 1rem;
        }
        
        .cardtotal{
            width: 100%;
        }

        .tableRating{
            width: 100%;
        }

        .feedBack{
            flex-direction: column;
        }

        .recentFeedback{
            padding-bottom: 1rem;
            text-align: center;
            flex: 50%;
            padding-right: 0;
        }

        .feedBackGraph{
            text-align: center;
        }
            .context{
                flex-direction: column;
                height: 100%;
                width: auto;
            }

            .text{
                align-items: center;
                text-align: center;
                justify-content: center;
                padding: 1rem;
            }
    }
  

    .date-con{
        background: white;
        display: flex;
        flex-direction: row;
        width: 34%;
        align-items: center;
        padding: 0.5rem;
        padding-bottom: 1%;
        position: fixed;
        margin-left: 58%;
        z-index: 999;
        border-radius: 10px;
        box-shadow: 0px 5px 10px #bfbfbf;
        margin-bottom: 2%;
       

        @media (max-width: 450px){
            width: 89%;
            margin-left: 6%;
        }

            .datePicker{
            //margin-right: 0%;
            //padding-left: 2%;
            //box-shadow: 0px 0px 10px rgba(0,0,0 ,0.2);
            //margin-top: 10%;
            //left: 292%;
            display: flex;
            flex-direction: row;
            padding: 0%;
            margin: 0%;
            border-radius: 0.2rem;
            padding-top: 0.5%;
            padding-bottom: 0.5%; 
            padding-left: 4%;    
            width: 100%;
                @media (max-width: 450px){
                    width: 93%; 
                    padding-left: 0;
                    justify-content: space-between;
                }
        
                .form-control{
                    font-size: 1rem;
                    text-align: center;
                    @media (max-width: 450px){
                       
                    }
                }   
                
                .start-date{
                    display: flex;
                    align-items: center;
                    font-size: 0.8rem;
                    font-weight: bold;
                    margin: 0;
                    color: #203354;
                    width: auto;
                    @media (max-width: 450px){
                        width: auto;
                        font-size: 10px;
                        margin: 0;
                    }
                  
                }
                
                
                .end-date{
                    display: flex;
                    align-items: center;
                    font-size: 0.8rem;
                    font-weight: bold;
                    margin: 0;
                    margin-left: 12%;
                    color: #203354;
                    width: auto;
                    @media (max-width: 450px){
                        width: auto;
                        font-size: 12px;
                        margin: 0;
                        
                    }
               
                    
                }

            }
     }

    

    
    //DASHBOARD
    .dash-con{
        width: auto ;
        height: auto;
        margin: 0;
        padding: 0;
        margin-bottom: 5%;
        justify-content: center;
        display: flex;
        flex-direction: row;
            @media (max-width: 450px){
                 display: flex;
                 flex-direction: row;
                 
            }
        
            
        /*.upper{
            display: flex;
            flex-direction: row;
                 
        }
        .lower{
    
        }*/
  
        //LEFT CONTAINER
        .left-con{
            display: flex;
            flex-direction: column;
            width: 100%;
            height: auto;
        
            @media (max-width: 450px){
                 display: flex;
                 flex-direction: column;
                 width: 58%;
            }

            .admin-card{
                background:linear-gradient(114deg, rgba(28, 48, 83, 1) 43%, rgba(0, 0, 255, 1) 100%);
                color: white;
                width: auto ;
                margin: 1%;
                padding: 5%;
                border-radius: 1rem;
                
            
                h1{
                    font-weight: bold;
                    line-height: 0.6;
                    padding-bottom: 5%;
                    color: white;
                }
                h2{
                    line-height: 0.5;
                    font-size: 20px;
                    text-indent: 1.5%;
                    color: white;
                }
                h4{
                    color: white;
                }
                h5{
                    padding-top: 3%;
                    color: white;
                }
            }

            .attendance-card{
                background: white ;
                width: auto;
                display: flex;
                flex-direction: row;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                margin: 1%;
                padding: 2%;
              

                border-radius: 1rem;
                @media (max-width: 450px){
                        display: flex;
                        flex-direction: column;
                    }

                .left{ 
                    width: auto;
                    .attendance-con{
                        display: flex;
                        flex-direction: row;    
                        margin: 0;
                        padding: 0;
                        width: auto;
                        height: auto;
                  
                    }
                }

                .right{
                     width: auto;

                        .chart-con{
                            width: 40vw;
                            height: 65vh;
                            padding: 0;
                            margin-top: 5%;
                            align-items: center;
               
                            @media (max-width: 450px){
                                width: auto;
                                height: 50vh;
                            }
                        
                        }
                    }     

                .text{
                    padding: 0;
                    margin: 0;
                }

                h1{
                    background-color: #f3ec78;
                    background-image: linear-gradient(45deg, #f5d020 , #f53803);
                    background-size: 100%;
                    -webkit-background-clip: text;
                    -moz-background-clip: text;
                    -webkit-text-fill-color: transparent; 
                    -moz-text-fill-color: transparent;
                    font-size: 25px;
                    font-weight: bold;
                    }
                
                .text-center{
                    padding: 0.5; 
                    width: '20%'; 
                    margin-right: 1%;
                    @media (max-width: 450px){
                        display: flex;
                        flex-direction: column;
                    }
                }    
                    
            }

            .meal-card{
                background: white ;
                width: auto;
                display: flex;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                margin: 1%;
                padding: 2%;

                border-radius: 1rem;
                @media (max-width: 450px){
                        display: flex;
                        flex-direction: column;
                        margin-top: 2%;
                    }


                .left{
                    width: auto;
                    .meal-con{
                        display: flex;
                        flex-direction: row;    
                        margin: 0;
                        padding: 0;
                        width: 120%;
                        height: auto;
                        @media (max-width: 450px){
                            width: 100%;
                            height: auto;
                        }
                    }
                    
                }
                .right{
                    width: auto;
                    .chart-con{
                        width: 40vw;
                        height: 65vh;
                        padding: 0;
                        margin-top: 15%;
                        align-items: center;
                        @media (max-width: 450px){
                                width: auto;
                                height: 50vh;
                            }
                    }
                }

                .text{
                        padding: 0;
                        margin: 0;
                    }
  
                h1{
                    background-color: #f3ec78;
                    background-image: linear-gradient(45deg, #f5d020 , #f53803);
                    background-size: 100%;
                    -webkit-background-clip: text;
                    -moz-background-clip: text;
                    -webkit-text-fill-color: transparent; 
                    -moz-text-fill-color: transparent;
                    font-size: 30px;
                    font-weight: bold;
                    }

               
            }

            .bar-card{
                background: white ;
                width: auto;
                height: auto;
                display: flex;
                flex-direction: column;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                margin: 1%;
                padding: 3%;
                border-radius: 1rem;

                @media (max-width: 450px){
                        display: flex;
                        flex-direction: column;
                        margin-top: 2%;
                    }

                h1{
                    background-color: #f3ec78;
                    background-image: linear-gradient(45deg, #f5d020 , #f53803);
                    background-size: 100%;
                    -webkit-background-clip: text;
                    -moz-background-clip: text;
                    -webkit-text-fill-color: transparent; 
                    -moz-text-fill-color: transparent;
                    font-size: 30px;
                    font-weight: bold;
                   
                    @media (max-width: 450px){
                        display: flex;
                        text-align: center;
                        margin-top: 2%;
                    }

                }

                .bar-chart{
                    width: auto;
                    height: 60vh;
    
                    @media (max-width: 450px){
                        width: auto;
                        height: 50vh;
                        
                    }
                }
            }
        }

        .right-con{
            display: flex;
            flex-direction: column;
            width: 40%;
            padding-top: 0.5%;
            height: auto;

            .date-card{
                background: white;
                width: auto ;
                height: auto;
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;         
                margin: 1%;
                margin-bottom: 5%;
                padding: 3%;
                border-radius: 1rem;
                @media (max-width: 450px){
                    padding-left: 10%;
                    padding-bottom: 8%;
                }

                
            
                @media (max-width: 450px){
                 display: flex;
                 flex-direction: row;
                 width: auto;
            }
    
            }
         
            .feedback-card{
                background-color: #2a2a72;
                background-image: linear-gradient(180deg, #0f3f5c 0%, #009ffd 74%);
                color: white;
                width: auto;
                max-height: 250vh;
                margin: 1%;
                padding: 5%;
                padding-bottom: 5%;
                border-radius: 1rem;
                overflow-y: scroll;
                max-height: 256.5vh;

                @media (max-width: 450px){
                    font-size: 10%;
                    max-height: 399vh;
                   
                }


                h1{
                    font-weight: bold;
                    @media (max-width: 450px){
                    font-size: 1.5rem;
                    margin: 1%;
    
                    
                }

                }
                .feed-card{
                background: white;
                color: black;
                width: auto;
                height: auto;
                margin: 1%;
                padding: 5%;
                margin-top: 5%;
                border-radius: 10px;
                

                @media (max-width: 450px){
                    padding: 5%;
                    width: auto;
                    
                }

                    h1{
                        font-weight: bold;
                        font-size: 2rem;   
                        @media (max-width: 450px){
                            font-size: 1.3rem; 
                            display: flex;
                            flex-direction: column;
                        }
                    }
                    h3{
                        font-size: 20px;
                        margin-top: 6%;
                        @media (max-width: 450px){
                            font-size: 1.2rem; 

                            }
                        }
                    h2{
                        font-weight: bold;
                        font-size: 20px;
                        @media (max-width: 450px){
                            font-size: 1.3rem; 

                            }

                    }
                    h5{
                        font-size: 16px;
                        @media (max-width: 450px){
                            font-size: 0.8rem; 

                            }

                    }
                }
            }
        }
     
    }       
     

    
`;

export const CustomDatePickDiv = styled.div`
    border: solid 0.1em #203354;
    border-radius: 0.25em;
    width: 135%;
    margin-left: 4%;
    padding-bottom: 2%;
    padding-right: 8%;
    
    .btn-label{
    font-size: 0.9rem;
    margin-right: 6%;
    margin-left: 9%;
    margin-bottom: 2%;  
    
    }

    .date-icon{
        color: #203354;   
    }
    @media (max-width: 450px){
        width: 130%;
        padding: 0;
        .btn-label{
            font-size: 12px;
            //margin-right: 9%;
            //margin-left: 9%;
            margin-bottom: 2%;
        }
        
    }
`;

export const Container = styled.div`
    display:flex;
    height: 100%;
    margin-bottom: 1rem;


    @media (max-width: 450px){
        flex-direction: column;
    }
  
    
`;

export const LeftLayout = styled.div`
display: flex;
width: 50%;
justify-content: center;

@media (max-width: 450px){
    width: 95%;
    margin-bottom: 1rem;
}
   
    
`;

export const RightLayout = styled.div`
   width: 50%;
   @media (max-width: 450px){
    width: 100%;
}

   
    
`;

export const Charts = styled.div`


`;

export const Line = styled.div`
width: 98%;
border-bottom: 2px solid #2f2f31;
`;

export const Lines = styled.div`
width: 98%;
border-bottom: 2px solid #FFFFFF;
`;


