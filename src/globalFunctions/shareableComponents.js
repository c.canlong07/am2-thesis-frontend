import React from "react";
import { Card, Button, OverlayTrigger, Popover } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";

export const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th className="w-1"></th>
        <th
          className="px-6 py-3 text-xs font-bold leading-4 tracking-wider
        text-left text-bb-gray-600 text-opacity-50 uppercase bg-gray-50"
        >
          Title
        </th>
        <th
          className="px-6 py-3 text-sm font-bold leading-4 tracking-wider
        text-left text-bb-gray-600 text-opacity-50 bg-gray-50"
        >
          Assigned To
        </th>
        <th className="px-6 py-3 bg-gray-50"></th>
      </tr>
    </thead>
  );
};

export const OthersButton = (props) => {
  return (
    <Button key={props.id} variant="secondary" size="sm" onClick={() => props.otherRecommendation(
      props.data,
      props.condition,
      props.id
    )}>...</Button>
  );
};

export const ButtonTooltip = (props) => {

  const popover = (
    <Popover key="popover-basic" id="popover-basic">
      <Popover.Header as="h3" style={{ backgroundColor: '#203354', color: 'white' }}>{props.title}</Popover.Header>
      <Popover.Body style={{ whiteSpace: 'pre-wrap', fontSize: '16px', backgroundColor: '#e5e5e5' }}>
        {props.text}
      </Popover.Body>
    </Popover>
  );

  return (
    <OverlayTrigger key="trigger" trigger={['hover', 'focus']} placement="right" overlay={popover}>
      <Button style={{ all: 'unset' }}><FontAwesomeIcon icon={faInfoCircle} style={{ color: 'gray', height: '25px', padding: '10px 5px 15px 5px' }} /></Button>
    </OverlayTrigger>
  );
};

export const ButtonTooltip2 = (props) => {

  const popover = (
    <Popover key="popover-basic" id="popover-basic">
      <Popover.Header as="h3" style={{ backgroundColor: '#203354', color: 'white' }}>{props.title}</Popover.Header>
      <Popover.Body style={{ whiteSpace: 'pre-wrap', fontSize: '16px', backgroundColor: '#e5e5e5' }}>
        <strong style={{ color: '#203354' }}>Yesterday Attendance</strong>
        <div className="d-grid gap-2">
          <Button variant="primary" style={{ width: '100%' }} onClick={() => props.handleOnClick(props.yesterday?.[0])}>Morning Present: {props.yesterday?.[0]}</Button>
          <Button variant="primary mt-1" style={{ width: '100%' }} onClick={() => props.handleOnClick(props.yesterday?.[1])}>Afternoon Present: {props.yesterday?.[1]}</Button>
        </div>

        <br />
        <strong style={{ color: '#203354' }}>Today's Attendance</strong>
        <div className="d-grid gap-2">
          <Button variant="primary" style={{ width: '100%' }} onClick={() => props.handleOnClick(props.today?.[0])}>Morning Present: {props.today?.[0]}</Button>
          <Button variant="primary mt-1" style={{ width: '100%' }} onClick={() => props.handleOnClick(props.today?.[1])}>Afternoon Present: {props.today?.[1]}</Button>
        </div>

      </Popover.Body>
    </Popover >
  );

  return (
    <OverlayTrigger key="trigger" trigger="click" placement="right" overlay={popover}>
      <Button style={{ all: 'unset' }}><FontAwesomeIcon icon={faInfoCircle} style={{ color: 'gray', height: '25px', padding: '10px 5px 5px 5px' }} /></Button>
    </OverlayTrigger>
  );
};


// export default TableHeader;