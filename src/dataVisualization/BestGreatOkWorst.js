import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import { Line } from '../styles/DashboardLayout';
import noDataFound from '../img/no-data-found.png';
import { Cards, CardTitle, CheckLists } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCheck, faUserPlus, faUserTimes, faComment, faStar, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { Card, Modal, Button, Tab, Row, Nav, Col, Popover, OverlayTrigger } from "react-bootstrap";
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import Tooltip from '@mui/material/Tooltip';
import { ButtonTooltip } from '../globalFunctions/shareableComponents';

export default function BestGreatOkWorst(props) {
  const [allTimeFav, setallTimeFav] = useState({});
  const [modalShow, setmodalShow] = useState(false);
  const [modalID, setmodalID] = useState('');
  const [modalName, setmodalName] = useState('');
  const [modalNoContent, setmodalNoContent] = useState('');

  useEffect(() => {
    setallTimeFav(props.allTimeFav);
  }, [props.allTimeFav]);

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }
  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">Popover right</Popover.Header>
      <Popover.Body>
        And here's some <strong>amazing</strong> content. It's very engaging.
        right?
      </Popover.Body>
    </Popover>
  );

  return (
    <React.Fragment>
      <Modal
        // {...props}
        show={modalShow}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable='true'
      >
        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
          <Modal.Title id="contained-modal-title-vcenter">
            {!modalNoContent ? `Feedback for ${modalName}` : 'Feedback'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.allTimeFav.filter(v => v.mealID === modalID).length !== 0 ?
            props.allTimeFav.filter(v => v.mealID === modalID).sort(compareRating).map((station2, i) =>
              <div>
                <Card.Header style={{ marginBottom: '4%', background: 'none', border: 'none', paddingBottom: '0px' }}>
                  <div className="grid-container">
                    <div className="grid-item"><Rating
                      name={station2.name}
                      precision={0.5}
                      size="large"
                      value={station2.rating}
                      readOnly
                    /> </div>
                    <div className="grid-item">{station2.rating !== 0 ? station2.rating.toFixed(2) : 0}</div>
                    <div className="grid-item">{station2.date}</div>

                  </div>
                </Card.Header>

              </div>
            )
            : null
          }

          <Divider style={{ marginBottom: '3%' }}>
            <Chip label="Employees Feedback" />
          </Divider>
          <Tab.Container id="left-tabs-example" defaultActiveKey={props.modalMeals.filter(v => v.meal._id === modalID)?.[0]?.user.firstname}>
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  {!modalNoContent ? props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>

                      <Nav.Item>
                        <Nav.Link eventKey={station.user.firstname}>{station.user.firstname + ' ' + station.user.lastname} <br />
                          {station.rating > 0 ? <FontAwesomeIcon icon={faStar} style={{ color: '#ff9800', marginRight: '5%', height: '20px' }} /> : null}
                          {station.ingToAdd?.length ? <FontAwesomeIcon icon={faUserCheck} style={{ color: 'green', height: '20px', marginRight: '5%', height: '20px' }} /> : null}
                          {station.ingToRemove?.length ? <FontAwesomeIcon icon={faUserTimes} style={{ color: 'red', marginRight: '5%', height: '20px' }} /> : null}
                          {station.feedback2 ? <FontAwesomeIcon icon={faComment} style={{ marginRight: '5%', height: '20px', color: 'gray', height: '20px' }} /> : null}
                        </Nav.Link>
                      </Nav.Item>
                    </>
                  ) :
                    <p>No Data Found</p>
                  }
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content style={{}}>
                  {props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>
                      <Tab.Pane eventKey={station.user.firstname}>
                        <CheckLists>
                          <h4>{station.user.firstname + ' ' + station.user.lastname}</h4>
                          <Rating
                            value={station.rating}
                            readOnly
                            precision={0.5}
                          />
                          {/* <p><u>Request to ADD:</u> {station.ingToAdd?.length ? station.ingToAdd.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p>
                          <p><u>Request to REMOVE:</u> {station.ingToRemove?.length ? station.ingToRemove.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'red' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p> */}

                          <p><u>Category:</u> <h5> {station.mealCategory ? station.mealCategory : 'No Feedback'}</h5></p>
                          <p><u>Feedback:</u> <h5> {station.feedback2 ? station.feedback2 : 'No Feedback'}</h5></p>
                        </CheckLists>
                      </Tab.Pane>

                    </>
                  )}

                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>


        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => [setmodalShow(false), setmodalID(''), setmodalName(''), setmodalNoContent('')]}>Close</Button>
        </Modal.Footer>
      </Modal>
      {/*MEAL RANKING CONTAINER*/}
      <div className="mealrank-con">
        <CardTitle>
          <label>
            <h1 style={{ marginBottom: '2rem' }} >Meal Ranking</h1>
          </label>
          <ButtonTooltip text={'- Meals are Rank base on their overall ratings. \n- Give full attention to those that are in lower levels.\n- Click the meal card for other details of the meal. \n- Icons indicate if a meal has some feedback from the employees.'} title={'Meal Ranking'} />
        </CardTitle>


        {/*Choices*/}
        <div className="choices-con">
          <div className="con1">
            <h4>BREAKFAST</h4>
          </div>

          <div className="con2">
            <h4>LUNCH</h4>
          </div>

          <div className="con3">
            <h4>DINNER</h4>
          </div>

          <div className="con4">
            <h4>SNACKS</h4>
          </div>
        </div>

        {/*BEST*/}
        <div className="best-con">
          <div className="con1 best" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>BEST</h2>
            <h3 style={{ borderRadius: '10px', padding: '0.2rem', paddingLeft: '0.2rem', background: '#8B6C2F', color: 'white' }}>4.5+</h3>
          </div>

          <div className="con2">

            {props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'breakfast').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }


                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          {/* <div className="con2">
          <h5>LUMPIANG SHANGHAI</h5>
          <h4>4.9</h4>
        </div> */}

          <div className="con3">

            {props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con4">

            {props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'dinner').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con5">

            {props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4.5 && v.type === 'snack').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>
        </div>


        {/*Great*/}
        <div className="great-con">
          <div className="con1" style={{ backgroundColor: '#7ee8fa', backgroundImage: 'linear-gradient(315deg, #7ee8fa 0%, #80ff72 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>GREAT</h2>
            <h3 style={{ borderRadius: '10px', padding: '0.2rem', paddingLeft: '0.2rem', background: '#004027', color: 'white' }}>4.0+</h3>
          </div>

          <div className="con2" >

            {props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'breakfast').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con3">

            {props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con4">

            {props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'dinner').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con5">

            {props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 4 && v.rating <= 4.5 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>
        </div>

        {/*Ok*/}
        <div className="ok-con">
          <div className="con1" style={{ backgroundColor: '#000e3d', backgroundImage: 'linear-gradient(315deg, #abe9cd 0%, #3eadcf 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>OK</h2>
            <h3 style={{ borderRadius: '10px', padding: '0.2rem', paddingLeft: '0.2rem', background: '#000e3d', color: 'white' }}>3.5+</h3>
          </div>

          <div className="con2">

            {props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'breakfast').sort(compareRating).map(station2 =>
                <Cards key={station2._id} >
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con3">

            {props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con4">

            {props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'dinner').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con5">

            {props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.rating > 3.5 && v.rating <= 4.0 && v.type === 'snack').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>
        </div>

        {/*Worst*/}
        <div className="worst-con" >
          <div className="con1" style={{ backgroundColor: '##ffac81', backgroundImage: 'linear-gradient(315deg, #ffac81 0%, #ff928b 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>WORST</h2>
            <h3 style={{ borderRadius: '10px', padding: '0.2rem', paddingLeft: '0.2rem', background: '#380006', color: 'white' }}>3.5</h3>
          </div>

          <div className="con2">

            {props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'breakfast').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>

                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                  </div>


                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con3">

            {props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>


                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con4">

            {props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'dinner').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>

                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con5">

            {props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.rating <= 3.5 && v.rating !== 0 && v.type === 'snack').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>

                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>
        </div>
        {/*Unset*/}
        <div className="notrated-con">
          <div className="con1" style={{ backgroundColor: '#5b6467', backgroundImage: 'linear-gradient(315deg, #5b6467 0%, #8b939a 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>Not Rated</h2>

          </div>

          <div className="con2">

            {props.allTimeFav.filter(v => v.rating === 0 && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.rating === 0 && v.type === 'breakfast').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <p>{station2?.ingToRemove?.length}</p>

                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>

                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con3">

            {props.allTimeFav.filter(v => v.rating === 0 && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.rating === 0 && v.type === 'lunch').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>

                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con4">

            {props.allTimeFav.filter(v => v.rating === 0 && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.rating === 0 && v.type === 'dinner').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>

          <div className="con5">

            {props.allTimeFav.filter(v => v.rating === 0 && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.rating === 0 && v.type === 'snack').sort(compareRating).map(station2 =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br></br>
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed2 ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }

                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }

          </div>
        </div>
      </div>

      <br />

    </React.Fragment >
  );
}
