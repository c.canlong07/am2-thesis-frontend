import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import noDataFound from '../img/no-data-found.png';
import { Card, Modal, Button, Tab, Row, Nav, Col, OverlayTrigger, Popover, Offcanvas } from "react-bootstrap";
import { Cards } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import moment from 'moment';
import { faUserCheck, faUserPlus, faUserTimes, faComment, faStar } from "@fortawesome/free-solid-svg-icons";
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import { OthersButton } from '../globalFunctions/shareableComponents';
import { Line } from '../styles/DashboardLayout'
import dottedline from '../img/dottedline.svg';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import { ButtonTooltip } from '../globalFunctions/shareableComponents';
import { MainLayout } from '../styles/DashboardLayout';
import styled from 'styled-components';

export default function LastWeekInRecommendation(props) {
  // const [allTimeFav, setallTimeFav] = useState({});
  const [modalShow, setmodalShow] = useState(false);
  const [modalID, setmodalID] = useState('');
  const [modalName, setmodalName] = useState('');
  const [modalNoContent, setmodalNoContent] = useState('');
  const [weeklyData, setweeklyData] = useState([]);
  const [show, setShow] = useState(false);
  const [modalShow2, setModalShow2] = React.useState(false);
  const [otherMealID, setotherMealID] = React.useState([]);
  const [otherMealData, setotherMealData] = React.useState([]);
  const [otherMealCondition, setotherMealCondition] = React.useState([]);
  const [onOff, setonOff] = React.useState(true);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const getLastWeek = () => {
    var today = new Date();

    const date = new Date();
    const tod = date.getDate();
    const dayOfTheWeek = date.getDay();
    const newDate = date.setDate(tod - dayOfTheWeek - 7);
    var lastWeek = new Date(new Date(newDate).getFullYear(), new Date(newDate).getMonth(), new Date(newDate).getDate());

    return lastWeek;
  }
  useEffect(() => {
    let theWeekly = getLastWeek();
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
      params: {
        startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
        endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
      }
    })
      .then(res => {

        let sumData = {};
        let ratingData = res.data;

        for (let element of ratingData) {
          if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
            sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
            sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealType = element.meal.mealType;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealDesc = element.meal.mealDesc;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealIng = element.meal.mealIng;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealServedEst = element.meal.mealServedEst;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealTotalPrice = element.meal.mealTotalPrice;
            sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
            sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
          } else {
            sumData[element.meal.mealName + '+' + element.meal.mealType] = {
              sum: element.rating ? element.rating : 0,
              n: 1,
              mealType: element.meal.mealType,
              mealDesc: element.meal.mealDesc,
              mealIng: element.meal.mealIng,
              mealServedEst: element.meal.mealServedEst,
              mealTotalPrice: element.meal.mealTotalPrice,
              date: element.meal.mealDate,
              day: element.meal.mealDate,
              feed: element.feedback,
              _id: element._id
            };
          }
        }

        let averageData = [];

        for (let element of Object.keys(sumData)) {
          averageData.push({
            mealName: element.split('+')[0],
            users: sumData[element].n,
            rating: sumData[element].sum / sumData[element].n,
            mealType: sumData[element].mealType,
            mealIng: sumData[element].mealIng,
            mealDesc: sumData[element].mealDesc,
            mealServedEst: sumData[element].mealServedEst,
            mealTotalPrice: sumData[element].mealTotalPrice,
            date: moment(sumData[element].date).format('ddd, MMM DD YYYY'),
            day: moment(sumData[element].date).format('dddd'),
            feed: sumData[element].feed,
            _id: sumData[element]._id,
          });
        }



        // this.setState({ allTimeFav: averageData })
        setweeklyData(averageData);

        //   setallTimeFav(averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)));

      })
      .catch(error => {
        console.log(error);
      });
  }, [])

  const otherRecommendation = (allTime, condition, id) => {
    setModalShow2(true);
    setotherMealData(allTime);
    setotherMealID(id);
    setotherMealCondition(condition);
  }

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }

  useEffect(() => {
    if (onOff) {
      document.getElementById("recommendations").style.display = "none";
    } else {
      document.getElementById("recommendations").style.display = "block";

    }
  }, [onOff])

  return (
    <>
      <>
        <MainLayout>
          <div className="" style={{ marginRight: '4rem', display: 'flex', flexDirection: 'column' }}>
            <div className="container" style={{ padding: '0', width: 'auto' }}>
              <Modal
                show={modalShow2}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                scrollable='true'
              >
                <Modal.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >
                  <Modal.Title id="contained-modal-title-vcenter">
                    Other Meals
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {otherMealData.filter(v => v.day === otherMealCondition[0] && v.mealType === otherMealCondition[1]).length !== 0 ?
                    props.allTimeFav.filter(v => v.day === otherMealCondition[0] && v.mealType === otherMealCondition[1]).sort(compareRating).map((station2, i) =>
                      station2._id !== otherMealID ?
                        <>
                          <Cards style={{ margin: 'auto', width: '60%', textAlign: 'center', }}>
                            <div onClick={() => props.handleOnClick(station2)}>
                              <h5 className="cardTitle" style={{ background: 'rgb(203 155 22)', color: 'white' }}>{station2.mealName}</h5>
                              <Rating
                                size="large"
                                value={station2.rating}
                                readOnly
                                precision={0.5}
                              /><br></br>

                              {
                                station2?.ingToRemove?.length ?
                                  <>
                                    <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '1%', height: '20px' }} />
                                  </>
                                  : null
                              }

                              {
                                station2?.ingToAdd?.length ?
                                  <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                              }

                              {
                                station2.feed ?
                                  <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                              }
                              <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                            </div>
                          </Cards><br></br>
                        </>
                        :
                        <>
                          {/* <Card.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >Meal Recommended</Card.Header> */}
                          <Divider style={{ marginBottom: '3%' }}>
                            <Chip label="Meal Recommended" />
                          </Divider>
                          <Card.Body>
                            <Cards style={{ margin: 'auto', width: '70%', textAlign: 'center' }}>
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  size="large"
                                  value={station2.rating}
                                  readOnly
                                  precision={0.5}
                                /><br></br>

                                {
                                  station2?.ingToRemove?.length ?
                                    <>
                                      <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '1%', height: '20px' }} />
                                    </>
                                    : null
                                }

                                {
                                  station2?.ingToAdd?.length ?
                                    <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                }

                                {
                                  station2.feed ?
                                    <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                }
                                <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                              </div>
                            </Cards>
                          </Card.Body>
                          {/* <Line style={{ marginBottom: '3%' }}></Line> */}
                          {/* <Card.Header style={{ background: 'rgb(75 86 114)', color: 'white', marginBottom: '3%', }} >Other Meals</Card.Header> */}
                          <Divider style={{ marginBottom: '3%' }}>
                            <Chip label="Other Meals" />
                          </Divider>
                        </>
                    )
                    : <Cards><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
                  }
                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={() => setModalShow2(false)}>Close</Button>
                </Modal.Footer>
              </Modal>

              <Modal
                show={modalShow}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                scrollable='true'
              >
                <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }} >
                  <Modal.Title id="contained-modal-title-vcenter">
                    {!modalNoContent ? `Feedback for ${modalName}` : 'Feedback'}
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {props.allTimeFav.filter(v => v.mealID === modalID).length !== 0 ?
                    props.allTimeFav.filter(v => v.mealID === modalID).sort(compareRating).map((station2, i) =>

                      <div>
                        <Card.Header style={{ marginBottom: '4%', background: 'none', border: 'none', paddingBottom: '0px' }}>
                          <div className="grid-container">
                            <div className="grid-item"><Rating
                              name={station2.mealName}
                              precision={0.5}
                              size="large"
                              value={station2.rating}
                              readOnly
                            /> </div>
                            <div className="grid-item">{station2.rating !== 0 ? station2.rating.toFixed(2) : 0}</div>
                            <div className="grid-item">{station2.date}</div>

                          </div>
                        </Card.Header>

                      </div>
                    )
                    : null
                  }

                  <Divider style={{ marginBottom: '3%' }}>
                    <Chip label="Employees Feedback" />
                  </Divider>
                  <Tab.Container id="left-tabs-example" defaultActiveKey={props.modalMeals.filter(v => v.meal._id === modalID)?.[0]?.user.firstname}>
                    <Row>
                      <Col sm={3}>
                        <Nav variant="pills" className="flex-column">
                          {!modalNoContent ? props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                            <>

                              <Nav.Item>
                                <Nav.Link eventKey={station.user.firstname}>{station.user.firstname + ' ' + station.user.lastname} <br />
                                  {station.rating > 0 ? <FontAwesomeIcon icon={faStar} style={{ color: '#ff9800', marginRight: '5%' }} /> : null}
                                  {station.ingToRemove?.length ? <FontAwesomeIcon icon={faUserTimes} style={{ color: 'red', marginRight: '5%' }} /> : null}
                                  {station.ingToAdd?.length ? <FontAwesomeIcon icon={faUserCheck} style={{ color: 'green', marginRight: '5%' }} /> : null}
                                  {station.feedback ? <FontAwesomeIcon icon={faComment} style={{ marginRight: '5%' }} /> : null}
                                </Nav.Link>
                              </Nav.Item>
                            </>
                          ) :
                            <p>No Data Found</p>
                          }
                        </Nav>
                      </Col>
                      <Col sm={9}>
                        <Tab.Content>
                          {props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                            <>
                              <Tab.Pane eventKey={station.user.firstname}>
                                <h4>{station.user.firstname + ' ' + station.user.lastname}</h4>
                                <Rating
                                  value={station.rating}
                                  readOnly
                                />
                                {/* <p>Request to ADD: {station.ingToAdd?.length ? station.ingToAdd : 'No Ingredients wanted to be ADD'}</p>
                                <p>Request to REMOVE: {station.ingToRemove?.length ? station.ingToRemove : 'No Ingredients wanted to be REMOVE'}</p> */}

                                <p>CATEGORY: {station.mealCategory ? station.mealCategory : 'No mealCategory'}</p>
                                <p>FEEDBACK: {station.feedback ? station.feedback : 'No Feedback'}</p>

                              </Tab.Pane>

                            </>
                          )}

                        </Tab.Content>
                      </Col>
                    </Row>
                  </Tab.Container>


                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={() => [setmodalShow(false), setmodalID(''), setmodalName(''), setmodalNoContent('')]}>Close</Button>
                </Modal.Footer>
              </Modal>

              {/*DAILY RANKING CONTAINER*/}
              <div className="dailyrank-con" style={{ marginTop: 0 }}>
                <label><h2 >Last Week Meals & Recommendation
                </h2>
                </label>

                <ButtonTooltip text={'- Display the last week meal (first meal) and recommended meal (below the first meal) for each day \n- Click the meal card for other details of the meal. \n- Icons indicate if a meal has some feedback from the employees. \n- Click the ... button in the Recommended Meal to see Other Meals'} title={'Last Week Meals and Recommendation'} />
                <BootstrapSwitchButton style={{ margin: '13%' }} checked={true} width={100}
                  onlabel='Show'
                  offlabel='Hide'
                  onChange={(checked) => {
                    setonOff(checked);
                  }} />

                {/*Choices*/}
                <div id="recommendations">
                  <div className="choices-con">
                    <div className="con1">
                      <h2>MONDAY</h2>
                    </div>

                    <div className="con2">
                      <h2>TUESDAY</h2>
                    </div>

                    <div className="con3">
                      <h2>WEDNESDAY</h2>
                    </div>

                    <div className="con4">
                      <h2>THURSDAY</h2>
                    </div>

                    <div className="con5">
                      <h2>FRIDAY</h2>
                    </div>
                  </div>

                  {/*BREAKFAST*/}
                  <div className="breakfast-con">
                    <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
                      <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>BREAKFAST</h2>

                    </div>

                    <div className="con2">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Monday" && v.mealType === 'breakfast').length !== 0 ?
                          weeklyData.filter(v => v.day === "Monday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?

                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />
                              </div> : null

                          )


                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        {/* <OverlayTrigger
                trigger="click"
                key={'right'}
                placement={'right'}
                overlay={
                  <Popover id={'...'}>
                    <Popover.Header as="h3">{'...'}</Popover.Header>
                    <Popover.Body>
                      <strong>Holy guacamole!</strong> Check this info.
                    </Popover.Body>
                  </Popover>
                }
              >
                <Button variant="secondary">...</Button>
              </OverlayTrigger> */}
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'breakfast').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'breakfast').sort(compareRating)}
                                  condition={['Monday', 'breakfast']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }

                      </Cards>
                    </div>


                    <div className="con3">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').length !== 0 ?
                          weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />
                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').sort(compareRating)}
                                  condition={['Tuesday', 'breakfast']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>

                    </div>

                    <div className="con4">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').length !== 0 ?
                          weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').sort(compareRating)}
                                  condition={['Wednesday', 'breakfast']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con5">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').length !== 0 ?
                          weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').sort(compareRating)}
                                  condition={['Thursday', 'breakfast']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con6">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Friday" && v.mealType === 'breakfast').length !== 0 ?
                          weeklyData.filter(v => v.day === "Friday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'breakfast').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'breakfast').sort(compareRating)}
                                  condition={['Friday', 'breakfast']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>
                  </div>


                  {/*LUNCH*/}
                  <div className="lunch-con">
                    <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
                      <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>LUNCH</h2>

                    </div>

                    <div className="con2">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Monday" && v.mealType === 'lunch').length !== 0 ?
                          weeklyData.filter(v => v.day === "Monday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?


                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'lunch').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'lunch').sort(compareRating)}
                                  condition={['Monday', 'lunch']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con3">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').length !== 0 ?
                          weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').sort(compareRating)}
                                  condition={['Tuesday', 'lunch']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con4">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').length !== 0 ?
                          weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').sort(compareRating)}
                                  condition={['Wednesday', 'lunch']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con5">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'lunch').length !== 0 ?
                          weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'lunch').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'lunch').sort(compareRating)}
                                  condition={['Thursday', 'lunch']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con6">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Friday" && v.mealType === 'lunch').length !== 0 ?
                          weeklyData.filter(v => v.day === "Friday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'lunch').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'lunch').sort(compareRating)}
                                  condition={['Friday', 'lunch']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>
                  </div>


                  {/*DINNER*/}
                  <div className="dinner-con" >
                    <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
                      <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>DINNER</h2>

                    </div>

                    <div className="con2">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Monday" && v.mealType === 'dinner').length !== 0 ?
                          weeklyData.filter(v => v.day === "Monday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>

                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'dinner').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'dinner').sort(compareRating)}
                                  condition={['Monday', 'dinner']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con3">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').length !== 0 ?
                          weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').sort(compareRating)}
                                  condition={['Tuesday', 'dinner']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con4">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').length !== 0 ?
                          weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').sort(compareRating)}
                                  condition={['Wednesday', 'dinner']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con5">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'dinner').length !== 0 ?
                          weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'dinner').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'dinner').sort(compareRating)}
                                  condition={['Thursday', 'dinner']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con6">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Friday" && v.mealType === 'dinner').length !== 0 ?
                          weeklyData.filter(v => v.day === "Friday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'dinner').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'dinner').sort(compareRating)}
                                  condition={['Friday', 'dinner']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>
                  </div>


                  {/*SNACK*/}
                  <div className="dinner-con" >
                    <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
                      <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>SNACKS</h2>

                    </div>

                    <div className="con2">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Monday" && v.mealType === 'snack').length !== 0 ?
                          weeklyData.filter(v => v.day === "Monday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'snack').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'snack').sort(compareRating)}
                                  condition={['Monday', 'snack']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con3">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'snack').length !== 0 ?
                          weeklyData.filter(v => v.day === "Tuesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'snack').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'snack').sort(compareRating)}
                                  condition={['Tuesday', 'snack']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con4">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'snack').length !== 0 ?
                          weeklyData.filter(v => v.day === "Wednesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'snack').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'snack').sort(compareRating)}
                                  condition={['Wednesday', 'snack']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con5">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'snack').length !== 0 ?
                          weeklyData.filter(v => v.day === "Thursday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'snack').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'snack').sort(compareRating)}
                                  condition={['Thursday', 'snack']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>

                    <div className="con6">
                      <Cards>
                        {weeklyData.filter(v => v.day === "Friday" && v.mealType === 'snack').length !== 0 ?
                          weeklyData.filter(v => v.day === "Friday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <div onClick={() => props.handleOnClick(station2)}>
                                <h5 className="cardTitle">{station2.mealName}</h5>
                                <Rating
                                  name={station2.mealName}
                                  precision={0.5}
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                />

                              </div> : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                      <Cards>
                        <h6>Recommendation</h6>
                        {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'snack').length !== 0 ?
                          props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                            i === 0 ?
                              <>
                                <div onClick={() => props.handleOnClick(station2)}>
                                  <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.mealName}</h5>
                                  <Rating
                                    name="size-small"
                                    size="small"
                                    value={station2.rating}
                                    readOnly
                                    precision={0.5}
                                    style={{ fontSize: '2px' }}
                                  /><br></br>
                                  {
                                    station2?.ingToRemove?.length ?
                                      <>
                                        <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                                      </>
                                      : null
                                  }
                                  {
                                    station2?.ingToAdd?.length ?
                                      <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                                  }
                                  {
                                    station2.feed ?
                                      <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                                  }
                                  <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                                </div>

                                <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'snack').sort(compareRating)}
                                  condition={['Friday', 'snack']}
                                  id={station2._id}
                                  otherRecommendation={otherRecommendation} />

                              </>
                              : null

                          )
                          : <div><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
                        }
                      </Cards>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <br />
        </MainLayout>
      </>
    </ >
  );
}
const DashboardStyled = styled.div`
    background-color: #f1f1f1;
    margin-top: 0;
    padding-top: 0;
    

  
 
    .dashBoardText{
        padding-top: 1rem;
        margin-bottom: 2%;
    }
    .dashBoardText h2{
        font-size: 5vh;
    }

        .workers-card{
            height: 30vh;
            width: 22vw;
            background-color: red;
            border-radius: 20px;
        }

        .chart{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius: 50px 0px 0px 50px;
        }

        .chart2{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 50px 50px 0px;
        }

        .chart3{
            padding-bottom: 1rem;
            height: 50vh;
            background-color: #b5b5b5;
            border-radius:  0px 0px 0px 0px;
        }

        @media (max-width: 450px){
            .chart{
                padding: 1rem;
            }
        }

        .attribution { 
          font-size: 11px; text-align: center; 
      }
      .attribution a { 
          color: hsl(228, 45%, 44%); 
      }
      
      h1:first-of-type {
          font-weight: var(--weight1);
          color: var(--varyDarkBlue);
      
      }
      
      h1:last-of-type {
          color: var(--varyDarkBlue);
      }
      
      @media (max-width: 400px) {
          h1 {
              font-size: 1.5rem;
          }
      }
      
      .header {
          text-align: center;
          line-height: 0.8;
          margin-bottom: 50px;
          margin-top: 100px;
      }
      
      .header p {
          margin: 0 auto;
          line-height: 2;
          color: var(--grayishBlue);
      }
      
      
      .box p {
          color: var(--grayishBlue);
      }
      
      .box {
          border-radius: 5px;
          box-shadow: 0px 30px 40px -20px var(--grayishBlue);
          padding: 30px;
          margin: 20px;  
      }
      
      img {
          float: right;
      }
      
      @media (max-width: 450px) {
          .box {
              height: 200px;
          }
      }
      
      @media (max-width: 950px) and (min-width: 450px) {
          .box {
              text-align: center;
              height: 180px;
          }
      }
      
      .cyan {
          border-top: 3px solid var(--cyan);
          background-color: white;
      }
      .red {
          border-top: 3px solid var(--red);
          background-color: white;
      }
      .blue {
          border-top: 3px solid var(--blue);
          background-color: white;
      }
      .orange {
          border-top: 3px solid var(--orange);
      }
      
      h2 {
          color: var(--varyDarkBlue);
          font-weight: var(--weight3);
      }
      
      
      @media (min-width: 950px) {
          .row1-container {
              display: flex;
              justify-content: center;
              align-items: center;
          }
          
          .row2-container {
              display: flex;
              justify-content: center;
              align-items: center;
          }
          .box-down {
              position: relative;
              top: 150px;
          }
          .box {
              width: 28%;
           
          }
          .header p {
              width: 30%;
          }
          
      }
      .header {
        display: flex;
      }
      
      .item {
        width: 100%;
      }
      
      
      /* Demo styles, for aesthetics. */
      
      .demo {
        margin: 3rem;
      }
      
      .demo .item {
        text-align: center;
        padding: 3rem;
        background-color: #eee;
        margin: 0 1.5rem;
      }
    
    .slider {
      width: 100%;
      text-align: center;
      overflow: hidden;
    }
    
    .slides {
      display: flex;
      
      overflow-x: auto;
      scroll-snap-type: x mandatory;
      
      
      
      scroll-behavior: smooth;
      -webkit-overflow-scrolling: touch;
      
      /*
      scroll-snap-points-x: repeat(300px);
      scroll-snap-type: mandatory;
      */
    }
    .slides::-webkit-scrollbar {
      width: 10px;
      height: 10px;
    }
    .slides::-webkit-scrollbar-thumb {
      background: black;
      border-radius: 10px;
    }
    .slides::-webkit-scrollbar-track {
      background: transparent;
    }
    .slides > div {
      scroll-snap-align: start;
      flex-shrink: 0;
      width: 300px;
      height: 300px;
      margin-right: 50px;
      border-radius: 10px;
      background: #eee;
      transform-origin: center center;
      transform: scale(1);
      transition: transform 0.5s;
      position: relative;
      
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 100px;
    }
    .slides > div:target {
    /*   transform: scale(0.8); */
    }
    .author-info {
      background: rgba(0, 0, 0, 0.75);
      color: white;
      padding: 0.75rem;
      text-align: center;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      margin: 0;
    }
    .author-info a {
      color: white;
    }
    img {
      object-fit: cover;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    
    .slider > a {
      display: inline-flex;
      width: 1.5rem;
      height: 1.5rem;
      background: white;
      text-decoration: none;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      margin: 0 0 0.5rem 0;
      position: relative;
    }
    .slider > a:active {
      top: 1px;
    }
    .slider > a:focus {
      background: #000;
    }
    
    /* Don't need button navigation */
    @supports (scroll-snap-type) {
      .slider > a {
        display: none;
      }
    }
    
    html, body {
      height: 100%;
      overflow: hidden;

    }
    body {
      display: flex;
      align-items: center;
      justify-content: center;
      background: linear-gradient(to bottom, #74ABE2, #5563DE);
      font-family: 'Ropa Sans', sans-serif;
      
    }

    /* The container2 */
.container2 {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.modal-dialog{
  transform: translateY(30px) !important;
}
`;