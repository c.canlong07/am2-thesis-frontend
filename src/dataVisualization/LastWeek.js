import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import { Line } from '../styles/DashboardLayout';
import noDataFound from '../img/no-data-found.png';
import { Row, Tabs, Tab, Table, Nav, Col, ListGroup, Card, Button } from "react-bootstrap";
export default function LastWeek(props) {
  const [allTimeFav, setallTimeFav] = useState({});

  useEffect(() => {
    setallTimeFav(props.allTimeFav);
  }, [props.allTimeFav]);

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }

  return (
    <React.Fragment>
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
        <Row>
          <Col sm={3}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="first">Monday</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="second">Tuesday</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="third">Wednesday</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="fourth">Thursday</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="fifth">Friday</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={9}>
            <Tab.Content>
              <Tab.Pane eventKey="first">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="breakfast" title="Breakfast">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Breakfast to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="lunch" title="Lunch">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'lunch').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Lunch to Display</div>
                      }
                    </div>
                  </Tab>
                  <Tab eventKey="dinner" title="Dinner">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'dinner').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Dinner to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="snacks" title="Snack">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'snack').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Monday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Snack to Display</div>
                      }
                    </div>

                  </Tab>
                </Tabs>
              </Tab.Pane>
              {/* Tuesday */}
              <Tab.Pane eventKey="second">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="breakfast" title="Breakfast">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Breakfast to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="lunch" title="Lunch">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Lunch to Display</div>
                      }
                    </div>
                  </Tab>
                  <Tab eventKey="dinner" title="Dinner">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Dinner to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="snacks" title="Snack">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'snack').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Tuesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Snack to Display</div>
                      }
                    </div>

                  </Tab>
                </Tabs>
              </Tab.Pane>

              {/* Wednesday */}
              <Tab.Pane eventKey="third">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="breakfast" title="Breakfast">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Breakfast to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="lunch" title="Lunch">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Lunch to Display</div>
                      }
                    </div>
                  </Tab>
                  <Tab eventKey="dinner" title="Dinner">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Dinner to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="snacks" title="Snack">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'snack').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Wednesday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Snack to Display</div>
                      }
                    </div>

                  </Tab>
                </Tabs>
              </Tab.Pane>
              {/* Thursday */}
              <Tab.Pane eventKey="fourth">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="breakfast" title="Breakfast">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Breakfast to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="lunch" title="Lunch">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'lunch').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Lunch to Display</div>
                      }
                    </div>
                  </Tab>
                  <Tab eventKey="dinner" title="Dinner">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'dinner').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Dinner to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="snacks" title="Snack">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'snack').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Thursday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Snack to Display</div>
                      }
                    </div>

                  </Tab>
                </Tabs>
              </Tab.Pane>

              {/* Friday */}
              <Tab.Pane eventKey="fifth">
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="breakfast" title="Breakfast">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'breakfast').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Breakfast to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="lunch" title="Lunch">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'lunch').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'lunch').sort(compareRating).map((station2, i) =>

                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>

                          </>

                        )
                        : <div>No Lunch to Display</div>
                      }
                    </div>
                  </Tab>
                  <Tab eventKey="dinner" title="Dinner">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'dinner').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'dinner').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Dinner to Display</div>
                      }
                    </div>

                  </Tab>
                  <Tab eventKey="snacks" title="Snack">
                    <div className="grid-container" style={{ backgroundColor: 'unset' }}>
                      {props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'snack').length !== 0 ?
                        props.allTimeFav.filter(v => v.day === "Friday" && v.mealType === 'snack').sort(compareRating).map((station2, i) =>
                          <>
                            <Card onClick={() => props.handleOnClick(station2)} className="m-2">
                              <Card.Header>{station2.mealName.split('+')[0]}</Card.Header>
                              <Card.Body>
                                <Card.Title><Rating
                                  size="small"
                                  value={station2.rating}
                                  readOnly
                                /></Card.Title>
                                <Card.Text>
                                  {station2.rating !== 0 ? station2.rating.toFixed(2) : 0}
                                </Card.Text>
                                <Card.Text>
                                  {station2.date}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </>
                        )
                        : <div>No Snack to Display</div>
                      }
                    </div>

                  </Tab>
                </Tabs>
              </Tab.Pane>

            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>

    </React.Fragment >
  );
}
