import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import noDataFound from '../img/no-data-found.png';
import { Card, Modal, Button, Tab, Row, Nav, Col, OverlayTrigger, Popover, Offcanvas } from "react-bootstrap";
import { Cards, CardTitle, CheckLists } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import moment from 'moment';
import { faUserCheck, faUserPlus, faUserTimes, faComment, faStar } from "@fortawesome/free-solid-svg-icons";
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import { OthersButton } from '../globalFunctions/shareableComponents';
import { Line } from '../styles/DashboardLayout'
import dottedline from '../img/dottedline.svg';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import { ButtonTooltip } from '../globalFunctions/shareableComponents';

export default function LastWeekInRecommendation(props) {
  const [allTimeFav, setallTimeFav] = useState({});
  const [modalShow, setmodalShow] = useState(false);
  const [modalID, setmodalID] = useState('');
  const [modalName, setmodalName] = useState('');
  const [modalNoContent, setmodalNoContent] = useState('');
  const [weeklyData, setweeklyData] = useState([]);
  const [show, setShow] = useState(false);
  const [modalShow2, setModalShow2] = React.useState(false);
  const [otherMealID, setotherMealID] = React.useState([]);
  const [otherMealData, setotherMealData] = React.useState([]);
  const [otherMealCondition, setotherMealCondition] = React.useState([]);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const getLastWeek = () => {
    var today = new Date();

    const date = new Date();
    const tod = date.getDate();
    const dayOfTheWeek = date.getDay();
    const newDate = date.setDate(tod - dayOfTheWeek - 7);
    var lastWeek = new Date(new Date(newDate).getFullYear(), new Date(newDate).getMonth(), new Date(newDate).getDate());

    return lastWeek;
  }
  useEffect(() => {
    let theWeekly = getLastWeek();
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
      params: {
        startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
        endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
      }
    })
      .then(res => {

        let sumData = {};
        let ratingData = res.data;
        for (let element of ratingData) {
          if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
            sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
            sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
            sumData[element.meal.mealName + '+' + element.meal.mealType].type = element.meal.mealType;
            sumData[element.meal.mealName + '+' + element.meal.mealType].mealID = element.meal._id;
            sumData[element.meal.mealName + '+' + element.meal.mealType].ingToAdd = element.ingToAdd;
            sumData[element.meal.mealName + '+' + element.meal.mealType].ingToRemove = element.ingToRemove;
            sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
            sumData[element.meal.mealName + '+' + element.meal.mealType].feed2 = element.feedback2;
            sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
          } else {
            sumData[element.meal.mealName + '+' + element.meal.mealType] = {
              sum: element.rating ? element.rating : 0,
              n: 1,
              type: element.meal.mealType,
              mealID: element.meal._id,
              date: element.meal.mealDate,
              day: element.meal.mealDate,
              feed: element.feedback2,
              _id: element._id,
              ingToAdd: element.ingToAdd,
              ingToRemove: element.ingToRemove,
            };
          }
        }

        let averageData = [];

        for (let element of Object.keys(sumData)) {
          averageData.push({
            name: element,
            users: sumData[element].n,
            rating: sumData[element].sum / sumData[element].n,
            type: sumData[element].type,
            mealID: sumData[element].mealID,
            date: moment(sumData[element].date).format('MM/DD/YYYY'),
            day: moment(sumData[element].date).format('dddd'),
            feed: sumData[element].feed2,
            _id: sumData[element]._id,
            ingToAdd: sumData[element].ingToAdd,
            ingToRemove: sumData[element].ingToRemove,
          });
        }


        // this.setState({ allTimeFav: averageData })
        setweeklyData(averageData);

        //   setallTimeFav(averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)));

      })
      .catch(error => {
        console.log(error);
      });
  }, [])

  const otherRecommendation = (allTime, condition, id) => {
    setModalShow2(true);
    setotherMealData(allTime);
    setotherMealID(id);
    setotherMealCondition(condition);
  }

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }


  return (
    <React.Fragment>
      <Modal
        show={modalShow2}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable='true'
      >
        <Modal.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >
          <Modal.Title id="contained-modal-title-vcenter">
            Other Meals
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {otherMealData.filter(v => v.day === otherMealCondition[0] && v.type === otherMealCondition[1]).length !== 0 ?
            props.allTimeFav.filter(v => v.day === otherMealCondition[0] && v.type === otherMealCondition[1]).sort(compareRating).map((station2, i) =>
              station2._id !== otherMealID ?
                <>
                  <Cards key={station2._id} style={{ margin: 'auto', width: '60%', textAlign: 'center', }}>
                    <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle" style={{ background: 'rgb(203 155 22)', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                      <Rating
                        size="large"
                        value={station2.rating}
                        readOnly
                        precision={0.5}
                      /><br></br>

                      {
                        station2?.ingToRemove?.length ?
                          <>
                            <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '1%', height: '20px' }} />
                          </>
                          : null
                      }

                      {
                        station2?.ingToAdd?.length ?
                          <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                      }

                      {
                        station2.feed2 ?
                          <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                      }
                      <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                    </div>
                  </Cards><br></br>
                </>
                :
                <>
                  {/* <Card.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >Meal Recommended</Card.Header> */}
                  <Divider style={{ marginBottom: '3%' }}>
                    <Chip label="Meal Recommended" />
                  </Divider>
                  <Card.Body key={station2._id}>
                    <Cards style={{ margin: 'auto', width: '70%', textAlign: 'center' }}>
                      <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                        <Rating
                          size="large"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                        /><br></br>

                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '1%', height: '20px' }} />
                            </>
                            : null
                        }

                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }

                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>
                    </Cards>
                  </Card.Body>
                  {/* <Line style={{ marginBottom: '3%' }}></Line> */}
                  {/* <Card.Header style={{ background: 'rgb(75 86 114)', color: 'white', marginBottom: '3%', }} >Other Meals</Card.Header> */}
                  <Divider style={{ marginBottom: '3%' }}>
                    <Chip label="Other Meals" />
                  </Divider>
                </>
            )
            : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setModalShow2(false)}>Close</Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={modalShow}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable='true'
      >
        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }} >
          <Modal.Title id="contained-modal-title-vcenter">
            {!modalNoContent ? `Feedback for ${modalName}` : 'Feedback'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.allTimeFav.filter(v => v.mealID === modalID).length !== 0 ?
            props.allTimeFav.filter(v => v.mealID === modalID).sort(compareRating).map((station2, i) =>
              <div key={station2._id}>
                <Card.Header style={{ marginBottom: '4%', background: 'none', border: 'none', paddingBottom: '0px' }}>
                  <div className="grid-container">
                    <div className="grid-item"><Rating
                      name={station2.name}
                      precision={0.5}
                      size="large"
                      value={station2.rating}
                      readOnly
                    /> </div>
                    <div className="grid-item">{station2.rating !== 0 ? station2.rating.toFixed(2) : 0}</div>
                    <div className="grid-item">{station2.date}</div>

                  </div>
                </Card.Header>

              </div>
            )
            : null
          }

          <Divider style={{ marginBottom: '3%' }}>
            <Chip label="Employees Feedback" />
          </Divider>
          <Tab.Container id="left-tabs-example" defaultActiveKey={props.modalMeals.filter(v => v.meal._id === modalID)?.[0]?.user.firstname}>
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  {!modalNoContent ? props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>
                      <Nav.Item key={station._id}>
                        <Nav.Link eventKey={station.user.firstname}>{station.user.firstname + ' ' + station.user.lastname} <br />
                          {station.rating > 0 ? <FontAwesomeIcon icon={faStar} style={{ color: '#ff9800', marginRight: '5%' }} /> : null}
                          {station.ingToRemove?.length ? <FontAwesomeIcon icon={faUserTimes} style={{ color: 'red', marginRight: '5%' }} /> : null}
                          {station.ingToAdd?.length ? <FontAwesomeIcon icon={faUserCheck} style={{ color: 'green', marginRight: '5%' }} /> : null}
                          {station.feedback2 ? <FontAwesomeIcon icon={faComment} style={{ marginRight: '5%' }} /> : null}
                        </Nav.Link>
                      </Nav.Item>
                    </>
                  ) :
                    <p>No Data Found</p>
                  }
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  {props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>
                      <Tab.Pane key={station._id} eventKey={station.user.firstname}>
                        <CheckLists>
                          <h4>{station.user.firstname + ' ' + station.user.lastname}</h4>
                          <Rating
                            value={station.rating}
                            readOnly
                            precision={0.5}
                          />
                          {/* <p><u>Request to ADD:</u> {station.ingToAdd?.length ? station.ingToAdd.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p>
                          <p><u>Request to REMOVE:</u> {station.ingToRemove?.length ? station.ingToRemove.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'red' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p> */}

                          <p><u>Category:</u> <h5> {station.mealCategory ? station.mealCategory : 'No Feedback'}</h5></p>
                          <p><u>Feedback:</u> <h5> {station.feedback2 ? station.feedback2 : 'No Feedback'}</h5></p>
                        </CheckLists>
                      </Tab.Pane>

                    </>
                  )}

                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>


        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => [setmodalShow(false), setmodalID(''), setmodalName(''), setmodalNoContent('')]}>Close</Button>
        </Modal.Footer>
      </Modal>

      {/*DAILY RANKING CONTAINER*/}
      <div className="dailyrank-con">
        <CardTitle>
          <label><h1>Last Week Meals & Recommendation
          </h1>
          </label>
          <ButtonTooltip text={'- Display the last week meal (first meal) and recommended meal (below the first meal) for each day \n- Click the meal card for other details of the meal. \n- Icons indicate if a meal has some feedback from the employees. \n- Click the ... button in the Recommended Meal to see Other Meals'} title={'Last Week Meals and Recommendation'} />
        </CardTitle>




        {/*Choices*/}
        <div className="choices-con">
          <div className="con1">
            <h2>MONDAY</h2>
          </div>

          <div className="con2">
            <h2>TUESDAY</h2>
          </div>

          <div className="con3">
            <h2>WEDNESDAY</h2>
          </div>

          <div className="con4">
            <h2>THURSDAY</h2>
          </div>

          <div className="con5">
            <h2>FRIDAY</h2>
          </div>
        </div>

        {/*BREAKFAST*/}
        <div className="breakfast-con">
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>BREAKFAST</h2>

          </div>

          <div className="con2">
            <Cards>
              {weeklyData.filter(v => v.day === "Monday" && v.type === 'breakfast').length !== 0 ?
                weeklyData.filter(v => v.day === "Monday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />
                    </div> : null

                )


                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              {/* <OverlayTrigger
                trigger="click"
                key={'right'}
                placement={'right'}
                overlay={
                  <Popover id={'...'}>
                    <Popover.Header as="h3">{'...'}</Popover.Header>
                    <Popover.Body>
                      <strong>Holy guacamole!</strong> Check this info.
                    </Popover.Body>
                  </Popover>
                }
              >
                <Button variant="secondary">...</Button>
              </OverlayTrigger> */}
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'breakfast').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'breakfast').sort(compareRating)}
                        condition={['Monday', 'breakfast']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }

            </Cards>
          </div>


          <div className="con3">
            <Cards>
              {weeklyData.filter(v => v.day === "Tuesday" && v.type === 'breakfast').length !== 0 ?
                weeklyData.filter(v => v.day === "Tuesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />
                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'breakfast').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'breakfast').sort(compareRating)}
                        condition={['Tuesday', 'breakfast']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>

          </div>

          <div className="con4">
            <Cards>
              {weeklyData.filter(v => v.day === "Wednesday" && v.type === 'breakfast').length !== 0 ?
                weeklyData.filter(v => v.day === "Wednesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'breakfast').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'breakfast').sort(compareRating)}
                        condition={['Wednesday', 'breakfast']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con5">
            <Cards>
              {weeklyData.filter(v => v.day === "Thursday" && v.type === 'breakfast').length !== 0 ?
                weeklyData.filter(v => v.day === "Thursday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'breakfast').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'breakfast').sort(compareRating)}
                        condition={['Thursday', 'breakfast']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con6">
            <Cards>
              {weeklyData.filter(v => v.day === "Friday" && v.type === 'breakfast').length !== 0 ?
                weeklyData.filter(v => v.day === "Friday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'breakfast').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'breakfast').sort(compareRating)}
                        condition={['Friday', 'breakfast']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>
        </div>


        {/*LUNCH*/}
        <div className="lunch-con">
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>LUNCH</h2>

          </div>

          <div className="con2">
            <Cards>
              {weeklyData.filter(v => v.day === "Monday" && v.type === 'lunch').length !== 0 ?
                weeklyData.filter(v => v.day === "Monday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'lunch').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'lunch').sort(compareRating)}
                        condition={['Monday', 'lunch']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con3">
            <Cards>
              {weeklyData.filter(v => v.day === "Tuesday" && v.type === 'lunch').length !== 0 ?
                weeklyData.filter(v => v.day === "Tuesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'lunch').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'lunch').sort(compareRating)}
                        condition={['Tuesday', 'lunch']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con4">
            <Cards>
              {weeklyData.filter(v => v.day === "Wednesday" && v.type === 'lunch').length !== 0 ?
                weeklyData.filter(v => v.day === "Wednesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'lunch').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'lunch').sort(compareRating)}
                        condition={['Wednesday', 'lunch']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con5">
            <Cards>
              {weeklyData.filter(v => v.day === "Thursday" && v.type === 'lunch').length !== 0 ?
                weeklyData.filter(v => v.day === "Thursday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'lunch').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'lunch').sort(compareRating)}
                        condition={['Thursday', 'lunch']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con6">
            <Cards>
              {weeklyData.filter(v => v.day === "Friday" && v.type === 'lunch').length !== 0 ?
                weeklyData.filter(v => v.day === "Friday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'lunch').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'lunch').sort(compareRating)}
                        condition={['Friday', 'lunch']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>
        </div>


        {/*DINNER*/}
        <div className="dinner-con" >
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>DINNER</h2>

          </div>

          <div className="con2">
            <Cards>
              {weeklyData.filter(v => v.day === "Monday" && v.type === 'dinner').length !== 0 ?
                weeklyData.filter(v => v.day === "Monday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>

            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'dinner').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'dinner').sort(compareRating)}
                        condition={['Monday', 'dinner']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con3">
            <Cards>
              {weeklyData.filter(v => v.day === "Tuesday" && v.type === 'dinner').length !== 0 ?
                weeklyData.filter(v => v.day === "Tuesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'dinner').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'dinner').sort(compareRating)}
                        condition={['Tuesday', 'dinner']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con4">
            <Cards>
              {weeklyData.filter(v => v.day === "Wednesday" && v.type === 'dinner').length !== 0 ?
                weeklyData.filter(v => v.day === "Wednesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'dinner').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'dinner').sort(compareRating)}
                        condition={['Wednesday', 'dinner']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con5">
            <Cards>
              {weeklyData.filter(v => v.day === "Thursday" && v.type === 'dinner').length !== 0 ?
                weeklyData.filter(v => v.day === "Thursday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'dinner').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'dinner').sort(compareRating)}
                        condition={['Thursday', 'dinner']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con6">
            <Cards>
              {weeklyData.filter(v => v.day === "Friday" && v.type === 'dinner').length !== 0 ?
                weeklyData.filter(v => v.day === "Friday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'dinner').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'dinner').sort(compareRating)}
                        condition={['Friday', 'dinner']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>
        </div>


        {/*SNACK*/}
        <div className="dinner-con" >
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>SNACKS</h2>

          </div>

          <div className="con2">
            <Cards>
              {weeklyData.filter(v => v.day === "Monday" && v.type === 'snack').length !== 0 ?
                weeklyData.filter(v => v.day === "Monday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'snack').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'snack').sort(compareRating)}
                        condition={['Monday', 'snack']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con3">
            <Cards>
              {weeklyData.filter(v => v.day === "Tuesday" && v.type === 'snack').length !== 0 ?
                weeklyData.filter(v => v.day === "Tuesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'snack').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'snack').sort(compareRating)}
                        condition={['Tuesday', 'snack']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con4">
            <Cards>
              {weeklyData.filter(v => v.day === "Wednesday" && v.type === 'snack').length !== 0 ?
                weeklyData.filter(v => v.day === "Wednesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'snack').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'snack').sort(compareRating)}
                        condition={['Wednesday', 'snack']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con5">
            <Cards>
              {weeklyData.filter(v => v.day === "Thursday" && v.type === 'snack').length !== 0 ?
                weeklyData.filter(v => v.day === "Thursday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'snack').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'snack').sort(compareRating)}
                        condition={['Thursday', 'snack']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>

          <div className="con6">
            <Cards>
              {weeklyData.filter(v => v.day === "Friday" && v.type === 'snack').length !== 0 ?
                weeklyData.filter(v => v.day === "Friday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                    setmodalName(station2.name.split('+')[0])]}>
                      <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                      <Rating
                        name={station2.name}
                        precision={0.5}
                        size="small"
                        value={station2.rating}
                        readOnly
                      />

                    </div> : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
            <Cards>
              <h6>Recommendation</h6>
              {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'snack').length !== 0 ?
                props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                  i === 0 ?
                    <>
                      <div key={station2._id} onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                      setmodalName(station2.name.split('+')[0])]}>
                        <h5 className="cardTitle" style={{ backgroundColor: '#203354', color: 'white' }}>{station2.name.split('+')[0]}</h5>
                        <Rating
                          name="size-small"
                          size="small"
                          value={station2.rating}
                          readOnly
                          precision={0.5}
                          style={{ fontSize: '2px' }}
                        /><br></br>
                        {
                          station2?.ingToRemove?.length ?
                            <>
                              <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />
                            </>
                            : null
                        }
                        {
                          station2?.ingToAdd?.length ?
                            <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null
                        }
                        {
                          station2.feed2 ?
                            <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null
                        }
                        <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                      </div>

                      <OthersButton data={props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'snack').sort(compareRating)}
                        condition={['Friday', 'snack']}
                        id={station2._id}
                        otherRecommendation={otherRecommendation} />
                    </>
                    : null

                )
                : <div onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></div>
              }
            </Cards>
          </div>
        </div>
      </div>
      <br />
    </React.Fragment >
  );
}
