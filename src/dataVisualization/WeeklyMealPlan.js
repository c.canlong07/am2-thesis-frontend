import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import { Line } from '../styles/DashboardLayout';
import noDataFound from '../img/no-data-found.png';
import { Modal, ListGroup, Card, Button, Spinner } from "react-bootstrap";
import { MealPlan, Cards, Days, Naming } from '../styles/WeekMealPlan';
import { MainLayout, TableLayout } from '../styles/TableLayout';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamation, faUtensils, } from "@fortawesome/free-solid-svg-icons";
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import Chip from '@mui/material/Chip';
import TextareaAutosize from 'react-textarea-autosize';
import styled from 'styled-components';
import { ModalLayout, ButtonLayout } from '../styles/LayoutForm';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { BiEdit } from "react-icons/bi";
import { FaAngleLeft } from "react-icons/fa";
import { FaAngleRight } from "react-icons/fa";

export default function WeeklyMealPlan(props) {
  const [allMeals, setallMeals] = useState({});
  const [modalShow, setModalShow] = React.useState(false);
  const [usermodalShow, setuserModalShow] = React.useState(false);
  const [mealSelected, setmealSelected] = React.useState([]);
  const [mealSelectedRecommendation, setmealSelectedRecommendation] = React.useState([]);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  const [userFeedback, setUserFeedback] = React.useState('');
  const [checked, setChecked] = useState([]);
  const [checked2, setChecked2] = useState([]);
  const [mealCategory, setmealCategory] = useState('');
  const [availingCheck, setavailingCheck] = React.useState(false);
  const [theRecord, settheRecord] = React.useState([]);
  const [mealAvailing, setmealAvailing] = React.useState(0);
  const [mealFeedback, setmealFeedback] = React.useState([]);
  const [employeeMealCategory, setemployeeMealCategory] = React.useState([]);
  const [employeeingToRemove, setemployeeingToRemove] = React.useState([]);
  const [employeeingToAdd, setemployeeingToAdd] = React.useState([]);
  const [mealCategoryRecommended, setmealCategoryRecommended] = React.useState([]);
  const [show, setShow] = useState(false);

  let userMealRecord;
  const countCategory = {};
  let theMealCount = {};

  function allAreEqual(obj) {
    console.log('objjjjj', Object.keys(obj).length)
    if (Object.keys(obj).length > 1) {
      return new Set(Object.values(obj)).size === 1;
    } else {
      return false;
    }
  }

  useEffect(() => {
    const mealRecommendationData = {};
    const mealData = {};
    for (const element of employeeMealCategory) {
      if (countCategory[element]) {
        countCategory[element] += 1;
      } else {
        countCategory[element] = 1;
      }
    }
    theMealCount = Object.fromEntries(
      Object.entries(countCategory).sort(([, a], [, b]) => b - a)
    );
    setmealCategoryRecommended(!allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory);
    console.log('****************', Object.keys(theMealCount).length, theMealCount, !allAreEqual(theMealCount), !allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory)

  }, [employeeMealCategory, mealSelected]);

  const considerRecommendation = () => {
    console.log(' considerRecommendation',)
    const mealRecommendationData = {};
    const mealData = {};
    for (const element of employeeMealCategory) {
      if (countCategory[element]) {
        countCategory[element] += 1;
      } else {
        countCategory[element] = 1;
      }
    }
    theMealCount = Object.fromEntries(
      Object.entries(countCategory).sort(([, a], [, b]) => b - a)
    );
    setmealCategoryRecommended(!allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory);

    mealData['mealName'] = mealSelected?.mealName;
    mealData['mealIng'] = mealSelected?.mealIng;
    mealData['mealDesc'] = mealSelected?.mealDesc;
    mealData['mealType'] = mealSelected?.mealType;
    mealData['mealDate'] = mealSelected?.mealDate;
    mealData['mealTotalPrice'] = mealSelected?.mealTotalPrice;
    mealData['mealTotalPerEmployee'] = mealSelected?.mealTotalPerEmployee;
    mealData['mealingToAdd'] = employeeingToAdd;
    mealData['mealingToRemove'] = employeeingToRemove;
    mealData['mealServedEst'] = mealSelected?.mealServedEst === mealAvailing ? mealSelected?.mealServedEst : mealAvailing;
    mealData['mealCategory'] = !allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory === mealSelected?.mealCategory ?
      mealSelected?.mealCategory : !allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory;

    mealRecommendationData['mealRecommendationData'] = [mealData];
    console.log('countCategory', employeeMealCategory,
      !allAreEqual(theMealCount) ? Object.keys(theMealCount)?.[0] : mealSelected?.mealCategory,
      theMealCount)

    axios.post(process.env.REACT_APP_BASE_URL + '/employee/updatemealRecommendation/' + mealSelected?._id, mealRecommendationData)
      .then(res =>
        window.location.href = `editMealRecommendation/${mealSelected?._id}/${mealSelected?.mealDate}/${mealSelected?.budget}`
      )
      .catch(err => console.log('Error :' + err));
  }

  useEffect(() => {

  }, []);

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }

  const onMealClick = (meal) => {

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/adminmealRecords', {
      params: {
        mealID: meal._id,
      }
    })
      .then(res => [
        // console.log('adminnnn', res.data, res.data.map(value => value.ingToRemove),
        //   [].concat(...res.data.map(value => value.ingToRemove))),
        setmealFeedback(res.data.filter(function (meal) {
          return meal.feedback !== undefined;
        })),
        setmealAvailing(res.data.filter(function (meal) {
          return meal.availing !== undefined;
        }).length),
        console.log('mealselected', meal, res.data.map(value => value.mealCategory)),

        setemployeeMealCategory(res.data.map(value => value.mealCategory)),
        setemployeeingToRemove([].concat(...res.data.map(value => value.ingToRemove))),
        setemployeeingToAdd([].concat(...res.data.map(value => value.ingToAdd))),
      ]

      )
      .catch(err => console.log('Error :' + err));
    setmealSelected(meal);
    setmealSelectedRecommendation(meal.mealRecommendationData?.[0])
    console.log('meallll', meal, meal.mealRecommendationData?.[0])

    setModalShow(true);
  }

  const onMealClickUser = (meal) => {

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/usermealRecords', {
      params: {
        userID: user._id,
        mealID: meal._id,
      }
    })
      .then(res => [
        userMealRecord = res.data,
        settheRecord(res.data),
        setmealSelected(meal),
        // console.log('meal.mealCategory', userMealRecord?.[0]?.ingToAdd,
        //   userMealRecord?.[0]?.ingToAdd !== undefined ? setChecked2(userMealRecord?.[0]?.ingToAdd) : setChecked2(meal.ingToAdd) !== undefined ? setChecked2(meal.ingToAdd) : [],
        //   meal.ingToAdd),
        userMealRecord?.[0]?.mealCategory ? setmealCategory(userMealRecord?.[0]?.mealCategory) : setmealCategory(meal.mealCategory),
        userMealRecord?.[0]?.feedback ? setUserFeedback(userMealRecord?.[0]?.feedback) : setUserFeedback(meal.feedback),
        userMealRecord?.[0]?.ingToAdd !== undefined ? setChecked2(userMealRecord?.[0]?.ingToAdd) : setChecked2(meal.ingToAdd) !== undefined ? setChecked2(meal.ingToAdd) : setChecked2([]),
        userMealRecord?.[0]?.ingToRemove !== undefined ? setChecked(userMealRecord?.[0]?.ingToRemove) : setChecked(meal.ingToRemove) !== undefined ? setChecked(meal.ingToRemove) : setChecked([]),
        userMealRecord?.[0]?.availing ? setavailingCheck(userMealRecord?.[0]?.availing) : setavailingCheck(meal.availing),
        // setChecked2(meal.ingToAdd),
        // setChecked(meal.ingToRemove),
        // setavailingCheck(meal.availing),
        setuserModalShow(true)
      ]

      )
      .catch(err => console.log('Error :' + err));
  }

  const onMealCLose = (meal) => {
    setmealSelected([]);
    setModalShow(false);
    setShow(false)
    setuserModalShow(false);
    setmealCategoryRecommended('');

  }

  const onValueChange = (e) => {
    setUserFeedback(e.target.value)
  }

  const onFeedbackSave = () => {
    // console.log('onFeedbackSave', user, availingCheck, userFeedback, mealCategory, checked, checked2, theRecord)
    if (theRecord.length) {
      const mealFeed = {
        user: user._id,
        meal: mealSelected?._id,
        availing: availingCheck,
        mealCategory: mealCategory,
        ingToAdd: checked2,
        ingToRemove: checked,
        feedback: userFeedback,
        mealType: mealSelected?.mealType
      }


      axios.post(process.env.REACT_APP_BASE_URL + '/employee/updateMealRecords/' + theRecord?.[0]?._id, mealFeed)
        .then(res =>
          Swal.fire({
            title: 'Feedback has been Sent',
            text: "Successfully Update a Feedback",
            icon: 'success',
            confirmButtonText: 'Okay'
          }).then((result) => {
            if (result.isConfirmed) {
              window.location = "/meals"
            }
          })
        )
        .catch(err => console.log('Error :' + err));
    } else {
      axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/mealRecord',
        {
          user: user._id,
          meal: mealSelected?._id,
          availing: availingCheck,
          mealCategory: mealCategory,
          ingToAdd: checked2,
          ingToRemove: checked,
          feedback: userFeedback,
          mealType: mealSelected?.mealType
        })
        .then(res =>
          Swal.fire({
            icon: 'info',
            title: 'Feedback Saved',
            // text: 'Please make you sure to have Attendance',
            confirmButtonText: 'Okay',
          }).then((result) => {
            if (result.isConfirmed) {
              window.location = "/meals"
            }
          })
        )
        .catch(err => console.log('Error :' + err));
    }

  }

  // Add/Remove checked item from list
  const handleCheck = (event) => {
    // console.log('checked', checked)
    var updatedList = [...checked];
    if (event.target.checked) {
      updatedList = [...checked, event.target.value];
    } else {
      updatedList.splice(checked.indexOf(event.target.value), 1);
    }
    setChecked(updatedList);
  };

  // Add/Remove checked item from list
  const handleCheck2 = (event) => {
    var updatedList = [...checked2];
    if (event.target.checked) {
      updatedList = [...checked2, event.target.value];
    } else {
      updatedList.splice(checked2.indexOf(event.target.value), 1);
    }
    setChecked2(updatedList);
  };

  const onCategoryChange = (e) => {
    setmealCategory(e.target.value)
  }

  const removeMeasurementWords = (txt) => {
    var uselessWordsArray =
      [
        "teaspoon", "teaspoons", "piece", "tablespoons", "tablespoon", "pieces", "ounce", "ounces", "miligram", "miligrams",
        "kilogram", "kilograms", "gram", "grams", "fluid ounce", "fluid ounces", "gill", "gills",
        "cup", "cups", "pint", "pints", "quart", "quarts", "gallon", "gallons", "milliliter",
        "milliliter", "liter", "liters", "deciliter", "deciliters", "pound", "pounds", "millimeter",
        "millimeters", "centimeter", "centimeters", "meter", "meters", "inch", "inches", "t", "tsp",
        "t", "tbl", "tbs", "tbsp", "fl oz", "c", "p", "pt", "fl pt", "q", "qt", "fl qt", "g", "gal",
        "ml", "l", "dl", "lb", "oz", "mg", "g", "kg", "mm", "cm", "m", "inch", " in ", "pc", "pcs", "lbs"
      ];

    var expStr = uselessWordsArray.join("|");
    return txt.replace(new RegExp('\\b(' + expStr + ')\\b', 'gi'), ' ')
      .replace(/\s{2,}/g, ' ').replace(/[0-9]/g, '').replace(/[^\w\s]/gi, '');
  }

  const getCount = (arr, val) => {
    var ob = {};
    var len = arr.length;
    for (var k = 0; k < len; k++) {
      if (ob.hasOwnProperty(arr[k])) {
        ob[arr[k]]++;
        continue;
      }
      ob[arr[k]] = 1;
    }
    return ob[val] ? ob[val] : 0;
  }

  return (
    <React.Fragment>

      <Modal show={show} onHide={() => onMealCLose()} centered size="xl"
        aria-labelledby="contained-modal-title-vcenter">
        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Grid container>
          <Grid item xs>
            <Card style={{ width: 'auto', margin: '3%' }}>
              <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>Original Meal</Card.Header>
              <Card.Body>

                <Naming>
                  <h3>{mealSelected?.originalMealData?.mealOrigData?.[0]?.mealName}</h3>
                  <p><u>Meal Category: </u>
                    <h5> {mealSelected?.originalMealData?.mealOrigData?.[0]?.mealCategory}</h5>
                  </p>
                  {/* <p><u> Meal Description: </u> <h5> {mealSelected?.mealDesc}</h5></p> */}
                  <p><u> Meal Served Estimation: </u>
                    <h5 > {mealSelected?.originalMealData?.mealOrigData?.[0]?.mealServedEst}</h5>
                  </p>
                  <p><u>Meal Ingredients:</u> {mealSelected?.originalMealData?.mealOrigData?.[0]?.mealIng?.length ? mealSelected?.originalMealData?.mealOrigData?.[0]?.mealIng.split("\n").map(txt =>

                    <h5 className="container2" style={{ textAlign: 'left', color: 'black' }}>
                      {txt}
                      <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                      <span className="checkmark"></span>
                    </h5>
                  )
                    : <h5>None</h5>}</p>
                </Naming>
              </Card.Body>
            </Card>
          </Grid>
          <Divider orientation="vertical" flexItem>
            COMPARE
          </Divider>
          <Grid item xs>  <Card style={{ width: 'auto', margin: '3%' }}>
            <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>Updated Meal</Card.Header>
            <Card.Body>
              <Naming>
                <h3>{mealSelected?.mealName}</h3>
                <p><u>Meal Category: </u>
                  <h5> {mealSelectedRecommendation?.mealCategory}</h5>
                </p>
                {/* <p><u> Meal Description: </u> <h5> {mealSelected?.mealDesc}</h5></p> */}
                <p><u> Meal Served Estimation: </u>
                  <h5 > {mealSelectedRecommendation?.mealServedEst}</h5>
                </p>
                <p><u>Meal Ingredients:</u> {mealSelectedRecommendation?.mealIng?.length ? mealSelectedRecommendation?.mealIng.split("\n").map(txt =>

                  <h5 className="container2" style={{ textAlign: 'left', color: 'black' }}>
                    {txt}
                    <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                    <span className="checkmark"></span>
                  </h5>
                )
                  : <h5>None</h5>}</p>
              </Naming>
            </Card.Body>
          </Card>
          </Grid>
        </Grid>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => onMealCLose()}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={modalShow}
        onHide={() => onMealCLose()}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
          <Modal.Title id="contained-modal-title-vcenter">
            {mealSelected?.mealName}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid container>
            <Grid item xs>
              <Card style={{ height: '66vh', overflow: 'scroll', overflowX: 'hidden', marginTop: '5%' }}>
                <Card style={{ width: 'auto', margin: '3%' }}>
                  <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>Original Meal</Card.Header>
                  <Card.Body>
                    <Card.Title>{mealSelected?.originalMealData?.mealOrigData?.[0].mealName}</Card.Title>
                    <Card.Text>
                      Meal Category: {mealSelected?.originalMealData?.mealOrigData?.[0].mealCategory}
                    </Card.Text>
                    <Card.Text>
                      Meal Description: {mealSelected?.originalMealData?.mealOrigData?.[0].mealDesc}
                    </Card.Text>
                    <Card.Text>
                      <>
                        <p><u>Meal Ingredients:</u> {mealSelected?.originalMealData?.mealOrigData?.[0].mealIng?.length ? mealSelected?.originalMealData?.mealOrigData?.[0].mealIng.split("\n").map(txt =>
                          <>
                            <label className="container2" style={{ textAlign: 'left', marginLeft: '3%' }}>{txt}
                            </label><br />
                          </>
                        )
                          : <h5>None</h5>}</p>
                      </>
                    </Card.Text>
                    <Card.Text>
                      Meal Served Estimation: {mealSelected?.originalMealData?.mealOrigData?.[0].mealServedEst}

                    </Card.Text>

                  </Card.Body>
                </Card>
                {
                  mealSelectedRecommendation ?
                    <Card style={{ width: 'auto', margin: '3%' }}>
                      <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}
                        onClick={() => setShow(true)}
                      >Updated Meal</Card.Header>
                      <Card.Body>
                        {/* <Card.Title>{mealSelectedRecommendation?.mealName}</Card.Title>
                        <Card.Text>
                          Meal Category: {mealSelectedRecommendation?.mealCategory}
                        </Card.Text>
                        <Card.Text>
                          Meal Description: {mealSelectedRecommendation?.mealDesc}
                        </Card.Text>
                        <Card.Text>
                          <>
                            <p><u>Meal Ingredients:</u> {mealSelectedRecommendation?.mealIng?.length ? mealSelectedRecommendation?.mealIng.split("\n").map(txt =>
                              <>
                                <label className="container2" style={{ textAlign: 'left', marginLeft: '3%' }}>{txt}
                                </label><br />
                              </>
                            )
                              : <h5>None</h5>}</p>
                          </>

                        </Card.Text>
                        <Card.Text>
                          Meal Served Estimation: {mealSelectedRecommendation?.mealServedEst}

                        </Card.Text> */}
                        <Naming>
                          <h3>{mealSelected?.mealName}</h3>
                          <p><u>Meal Category: </u>{mealSelectedRecommendation?.mealCategory === mealSelected?.mealCategory ?
                            <h5> {mealSelected?.mealCategory}</h5>
                            :
                            <>
                              <h5 style={{ textDecoration: 'line-through' }}> {mealSelected?.mealCategory}</h5>
                              <h5> {mealSelectedRecommendation?.mealCategory}</h5>
                            </>
                          }</p>
                          {/* <p><u> Meal Description: </u> <h5> {mealSelected?.mealDesc}</h5></p> */}
                          <p><u> Meal Served Estimation: </u>
                            {
                              mealSelected?.mealServedEst === mealAvailing ?
                                <h5> {mealSelected?.mealServedEst}</h5>
                                :
                                <>
                                  <h5 style={{ textDecoration: 'line-through' }}> {mealSelected?.mealServedEst}</h5>
                                  <h5 > {mealSelectedRecommendation?.mealServedEst}</h5>
                                </>
                            }
                          </p>
                          <p><u>Meal Ingredients:</u> {mealSelected?.mealIng?.length ? mealSelected?.mealIng.split("\n").map(txt =>
                            // {getCount(employeeingToRemove, removeMeasurementWords(item))}
                            getCount(employeeingToRemove, removeMeasurementWords(txt)) > (mealAvailing / 2) ?
                              <h5 className="container2" style={{ textAlign: 'left', color: 'red', textDecoration: 'line-through' }}>
                                {txt}
                                <input value={txt} type="checkbox" checked='' onChange={() => { }} />
                                <span className="checkmark"></span>
                              </h5>
                              :
                              getCount(employeeingToAdd, removeMeasurementWords(txt)) > (mealAvailing / 2) ?
                                <h5 className="container2" style={{ textAlign: 'left', color: 'black' }}>
                                  {txt}
                                  <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                  <span className="checkmark"></span>
                                </h5>
                                :
                                <h5 className="container2" style={{ textAlign: 'left', color: 'black' }}>
                                  {txt}
                                  <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                  <span className="checkmark"></span>
                                </h5>
                          )
                            : <h5>None</h5>}</p>

                          {/* <Naming>
                            {mealSelected?.mealIng?.length ? mealSelected?.mealIng.split("\n").map((item, index) => (
                              <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{item}
                                <input value={item} type="checkbox" checked={employeeingToRemove.includes(removeMeasurementWords(item)) ? 'checked' : ''} onChange={handleCheck} />
                                <span className="checkmark"></span>
                              </label>
                            )) : <h5>None</h5>}

                          </Naming> */}
                        </Naming>

                      </Card.Body>
                      {/* <Card.Footer style={{ textAlign: 'right' }}>
                  <Button onClick={() => window.open(`editMeal/${mealSelected?._id}/${mealSelected?.mealDate}/${mealSelected?.budget}`, '_blank', 'toolbar=0,location=0,menubar=0')} style={{ background: '#00C2DE', marginLeft: '24%', padding: '3%', paddingTop: '1%' }} className="btn "><BiEdit style={{ fontSize: 20, color: 'white' }} /></Button>
                </Card.Footer> */}
                    </Card>
                    : null

                }


                {
                  mealCategoryRecommended?.length || mealAvailing?.length || employeeingToRemove?.length || employeeingToAdd?.length ?
                    <Card style={{ width: 'auto', marginTop: '5%', margin: '3%' }}>
                      <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>Recommendation</Card.Header>
                      <Card.Body>
                        <Naming>
                          <h3>{mealSelected?.mealName}</h3>
                          <p>{mealCategoryRecommended}{mealSelected?.mealCategory}</p>
                          <p><u>Meal Category: </u>{mealCategoryRecommended === mealSelected?.mealCategory ?
                            <h5> {mealSelected?.mealCategory}</h5>
                            :
                            <>
                              <h5 style={{ textDecoration: 'line-through' }}> {mealSelected?.mealCategory}</h5>
                              <h5> {mealCategoryRecommended}</h5>
                            </>
                          }</p>
                          {/* <p><u> Meal Description: </u> <h5> {mealSelected?.mealDesc}</h5></p> */}
                          <p><u> Meal Served Estimation: </u>
                            {
                              mealSelected?.mealServedEst === mealAvailing ?
                                <h5> {mealSelected?.mealServedEst}</h5>
                                :
                                <>
                                  <h5 style={{ textDecoration: 'line-through' }}> {mealSelected?.mealServedEst}</h5>
                                  <h5 > {mealAvailing}</h5>
                                </>
                            }
                          </p>
                          <p><u>Meal Ingredients:</u> {mealSelected?.mealIng?.length ? mealSelected?.mealIng.split("\n").map(txt =>
                            // {getCount(employeeingToRemove, removeMeasurementWords(item))}
                            getCount(employeeingToRemove, removeMeasurementWords(txt)) > (mealAvailing / 2) ?
                              <h5 className="container2" style={{ textAlign: 'left', color: 'red', textDecoration: 'line-through' }}>
                                {txt}
                                <input value={txt} type="checkbox" checked='' onChange={() => { }} />
                                <span className="checkmark"></span>
                              </h5>
                              :
                              getCount(employeeingToAdd, removeMeasurementWords(txt)) > (mealAvailing / 2) ?
                                <h5 className="container2" style={{ textAlign: 'left', color: 'green' }}>
                                  {txt}
                                  <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                  <span className="checkmark"></span>
                                </h5>
                                :
                                <h5 className="container2" style={{ textAlign: 'left', color: 'black' }}>
                                  {txt}
                                  <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                                  <span className="checkmark"></span>
                                </h5>
                          )
                            : <h5>None</h5>}</p>
                        </Naming>
                      </Card.Body>
                      <Card.Footer style={{ textAlign: 'right' }}>
                        <Button onClick={() => considerRecommendation()} style={{ background: '#00C2DE', marginLeft: '24%', padding: '3%', paddingTop: '1%' }} className="btn "><BiEdit style={{ fontSize: 20, color: 'white' }} />Consider Recommendation</Button>

                      </Card.Footer>
                    </Card>
                    :
                    <p style={{ fontWeight: 'bold', textAlign: 'center' }}>Waiting for Employees Feedback</p>
                }

              </Card>
            </Grid>
            <Divider orientation="vertical" flexItem>
              AM²
            </Divider>
            <Grid item xs>

              <Card style={{ height: '66vh', overflow: 'scroll', textAlign: 'center', overflowX: 'hidden' }}>
                <h3 className="text-uppercase"><strong>{mealSelected?.mealName}</strong></h3>
                <h5 className="text-uppercase"><strong>{mealSelected?.mealType}</strong></h5>

                <Card style={{ textAlign: 'center', margin: '3%' }}>
                  <Card.Body>
                    <Card.Title style={{ fontWeight: 'bold', textTransform: 'uppercase' }}>Summary</Card.Title>
                    {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle> */}
                    <Card.Text>
                      <strong>{mealAvailing}</strong> Employees will avail <strong>{mealSelected?.mealName}</strong>. <br />
                      <strong>{mealFeedback?.length}</strong> Employees give feedback.
                      {
                        employeeingToRemove.length ?
                          <div>
                            <strong>{employeeingToRemove.length}</strong> employee suggest to remove the following ingredients ({employeeingToRemove.filter(function (elem, index, self) {
                              return index === self.indexOf(elem);
                            }).map(txt => <strong>{txt}, </strong>)})</div> : <div>No Ingredients is suggested to be remove.</div>
                      }

                      {
                        employeeingToAdd.length ?
                          <div>
                            <strong>{employeeingToAdd.length}</strong> employee suggest to add the following ingredients ({employeeingToAdd.filter(function (elem, index, self) {
                              return index === self.indexOf(elem);
                            }).map(txt => <strong>{txt}, </strong>)})</div> : <div>No Ingredients is suggested to be add.</div>
                      }

                    </Card.Text>
                  </Card.Body>
                </Card>
                <div style={{ margin: 'auto', display: 'block', textAlign: 'center', margin: '3%' }}>

                  <Divider style={{ marginBottom: '3%', marginTop: '3%' }}>
                    <Chip label="Employees Availing this Meal" />
                  </Divider>
                  <div className="form-group" style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '1.2rem', margin: '2%' }}>
                    {/* {moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? */}

                    <>
                      <h1 style={{ margin: '0px', fontWeight: 'bold', }}>{mealAvailing}</h1>
                      <label className="checkbox-inline" style={{ margin: '5%' }}>
                        No. of Employees Availing this Meal
                      </label>

                    </>
                  </div>
                  <div className="form-group">
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Employees Feedback" />
                    </Divider>
                    <h1 style={{ margin: '0px', fontWeight: 'bold', }}>{mealFeedback?.length}</h1>

                    <Card style={{ width: 'auto', maxHeight: '250px', height: 'auto', overflow: 'scroll', overflowX: 'hidden' }}>
                      <Card.Header style={{ fontWeight: 'bold' }}>FEEDBACK</Card.Header>
                      <ListGroup variant="flush">
                        {mealFeedback.length !== 0 ?
                          mealFeedback.map((meal, i) =>

                            <ListGroup.Item>
                              <Card.Title style={{ fontSize: '1rem' }}> {meal.feedback}</Card.Title>
                              <Card.Subtitle style={{ textTransform: 'capitalize' }} className=" text-muted">{meal.user?.firstname} {meal.user?.lastname}</Card.Subtitle>
                              {/* <blockquote className="blockquote mb-0">
                              <p>
                                {' '}
                                {meal.feedback}{' '}
                              </p>
                              <footer className="blockquote-footer">
                                {meal.user?.firstname}
                              </footer>
                            </blockquote> */}
                            </ListGroup.Item>

                          )
                          :
                          <div >
                            <p>None</p>
                          </div>
                        }
                      </ListGroup>
                    </Card>
                  </div>

                  <div className="">
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Employee Suggested Meal Category" />
                    </Divider>
                    {/* <div style={{ display: 'list-item', listStyle: 'disc' }}>
                      <ul>
                        <li value="vegetable">Vegetable</li>
                        <li>Meat
                          <ul>
                            <li value="Meat: Beef">Beef</li>
                            <li value="Meat: Chicken">Chicken</li>
                            <li value="Meat: Chicken">Chicken</li>
                            <li value="Meat: Pig">Pig</li>
                            <li value="Meat: Pork">Pork</li>
                          </ul>
                        </li>
                        <li>Seafood
                          <li value="Seafood: Crab">Crab</li>
                          <li value="Seafood: Fish">Fish</li>
                          <li value="Seafood: Lobster">Lobster</li>
                          <li value="Seafood: Mussels">Mussels</li>
                          <li value="Seafood: Oyster">Oyster</li>
                          <li value="Seafood: Shrimp">Shrimp</li>
                          <li value="Seafood: Squid">Squid</li>
                        </li>
                      </ul>
                    </div> */}
                    <select value={mealCategory} className="mealType  form-control" data-name="mealType">
                      <option style={{ textTransform: 'bold' }} value="vegetable">
                        Vegetable({getCount(employeeMealCategory, 'vegetable').toString()})
                      </option>
                      {/* <option value="Meat">Meat</option> */}
                      <optgroup label="Meat">
                        <option value="Meat: Beef">Beef
                          ({getCount(employeeMealCategory, 'Meat: Beef').toString()})
                        </option>
                        <option value="Meat: Chicken">Chicken
                          ({getCount(employeeMealCategory, 'Meat: Chicken').toString()})
                        </option>
                        <option value="Meat: Pig">Pig
                          ({getCount(employeeMealCategory, 'Meat: Pig').toString()})
                        </option>
                        <option value="Meat: Pork">Pork
                          ({getCount(employeeMealCategory, 'Meat: Pork').toString()})
                        </option>
                      </optgroup>
                      <optgroup label="Seafood">
                        <option value="Seafood: Crab">Crab
                          ({getCount(employeeMealCategory, 'Seafood: Crab').toString()})
                        </option>
                        <option value="Seafood: Fish">Fish
                          ({getCount(employeeMealCategory, 'Seafood: Fish').toString()})
                        </option>
                        <option value="Seafood: Lobster">Lobster
                          ({getCount(employeeMealCategory, 'Seafood: Lobster').toString()})
                        </option>
                        <option value="Seafood: Mussels">Mussels
                          ({getCount(employeeMealCategory, 'Seafood: Mussels').toString()})
                        </option>
                        <option value="Seafood: Oyster">Oyster
                          ({getCount(employeeMealCategory, 'Seafood: Oyster').toString()})
                        </option>
                        <option value="Seafood: Shrimp">Shrimp
                          ({getCount(employeeMealCategory, 'Seafood: Shrimp').toString()})
                        </option>
                        <option value="Seafood: Squid">Squid
                          ({getCount(employeeMealCategory, 'Seafood: Squid').toString()})
                        </option>
                      </optgroup>
                    </select>
                  </div>
                  <div className="form-group" >
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Employees Suggested to Remove Ingredients" />
                    </Divider>
                    <MealStyled>
                      {mealSelected?.mealIng?.split("\n").map((item, index) => (
                        <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{removeMeasurementWords(item)} ({getCount(employeeingToRemove, removeMeasurementWords(item))})
                          <input value={removeMeasurementWords(item)} type="checkbox" checked={employeeingToRemove?.includes(removeMeasurementWords(item)) ? 'checked' : ''} onChange={handleCheck} />
                          <span className="checkmark"></span>
                        </label>
                      ))}

                    </MealStyled>
                  </div>

                  <div className="form-group" >
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Employees Suggested to Add More Ingredients" />
                    </Divider>
                    <MealStyled>
                      {mealSelected?.mealIng?.split("\n").map((item, index) => (
                        <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{removeMeasurementWords(item)} ({getCount(employeeingToAdd, removeMeasurementWords(item))})
                          <input value={removeMeasurementWords(item)} type="checkbox" checked={employeeingToAdd?.includes(removeMeasurementWords(item)) ? 'checked' : ''} onChange={handleCheck} />
                          <span className="checkmark"></span>
                        </label>
                      ))}

                    </MealStyled>
                  </div>
                </div>
                <Modal.Footer>
                  <ButtonLayout>
                    {/* <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark" onClick={() => onMealCLose()}> Close </Link> */}
                  </ButtonLayout>
                </Modal.Footer>
              </Card>
            </Grid>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <Button className='withClick' variant="secondary" onClick={() => onMealCLose()}>Close</Button>
        </Modal.Footer>
      </Modal>
      {/* USER */}
      <Modal
        show={usermodalShow}
        onHide={() => onMealCLose()}
        size="xl"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable
      >

        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
          <Modal.Title id="contained-modal-title-vcenter">
            {mealSelected?.mealName} USER
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid container>
            <Grid item xs>
              {/* <Card style={{ width: 'auto', margin: '3%' }} >
                <Card.Body>
                  <Card.Title>{mealSelected?.mealName}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">{mealSelected?.mealName}</Card.Subtitle>
                  <Card.Text>
                    {mealSelected?.mealCategory}
                  </Card.Text>
                  <Card.Text>
                    {mealSelected?.mealDesc}
                  </Card.Text>
                  <Card.Text>
                  </Card.Text>

                </Card.Body>
              </Card> */}

              <Card style={{ height: '66vh', overflow: 'scroll', overflowX: 'hidden', marginTop: '5%' }}>
                <Card style={{ width: 'auto', margin: '3%' }}>
                  <Card.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>Original Meal</Card.Header>
                  <Card.Body>
                    <Card.Title>{mealSelected?.originalMealData?.mealOrigData?.[0].mealName}</Card.Title>
                    <Card.Text>
                      Meal Category: {mealSelected?.originalMealData?.mealOrigData?.[0].mealCategory}
                    </Card.Text>
                    <Card.Text>
                      Meal Description: {mealSelected?.originalMealData?.mealOrigData?.[0].mealDesc}
                    </Card.Text>
                    <Card.Text>
                      <>
                        <p><u>Meal Ingredients:</u> {mealSelected?.originalMealData?.mealOrigData?.[0].mealIng?.length ? mealSelected?.originalMealData?.mealOrigData?.[0].mealIng.split("\n").map(txt =>
                          <>
                            <label className="container2" style={{ textAlign: 'left', marginLeft: '3%' }}>{txt}
                            </label><br />
                          </>
                        )
                          : <h5>None</h5>}</p>
                      </>
                    </Card.Text>
                    <Card.Text>
                      Meal Served Estimation: {mealSelected?.originalMealData?.mealOrigData?.[0].mealServedEst}

                    </Card.Text>

                  </Card.Body>
                </Card>
              </Card>
            </Grid>
            <Divider orientation="vertical" flexItem>
              AM²
            </Divider>
            <Grid item xs>
              <Card style={{ height: '66vh', overflow: 'scroll', overflowX: 'hidden' }}>
                <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                  <p className="text-uppercase"><strong>{mealSelected?.mealName}</strong></p>
                  <p className="text-uppercase"><strong>{mealSelected?.mealType}</strong></p>
                  <Divider style={{ marginBottom: '3%', marginTop: '3%' }}>
                    <Chip label="Let us know if you are availing this Meal:" />
                  </Divider>
                  <div className="form-group" style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '1.2rem' }}>
                    {/* {moment(formatAMPM(new Date), 'h:mma').isBefore(moment(morningTo, 'h:mma')) ? */}

                    <>
                      <label className="checkbox-inline" style={{ margin: '5%' }}>
                        <input className="form-control" type="checkbox" checked={availingCheck} onChange={(e) => setavailingCheck(e.target.checked)} style={{ height: '30px' }} />Availing this Meal
                      </label>

                    </>
                  </div>
                  <div className="form-group">
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Feedback" />
                    </Divider>
                    <TextareaAutosize
                      className="textArea form-control" data-name="mealIng" value={userFeedback !== undefined && userFeedback !== null ? userFeedback : ''} required onChange={onValueChange}
                      minRows={3}
                      maxRows={6}
                      placeholder="Your feedback..."
                    />
                  </div>

                  <div className="form-group">
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Suggest Meal Category" />
                    </Divider>
                    <select value={mealCategory} className="mealType  form-control" data-name="mealType" required onChange={onCategoryChange}>
                      <option style={{ textTransform: 'bold' }} value="vegetable">Vegetable</option>
                      {/* <option value="Meat">Meat</option> */}
                      <optgroup label="Meat">
                        <option value="Meat: Beef">Beef</option>
                        <option value="Meat: Chicken">Chicken</option>
                        <option value="Meat: Pig">Pig</option>
                        <option value="Meat: Pork">Pork</option>
                      </optgroup>
                      <optgroup label="Seafood">
                        <option value="Seafood: Crab">Crab</option>
                        <option value="Seafood: Fish">Fish</option>
                        <option value="Seafood: Lobster">Lobster</option>
                        <option value="Seafood: Mussels">Mussels</option>
                        <option value="Seafood: Oyster">Oyster</option>
                        <option value="Seafood: Shrimp">Shrimp</option>
                        <option value="Seafood: Squid">Squid</option>
                      </optgroup>
                    </select>
                  </div>
                  <div className="form-group" >
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Check Ingredients you Suggest to Remove" />
                    </Divider>
                    <MealStyled>
                      {mealSelected?.mealIng?.split("\n").map((item, index) => (
                        <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{removeMeasurementWords(item)}
                          <input value={removeMeasurementWords(item)} type="checkbox" checked={checked?.includes(removeMeasurementWords(item)) ? 'checked' : ''} onChange={handleCheck} />
                          <span className="checkmark"></span>
                        </label>
                      ))}

                    </MealStyled>
                  </div>

                  <div className="form-group" >
                    <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                      <Chip label="Check Ingredients you Suggest to be Added More" />
                    </Divider>
                    <MealStyled>
                      {mealSelected?.mealIng?.split("\n").map((item, index) => (
                        <label className="container2" style={{ paddingLeft: '50px', textAlign: 'left', color: 'black' }}>{removeMeasurementWords(item)}
                          <input value={removeMeasurementWords(item)} type="checkbox" checked={checked2?.includes(removeMeasurementWords(item)) ? 'checked' : ''} onChange={handleCheck2} />
                          <span className="checkmark"></span>
                        </label>
                      ))}

                    </MealStyled>
                  </div>
                </div>
                <Modal.Footer>
                  <ButtonLayout>
                    {/* <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark" onClick={() => onMealCLose()}> Close </Link> */}
                  </ButtonLayout>
                </Modal.Footer>
              </Card>
            </Grid>
          </Grid>


        </Modal.Body>
        <Modal.Footer>
          <button type="submit" style={{ marginRight: 10 }} className="btn btn-primary" onClick={() => onFeedbackSave()}> Save </button>
          <Button className='withClick' variant="secondary" onClick={() => onMealCLose()}>Close</Button>
        </Modal.Footer>
      </Modal>

      {/* Week Meal Plan */}
      <TableLayout style={{ padding: '1rem 2rem', fontSize: '0.5rem' }}>
        <p style={{ fontWeight: 'bold', marginTop: '1%', marginBottom: 0, fontSize: '1.8rem', textAlign: 'center' }}>Week Meal Plan</p>
        <p style={{ fontSize: '0.9rem', textAlign: 'center' }}>{moment(props.week?.[0]).format('LL')} - {moment(props.week?.[1]).format('LL')}</p>
        <div className="prev-next-div" style={{ textAlign: 'right' }}>
          <Button className="btn-prev" onClick={() => props.handleOnClick(props.week?.[0], 'prev')}><FaAngleLeft /></Button>
          <Button className="btn-next" onClick={() => props.handleOnClick(props.week?.[0], 'next')}><FaAngleRight /></Button>
        </div>
        <br></br>
        {
          props.isLoading ?
            <div style={{ textAlign: 'center' }}>
              <Spinner animation="grow" variant="secondary" />
            </div>
            :
            <MealPlan>
              <div className="board"> {/* Board div */}


                {/* Labels */}
                <div className="Cards" style={{ padding: '0.5rem' }}>

                  <div className="labels" style={{ visibility: 'hidden', height: '3vh' }}>
                    <h2>Breakfast</h2>
                  </div>

                  <div className="mealCards" style={{ textAlign: 'center' }}>
                    <Days className="monday" >
                      <p style={{ marginBottom: 0 }}>Monday</p>
                    </Days>

                    <Days className="tuesday" >
                      <p>Tuesday</p>
                    </Days>

                    <Days className="wednesday" >
                      <p>Wednesday</p>
                    </Days>

                    <Days className="thursday" >
                      <p>Thursday</p>
                    </Days>

                    <Days className="friday" >
                      <p>Friday</p>
                    </Days>
                  </div>

                </div>
                {/* End of Morning */}

                {/* Morning */}
                <div className="Cards" style={{ marginBottom: '0.5rem' }}>

                  <div className="labels">
                    <h2>Breakfast</h2>
                  </div>

                  <div className="mealCards">
                    <Cards className="monday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'breakfast').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>
                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards >


                    <Cards className="tuesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'breakfast').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="wednesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'breakfast').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="thursday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'breakfast').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="friday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'breakfast').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'breakfast').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>
                  </div>

                </div>
                {/* End of Morning */}

                {/* Lunch */}
                <div className="Cards" style={{ marginBottom: '0.5rem' }}>

                  <div className="labels">
                    <h2>Lunch</h2>
                  </div>

                  <div className="mealCards">
                    <Cards className="monday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'lunch').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'lunch').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards >

                    <Cards className="tuesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'lunch').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'lunch').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="wednesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'lunch').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'lunch').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="thursday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'lunch').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'lunch').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="friday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'lunch').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'lunch').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>
                  </div>

                </div>
                {/* End of Lunch */}

                {/* Dinner */}
                <div className="Cards">

                  <div className="labels">
                    <h2>Dinner</h2>
                  </div>

                  <div className="mealCards">
                    <Cards className="monday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'dinner').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'dinner').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards >

                    <Cards className="tuesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'dinner').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'dinner').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="wednesday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'dinner').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'dinner').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="thursday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'dinner').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'dinner').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>

                    <Cards className="friday">
                      {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'dinner').length !== 0 ?
                        props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'dinner').sort(compareRating).map((meal, i) =>
                          <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                            ? onMealClickUser(meal) : onMealClick(meal)}>
                            <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                            <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                          </div>
                        )
                        :
                        <div >
                          <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                        </div>
                      }
                    </Cards>
                  </div>

                </div>
                {/* End of Dinner */}

              </div> {/*End of Board div */}

              {/* Snack */}
              <div className="Cards">

                <div className="labels">
                  <h2>Snack</h2>
                </div>

                <div className="mealCards">
                  <Cards className="monday">
                    {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'snack').length !== 0 ?
                      props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Monday" && v.mealType === 'snack').sort(compareRating).map((meal, i) =>
                        <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                          ? onMealClickUser(meal) : onMealClick(meal)}>
                          <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                          <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                        </div>
                      )
                      :
                      <div >
                        <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                      </div>
                    }
                  </Cards >

                  <Cards className="tuesday">
                    {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'snack').length !== 0 ?
                      props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Tuesday" && v.mealType === 'snack').sort(compareRating).map((meal, i) =>
                        <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                          ? onMealClickUser(meal) : onMealClick(meal)}>
                          <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                          <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                        </div>
                      )
                      :
                      <div >
                        <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                      </div>
                    }
                  </Cards>

                  <Cards className="wednesday">
                    {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'snack').length !== 0 ?
                      props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Wednesday" && v.mealType === 'snack').sort(compareRating).map((meal, i) =>
                        <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                          ? onMealClickUser(meal) : onMealClick(meal)}>
                          <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                          <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                        </div>
                      )
                      :
                      <div >
                        <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                      </div>
                    }
                  </Cards>

                  <Cards className="thursday">
                    {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'snack').length !== 0 ?
                      props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Thursday" && v.mealType === 'snack').sort(compareRating).map((meal, i) =>
                        <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                          ? onMealClickUser(meal) : onMealClick(meal)}>
                          <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                          <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                        </div>
                      )
                      :
                      <div >
                        <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                      </div>
                    }
                  </Cards>

                  <Cards className="friday">
                    {props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'snack').length !== 0 ?
                      props.allMeals.filter(v => moment(v.mealDate).format('dddd') === "Friday" && v.mealType === 'snack').sort(compareRating).map((meal, i) =>
                        <div className='withClick' onClick={() => user?.role === 'Employee' || localStorage.getItem("manage") !== null
                          ? onMealClickUser(meal) : onMealClick(meal)}>
                          <p className="firstList" key={meal.createdAt}>{meal.mealName}</p>

                          <FontAwesomeIcon icon={faUtensils} size="lg" className="faUtensils" />
                        </div>
                      )
                      :
                      <div >
                        <FontAwesomeIcon icon={faExclamation} size="lg" className="faExclamation" />
                      </div>
                    }
                  </Cards>
                </div>

              </div>
              {/* End of Snack */}

            </MealPlan>
        }


      </TableLayout >

    </React.Fragment >
  );
}

const MealStyled = styled.header`
/* The container2 */
.container2 {
  display: block;
  position: relative;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;


}

/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  margin-left: 0px !important;
}



/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container2:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.modal-dialog{
  transform: translateY(30px) !important;
}

`;
