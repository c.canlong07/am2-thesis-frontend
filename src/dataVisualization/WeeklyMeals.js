import React, { useEffect, useState } from 'react';
import Rating from '@mui/material/Rating';
import noDataFound from '../img/no-data-found.png';
import { Card, Modal, Button, Tab, Row, Nav, Col } from "react-bootstrap";
import { Cards, CardTitle, CheckLists } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import { faUserCheck, faUserPlus, faUserTimes, faComment, faStar } from "@fortawesome/free-solid-svg-icons";
import { ButtonTooltip } from '../globalFunctions/shareableComponents';

export default function WeeklyMeals(props) {
  const [allTimeFav, setallTimeFav] = useState({});
  const [modalShow, setmodalShow] = useState(false);
  const [modalID, setmodalID] = useState('');
  const [modalName, setmodalName] = useState('');
  const [modalNoContent, setmodalNoContent] = useState('');


  useEffect(() => {
    setallTimeFav(props.allTimeFavWeekly);
  }, [props.allTimeFavWeekly]);

  function compareRating(a, b) {
    let first = a.rating ? a.rating : 0;
    let second = b.rating ? b.rating : 0;

    return second - first;
  }

  return (
    <React.Fragment>
      <Modal
        scrollable='true'
        show={modalShow}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
          <Modal.Title id="contained-modal-title-vcenter">
            {!modalNoContent ? `Feedback for ${modalName}` : 'Feedback'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.allTimeFav.filter(v => v.mealID === modalID).length !== 0 ?
            props.allTimeFav.filter(v => v.mealID === modalID).sort(compareRating).map((station2, i) =>
              <div key={station2._id}>
                <Card.Header style={{ marginBottom: '4%', background: 'none', border: 'none', paddingBottom: '0px' }}>
                  <div className="grid-container">
                    <div className="grid-item"><Rating
                      name={station2.name}
                      precision={0.5}
                      size="large"
                      value={station2.rating}
                      readOnly
                    /> </div>
                    <div className="grid-item">{station2.rating !== 0 ? station2.rating.toFixed(2) : 0}</div>
                    <div className="grid-item">{station2.date}</div>

                  </div>
                </Card.Header>

              </div>
            )
            : null
          }

          <Divider style={{ marginBottom: '3%' }}>
            <Chip label="Employees Feedback" />
          </Divider>
          <Tab.Container id="left-tabs-example" defaultActiveKey={props.modalMeals.filter(v => v.meal._id === modalID)?.[0]?.user.firstname}>
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  {!modalNoContent ? props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>

                      <Nav.Item key={station._id}>
                        <Nav.Link eventKey={station.user.firstname}>{station.user.firstname + ' ' + station.user.lastname} <br />
                          {station.rating > 0 ? <FontAwesomeIcon icon={faStar} style={{ color: '#ff9800', marginRight: '5%', height: '20px' }} /> : null}
                          {station.ingToAdd?.length ? <FontAwesomeIcon icon={faUserCheck} style={{ color: 'green', marginRight: '5%', height: '20px' }} /> : null}
                          {station.ingToRemove?.length ? <FontAwesomeIcon icon={faUserTimes} style={{ color: 'red', height: '20px', marginRight: '5%', height: '20px' }} /> : null}
                          {station.feedback ? <FontAwesomeIcon icon={faComment} style={{ marginRight: '5%', height: '20px' }} /> : null}
                        </Nav.Link>
                      </Nav.Item>
                    </>
                  ) :
                    <p>No Data Found</p>
                  }
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  {props.modalMeals.filter(v => v.meal._id === modalID).map(station =>
                    <>
                      <Tab.Pane key={station._id} eventKey={station.user.firstname}>
                        <CheckLists>
                          <h4>{station.user.firstname + ' ' + station.user.lastname}</h4>
                          <Rating
                            value={station.rating}
                            readOnly
                            precision={0.5}
                          />
                          {/* <p><u>Request to ADD:</u> {station.ingToAdd?.length ? station.ingToAdd.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'green' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p>
                          <p><u>Request to REMOVE:</u> {station.ingToRemove?.length ? station.ingToRemove.map(txt =>
                            <label key={station._id} className="container2" style={{ textAlign: 'left', color: 'red' }}>{txt}
                              <input value={txt} type="checkbox" checked='checked' onChange={() => { }} />
                              <span className="checkmark"></span>
                            </label>
                          )
                            : <h5>None</h5>}</p> */}

                          <p><u>Category:</u> <h5> {station.mealCategory ? station.mealCategory : 'No Feedback'}</h5></p>

                          <p><u>Feedback:</u> <h5> {station.feedback ? station.feedback : 'No Feedback'}</h5></p>
                        </CheckLists>
                      </Tab.Pane>

                    </>
                  )}

                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>


        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => [setmodalShow(false), setmodalID(''), setmodalName(''), setmodalNoContent('')]}>Close</Button>
        </Modal.Footer>
      </Modal>

      {/*DAILY RANKING CONTAINER*/}
      <div className="dailyrank-con">
        <CardTitle>
          <label> <h1>Top Meals</h1>
          </label>
          <ButtonTooltip text={'- Display the top meals in highest ranking order \n- Click the meal card for other details of the meal. \n- Icons indicate if a meal has some feedback from the employees. '} title={'Top Meals'} />
        </CardTitle>


        {/*Choices*/}
        <div className="choices-con">
          <div className="con1">
            <h2>MONDAY</h2>
          </div>

          <div className="con2">
            <h2>TUESDAY</h2>
          </div>

          <div className="con3">
            <h2>WEDNESDAY</h2>
          </div>

          <div className="con4">
            <h2>THURSDAY</h2>
          </div>

          <div className="con5">
            <h2>FRIDAY</h2>
          </div>
        </div>

        {/*BREAKFAST*/}
        <div className="breakfast-con">
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>BREAKFAST</h2>

          </div>

          <div className="con2">
            {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>

                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      name="size-small"
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                      style={{ fontSize: '2px' }}
                    />

                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>


                  </div>
                </Cards>

              )

              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>


          <div className="con2">
            {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />

                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con4">
            {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con5">
            {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con6">
            {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'breakfast').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'breakfast').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>
        </div>


        {/*LUNCH*/}
        <div className="lunch-con">
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>LUNCH</h2>

          </div>

          <div className="con2">
            {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con3">
            {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con4">
            {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con5">
            {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con6">
            {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'lunch').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'lunch').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>
        </div>


        {/*DINNER*/}
        <div className="dinner-con" >
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>DINNER</h2>

          </div>

          <div className="con2">
            {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con3">
            {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con4">
            {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con5">
            {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con6">
            {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'dinner').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'dinner').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>
        </div>


        {/*DINNER*/}
        <div className="dinner-con" >
          <div className="con1" style={{ backgroundColor: '#facc6b', backgroundImage: 'linear-gradient(315deg, #facc6b 0%, #fabc3c 74%)', borderRaduis: '10px 10px 0 0' }}>
            <h2 style={{ fontFamily: 'Montserrat, san serif', fontWeight: '900', color: 'white', textShadow: '0px 0px 5px rgba(0,0,0,0.5)' }}>SNACKS</h2>

          </div>

          <div className="con2">
            {props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Monday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con3">
            {props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Tuesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con4">
            {props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Wednesday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con5">
            {props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Thursday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>

          <div className="con6">
            {props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'snack').length !== 0 ?
              props.allTimeFav.filter(v => v.day === "Friday" && v.type === 'snack').sort(compareRating).map((station2, i) =>
                <Cards key={station2._id}>
                  <div onClick={() => [setmodalID(station2.mealID), setmodalShow(true),
                  setmodalName(station2.name.split('+')[0])]}>
                    <h5 className="cardTitle">{station2.name.split('+')[0]}</h5>
                    <Rating
                      size="small"
                      value={station2.rating}
                      readOnly
                      precision={0.5}
                    /><br />
                    {
                      station2?.ingToRemove?.length ?
                        <>

                          <FontAwesomeIcon icon={faUserTimes} size="lg" style={{ color: 'red', marginRight: '5%', height: '20px' }} />

                        </>
                        : null
                    }

                    {
                      station2?.ingToAdd?.length ?
                        <FontAwesomeIcon icon={faUserCheck} size="lg" style={{ color: 'green', height: '20px' }} /> : null

                    }

                    {
                      station2.feed ?
                        <FontAwesomeIcon icon={faComment} size="lg" style={{ marginLeft: '7px', color: 'gray', height: '20px' }} /> : null

                    }
                    <p style={{ marginBottom: '0px' }}>{station2.date}</p>
                  </div>
                </Cards>
              )
              : <Cards onClick={() => [setmodalShow(true), setmodalNoContent('No Data Found')]}><img style={{ all: 'initial', height: '100px' }} src={noDataFound} alt="noDataFound" /></Cards>
            }
          </div>
        </div>
      </div>
      <br />
    </React.Fragment>
  );
}
