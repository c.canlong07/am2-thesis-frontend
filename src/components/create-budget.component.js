import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import { height } from '@mui/material';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import TextareaAutosize from 'react-textarea-autosize';
import { Table, Modal, Button } from "react-bootstrap";
import moment from 'moment';
import { BudgetCards } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faPlusCircle, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import Chip from '@mui/material/Chip';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Swal from 'sweetalert2';
export default class CreateEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            budget: 0,
            budgetNote: '',
            budgetDate: new Date(),
            totalEmployees: 0,
            allDate: [],
            budgetThisWeek: [],
            budgetAndMeals: [],
            modalShow2: false,
            opening: [],
            allMeals: [],
            addThisBudget: 0,
            addThisBudgetArray: [],
            fixedRemainingBudget: 0
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.selectDateHandler = this.selectDateHandler.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onDefaultChange = this.onDefaultChange.bind(this);
        this.addRemainingBudget = this.addRemainingBudget.bind(this);
        this.removeRemainingBudget = this.removeRemainingBudget.bind(this);


    }

    componentDidMount() {

        let startOfWeek = moment().startOf('week').toDate();
        let endOfWeek = moment().endOf('week').toDate();

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget', {
            params: {
                startDate: (startOfWeek.getMonth() + 1) + '/' + (startOfWeek.getDate() + 1) + '/' + startOfWeek.getFullYear(),
                endDate: (endOfWeek.getMonth() + 1) + '/' + (endOfWeek.getDate() + 1) + '/' + endOfWeek.getFullYear()
            }
        })
            .then(res => {
                this.setState({ budgetThisWeek: res.data })
            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/mealTodayWeekly', {
            params: {
                startDate: (startOfWeek.getMonth() + 1) + '/' + (startOfWeek.getDate() + 1) + '/' + startOfWeek.getFullYear(),
                endDate: (endOfWeek.getMonth() + 1) + '/' + (endOfWeek.getDate() + 1) + '/' + endOfWeek.getFullYear()
            }
        })
            .then(res => {
                this.setState({ allMeals: res.data })

                let weeklyData = {};
                let ratingData = res.data;
                for (let element of ratingData) {
                    if (weeklyData[moment(element.mealDate).format('LL')]) {
                        weeklyData[moment(element.mealDate).format('LL')].weekly = weeklyData[moment(element.mealDate).format('LL')].weekly + (element.rating !== undefined ? element.rating : 0);
                        weeklyData[moment(element.mealDate).format('LL')].n++;
                        weeklyData[moment(element.mealDate).format('LL')].mealType += '#' + element.mealType;
                        weeklyData[moment(element.mealDate).format('LL')].mealName += '#' + element.mealName;
                        weeklyData[moment(element.mealDate).format('LL')].mealDesc += '#' + element.mealDesc;
                        weeklyData[moment(element.mealDate).format('LL')].mealIng += '#' + element.mealIng;
                        weeklyData[moment(element.mealDate).format('LL')].mealServedEst += '#' + element.mealServedEst;
                        weeklyData[moment(element.mealDate).format('LL')].mealTotalPrice += element.mealTotalPrice;
                        weeklyData[moment(element.mealDate).format('LL')].priceBreakdown += '#' + element.mealTotalPrice;
                        weeklyData[moment(element.mealDate).format('LL')].date = element.mealDate;
                        weeklyData[moment(element.mealDate).format('LL')].day = element.mealDate;
                        weeklyData[moment(element.mealDate).format('LL')]._id = element._id;
                    } else {
                        weeklyData[moment(element.mealDate).format('LL')] = {
                            weekly: element.rating ? element.rating : 0,
                            n: 1,
                            mealType: element.mealType,
                            mealName: element.mealName,
                            mealDesc: element.mealDesc,
                            mealIng: element.mealIng,
                            mealServedEst: element.mealServedEst,
                            priceBreakdown: element.mealTotalPrice,
                            mealTotalPrice: element.mealTotalPrice,
                            date: element.mealDate,
                            day: element.mealDate,
                            _id: element._id
                        };
                    }
                }

                let averageData = [];

                for (let element of Object.keys(weeklyData)) {
                    averageData.push({
                        mealDate: element.split('+')[0],
                        users: weeklyData[element].n,
                        rating: weeklyData[element].weekly / weeklyData[element].n,
                        mealType: weeklyData[element].mealType,
                        mealName: weeklyData[element].mealName,
                        mealIng: weeklyData[element].mealIng,
                        mealDesc: weeklyData[element].mealDesc,
                        mealServedEst: weeklyData[element].mealServedEst,
                        priceBreakdown: weeklyData[element].priceBreakdown,
                        mealTotalPrice: weeklyData[element].mealTotalPrice,
                        date: moment(weeklyData[element].date).format('ddd, MMM DD YYYY'),
                        day: moment(weeklyData[element].date).format('dddd'),
                        // _id: weeklyData[element]._id,
                    });
                }

                let theBudgetWithMeals = this.state.budgetThisWeek.map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
                this.setState({
                    budgetAndMeals: theBudgetWithMeals,
                    budget: theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[2]).format('LL') === moment(this.state.budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0)
                })

                console.log('theBudgetWithMeals', theBudgetWithMeals, theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[2]).format('LL') === moment(this.state.budgetDate).format('LL')),
                    theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[2]).format('LL') === moment(this.state.budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0))

            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/allbudgets')
            .then(res => {
                let dateGather = [];
                const mapping = res.data.map(year => dateGather.push(moment(year.budgetDate).format('MMMM DD YYYY')));
                this.setState({ allDate: dateGather });
            })
            .catch(error => {
                console.log(error);
            })

    }

    componentDidUpdate(prevProps, prevState) {

        if (prevState.budgetDate !== this.state.budgetDate || prevState.addThisBudget !== this.state.addThisBudget) {
            let startOfWeek = moment().startOf('week').toDate();
            let endOfWeek = moment().endOf('week').toDate();

            axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget', {
                params: {
                    startDate: (startOfWeek.getMonth() + 1) + '/' + (startOfWeek.getDate() + 1) + '/' + startOfWeek.getFullYear(),
                    endDate: (endOfWeek.getMonth() + 1) + '/' + (endOfWeek.getDate() + 1) + '/' + endOfWeek.getFullYear()
                }
            })
                .then(res => {
                    this.setState({ budgetThisWeek: res.data })
                })
                .catch(error => {
                    console.log(error);
                })

            axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/mealTodayWeekly', {
                params: {
                    startDate: (startOfWeek.getMonth() + 1) + '/' + (startOfWeek.getDate() + 1) + '/' + startOfWeek.getFullYear(),
                    endDate: (endOfWeek.getMonth() + 1) + '/' + (endOfWeek.getDate() + 1) + '/' + endOfWeek.getFullYear()
                }
            })
                .then(res => {
                    this.setState({ allMeals: res.data })

                    let weeklyData = {};
                    let ratingData = res.data;
                    for (let element of ratingData) {
                        if (weeklyData[moment(element.mealDate).format('LL')]) {
                            weeklyData[moment(element.mealDate).format('LL')].weekly = weeklyData[moment(element.mealDate).format('LL')].weekly + (element.rating !== undefined ? element.rating : 0);
                            weeklyData[moment(element.mealDate).format('LL')].n++;
                            weeklyData[moment(element.mealDate).format('LL')].mealType += '#' + element.mealType;
                            weeklyData[moment(element.mealDate).format('LL')].mealName += '#' + element.mealName;
                            weeklyData[moment(element.mealDate).format('LL')].mealDesc += '#' + element.mealDesc;
                            weeklyData[moment(element.mealDate).format('LL')].mealIng += '#' + element.mealIng;
                            weeklyData[moment(element.mealDate).format('LL')].mealServedEst += '#' + element.mealServedEst;
                            weeklyData[moment(element.mealDate).format('LL')].mealTotalPrice += element.mealTotalPrice;
                            weeklyData[moment(element.mealDate).format('LL')].priceBreakdown += '#' + element.mealTotalPrice;
                            weeklyData[moment(element.mealDate).format('LL')].date = element.mealDate;
                            weeklyData[moment(element.mealDate).format('LL')].day = element.mealDate;
                            weeklyData[moment(element.mealDate).format('LL')]._id = element._id;
                        } else {
                            weeklyData[moment(element.mealDate).format('LL')] = {
                                weekly: element.rating ? element.rating : 0,
                                n: 1,
                                mealType: element.mealType,
                                mealName: element.mealName,
                                mealDesc: element.mealDesc,
                                mealIng: element.mealIng,
                                mealServedEst: element.mealServedEst,
                                priceBreakdown: element.mealTotalPrice,
                                mealTotalPrice: element.mealTotalPrice,
                                date: element.mealDate,
                                day: element.mealDate,
                                _id: element._id
                            };
                        }
                    }

                    let averageData = [];

                    for (let element of Object.keys(weeklyData)) {
                        averageData.push({
                            mealDate: element.split('+')[0],
                            users: weeklyData[element].n,
                            rating: weeklyData[element].weekly / weeklyData[element].n,
                            mealType: weeklyData[element].mealType,
                            mealName: weeklyData[element].mealName,
                            mealIng: weeklyData[element].mealIng,
                            mealDesc: weeklyData[element].mealDesc,
                            mealServedEst: weeklyData[element].mealServedEst,
                            priceBreakdown: weeklyData[element].priceBreakdown,
                            mealTotalPrice: weeklyData[element].mealTotalPrice,
                            date: moment(weeklyData[element].date).format('ddd, MMM DD YYYY'),
                            day: moment(weeklyData[element].date).format('dddd'),
                            // _id: weeklyData[element]._id,
                        });
                    }

                    let theBudgetWithMeals = this.state.budgetThisWeek.map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
                    this.setState({
                        budgetAndMeals: theBudgetWithMeals,
                        budget: theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[2]).format('LL') === moment(this.state.budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0)
                    })

                })
                .catch(error => {
                    console.log(error);
                })
        }

        // if (prevState.addThisBudget !== this.state.addThisBudget) {
        //     this.setState({
        //         budget: Number(this.state.budget) + Number(this.state.addThisBudget)
        //     })
        // }



    }

    onValueChange(e, other) {
        if (e === 'remaining') {
            // this.setState({
            //     budget: other
            // })

        } else {
            this.setState({
                [e.target.dataset.name]: e.target.value
            })
        }

    }

    onSelectChange(e) {
        this.setState({
            ['mealType']: e.target.value
        })
    }

    onDefaultChange(e) {
        this.setState({
            budget: e.budget,
            budgetNote: e.budgetNote,
            mealIng: e.mealIng,
            mealServedEst: e.mealServedEst,
            mealTotalPrice: e.mealTotalPrice,
            mealType: e.mealType,
        })
    }

    selectDateHandler(value) {
        this.setState({
            ['budgetDate']: value,
            addThisBudget: 0,
            addThisBudgetArray: [],
            // budget: 0
        })
    }

    onSubmit(e) {
        e.preventDefault();

        const meals = {
            budget: this.state.budget,
            budgetNote: this.state.budgetNote,
            budgetDate: this.state.budgetDate,
        }



        axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/budget', meals)
            .then(res => window.location = "/meals")
            .catch(err => console.log('Error :' + err));
    }

    addRemainingBudget(station) {
        Swal.fire({
            title: station.budget - station.mealTotalPrice,
            // text: `Add the remaining budget of ${moment(station.budgetDate).format('LL')}?`,
            html:
                `<h4>Add the remaining budget of ${moment(station.budgetDate).format('LL')} to ${moment(this.state.budgetDate).format('LL')}?</h4>` +
                '<strong style="color: red; white-space: pre-wrap;">NOTE:</strong> ' +
                '<span style="color: red;">Please make sure you want to add the Remaining Budget to the Date Selected,\nUpon adding Remaining Budget, Date will be DISABLED</span>',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, add it!'
        }).then((result) => {
            if (result.isConfirmed) {
                this.setState({
                    addThisBudget: station.budget - station.mealTotalPrice,
                    addThisBudgetArray: [...this.state.addThisBudgetArray, `${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`]
                })

                axios.get(process.env.REACT_APP_BASE_URL + '/employee/add/usedremainingbudget',
                    { params: { id: station._id, remainingBudget: `${station.budget - station.mealTotalPrice}/${moment(this.state.budgetDate).format('LL')}` } })
                    .then(res => {
                        Swal.fire(
                            'Added!',
                            'Budget has been added.',
                            'success'
                        );

                        // window.location.href = "/meals";
                    })
                    .catch(error => {
                        console.log(error);
                    })


            }
        })

    }

    removeRemainingBudget(station) {

        Swal.fire({
            title: station.budget - station.mealTotalPrice,
            text: `Deduct the remaining budget of ${moment(station.budgetDate).format('LL')}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.isConfirmed) {
                this.setState({
                    budget: this.state.budget > station.mealTotalPrice ? this.state.budget - (station.budget - station.mealTotalPrice) : (station.budget - station.mealTotalPrice) - this.state.budget,
                    addThisBudgetArray: this.state.addThisBudgetArray.filter(item => item !== `${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`)
                })

                axios.get(process.env.REACT_APP_BASE_URL + '/employee/remove/removedremainingbudget',
                    { params: { id: station._id, remainingBudget: `${station.budget - station.mealTotalPrice}/${moment(this.state.budgetDate).format('LL')}` } })
                    .then(res => {
                        Swal.fire(
                            'Deducted!',
                            'Budget has been deducted.',
                            'success'
                        );
                    })
                    .catch(error => {
                        console.log(error);
                    })
            }
        })

    }

    // componentDidUpdate(prevProps, prevState) {

    //     console.log('this.state.budget + this.state.fixedRemainingBudget', this.state.budget + this.state.fixedRemainingBudget, this.state.budget, this.state.fixedRemainingBudget)

    //     // if (prevState.addThisBudget !== this.state.addThisBudget) {
    //     //     this.setState({
    //     //         budget: Number(this.state.budget) + Number(this.state.addThisBudget)
    //     //     })

    //     //     if (prevState.fixedRemainingBudget !== this.state.fixedRemainingBudget) {
    //     //         console.log('this.state.budget + this.state.fixedRemainingBudget', this.state.budget + this.state.fixedRemainingBudget, this.state.budget, this.state.fixedRemainingBudget)
    //     //         this.setState({
    //     //             budget: Number(this.state.budget) + Number(this.state.fixedRemainingBudget)
    //     //         })


    //     //     }
    //     // }

    // }





    render() {
        let data = this.state.defaultMeal;
        let defaultch = this.onDefaultChange;
        const mealTo = {
            budget: this.state.budget,
            budgetNote: this.state.budgetNote,
            budgetDate: this.state.budgetDate,
        }

        return (
            <LayoutForm>

                <Modal
                    show={this.state.modalShow2}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    scrollable='true'
                >
                    <Modal.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >
                        <Modal.Title id="contained-modal-title-vcenter">
                            Budget Details
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Divider style={{ marginBottom: '3%' }}>
                            <Chip label="Budget Details" />
                        </Divider>
                        <BudgetCards>
                            {/* <p>Request to ADD: <h6>{this.state.opening.toString()}</h6> </p> */}
                            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                                <ListItem alignItems="flex-start">
                                    <ListItemAvatar>
                                        <Avatar alt="B" src="/static/images/avatar/1.jpg" />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={this.state.opening}
                                        secondary={

                                            this.state.budgetAndMeals.filter(v => moment(v.budgetDate).format('LL') === this.state.opening).map((station, i) =>
                                                <React.Fragment>
                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary"
                                                    >
                                                        Budget Note: &nbsp;
                                                    </Typography>
                                                    {station.budgetNote}<br></br>

                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary"
                                                    >
                                                        Budget: &nbsp;
                                                    </Typography>
                                                    {station.budget} <br></br>

                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary"
                                                    >
                                                        Used Budget: &nbsp;
                                                    </Typography>
                                                    {station.mealTotalPrice} <br></br>

                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary"
                                                    >
                                                        Remaining Budget: &nbsp;
                                                    </Typography>
                                                    {station.budget - station.mealTotalPrice} <br></br>
                                                </React.Fragment>

                                            )
                                        }
                                    />
                                </ListItem>
                                <Divider variant="inset" component="li" />
                            </List>
                        </BudgetCards>

                        <Divider style={{ marginBottom: '3%' }}>
                            <Chip label="Meal within this Budget" />
                        </Divider>


                        <BudgetCards>
                            {/* <p>Request to ADD: <h6>{this.state.opening.toString()}</h6> </p> */}
                            {
                                this.state.allMeals.filter(v => moment(v.mealDate).format('LL') === this.state.opening).map((station, index) =>
                                    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar alt={(index + 1).toString()} src="/static/images/avatar/1.jpg" />
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={moment(station.mealDate).format('LL') + ' (' + station.mealTotalPrice + ')'}
                                                secondary={

                                                    <React.Fragment>
                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            Meal Name: &nbsp;
                                                        </Typography>
                                                        {station.mealName}<br></br>

                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            Meal Type: &nbsp;
                                                        </Typography>
                                                        {station.mealType}<br></br>
                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            Meal Description: &nbsp;
                                                        </Typography>
                                                        {station.mealDesc}<br></br>

                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            Meal Ingredients: &nbsp;
                                                        </Typography>
                                                        {station.mealIng}<br></br>

                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            Meal Serve Estimation: &nbsp;
                                                        </Typography>
                                                        {station.mealServedEst}<br></br>
                                                    </React.Fragment>

                                                }
                                            />
                                        </ListItem>
                                        <Divider variant="inset" component="li" />
                                    </List>

                                )

                            }

                        </BudgetCards>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => this.setState({ modalShow2: false, opening: [] })}>Close</Button>
                    </Modal.Footer>
                </Modal>
                <div className="container">
                    <h1>Budget</h1>

                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Budget Date</th>
                                <th>Budget Day</th>
                                <th>Total Budget</th>
                                <th>Excess Budget</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            {this.state.budgetAndMeals.map((station, i) =>
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{moment(station.budgetDate).format('LL')}</td>
                                    <td>{moment(station.budgetDate).format('dddd')}</td>
                                    <td>{station.budget}</td>
                                    <td style={{ zIndex: 9999 }} >{station.budget - station.mealTotalPrice} </td>
                                    <td>
                                        <FontAwesomeIcon
                                            icon={faInfoCircle} size="lg" style={{ color: 'gray', marginRight: '8%', height: '20px' }}
                                            onClick={() => this.setState({ modalShow2: true, opening: moment(station.budgetDate).format('LL') })}
                                        />

                                        {
                                            this.state.addThisBudgetArray.includes(`${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`)
                                                ||
                                                station.usedBudget?.filter(s => s.includes(moment(this.state.budgetDate).format('LL'))).length === 1
                                                ?
                                                <>
                                                    {this.state.addThisBudgetArray.includes(`${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`)}
                                                    {station.usedBudget?.filter(s => s.includes(moment(this.state.budgetDate).format('LL'))).length
                                                        ? this.onValueChange('remaining', station.budget - station.mealTotalPrice) : null}
                                                    <FontAwesomeIcon
                                                        icon={faMinusCircle} size="lg" style={{ color: 'red', marginRight: '8%', height: '20px' }}
                                                        // onClick={() => console.log('clickinggg')
                                                        onClick={() => this.removeRemainingBudget(station)}
                                                    /> </> :
                                                <FontAwesomeIcon
                                                    icon={faPlusCircle} size="lg" style={{ color: 'green', marginRight: '8%', height: '20px' }}
                                                    // onClick={() => console.log('clickinggg')
                                                    onClick={() => this.addRemainingBudget(station)}
                                                />
                                        }


                                    </td>
                                </tr>
                            )
                            }
                        </tbody>
                    </Table>

                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">

                                <div className="leftSide">
                                    <div className="form-group" style={{ padding: '0%' }}>
                                        <label>Date
                                            {/* {this.state.addThisBudgetArray.length !== 0 ? <span style={{ color: 'red', textTransform: 'capitalize' }}> *Disabled</span> : null} */}
                                        </label>
                                        <DatePicker
                                            id="budgetDate"
                                            className="form-control"
                                            dateFormat="yyyy/MM/dd"
                                            selected={this.state.budgetDate}
                                            onChange={(value) => this.selectDateHandler(value)}
                                            // minDate={today}
                                            // readOnly={this.state.addThisBudgetArray.length === 0 ? false : true}
                                            todayButton={"Today"} />
                                    </div>
                                    <div className="form-group">
                                        <label>Budget</label>
                                        <input type="number" className="form-control" data-name="budget" value={mealTo.budget} required onChange={this.onValueChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Budget Note</label>
                                        <TextareaAutosize
                                            className="textArea form-control" data-name="budgetNote" value={mealTo.budgetNote} required onChange={this.onValueChange}
                                            minRows={3}
                                            maxRows={6}
                                            placeholder="Description..."
                                        />
                                        {/* <textarea style={{height: 'auto', overflowY:'hidden'}} type="text" className="form-control" data-name="budgetNote"  required onChange={this.onValueChange} /> */}
                                    </div>


                                </div>
                            </div>

                            <Buttons>
                                <button type="submit" style={this.state.allDate.includes(moment(this.state.budgetDate).format('MMMM DD YYYY')) ? { marginRight: 10, pointerEvents: 'none', color: 'white', background: 'red', border: 'red' } : { marginRight: 10 }} className="btn btn-primary">
                                    {this.state.allDate.includes(moment(this.state.budgetDate).format('MMMM DD YYYY')) ? 'Budget for Selected Date Already Exist' : 'Save Budget'}
                                </button>
                                <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
                            </Buttons>

                        </div>
                    </form>
                </div>
            </LayoutForm>
        )
    }


}
