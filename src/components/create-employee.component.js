import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { LayoutForm } from '../styles/LayoutForm';
import { Buttons } from '../styles/LayoutForm';
import Swal from 'sweetalert2';

import { companyDepartment, companyRoles } from '../globalConfig/config.js';
export default class CreateEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            firstname: '',
            lastname: '',
            address: '',
            department: 'IT Department',
            email: '',
            cpnumber: '',
            role: 'Employee',
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);

    }


    onValueChange(e) {
        this.setState({
            [e.target.dataset.name]: e.target.value
        })
    }

    onSelectChange(e) {
        this.setState({
            [e.target.dataset.name]: e.target.value
        })
    }


    onSubmit(e) {
        e.preventDefault();

        const employee = {
            firstname: this.state.firstname.replace(/(^\w|\s\w)/g, m => m.toUpperCase()),
            lastname: this.state.lastname.replace(/(^\w|\s\w)/g, m => m.toUpperCase()),
            address: this.state.address.replace(/(^\w|\s\w)/g, m => m.toUpperCase()),
            department: this.state.department.replace(/(^\w|\s\w)/g, m => m.toUpperCase()),
            email: this.state.email,
            cpnumber: this.state.cpnumber,
            role: this.state.role
        }



        axios.post(process.env.REACT_APP_BASE_URL + '/employee/add', employee)
            .then(res =>
                Swal.fire({
                    title: 'Employee Record has been Added',
                    text: "Successfully Added an Employee Record",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/employees"
                    }
                })
            )
            .catch(err =>
                Swal.fire(
                    'Saving Failed!',
                    'This happen when an Email is already used, Please make sure to use UNIQUE EMAIL.',
                    'warning'
                )
                // console.log('Error :' + err)
            );
    }

    render() {
        return (
            <LayoutForm>
                <div className="container">
                    <h1>Create Employee</h1>
                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">

                                <div className="leftSide">
                                    <div className="form-group">
                                        <label>Firstname</label>
                                        <input type="text" className="form-control" data-name="firstname" required onChange={this.onValueChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Lastname</label>
                                        <input type="text" className="form-control" data-name="lastname"  required onChange={this.onValueChange} />
                                    </div>

                                    <div className="form-group">
                                        <label>Address</label>
                                        <input type="text" className="form-control" data-name="address" required onChange={this.onValueChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Department</label>
                                        <select value={this.state.department} className="mealType  form-control" data-name="department" required onChange={this.onSelectChange}>
                                            {
                                                companyDepartment.map(item => (
                                                    <option value={item}>{item}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                </div>

                                <div className="rightSide">
                                    <div className="form-group">
                                        <label>Role</label>
                                        <select value={this.state.role} className="mealType  form-control" data-name="role" required onChange={this.onSelectChange}>
                                            {
                                                companyRoles.map(item => (
                                                    <option value={item}>{item}</option>
                                                ))
                                            }
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="email" className="form-control" data-name="email" required onChange={this.onValueChange} />
                                    </div>

                                    <div className="form-group">
                                        <label>Cellphone Number</label>
                                        <input type="tel" className="form-control" data-name="cpnumber" min="0" minlength="11" maxlength="11" required onChange={this.onValueChange} />
                                    </div>
                                </div>
                            </div>
                            <Buttons>
                                <button type="submit" style={{ marginRight: 10 }} className="btn btn-primary">Add Record</button>
                                <Link to='/employees' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-primary">Back</Link>
                            </Buttons>
                        </div>

                    </form>
                </div>
            </LayoutForm>
        )

    }


}