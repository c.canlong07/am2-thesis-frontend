import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import axios from 'axios';
import Swal from 'sweetalert2';
export default class EditEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            firstname: '',
            lastname: '',
            address: '',
            department: '',
            email: '',
            cpnumber: '',
            password: '',
            role: '',
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);

    }



    componentDidMount() {
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/' + this.props.match.params.id)
            .then(res => {
                this.setState({
                    firstname: res.data.firstname,
                    lastname: res.data.lastname,
                    address: res.data.address,
                    department: res.data.department,
                    email: res.data.email,
                    cpnumber: res.data.cpnumber,
                    password: res.data.password,
                    role: res.data.role
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

    onValueChange(e) {
        this.setState({
            [e.target.dataset.name]: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();

        const employee = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            address: this.state.address,
            department: this.state.department,
            email: this.state.email,
            cpnumber: this.state.cpnumber,
            password: this.state.password,
            role: this.state.role
        }



        axios.post(process.env.REACT_APP_BASE_URL + '/employee/update/' + this.props.match.params.id, employee)
            .then(res =>
                Swal.fire({
                    title: 'Employee Record has been Updated',
                    text: "Successfully Update an Employee Record",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/users"
                    }
                })
            )
            .catch(err =>
                Swal.fire(
                    'Saving Failed!',
                    'This happen when an Email is already used, Please make sure to use a unique email.',
                    'warning'
                )
            );
    }

    render() {
        return (
            <LayoutForm>
                <div className="container">
                    <h1>Update your info</h1>

                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">
                                <div className="leftSide">
                                    <div className="form-group">
                                        <label>Firstame</label>
                                        <input type="text" className="form-control" data-name="firstname" required onChange={this.onValueChange} value={this.state.firstname} />
                                    </div>
                                    <div className="form-group">
                                        <label>Lastname</label>
                                        <input type="text" className="form-control" data-name="lastname" required onChange={this.onValueChange} value={this.state.lastname} />
                                    </div>

                                    <div className="form-group">
                                        <label>Address</label>
                                        <input type="text" className="form-control" data-name="address" required onChange={this.onValueChange} value={this.state.address} />
                                    </div>
                                </div>

                                <div className="rightSide">
                                    {/* <div className="form-group">
                                        <label>Department</label>
                                        <input type="text" className="form-control" data-name="department" required onChange={this.onValueChange} value={this.state.department} />
                                    </div> */}

                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="email" className="form-control" data-name="email" required onChange={this.onValueChange} value={this.state.email} />
                                    </div>

                                    <div className="form-group">
                                        <label>Cellphone Number</label>
                                        <input type="tel" className="form-control" data-name="cpnumber" required onChange={this.onValueChange} value={this.state.cpnumber} />
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input type="text" className="form-control" data-name="password" required onChange={this.onValueChange} value={this.state.password} />
                                    </div>
                                </div>
                            </div>

                            <Buttons>
                                <button type="submit" style={{ marginRight: 10 }} className="btn btn-primary">Update</button>
                                <Link to='/users' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-primary">Back</Link>
                            </Buttons>
                        </div>
                    </form>
                </div>
            </LayoutForm>
        )
    }


}