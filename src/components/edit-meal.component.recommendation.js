import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import axios from 'axios';
import TextareaAutosize from 'react-textarea-autosize';
import Swal from 'sweetalert2';
import moment from 'moment';
import { Table } from "react-bootstrap";
export default class EditEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            budget: '',
            _id: '',
            mealName: '',
            mealDesc: '',
            mealIng: '',
            mealDate: '',
            mealServedEst: '',
            mealTotalPrice: '',
            mealTotalPerEmployee: '',
            mealType: 'breakfast',
            defaultMeal: undefined,
            budgetToday: 0,
            usedBudget: 0,
            mealOriginalPrice: 0,
            mealCategory: 'vegetable',
            allBudgets: [],
            allMeals: [],
            mealNameR: '',
            mealingToAdd: '',
            mealingToRemove: '',
            mealCategoryR: '',
            mealServedEstR: '',
            mealTotalPriceR: '',
            mealTotalPerEmployeeR: '',

        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.DefaultChange = this.DefaultChange.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.allAreEqual = this.allAreEqual.bind(this);

    }



    componentDidMount() {
        let paramsDate = window.location.pathname.split('/')[3];
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/getMeal/' + this.props.match.params.id)
            .then(res => {
                console.log('heyyyyyy', res.data)
                this.setState({
                    mealName: res.data.mealName,
                    budget: res.data.budget,
                    _id: res.data._id,
                    mealDesc: res.data.mealDesc,
                    // mealIng: res.data.mealIng,
                    mealDate: res.data.mealDate,
                    mealServedEst: res.data.mealServedEst,
                    mealTotalPrice: res.data.mealTotalPrice,
                    mealTotalPerEmployee: res.data.mealTotalPerEmployee,
                    mealType: res.data.mealType,
                    mealCategory: res.data.mealCategory,
                    defaultMeal: res.data.defaultMeal !== undefined ? eval(res.data.defaultMeal) : false,
                    mealOriginalPrice: res.data.mealTotalPrice,
                })

                if (res.data.mealRecommendationData) {
                    this.setState({
                        mealNameR: res.data.mealRecommendationData[0].mealName,
                        mealCategoryR: res.data.mealRecommendationData[0].mealCategory,
                        mealIng: res.data.mealRecommendationData[0].mealIng,
                        mealServedEstR: res.data.mealRecommendationData[0].mealServedEst,
                        mealingToAdd: res.data.mealRecommendationData[0].mealingToAdd.filter((data, index) => {
                            return res.data.mealRecommendationData[0].mealingToAdd.indexOf(data) === index;
                        }),
                        mealingToRemove: res.data.mealRecommendationData[0].mealingToRemove.filter((data, index) => {
                            return res.data.mealRecommendationData[0].mealingToRemove.indexOf(data) === index;
                        }),
                    })

                }
                console.log('resss', res.data, res.data.mealRecommendationData[0].mealingToRemove.filter((data, index) => {
                    return res.data.mealRecommendationData[0].mealingToRemove.indexOf(data) === index;
                }), res.data.mealRecommendationData[0].mealingToRemove)
            })
            .catch(error => {
                console.log(error);
            })
        let startDate = new Date();

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/allbudgets')
            .then(res => {
                this.setState({ allBudgets: res.data })
                this.setState({ budgetToday: res.data.filter(el => el.budgetDate === moment(paramsDate).format("MM-DD-YYYY") && el.deleted === undefined)?.[0]?.budget });
            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/getmealbybudget', {

            params: {
                id: window.location.pathname.split('/')[4]
            }
        })
            .then(res => {

                // console.log('heyyyyy', res.data,
                //     moment(paramsDate).format("M/DD/YYYY"),
                //     moment(paramsDate).add(1, "days").format("M/DD/YYYY"),
                //     (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                //     (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear(),
                //     res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next),
                // )
                console.log('useddddddddd', res.data)
                this.setState({
                    allMeals: res.data,
                    usedBudget: res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next)
                })
            })
            .catch(error => {
                console.log(error);
            })


    }

    onValueChange(e, id) {
        if (e.target.dataset.name === 'mealTotalPrice') {
            let objIndex = this.state.allMeals.findIndex((obj => obj._id === id));
            // console.log('res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next)',
            //     this.state.allMeals.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next))
            // console.log('heyyyyy', this.state.allBudgets, id, objIndex, this.state.allMeals, this.state.allBudgets.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next))
            this.state.allMeals[objIndex].mealTotalPrice = Number(e.target.value);
            this.setState({
                [e.target.dataset.name]: e.target.value,
                // usedBudget: e.target.value
            })
        } else if (e.target.dataset.name === 'mealTotalPerEmployee') {

            let objIndex = this.state.allMeals.findIndex((obj => obj._id === id));
            let newTotal = Number(e.target.value) * Number(this.state.mealServedEst);
            this.state.allMeals[objIndex].mealTotalPrice = newTotal;
            this.setState({
                [e.target.dataset.name]: e.target.value,
                // usedBudget: e.target.value
            })
        } else if (e.target.dataset.name === 'mealServedEstR') {

            let objIndex = this.state.allMeals.findIndex((obj => obj._id === id));
            // console.log('res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next)',
            //     this.state.allMeals.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next))
            // console.log('heyyyyy', this.state.allBudgets, id, objIndex, this.state.allMeals, this.state.allBudgets.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next))
            let newTotal = Number(e.target.value) * Number(this.state.mealTotalPerEmployee);
            console.log('mealServedEstR', this.state.mealServedEstR, e.target.value, this.state.allMeals,
                newTotal, objIndex)

            this.state.allMeals[objIndex].mealTotalPrice = newTotal;
            this.setState({
                [e.target.dataset.name]: e.target.value,
                // usedBudget: e.target.value
            })
        } else {
            this.setState({
                [e.target.dataset.name]: e.target.value
            })
        }
    }

    onSelectChange(e) {
        this.setState({
            ['mealType']: e.target.value
        })
    }

    DefaultChange(e) {
        this.setState({
            ['defaultMeal']: e.target.checked
        })
    }


    onCategoryChange(e) {
        this.setState({
            ['mealCategory']: e.target.value
        })
    }

    allAreEqual(obj) {
        return new Set(Object.values(obj)).size === 1;
    }

    onSubmit(e) {
        e.preventDefault();

        // const employee = {
        //     mealName: this.state.mealName,
        //     mealDesc: this.state.mealDesc,
        //     mealIng: this.state.mealIng,
        //     mealDate: this.state.mealDate,
        //     mealServedEstR: this.state.mealServedEstR,
        //     mealTotalPrice: Number(this.state.mealTotalPerEmployee) * Number(this.state.mealServedEst),
        //     mealTotalPerEmployee: this.state.mealTotalPerEmployee,
        //     mealType: this.state.mealType === undefined ? 'breakfast' : this.state.mealType,
        //     mealCategory: this.state.mealCategory === undefined ? 'vegetable' : this.state.mealCategory,
        //     defaultMeal: this.state.defaultMeal,
        // }
        const mealRecommendationData = {};
        const mealData = {};

        mealData['mealName'] = this.state.mealName;
        mealData['mealDesc'] = this.state.mealDesc;
        mealData['mealIng'] = this.state.mealIng;
        mealData['mealType'] = this.state.mealType;
        mealData['mealDate'] = this.state.mealDate;
        mealData['mealServedEst'] = this.state.mealServedEstR;
        mealData['mealCategory'] = this.state.mealCategoryR;
        mealData['mealTotalPrice'] = Number(this.state.mealTotalPerEmployee) * Number(this.state.mealServedEstR);
        mealData['mealTotalPerEmployee'] = this.state.mealTotalPerEmployee;
        mealData['defaultMeal'] = this.state.defaultMeal;

        mealRecommendationData['mealRecommendationData'] = [mealData];

        axios.post(process.env.REACT_APP_BASE_URL + '/employee/updatemealRecommendation/' + this.state._id, mealRecommendationData)
            .then(res =>
                Swal.fire({
                    title: 'Meal Recommendation has been Updated',
                    text: "Successfully Update a Meal",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/meals"
                    }
                }))
            .catch(err => console.log('Error :' + err));
    }

    render() {
        let theOverallBudget = this.state.budgetToday - (Number(this.state.mealServedEst) * Number(this.state.mealTotalPerEmployee));
        let useBudgets;
        if (this.state.allMeals.length) {
            useBudgets = this.state.allMeals.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next);

            console.log('allmeals', this.state.budgetToday - useBudgets, this.state.budgetToday, useBudgets)

        }
        return (
            <LayoutForm>
                <div className="container">
                    <h1>Budget</h1>
                    {/* <p style={theOverallBudget < 0 ? { float: 'center', textAlign: 'center', fontSize: '56px', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '56px', }}> {theOverallBudget}</p> */}
                    <p style={{ float: 'center', textAlign: 'center', fontSize: '2.5rem', margin: '0px', color: '#203354', fontWeight: 'bold' }}>
                        Total Budget: {this.state.budgetToday}
                    </p>
                    <p style={(Number(this.state.mealServedEst) * Number(this.state.mealTotalPerEmployee)) > this.state.budgetToday ? { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'green' }}>
                        Available Budget: {this.state.budgetToday - useBudgets}
                    </p>
                    <p style={(Number(this.state.mealServedEst) * Number(this.state.mealTotalPerEmployee)) > this.state.budgetToday ? { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '2%', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '2%', color: 'green' }}>
                        Used Budget: {useBudgets}
                    </p>
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Meal Name</th>
                                <th>Meal Type</th>
                                <th>Budget Alloted</th>
                                <th>Meal Estimation</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.allMeals.map((station, i) =>
                                <>
                                    <tr style={station._id === this.state._id ? { color: 'green', fontWeight: 'bold' } : {}}>
                                        <td>{i + 1}</td>
                                        <td>{station.mealName}</td>
                                        <td>{station.mealType}</td>
                                        <td>{station.mealTotalPrice}</td>
                                        <td>{station.mealServedEst}</td>
                                    </tr>
                                </>
                            )
                            }
                            {/* {
                                mealTo.mealName?.length !== 0 ?
                                    <tr style={{ color: 'green', fontWeight: 'bold' }}>
                                        <td>+</td>
                                        <td>{mealTo.mealName}</td>
                                        <td>{mealTo.mealType}</td>
                                        <td>{mealTo.mealTotalPrice}</td>
                                        <td>{mealTo.mealServedEst}</td>
                                    </tr> : null

                            } */}
                        </tbody>
                    </Table>

                </div>
                <div className="container">
                    <h1>Update Meal info base on Recommendation</h1>
                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">

                                <div className="leftSide">
                                    <div className="form-group">
                                        <label>Meal Name</label>
                                        <input type="text" className="form-control" data-name="mealName" required onChange={this.onValueChange} value={this.state.mealName} />
                                    </div>
                                    <div className="form-group">
                                        <label>Meal Description</label>
                                        <TextareaAutosize type="text" className="form-control" data-name="mealDesc" required onChange={this.onValueChange} value={this.state.mealDesc} />
                                    </div>

                                    <div className="form-group">
                                        <label>Meal Ingredients</label><br />
                                        <label style={{ color: 'red', fontWeight: 'bold' }}>({this.state.mealingToRemove})</label>
                                        <label style={{ color: 'green', fontWeight: 'bold' }}>({this.state.mealingToAdd})</label>
                                        <TextareaAutosize type="text" className="form-control" data-name="mealIng" required onChange={this.onValueChange} value={this.state.mealIng} />
                                    </div>
                                    <div className="form-group">
                                        <label>Meal Category <label style={{ color: 'gray', fontWeight: 'bold' }}> ({this.state.mealCategory ? this.state.mealCategory : null})</label></label>
                                        <select value={this.state.mealCategoryR} className="mealType  form-control" data-name="mealCategoryR" required onChange={this.onCategoryChange}>
                                            <option style={{ textTransform: 'bold' }} value="vegetable">Vegetable</option>
                                            {/* <option value="Meat">Meat</option> */}
                                            <optgroup label="Meat">
                                                <option value="Meat: Beef">Beef</option>
                                                <option value="Meat: Chicken">Chicken</option>
                                                <option value="Meat: Pig">Pig</option>
                                                <option value="Meat: Pork">Pork</option>
                                            </optgroup>
                                            <optgroup label="Seafood">
                                                <option value="Seafood: Crab">Crab</option>
                                                <option value="Seafood: Fish">Fish</option>
                                                <option value="Seafood: Lobster">Lobster</option>
                                                <option value="Seafood: Mussels">Mussels</option>
                                                <option value="Seafood: Oyster">Oyster</option>
                                                <option value="Seafood: Shrimp">Shrimp</option>
                                                <option value="Seafood: Squid">Squid</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div className="rightSide">
                                    <div className="form-group">
                                        <label>Meal Type</label>
                                        <select value={this.state.mealType} className="form-control" data-name="mealType" required onChange={this.onSelectChange}>
                                            <option value="breakfast" selected>Breakfast</option>
                                            <option value="lunch">Lunch</option>
                                            <option value="dinner">Dinner</option>
                                            <option value="snack">Snack</option>
                                        </select>
                                    </div>

                                    {/* <div className="form-group">
                            <label>Meal Date</label>
                            <input style={{width: '50%'}} type="text"  className="form-control" data-name="mealDate"  required onChange={this.onValueChange} value={this.state.mealDate} />
                        </div> */}

                                    <div className="form-group">
                                        <label>Serve Estimation <label style={{ color: 'gray', fontWeight: 'bold' }}> ({this.state.mealServedEst ? this.state.mealServedEst : null})</label></label>
                                        <input type="number" min="1" step='1' className="form-control" data-name="mealServedEstR" required onChange={(e) => this.onValueChange(e, this.state._id)} value={this.state.mealServedEstR} />
                                    </div>

                                    <div className="form-group" style={{ padding: '0%' }}>
                                        <label>Budget per Employee {useBudgets > this.state.budgetToday ? <label style={{ color: 'red', textTransform: 'none' }}>&nbsp; *Budget is not enough</label> : null}</label>
                                        <input style={useBudgets > this.state.budgetToday ? { color: 'red' } : {}} type="number" min="1" className="form-control" data-name="mealTotalPerEmployee" value={this.state.mealTotalPerEmployee} required onChange={(e) => this.onValueChange(e, this.state._id)} />
                                    </div>

                                    <div className="form-group">
                                        <label>Overall Meal Price Total</label>
                                        <input readOnly style={useBudgets > this.state.budgetToday ? { color: 'red' } : {}} type="number" min="1" step='1' className="form-control" data-name="mealTotalPrice" required onChange={(e) => this.onValueChange(e, this.state._id)} value={Number(this.state.mealTotalPerEmployee) * Number(this.state.mealServedEst)} />
                                    </div>

                                    {/* <div className="form-group">
                                        <label>Default</label>
                                    </div> */}
                                    <div className="form-group" style={{ textAlign: 'center' }}>
                                        <label style={{ textAlign: 'justify' }}>By Default this recommendation are only stored as Recommendation. The current meal for today
                                            is still the Original Meal.
                                            Hence if you want this Meal to start showing as your meal. Please check the box below.
                                        </label>
                                        <input type="checkbox" style={{ transform: 'scale(1.5)', marginRight: 10 }} checked={this.state.defaultMeal} data-name="defaultMeal" onChange={this.DefaultChange} />

                                    </div>
                                </div>

                            </div>

                            <Buttons>
                                <button style={useBudgets > this.state.budgetToday ? { marginRight: 10, pointerEvents: 'none', color: 'white', background: 'red', border: 'red' } : { marginRight: 10 }} className="btn btn-primary">{useBudgets > this.state.budgetToday ? 'Please Check the Available Budget' : 'Save Meal'}</button>
                                <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
                            </Buttons>

                        </div>
                    </form>
                </div >
            </LayoutForm >
        )
    }


}