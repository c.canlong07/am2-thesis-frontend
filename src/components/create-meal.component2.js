import React, { Component, useEffect, useState } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker'
import { height } from '@mui/material';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import TextareaAutosize from 'react-textarea-autosize';
import { Row, Tabs, Tab, Table, Nav, Col, ListGroup, Card, Button } from "react-bootstrap";
import moment from 'moment';
import Rating from '@mui/material/Rating';
import LastWeek from '../dataVisualization/LastWeek';
import TotalPresentIcon from '../img/icon_totalPresent.png';
import TotalAbsentIcon from '../img/icon_totalAbsent.png';
import { AttendanceStyled } from '../styles/TableLayout';
import LastWeekInCreate from '../dataVisualization/LastWeekInCreate';
import { CardTitle } from '../styles/RecommendationStyle';
import { ButtonTooltip2 } from '../globalFunctions/shareableComponents';
import Swal from 'sweetalert2';
let morningToTime = '';
let afternoonToTime = '';
let afternoonFromTime = '';
function CreateMeal() {
    const [mealName, setmealName] = useState('');
    const [mealDesc, setmealDesc] = useState('');
    const [mealIng, setmealIng] = useState('');
    const [mealDate, setmealDate] = useState(new Date());
    const [mealServedEst, setmealServedEst] = useState('');
    const [mealTotalPrice, setmealTotalPrice] = useState('');
    const [mealTotalPerEmployee, setmealTotalPerEmployee] = useState('');
    const [mealType, setmealType] = useState('breakfast');
    const [mealCategory, setmealCategory] = useState('vegetable');
    const [defaultMeal, setdefaultMeal] = useState([]);
    const [totalEmployees, settotalEmployees] = useState(0);
    const [budgetToday, setbudgetToday] = useState(0);
    const [usedBudget, setusedBudget] = useState(0);
    const [newBudget, setnewBudget] = useState(0);
    const [theBudgetDate, settheBudgetDate] = useState(new Date());
    const [mealWithinSpecificBudget, setmealWithinSpecificBudget] = useState([]);
    const [allTimeFav, setallTimeFav] = useState([]);
    const [morningPresent, setmorningPresent] = useState('');
    const [morningToIO, setmorningToIO] = useState('');
    const [afternoontoIO, setafternoontoIO] = useState('');
    const [afternoonPresent, setafternoonPresent] = useState('');
    const [removeDuplicates, setremoveDuplicates] = useState([]);
    const [modalMeals, setmodalMeals] = useState([]);
    const [yesterdayAtt, setyesterdayAtt] = useState([]);
    const [todaysAtt, settodaysAtt] = useState([]);

    const compareRating = (a, b) => {
        let first = a.rating ? a.rating : 0;
        let second = b.rating ? b.rating : 0;

        return second - first;
    }

    useEffect(() => {
        setmealServedEst(mealType !== 'dinner' ? morningPresent : afternoonPresent)

    }, [mealType])

    useEffect(() => {
        let theDate = new Date(window.location.pathname.split('/')[3]);
        setmealDate(theDate);

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/getMealbydefault')
            .then(res => {
                const mappingHistory = res.data.map(year => [{
                    mealName: year.mealName,
                    // _id: year._id,
                    // rating: year?.rating,
                    // date: Moment(year.createdAt).format('MMMM DD YYYY HH:mm:ss'),
                    mealDesc: year.mealDesc,
                    mealIng: year.mealIng,
                    mealType: year.mealType,
                    mealServedEst: year.mealServedEst,
                    mealTotalPrice: year.mealTotalPrice
                }]);
                setdefaultMeal(mappingHistory);

            })
            .catch(error => {
                console.log(error);
            })

        let startDate = new Date();
        let endDate = new Date();


        axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
            params: {
                startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
            }
        })
            .then(res => {
                setmodalMeals(res.data);
            })
            .catch(error => {
                console.log(error);
            })



        axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/budSpecificToday', { params: { id: window.location.pathname.split('/')[2] } })
            .then(res => {
                let date = new Date(res.data[0].budgetDate);
                setbudgetToday(res.data[0].budget);
                settheBudgetDate(date);
            })
            .catch(error => {
                console.log(error);
            })

        // axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealToday', {
        //     params: {
        //         startDate: (theDate.getMonth() + 1) + '/' + theDate.getDate() + '/' + theDate.getFullYear(),
        //         endDate: (theDate.getMonth() + 1) + '/' + (theDate.getDate() + 1) + '/' + theDate.getFullYear()
        //     }
        // })
        //     .then(res => {
        //         setusedBudget(res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next));
        //         setmealWithinSpecificBudget(res.data);
        //         const mapping = res.data.map(year => [{
        //             mealName: year.mealName,
        //             id: year._id,
        //             mealDesc: year.mealDesc,
        //             mealIng: year.mealIng,
        //             mealType: year.mealType,
        //         }]);

        //     })
        //     .catch(error => {
        //         console.log(error);
        //     })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
            .then(res => {
                let theNextWeeklyMeal = res.data.filter(function (weeklyDate) {
                    return moment(new Date(weeklyDate.mealDate)).isAfter(new Date(moment(theDate).subtract("days", 1).format("MM-DD-YYYY"))) &&
                        moment(new Date(weeklyDate.mealDate)).isBefore(new Date(moment(theDate).add("days", 1).format("MM-DD-YYYY")));
                });
                console.log('allmealsssssssssssssss', res.data, theDate, theNextWeeklyMeal);
                setusedBudget(theNextWeeklyMeal.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next));
                setmealWithinSpecificBudget(theNextWeeklyMeal);
                const mapping = theNextWeeklyMeal.map(year => [{
                    mealName: year.mealName,
                    id: year._id,
                    mealDesc: year.mealDesc,
                    mealIng: year.mealIng,
                    mealType: year.mealType,
                }]);


            })
            .catch(error => {
                console.log(error);
            })

        let theWeekly = getLastWeek();

        //weekly
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealWeekly', {
            params: {
                startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
                endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
            }
        })
            .then(res => {
                let theWeeklyData = res.data.map(val => ({ ...val, mealDate: moment(val.mealDate).format('MM/DD/yyyy') }));
                let weeklyData = {};
                for (let element of theWeeklyData) {
                    if (weeklyData[element.mealDate]) {
                        weeklyData[element.mealDate].sum = weeklyData[element.mealDate].sum + (element.rating !== undefined ? element.rating : 0);
                        weeklyData[element.mealDate].n++;
                        weeklyData[element.mealDate].mealDate = element.mealDate;
                        weeklyData[element.mealDate].mealName = element.mealName;
                        weeklyData[element.mealDate].mealDesc = element.mealDesc;

                    } else {
                        weeklyData[element.mealDate] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealDate: element.mealDate,
                            mealName: element.mealName,
                            mealDesc: element.mealDesc,
                        };
                    }
                }

                let weeklyOverall = [];

                for (let element of Object.keys(weeklyData)) {
                    weeklyOverall.push({
                        name: element,
                        users: weeklyData[element].n,
                        rating: weeklyData[element].sum / weeklyData[element].n,
                        mealDate: weeklyData[element].mealDate,
                        mealName: weeklyData[element].mealName,
                        mealDesc: weeklyData[element].mealDesc,

                    });
                }
            })
            .catch(error => {
                console.log(error);
            })


        axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
            params: {
                startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
                endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
            }
        })
            .then(res => {

                let sumData = {};
                let ratingData = res.data;
                for (let element of ratingData) {
                    if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
                        sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
                        sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealType = element.meal.mealType;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealDesc = element.meal.mealDesc;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealIng = element.meal.mealIng;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealServedEst = element.meal.mealServedEst;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealTotalPrice = element.meal.mealTotalPrice;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
                        sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
                    } else {
                        sumData[element.meal.mealName + '+' + element.meal.mealType] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealType: element.meal.mealType,
                            mealDesc: element.meal.mealDesc,
                            mealIng: element.meal.mealIng,
                            mealServedEst: element.meal.mealServedEst,
                            mealTotalPrice: element.meal.mealTotalPrice,
                            date: element.meal.mealDate,
                            day: element.meal.mealDate,
                            feed: element.feedback,
                            _id: element._id
                        };
                    }
                }

                let averageData = [];

                for (let element of Object.keys(sumData)) {
                    averageData.push({
                        mealName: element.split('+')[0],
                        users: sumData[element].n,
                        rating: sumData[element].sum / sumData[element].n,
                        mealType: sumData[element].mealType,
                        mealIng: sumData[element].mealIng,
                        mealDesc: sumData[element].mealDesc,
                        mealServedEst: sumData[element].mealServedEst,
                        mealTotalPrice: sumData[element].mealTotalPrice,
                        date: moment(sumData[element].date).format('ddd, MMM DD YYYY'),
                        day: moment(sumData[element].date).format('dddd'),
                        feed: sumData[element].feed,
                        _id: sumData[element]._id,
                    });
                }

                let unique = [];
                averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)).map(x => unique.filter(a => a.mealName == x.mealName && a.day === x.day).length > 0 ? null : unique.push(x));

                setallTimeFav(averageData);
            })
            .catch(error => {
                console.log(error);
            });

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
            params: {
                startDate: '1/1/2000',
                endDate: '1/1/2023'
            }
        })
            .then(res => {

                let sumData = {};
                let ratingData = res.data;


                for (let element of ratingData) {
                    if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
                        sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
                        sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealType = element.meal.mealType;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealDesc = element.meal.mealDesc;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealIng = element.meal.mealIng;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealServedEst = element.meal.mealServedEst;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealTotalPrice = element.meal.mealTotalPrice;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
                        sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
                    } else {
                        sumData[element.meal.mealName + '+' + element.meal.mealType] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealType: element.meal.mealType,
                            mealDesc: element.meal.mealDesc,
                            mealIng: element.meal.mealIng,
                            mealServedEst: element.meal.mealServedEst,
                            mealTotalPrice: element.meal.mealTotalPrice,
                            date: element.meal.mealDate,
                            day: element.meal.mealDate,
                            feed: element.feedback,
                            _id: element._id
                        };
                    }
                }

                let averageData = [];

                for (let element of Object.keys(sumData)) {
                    averageData.push({
                        mealName: element.split('+')[0],
                        users: sumData[element].n,
                        rating: sumData[element].sum / sumData[element].n,
                        mealType: sumData[element].mealType,
                        mealIng: sumData[element].mealIng,
                        mealDesc: sumData[element].mealDesc,
                        mealServedEst: sumData[element].mealServedEst,
                        mealTotalPrice: sumData[element].mealTotalPrice,
                        date: moment(sumData[element].date).format('ddd, MMM DD YYYY'),
                        day: moment(sumData[element].date).format('dddd'),
                        feed: sumData[element].feed,
                        _id: sumData[element]._id,
                    });
                }


                let unique = [];
                averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)).map(x => unique.filter(a => a.mealName == x.mealName && a.day === x.day).length > 0 ? null : unique.push(x));
                setremoveDuplicates(unique)
            })
            .catch(error => {
                console.log(error);
            });



    }, [])

    useEffect(() => {
        console.log('morningToTime', morningToIO.length, afternoontoIO.length)
        if (morningToIO.length === 0 && afternoontoIO.length === 0) {
            axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
                .then(res => {
                    setmorningToIO(res.data[0].morningTo);
                    setafternoontoIO(res.data[0].afternoonTo);
                    morningToTime = res.data[0].morningTo;
                    afternoonToTime = res.data[0].afternoonTo;
                    afternoonFromTime = res.data[0].afternoonFrom;
                })
                .catch(error => {
                    console.log(error);
                })
        }
    })

    useEffect(() => {
        let startDate = new Date();
        let endDate = new Date();

        let theDate = new Date(window.location.pathname.split('/')[3]);


        console.log('morningToTime.length === 0 && morningToTime.length === ', morningToIO.length, afternoontoIO.length)
        if (morningToIO.length === 0 || afternoontoIO.length === 0) {
            axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
                params: {
                    // startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                    // endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
                    startDate: moment(theDate).format("M/DD/YYYY"),
                    endDate: moment(theDate).add(1, "days").format("M/DD/YYYY")
                }
            })
                .then(res => {
                    // const ids = res.data.map(o => o.user?._id);
                    // let presentEmployees = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(1, "days").format("M/DD/YYYY"))) &&
                    //     moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).add(1, "days").format("M/DD/YYYY")))).filter(({ user }, index) => !ids.includes(user?._id, index + 1));
                    let newRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(1, "days").format("M/DD/YYYY"))) &&
                        moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).add(1, "days").format("M/DD/YYYY"))));
                    let theRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(21, "days").format("M/DD/YYYY"))) &&
                        moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).format("M/DD/YYYY"))));
                    const ids = theRes.map(o => o.user?._id);
                    let presentEmployees2 = theRes?.filter(({ user }, index) => !ids.includes(user?._id, index + 1));
                    let presentEmployees = newRes.filter(({ user }, index) => (!ids.includes(user?._id, index + 1)));
                    console.log('presentEmployees',
                        moment(theDate).subtract(1, "days").format("M/DD/YYYY"),
                        moment(theDate).add(1, "days").format("M/DD/YYYY"),
                        presentEmployees)
                    let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                    let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                    const idsMorning = presentInMorning.map(o => o.user?._id);
                    const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

                    settodaysAtt([presentInMorning.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; }).length,
                    presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length]);


                })
                .catch(error => {
                    console.log(error);
                })


            axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
                params: {
                    startDate: moment(theDate).subtract(1, "days").format("M/DD/YYYY"),
                    endDate: moment(theDate).subtract(0, "days").format("M/DD/YYYY")
                }
            })
                .then(res => {
                    // const ids = res.data.map(o => o.user?._id);
                    let newRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(2, "days").format("M/DD/YYYY"))) &&
                        moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).subtract(0, "days").format("M/DD/YYYY"))));
                    let theRes = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(2, "days").format("M/DD/YYYY"))) &&
                        moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).format("M/DD/YYYY"))));
                    const ids = theRes.map(o => o.user?._id);
                    let presentEmployees2 = theRes?.filter(({ user }, index) => !ids.includes(user?._id, index + 1));
                    let presentEmployees = newRes.filter(({ user }, index) => (!ids.includes(user?._id, index + 1)));

                    let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                    let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                    const idsMorning = presentInMorning.map(o => o.user?._id);
                    const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

                    console.log('yesterdayyy', ids,
                        newRes, presentEmployees, presentEmployees2,
                        presentInMorning.filter(el => (-1 == idsMorning.indexOf(el.user._id))),
                        presentInMorning.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; }),
                        morningToTime, afternoonFromTime)
                    setyesterdayAtt([presentInMorning.map(o => Object.assign({}, o, { timeIn: o.timeIn?.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) }))?.filter(function (el) { return el.timeIn?.length !== 0; }).length,
                    presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length]);
                })
                .catch(error => {
                    console.log(error);
                })

            // axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
            //     params: {
            //         // startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
            //         // endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
            //         startDate: moment(theDate).format("M/DD/YYYY"),
            //         endDate: moment(theDate).add(1, "days").format("M/DD/YYYY")
            //     }
            // })
            //     .then(res => {
            //         const ids = res.data.map(o => o.user?._id);
            //         let presentEmployees = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(1, "days").format("M/DD/YYYY"))) &&
            //             moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).add(1, "days").format("M/DD/YYYY")))).filter(({ user }, index) => !ids.includes(user?._id, index + 1));
            //         console.log('presentEmployees', presentEmployees)
            //         let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
            //         let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
            //         const idsMorning = presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFromTime, 'h:mma')) || v.includes("lateOUT")) } : null)).filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
            //         const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

            //         settodaysAtt([presentInMorning.filter(el => (-1 == idsMorning.indexOf(el.user._id))).length, 
            //             presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length]);


            //     })
            //     .catch(error => {
            //         console.log(error);
            //     })



            axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
                params: {
                    startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                    endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
                }
            })
                .then(res => {
                    const ids = res.data.map(o => o.user?._id)
                    settotalEmployees(res.data.filter(({ user }, index) => !ids.includes(user?._id, index + 1)).length);
                    setmealServedEst(mealType !== 'dinner' ? morningPresent : afternoonPresent)
                })
                .catch(error => {
                    console.log(error);
                })
        }

    }, [morningToIO, afternoontoIO])

    const onDefaultChange = (e, recommend) => {
        setmealName(e.mealName);
        setmealDesc(e.mealDesc);
        setmealIng(e.mealIng);
        setmealServedEst(e.mealServedEst);
        setmealTotalPrice(e.mealTotalPrice);
        setmealType(e.mealType);

        document.getElementById("create-meal").scrollIntoView({ behavior: 'smooth' });
    }

    const getLastWeek = () => {
        var today = new Date();

        const date = new Date();
        const tod = date.getDate();
        const dayOfTheWeek = date.getDay();
        const newDate = date.setDate(tod - dayOfTheWeek - 7);
        var lastWeek = new Date(new Date(newDate).getFullYear(), new Date(newDate).getMonth(), new Date(newDate).getDate());

        return lastWeek;
    }

    const onValueChange = (e) => {
        if (e.target.dataset.name === 'mealTotalPrice') {
            setmealTotalPrice(e.target.value);
            setnewBudget(e.target.value)

        } else if (e.target.dataset.name === 'mealName') {
            setmealName(e.target.value);
        } else if (e.target.dataset.name === 'mealDesc') {
            setmealDesc(e.target.value);
        } else if (e.target.dataset.name === 'mealDesc') {
            setmealDesc(e.target.value);
        } else if (e.target.dataset.name === 'mealIng') {
            setmealIng(e.target.value);
        } else if (e.target.dataset.name === 'mealTotalPrice') {
            setmealTotalPrice(e.target.value);
        } else if (e.target.dataset.name === 'mealServedEst') {
            setmealServedEst(e.target.value);
        } else if (e.target.dataset.name === 'mealTotalPerEmployee') {
            setmealTotalPerEmployee(e.target.value)
            setnewBudget(e.target.value)
        }

    }

    const onSubmit = (e) => {
        e.preventDefault();
        const mealOrigData = {};
        const mealData = {};

        mealData['mealName'] = mealName;
        mealData['mealDesc'] = mealDesc;
        mealData['mealIng'] = mealIng;
        mealData['mealType'] = mealType;
        mealData['mealDate'] = moment(mealDate).format('MM-DD-YYYY');
        mealData['mealServedEst'] = mealServedEst;
        mealData['mealCategory'] = mealCategory;
        mealData['mealTotalPrice'] = Number(mealTotalPerEmployee) * Number(mealServedEst);
        mealData['mealTotalPerEmployee'] = mealTotalPerEmployee;
        mealData['defaultMeal'] = defaultMeal;
        mealData['budget'] = window.location.pathname.split('/')[2];

        mealOrigData['mealOrigData'] = [mealData];
        console.log('countCategory', mealOrigData, mealData)

        const meals = {
            mealName: mealName,
            mealDesc: mealDesc,
            mealIng: mealIng,
            mealDate: moment(mealDate).format('MM-DD-YYYY'),
            mealServedEst: mealServedEst,
            mealTotalPrice: Number(mealTotalPerEmployee) * Number(mealServedEst),
            mealTotalPerEmployee: mealTotalPerEmployee,
            mealType: mealType,
            mealCategory: mealCategory,
            budget: window.location.pathname.split('/')[2],
            originalMealData: mealOrigData
        }


        console.log('mealsss', meals)
        axios.post(process.env.REACT_APP_BASE_URL + '/employee/addmeals', meals)
            .then(res =>
                Swal.fire({
                    title: 'Meal has been Created',
                    text: "Successfully Create a Meal",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/meals"
                    }
                })
            )
            .catch(err => console.log('Error :' + err));
    }

    const onSelectChange = (e) => {
        setmealType(e.target.value)
    }


    const onCategoryChange = (e) => {
        console.log('e.target.value', e.target.value)
        setmealCategory(e.target.value)
    }

    const onMealEstChange = (data) => {
        setmealServedEst(data);
    }

    const selectDateHandler = (value) => {
        setmealDate(value)
    }

    let theOverallBudget = (budgetToday - usedBudget) - (Number(mealTotalPerEmployee) * Number(mealServedEst));
    let tableBudget;
    if (mealWithinSpecificBudget.length) {
        tableBudget = mealWithinSpecificBudget.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next);
    } else {
        tableBudget = 0;
    }

    return (
        <LayoutForm>
            <div className="container" style={{ paddingBottom: '5px' }}>
                <h1>Budget</h1>
                <p style={{ float: 'center', textAlign: 'center', fontSize: '2.5rem', margin: '0px', color: '#203354', fontWeight: 'bold' }}>
                    Total Budget: {budgetToday}
                </p>
                <p style={theOverallBudget < 0 || budgetToday === 0 ? { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'green' }}>
                    Available Budget: {budgetToday - ((Number(mealTotalPerEmployee) * Number(mealServedEst)) + tableBudget)}
                </p>
                <p style={theOverallBudget < 0 || budgetToday === 0 ? { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '1.5rem', marginBottom: '0px', color: 'green' }}>
                    Used Budget: {(Number(mealTotalPerEmployee) * Number(mealServedEst)) + tableBudget}
                </p>

                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Meal Name</th>
                            <th>Meal Type</th>
                            <th>Budget Alloted</th>
                            <th>Meal Estimation</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mealWithinSpecificBudget.map((station, i) =>
                            <tr>
                                <td>{i + 1}</td>
                                <td>{station.mealName}</td>
                                <td>{station.mealType}</td>
                                <td>{station.mealTotalPrice}</td>
                                <td>{station.mealServedEst}</td>
                            </tr>
                        )
                        }
                        {
                            mealName?.length !== 0 ?
                                <tr style={{ color: 'green', fontWeight: 'bold' }}>
                                    <td>+</td>
                                    <td>{mealName}</td>
                                    <td>{mealType}</td>
                                    <td>{Number(mealTotalPerEmployee) * Number(mealServedEst)}</td>
                                    <td>{mealServedEst}</td>
                                </tr> : null

                        }
                    </tbody>
                </Table>
            </div>

            <LastWeekInCreate allTimeFav={removeDuplicates} modalMeals={modalMeals} handleOnClick={onDefaultChange} />

            <div className="container">
                <h1 id="create-meal">Create Meal</h1>
                <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                    {/* <Tab eventKey="lastweek" title="Lastweek Meal">
                        <LastWeek allTimeFav={allTimeFav} handleOnClick={defaultch} />
                    </Tab> */}
                    <Tab eventKey="default" title="Default Meal">
                        <div className="grid-container">
                            {Object.keys(defaultMeal).map(function (key) {
                                return (
                                    <div className="grid-item">  <button style={{ marginLeft: 20, marginBottom: 10 }}
                                        onClick={() => onDefaultChange(defaultMeal[key][0])}
                                        className="btn btn-primary defaultMeals">{defaultMeal[key][0].mealName}</button></div>
                                )
                            })}
                        </div>

                    </Tab>
                    <Tab eventKey="profile" title="Clear">
                        <Row xs={5} className="mealGroup">

                            <button style={{ marginLeft: 20, float: 'right' }}
                                onClick={() => [setmealName(''), setmealDesc(''), setmealIng(''), , setmealServedEst(''),
                                setmealTotalPrice(''), setmealTotalPerEmployee(''), setmealType('breakfast')]}
                                className="btn btn-secondary">Clear</button>
                        </Row>
                    </Tab>
                </Tabs>

                <form onSubmit={onSubmit}>
                    <div className="wholeForm">
                        <div className="form">

                            <div className="leftSide">
                                <div className="form-group">
                                    <label>Meal Name</label>
                                    <input type="text" className="form-control" data-name="mealName" value={mealName} required onChange={onValueChange} />
                                </div>
                                <div className="form-group">
                                    <label>Meal Description</label>
                                    <TextareaAutosize
                                        className="textArea form-control" data-name="mealDesc" value={mealDesc} required onChange={onValueChange}
                                        minRows={3}
                                        maxRows={6}
                                        placeholder="Description..."
                                    />
                                    {/* <textarea style={{height: 'auto', overflowY:'hidden'}} type="text" className="form-control" data-name="mealDesc"  required onChange={onValueChange} /> */}
                                </div>

                                <div className="form-group">
                                    <label>Meal Ingredients</label>
                                    <TextareaAutosize
                                        className="textArea form-control" data-name="mealIng" value={mealIng} required onChange={onValueChange}
                                        minRows={3}
                                        maxRows={6}
                                        placeholder="Ingredients..."
                                    />
                                    {/* <input type="text"  className="form-control" data-name="mealIng"  required onChange={onValueChange} /> */}
                                </div>
                                <div className="form-group">
                                    <label>Meal Category</label>
                                    <select value={mealCategory} className="mealType  form-control" data-name="mealType" required onChange={onCategoryChange}>
                                        <option style={{ textTransform: 'bold' }} value="vegetable">Vegetable</option>
                                        {/* <option value="Meat">Meat</option> */}
                                        <optgroup label="Meat">
                                            <option value="Meat: Beef">Beef</option>
                                            <option value="Meat: Chicken">Chicken</option>
                                            <option value="Meat: Pig">Pig</option>
                                            <option value="Meat: Pork">Pork</option>
                                        </optgroup>
                                        <optgroup label="Seafood">
                                            <option value="Seafood: Crab">Crab</option>
                                            <option value="Seafood: Fish">Fish</option>
                                            <option value="Seafood: Lobster">Lobster</option>
                                            <option value="Seafood: Mussels">Mussels</option>
                                            <option value="Seafood: Oyster">Oyster</option>
                                            <option value="Seafood: Shrimp">Shrimp</option>
                                            <option value="Seafood: Squid">Squid</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div className="rightSide">
                                <div className="form-group">
                                    <label>Meal Type</label>
                                    <select value={mealType} className="mealType  form-control" data-name="mealType" required onChange={onSelectChange}>
                                        <option value="breakfast">Breakfast</option>
                                        <option value="lunch">Lunch</option>
                                        <option value="dinner">Dinner</option>
                                        <option value="snack">Snack</option>
                                    </select>
                                </div>
                                <div className="form-group" >
                                    <CardTitle>
                                        <label>
                                            Serve Estimation
                                        </label>
                                        <ButtonTooltip2 yesterday={yesterdayAtt} today={todaysAtt} handleOnClick={onMealEstChange} title={'Estimate based on:'} />
                                    </CardTitle>
                                    <input type="number" min="1" className="form-control" data-name="mealServedEst" value={mealServedEst} required onChange={onValueChange} />
                                </div>

                                <div className="form-group" style={{ padding: '0%' }}>
                                    <label>Budget per Employee{theOverallBudget < 0 || budgetToday === 0 ? <label style={{ color: 'red', textTransform: 'none' }}>&nbsp; *Budget is not enough</label> : null}</label>
                                    <input style={theOverallBudget < 0 || budgetToday === 0 ? { color: 'red' } : {}} type="number" min="1" className="form-control" data-name="mealTotalPerEmployee" value={mealTotalPerEmployee} required onChange={onValueChange} />
                                </div>

                                <div className="form-group" style={{ padding: '0%' }}>
                                    <label>Overall Meal Price Total</label>
                                    <input readOnly style={theOverallBudget < 0 || budgetToday === 0 ? { color: 'red' } : {}} type="number" min="1" className="form-control" data-name="mealTotalPrice" value={Number(mealTotalPerEmployee) * Number(mealServedEst)} required onChange={onValueChange} />
                                </div>

                                <div className="form-group" style={{ padding: '0%' }}>
                                    <label>Date</label>
                                    <DatePicker
                                        id="mealDate"
                                        className="form-control"
                                        dateFormat="yyyy/MM/dd"
                                        selected={mealDate}
                                        onChange={(value) => selectDateHandler(value)}
                                        // minDate={today}
                                        todayButton={"Today"} />
                                </div>
                            </div>
                        </div>

                        <Buttons>
                            <button type="submit" style={theOverallBudget < 0 || budgetToday === 0 ? { marginRight: 10, pointerEvents: 'none', color: 'white', background: 'red', border: 'red' } : { marginRight: 10 }} className="btn btn-primary">{theOverallBudget < 0 || budgetToday === 0 ? 'Please Check Available Budget' : 'Save Meal'}</button>
                            <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
                        </Buttons>

                    </div>
                </form>
            </div>
        </LayoutForm>
    )
}




export default CreateMeal;