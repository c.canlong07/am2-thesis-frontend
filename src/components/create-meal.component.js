import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker'
import { height } from '@mui/material';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import TextareaAutosize from 'react-textarea-autosize';
import { Row, Tabs, Tab, Table, Nav, Col, ListGroup, Card, Button } from "react-bootstrap";
import moment from 'moment';
import Rating from '@mui/material/Rating';
import LastWeek from '../dataVisualization/LastWeek';
import TotalPresentIcon from '../img/icon_totalPresent.png';
import TotalAbsentIcon from '../img/icon_totalAbsent.png';
import { AttendanceStyled } from '../styles/TableLayout';
import LastWeekInCreate from '../dataVisualization/LastWeekInCreate';
import { CardTitle } from '../styles/RecommendationStyle';
import { ButtonTooltip2 } from '../globalFunctions/shareableComponents';
import Swal from 'sweetalert2';
export default class CreateEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            mealName: '',
            mealDesc: '',
            mealIng: '',
            mealDate: new Date(),
            mealServedEst: '',
            mealTotalPrice: '',
            mealType: 'breakfast',
            defaultMeal: [],
            totalEmployees: 0,
            budgetToday: 0,
            usedBudget: 0,
            newBudget: 0,
            theBudgetDate: new Date(),
            mealWithinSpecificBudget: [],
            allTimeFav: [],
            morningPresent: '',
            morningFromIO: '',
            morningToIO: '',
            afternoonPresent: '',
            afternoonFromIO: '',
            afternoonToIO: '',
            removeDuplicates: [],
            modalMeals: [],
            yesterdayAtt: [],
            todaysAtt: [],



            //   mapMeal: []
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.selectDateHandler = this.selectDateHandler.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onDefaultChange = this.onDefaultChange.bind(this);
        this.onMealEstChange = this.onMealEstChange.bind(this);
        this.getLastWeek = this.getLastWeek.bind(this);
        this.compareRating = this.compareRating.bind(this);


    }
    compareRating(a, b) {
        let first = a.rating ? a.rating : 0;
        let second = b.rating ? b.rating : 0;

        return second - first;
    }

    componentDidUpdate(prevProps, prevState) {
        // Typical usage (don't forget to compare props):

        if (prevState.mealType !== this.state.mealType) {
            this.setState({ mealServedEst: this.state.mealType !== 'dinner' ? this.state.morningPresent : this.state.afternoonPresent })
        }
    }

    componentDidMount() {
        let theDate = new Date(window.location.pathname.split('/')[3]);
        this.setState({ mealDate: theDate });

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/getMealbydefault')
            .then(res => {
                const mappingHistory = res.data.map(year => [{
                    mealName: year.mealName,
                    // _id: year._id,
                    // rating: year?.rating,
                    // date: Moment(year.createdAt).format('MMMM DD YYYY HH:mm:ss'),
                    mealDesc: year.mealDesc,
                    mealIng: year.mealIng,
                    mealType: year.mealType,
                    mealServedEst: year.mealServedEst,
                    mealTotalPrice: year.mealTotalPrice
                }]);
                this.setState({ defaultMeal: mappingHistory })

            })
            .catch(error => {
                console.log(error);
            })
        let startDate = new Date();
        let endDate = new Date();

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
            params: {
                startDate: moment(theDate).subtract(1, "days").format("M/DD/YYYY"),
                endDate: moment(theDate).subtract(0, "days").format("M/DD/YYYY")
            }
        })
            .then(res => {
                const ids = res.data.map(o => o.user?._id);
                let presentEmployees = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(2, "days").format("M/DD/YYYY"))) &&
                    moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).subtract(0, "days").format("M/DD/YYYY")))).filter(({ user }, index) => !ids.includes(user?._id, index + 1));

                let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                const idsMorning = presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFromTime, 'h:mma')) || v.includes("lateOUT")) } : null)).filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
                const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
                
                console.log('yesterdayyy' , presentInMorning.filter(el => (-1 == idsMorning.indexOf(el.user._id))).length, presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length,
                presentInMorning)
                this.setState({
                    yesterdayAtt: [presentInMorning.filter(el => (-1 == idsMorning.indexOf(el.user._id))).length, presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length],
                })
            })
            .catch(error => {
                console.log(error);
            })


        axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
            params: {
                // startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                // endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
                startDate: moment(theDate).format("M/DD/YYYY"),
                endDate: moment(theDate).add(1, "days").format("M/DD/YYYY")
            }
        })
            .then(res => {
                const ids = res.data.map(o => o.user?._id);
                let presentEmployees = res.data.filter(el => moment(new Date(el.dateRecorded)).isAfter(new Date(moment(theDate).subtract(1, "days").format("M/DD/YYYY"))) &&
                    moment(new Date(el.dateRecorded)).isBefore(new Date(moment(theDate).add(1, "days").format("M/DD/YYYY")))).filter(({ user }, index) => !ids.includes(user?._id, index + 1));
                console.log('presentEmployees',presentEmployees)
                let presentInMorning = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                let presentInAfternoon = presentEmployees.map(o => Object.assign({}, o, { timeIn: o.timeIn.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(morningToTime, 'h:mma'))) })).filter(function (el) { return el.timeIn.length !== 0; });
                const idsMorning = presentInMorning.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut.filter(v => moment(v.split('/')[0], 'h:mma').isBefore(moment(afternoonFromTime, 'h:mma')) || v.includes("lateOUT")) } : null)).filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);
                const idsAfternoon = presentInAfternoon.map(o => Object.assign({}, o, o.timeOut !== undefined ? { timeOut: o.timeOut?.filter(v => moment(v.split('/')[0], 'h:mma').isAfter(moment(afternoonFromTime, 'h:mma')) && v.includes("lateOUT") === false) } : null))?.filter(function (el) { return el.timeOut !== undefined && el.timeOut?.length !== 0; }).map(o => o.user?._id);

                this.setState({
                    todaysAtt: [presentInMorning.filter(el => (-1 == idsMorning.indexOf(el.user._id))).length, presentInAfternoon.filter(el => (-1 == idsAfternoon.indexOf(el.user._id))).length],
                })


            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealRecords', {
            params: {
                startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
            }
        })
            .then(res => {
                this.setState({ modalMeals: res.data })


            })
            .catch(error => {
                console.log(error);
            })


        axios.get(process.env.REACT_APP_BASE_URL + '/employee/att', {
            params: {
                startDate: (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear(),
                endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
            }
        })
            .then(res => {
                const ids = res.data.map(o => o.user?._id)
                this.setState({ totalEmployees: res.data.filter(({ user }, index) => !ids.includes(user?._id, index + 1)).length, mealServedEst: this.state.mealType !== 'dinner' ? this.state.morningPresent : this.state.afternoonPresent })
            })
            .catch(error => {
                console.log(error);
            })

        let morningToTime = '';
        let afternoonToTime = '';
        let afternoonFromTime = '';
        //Get Time Schedule set
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/time/timeSet')
            .then(res => {

                morningToTime = res.data[0].morningTo;
                afternoonToTime = res.data[0].afternoonTo;
                afternoonFromTime = res.data[0].afternoonFrom;
            })
            .catch(error => {
                console.log(error);
            })



        axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/budSpecificToday', { params: { id: window.location.pathname.split('/')[2] } })
            .then(res => {
                let date = new Date(res.data[0].budgetDate);
                this.setState({ budgetToday: res.data[0].budget, theBudgetDate: date });
            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealToday', {
            params: {
                startDate: (theDate.getMonth() + 1) + '/' + theDate.getDate() + '/' + theDate.getFullYear(),
                endDate: (theDate.getMonth() + 1) + '/' + (theDate.getDate() + 1) + '/' + theDate.getFullYear()
            }
        })
            .then(res => {

                this.setState({ usedBudget: res.data.map(item => item.mealTotalPrice).reduce((prev, next) => prev + next), mealWithinSpecificBudget: res.data })
                const mapping = res.data.map(year => [{
                    mealName: year.mealName,
                    id: year._id,
                    mealDesc: year.mealDesc,
                    mealIng: year.mealIng,
                    mealType: year.mealType,
                }]);

            })
            .catch(error => {
                console.log(error);
            })

        let theWeekly = this.getLastWeek();

        //weekly
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/mealWeekly', {
            params: {
                startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
                endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
            }
        })
            .then(res => {
                let theWeeklyData = res.data.map(val => ({ ...val, mealDate: moment(val.mealDate).format('MM/DD/yyyy') }));
                let weeklyData = {};
                for (let element of theWeeklyData) {
                    if (weeklyData[element.mealDate]) {
                        weeklyData[element.mealDate].sum = weeklyData[element.mealDate].sum + (element.rating !== undefined ? element.rating : 0);
                        weeklyData[element.mealDate].n++;
                        weeklyData[element.mealDate].mealDate = element.mealDate;
                        weeklyData[element.mealDate].mealName = element.mealName;
                        weeklyData[element.mealDate].mealDesc = element.mealDesc;

                    } else {
                        weeklyData[element.mealDate] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealDate: element.mealDate,
                            mealName: element.mealName,
                            mealDesc: element.mealDesc,
                        };
                    }
                }

                let weeklyOverall = [];

                for (let element of Object.keys(weeklyData)) {
                    weeklyOverall.push({
                        name: element,
                        users: weeklyData[element].n,
                        rating: weeklyData[element].sum / weeklyData[element].n,
                        mealDate: weeklyData[element].mealDate,
                        mealName: weeklyData[element].mealName,
                        mealDesc: weeklyData[element].mealDesc,

                    });
                }
            })
            .catch(error => {
                console.log(error);
            })

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
            params: {
                startDate: (theWeekly.getMonth() + 1) + '/' + theWeekly.getDate() + '/' + theWeekly.getFullYear(),
                endDate: (theWeekly.getMonth() + 1) + '/' + (theWeekly.getDate() + 7) + '/' + theWeekly.getFullYear()
            }
        })
            .then(res => {

                let sumData = {};
                let ratingData = res.data;
                for (let element of ratingData) {
                    if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
                        sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
                        sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealType = element.meal.mealType;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealDesc = element.meal.mealDesc;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealIng = element.meal.mealIng;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealServedEst = element.meal.mealServedEst;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealTotalPrice = element.meal.mealTotalPrice;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
                        sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
                    } else {
                        sumData[element.meal.mealName + '+' + element.meal.mealType] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealType: element.meal.mealType,
                            mealDesc: element.meal.mealDesc,
                            mealIng: element.meal.mealIng,
                            mealServedEst: element.meal.mealServedEst,
                            mealTotalPrice: element.meal.mealTotalPrice,
                            date: element.meal.mealDate,
                            day: element.meal.mealDate,
                            feed: element.feedback,
                            _id: element._id
                        };
                    }
                }

                let averageData = [];

                for (let element of Object.keys(sumData)) {
                    averageData.push({
                        mealName: element.split('+')[0],
                        users: sumData[element].n,
                        rating: sumData[element].sum / sumData[element].n,
                        mealType: sumData[element].mealType,
                        mealIng: sumData[element].mealIng,
                        mealDesc: sumData[element].mealDesc,
                        mealServedEst: sumData[element].mealServedEst,
                        mealTotalPrice: sumData[element].mealTotalPrice,
                        date: moment(sumData[element].date).format('ddd, MMM DD YYYY'),
                        day: moment(sumData[element].date).format('dddd'),
                        feed: sumData[element].feed,
                        _id: sumData[element]._id,
                    });
                }

                let unique = [];
                averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)).map(x => unique.filter(a => a.mealName == x.mealName && a.day === x.day).length > 0 ? null : unique.push(x));

                this.setState({ allTimeFav: averageData })
            })
            .catch(error => {
                console.log(error);
            });

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/employeeallmealhistory', {
            params: {
                startDate: '1/1/2000',
                endDate: '1/1/2023'
            }
        })
            .then(res => {

                let sumData = {};
                let ratingData = res.data;


                for (let element of ratingData) {
                    if (sumData[element.meal.mealName + '+' + element.meal.mealType]) {
                        sumData[element.meal.mealName + '+' + element.meal.mealType].sum = sumData[element.meal.mealName + '+' + element.meal.mealType].sum + (element.rating !== undefined ? element.rating : 0);
                        sumData[element.meal.mealName + '+' + element.meal.mealType].n++;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealType = element.meal.mealType;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealDesc = element.meal.mealDesc;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealIng = element.meal.mealIng;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealServedEst = element.meal.mealServedEst;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].mealTotalPrice = element.meal.mealTotalPrice;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].date = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].day = element.meal.mealDate;
                        sumData[element.meal.mealName + '+' + element.meal.mealType].feed = element.feedback;
                        sumData[element.meal.mealName + '+' + element.meal.mealType]._id = element._id;
                    } else {
                        sumData[element.meal.mealName + '+' + element.meal.mealType] = {
                            sum: element.rating ? element.rating : 0,
                            n: 1,
                            mealType: element.meal.mealType,
                            mealDesc: element.meal.mealDesc,
                            mealIng: element.meal.mealIng,
                            mealServedEst: element.meal.mealServedEst,
                            mealTotalPrice: element.meal.mealTotalPrice,
                            date: element.meal.mealDate,
                            day: element.meal.mealDate,
                            feed: element.feedback,
                            _id: element._id
                        };
                    }
                }

                let averageData = [];

                for (let element of Object.keys(sumData)) {
                    averageData.push({
                        mealName: element.split('+')[0],
                        users: sumData[element].n,
                        rating: sumData[element].sum / sumData[element].n,
                        mealType: sumData[element].mealType,
                        mealIng: sumData[element].mealIng,
                        mealDesc: sumData[element].mealDesc,
                        mealServedEst: sumData[element].mealServedEst,
                        mealTotalPrice: sumData[element].mealTotalPrice,
                        date: moment(sumData[element].date).format('ddd, MMM DD YYYY'),
                        day: moment(sumData[element].date).format('dddd'),
                        feed: sumData[element].feed,
                        _id: sumData[element]._id,
                    });
                }


                let unique = [];
                averageData.sort((a, b) => (b.rating - a.rating || b.users - a.users)).map(x => unique.filter(a => a.mealName == x.mealName && a.day === x.day).length > 0 ? null : unique.push(x));
                this.setState({ removeDuplicates: unique })
            })
            .catch(error => {
                console.log(error);
            });


    }

    getLastWeek() {
        var today = new Date();

        const date = new Date();
        const tod = date.getDate();
        const dayOfTheWeek = date.getDay();
        const newDate = date.setDate(tod - dayOfTheWeek - 7);
        var lastWeek = new Date(new Date(newDate).getFullYear(), new Date(newDate).getMonth(), new Date(newDate).getDate());

        return lastWeek;
    }

    onValueChange(e) {
        if (e.target.dataset.name === 'mealTotalPrice') {
            this.setState({
                [e.target.dataset.name]: e.target.value,
                newBudget: e.target.value
            })
        } else {
            this.setState({
                [e.target.dataset.name]: e.target.value
            })
        }

    }

    onSelectChange(e) {
        this.setState({
            ['mealType']: e.target.value
        })
    }

    onDefaultChange(e, recommend) {
        this.setState({
            mealName: e.mealName,
            mealDesc: e.mealDesc,
            mealIng: e.mealIng,
            mealServedEst: e.mealServedEst,
            mealTotalPrice: e.mealTotalPrice,
            mealType: e.mealType,
        })

        document.getElementById("create-meal").scrollIntoView({ behavior: 'smooth' });
    }


    onMealEstChange(data) {
        this.setState({
            mealServedEst: data,
        })
    }

    selectDateHandler(value) {
        this.setState({
            ['mealDate']: value
        })
    }

    onSubmit(e) {
        e.preventDefault();

        const meals = {
            mealName: this.state.mealName,
            mealDesc: this.state.mealDesc,
            mealIng: this.state.mealIng,
            mealDate: this.state.mealDate,
            mealServedEst: this.state.mealServedEst,
            mealTotalPrice: this.state.mealTotalPrice,
            mealType: this.state.mealType,
        }



        axios.post(process.env.REACT_APP_BASE_URL + '/employee/addmeals', meals)
            .then(res =>
                Swal.fire({
                    title: 'Meal has been Created',
                    text: "Successfully Create a Meal",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/meals"
                    }
                })
            )
            .catch(err => console.log('Error :' + err));
    }

    render() {
        let data = this.state.defaultMeal;
        let defaultch = this.onDefaultChange;
        let theRecommended = this.state.removeDuplicates;
        const mealTo = {
            mealName: this.state.mealName,
            mealDesc: this.state.mealDesc,
            mealIng: this.state.mealIng,
            mealDate: this.state.mealDate,
            mealServedEst: this.state.mealServedEst,
            mealTotalPrice: this.state.mealTotalPrice,
            mealType: this.state.mealType,
        }

        let theOverallBudget = (this.state.budgetToday - this.state.usedBudget) - Number(this.state.mealTotalPrice);

        return (
            <LayoutForm>
                <div className="container" style={{ paddingBottom: '5px' }}>
                    <h1>Budget</h1>

                    <p style={{ float: 'center', textAlign: 'center', fontSize: '2.5rem', marginBottom: '0px' }}>
                        Budget: {this.state.budgetToday}
                    </p>
                    <p style={theOverallBudget < 0 || this.state.budgetToday === 0 ? { float: 'center', textAlign: 'center', fontSize: '2.7rem', marginBottom: '1rem', color: 'red' } : { float: 'center', textAlign: 'center', fontSize: '2.7rem', marginBottom: '0px', color: 'green' }}>
                        Available Budget: {theOverallBudget}
                    </p>
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Meal Name</th>
                                <th>Meal Type</th>
                                <th>Budget Alloted</th>
                                <th>Meal Estimation</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.mealWithinSpecificBudget.map((station, i) =>
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{station.mealName}</td>
                                    <td>{station.mealType}</td>
                                    <td>{station.mealTotalPrice}</td>
                                    <td>{station.mealServedEst}</td>
                                </tr>
                            )
                            }
                            {
                                mealTo.mealName?.length !== 0 ?
                                    <tr style={{ color: 'green', fontWeight: 'bold' }}>
                                        <td>+</td>
                                        <td>{mealTo.mealName}</td>
                                        <td>{mealTo.mealType}</td>
                                        <td>{mealTo.mealTotalPrice}</td>
                                        <td>{mealTo.mealServedEst}</td>
                                    </tr> : null

                            }
                        </tbody>
                    </Table>
                </div>

                <LastWeekInCreate allTimeFav={theRecommended} modalMeals={this.state.modalMeals} handleOnClick={defaultch} />

                <div className="container">
                    <h1 id="create-meal">Create Meal</h1>
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
                        {/* <Tab eventKey="lastweek" title="Lastweek Meal">
                            <LastWeek allTimeFav={this.state.allTimeFav} handleOnClick={defaultch} />
                        </Tab> */}
                        <Tab eventKey="default" title="Default Meal">
                            <div className="grid-container">
                                {Object.keys(data).map(function (key) {
                                    return (
                                        <div className="grid-item">  <button style={{ marginLeft: 20, marginBottom: 10 }}
                                            onClick={() => defaultch(data[key][0])}
                                            className="btn btn-primary defaultMeals">{data[key][0].mealName}</button></div>
                                    )
                                })}
                            </div>

                        </Tab>
                        <Tab eventKey="profile" title="Clear">
                            <Row xs={5} className="mealGroup">

                                <button style={{ marginLeft: 20, float: 'right' }}
                                    onClick={() => this.setState({
                                        mealName: '',
                                        mealDesc: '',
                                        mealIng: '',
                                        mealServedEst: '',
                                        mealTotalPrice: '',
                                        mealType: 'breakfast',
                                    })}
                                    className="btn btn-secondary">Clear</button>
                            </Row>
                        </Tab>
                    </Tabs>

                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">

                                <div className="leftSide">
                                    <div className="form-group">
                                        <label>Meal Name</label>
                                        <input type="text" className="form-control" data-name="mealName" value={mealTo.mealName} required onChange={this.onValueChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Meal Description</label>
                                        <TextareaAutosize
                                            className="textArea form-control" data-name="mealDesc" value={mealTo.mealDesc} required onChange={this.onValueChange}
                                            minRows={3}
                                            maxRows={6}
                                            placeholder="Description..."
                                        />
                                        {/* <textarea style={{height: 'auto', overflowY:'hidden'}} type="text" className="form-control" data-name="mealDesc"  required onChange={this.onValueChange} /> */}
                                    </div>

                                    <div className="form-group">
                                        <label>Meal Ingredients</label>
                                        <TextareaAutosize
                                            className="textArea form-control" data-name="mealIng" value={mealTo.mealIng} required onChange={this.onValueChange}
                                            minRows={3}
                                            maxRows={6}
                                            placeholder="Ingredients..."
                                        />
                                        {/* <input type="text"  className="form-control" data-name="mealIng"  required onChange={this.onValueChange} /> */}
                                    </div>
                                </div>

                                <div className="rightSide">
                                    <div className="form-group">
                                        <label>Meal Type</label>
                                        <select value={this.state.mealType} className="mealType  form-control" data-name="mealType" required onChange={this.onSelectChange}>
                                            <option value="breakfast">Breakfast</option>
                                            <option value="lunch">Lunch</option>
                                            <option value="dinner">Dinner</option>
                                            <option value="snack">Snack</option>
                                        </select>
                                    </div>
                                    <div className="form-group" >
                                        <CardTitle>
                                            <label>
                                                Serve Estimation
                                            </label>
                                            <ButtonTooltip2 yesterday={this.state.yesterdayAtt} today={this.state.todaysAtt} handleOnClick={this.onMealEstChange} title={'Estimate based on:'} />
                                        </CardTitle>
                                        <input type="number" min="0" className="form-control" data-name="mealServedEst" value={mealTo.mealServedEst} required onChange={this.onValueChange} />
                                    </div>

                                    <div className="form-group" style={{ padding: '0%' }}>
                                        <label>Overall Meal Price Estimation{theOverallBudget < 0 || this.state.budgetToday === 0 ? <label style={{ color: 'red', textTransform: 'none' }}>&nbsp; *Budget is not enough</label> : null}</label>
                                        <input style={theOverallBudget < 0 || this.state.budgetToday === 0 ? { color: 'red' } : {}} type="number"  min="0" className="form-control" data-name="mealTotalPrice" value={mealTo.mealTotalPrice} required onChange={this.onValueChange} />
                                    </div>

                                    <div className="form-group" style={{ padding: '0%' }}>
                                        <label>Date</label>
                                        <DatePicker
                                            id="mealDate"
                                            className="form-control"
                                            dateFormat="yyyy/MM/dd"
                                            selected={this.state.mealDate}
                                            onChange={(value) => this.selectDateHandler(value)}
                                            // minDate={today}
                                            todayButton={"Today"} />
                                    </div>
                                </div>
                            </div>

                            <Buttons>
                                <button type="submit" style={theOverallBudget < 0 || this.state.budgetToday === 0 ? { marginRight: 10, pointerEvents: 'none', color: 'white', background: 'red', border: 'red' } : { marginRight: 10 }} className="btn btn-primary">{theOverallBudget < 0 || this.state.budgetToday === 0 ? 'Please Check Available Budget' : 'Save Meal'}</button>
                                <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
                            </Buttons>

                        </div>
                    </form>
                </div>
            </LayoutForm>
        )
    }


}
