import React, {
  useEffect,
  useState
} from 'react';
import ReactDOM from "react-dom";

import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import timeGridPlugin from "@fullcalendar/timegrid";
import resourceTimeGridPlugin from "@fullcalendar/resource-timegrid";
import {
  Card, Button, Modal, Col, Row, Spinner, ListGroup, Badge
} from "react-bootstrap";
import moment from 'moment';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { BiEdit } from "react-icons/bi";
import { RiDeleteBin5Line } from "react-icons/ri";
import { FiSave } from "react-icons/fi"
import Swal from 'sweetalert2'
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import LoadingScreen from "react-loading-screen";
import loading from '../img/loading.gif';

function Calendar2() {
  const calendarComponentRef = React.createRef();
  const [show, setShow] = useState(false);
  const [dateSelected, setdateSelected] = useState('');
  const [budgetID, setbudgetID] = useState('');
  const [ISODate, setISODate] = useState('');
  const [mealInBudget, setmealInBudget] = useState('');
  const [data, setdata] = useState({});
  const [allMeals, setallMeals] = useState({});
  const [allBudget, setallBudget] = useState({});
  const [budgetInDate, setbudgetInDate] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const handleClose = () => {
    setShow(false);
    setdateSelected('');
    setbudgetID('');
    setmealInBudget('');
  };
  const handleShow = () => setShow(true);


  useEffect(() => {
    let theBudget;

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allbudgets')
      .then(res => {
        var result = res.data.map(function (el) {
          var o = Object.assign({}, el);
          o.start = moment(el.budgetDate).format('YYYY-MM-DD')
          o.title = 'Budget ✓';
          return o;
        })
        // console.log('allbudgets', res.data, result,)
        setdata(result);
        theBudget = result;
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      })
    // if (Object.keys(data).length) {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        var result = res.data.map(function (el) {
          var o = Object.assign({}, el);
          o.start = moment(el.mealDate).format('YYYY-MM-DD')
          o.title = 'Meal ✓';
          return o;
        })
        // console.log('dattaaa', theBudget, data, [...data, ...result])
        // setdata(data => [...data, ...result]);
        setallMeals(result);
        setIsLoading(false)

      })
      .catch(error => {
        console.log(error);
      })

    // }



  }, [])

  // useEffect(() => {
  //   if (Object.keys(data).length) {
  //     setdata([...data, ...allMeals]);
  //   }
  // }, [allMeals])

  const handleDateClick = (arg) => {
    let filterBudgetSelected;
    handleShow();
    setdateSelected(arg.date)
    console.log('data', data, data.length)

    filterBudgetSelected = data?.filter(function (el) {
      console.log('ressss', el.start, el, moment(arg.date).format('YYYY-MM-DD'), arg.date)
      return el.start === moment(arg.date).format('YYYY-MM-DD');
    })
    if (filterBudgetSelected.length) {
      setbudgetInDate(filterBudgetSelected);
      setbudgetID(filterBudgetSelected?.[0]._id);
      setISODate(filterBudgetSelected?.[0].budgetDate);

      axios.get(process.env.REACT_APP_BASE_URL + '/employee/getmealbybudget', {
        params: {
          id: filterBudgetSelected?.[0]._id
        }
      })
        .then(res => {
          console.log('meals by budget id', res.data);
          setmealInBudget(res.data);

        })
        .catch(error => {
          console.log(error);
        })
    } else {
      setISODate(arg.date.toISOString())
    }

    console.log('arg.dateStr', arg.dateStr, arg, data, filterBudgetSelected, arg.date.toISOString());



  };

  const handleSelectedDates = (info) => {
    // alert("selected " + info.startStr + " to " + info.endStr);
    console.log('infoooo', info)
    // const title = prompt("What's the name of the title");
    // console.log(info);
    // if (title != null) {
    //   const newEvent = {
    //     title,
    //     start: info.startStr,
    //     end: info.endStr
    //   };
    //   const data = [...this.state.events, newEvent];
    //   this.setState({ events: data });
    //   console.log("here", data);
    // } else {
    //   console.log("nothing");
    // }
  };

  const deleteEmployee = (id) => {
    axios.delete(process.env.REACT_APP_BASE_URL + '/employee/deleteMeal/' + id)
      .then(res => window.location.href = "/meals")
  }


  const deleteBudget = (id) => {
    axios.delete(process.env.REACT_APP_BASE_URL + '/employee/deleteBudget/' + budgetID)
      .then(res => window.location.href = "/meals")
  }

  return (

    <div>

      {
        isLoading ?
          <div style={{ textAlign: 'center' }}>
            <Spinner animation="grow" variant="secondary" />
          </div>
          :
          <>
            <Modal show={show} onHide={handleClose} centered scrollable>
              <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                <Modal.Title style={{ color: 'white' }}>{moment(dateSelected).format('dddd, MMMM D, YYYY')}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Divider style={{ marginBottom: '5%' }}>
                  <Chip label="Manage Budget" />
                </Divider>
                <div >
                  {
                    budgetID ?
                      <div style={{ textAlign: 'center' }}>
                        <p style={{ fontWeight: 'bold', }}></p>
                        <Card className="text-center" >
                          {/* <Card.Header>{budgetInDate?.[0].budgetNote}</Card.Header> */}
                          <Card.Body style={{ paddingTop: '3%', fontWeight: 'bold' }}>
                            <Card.Title style={{ fontWeight: 'bold', margin: '0px', fontSize: '2rem' }}>{budgetInDate?.[0].budget}</Card.Title>
                          </Card.Body>
                          <Card.Footer className="text-muted">
                            <Link to={'/editBudget/' + budgetID} style={{ background: '#00C2DE', marginLeft: '4%', paddingTop: '1%', color: 'white', width: '50%' }} className="btn "><BiEdit style={{ fontSize: 20, color: 'white', marginRight: '4%' }} />Edit Budget</Link> <br />
                            <Link to='#'
                              onClick={() => {
                                Swal.fire({
                                  title: 'Are you sure?',
                                  text: "You won't be able to revert this!",
                                  icon: 'warning',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'Yes, delete it!'
                                }).then((result) => {
                                  if (result.isConfirmed) {
                                    deleteBudget('row.original._id')
                                  }
                                })
                              }} style={{ background: 'red', marginLeft: '4%', paddingTop: '1%', color: 'white', width: '50%', marginTop: '3%' }} className="btn "><RiDeleteBin5Line style={{ fontSize: 20, color: 'white', marginRight: '4%' }} />Delete Budget</Link>

                          </Card.Footer>
                        </Card>
                        <div div style={{ textAlign: 'center' }}>
                          <Divider style={{ marginBottom: '5%', marginTop: '5%' }}>
                            <Chip label="Manage Meals" />
                          </Divider>

                          <Card>
                            <Card.Body>
                              {

                                mealInBudget.length ?
                                  <ListGroup style={{ marginTop: '3%', listStyleType: 'unset', textTransform: 'capitalize' }} as="ol" numbered>
                                    {mealInBudget.map(link =>
                                      // <ListGroup.Item key={link.mealName}>{link.mealName}</ListGroup.Item>
                                      <>
                                        <ListGroup.Item
                                          onClick={() => window.location.href = `editMeal/${link._id}/${link.mealDate}/${link.budget}`}
                                          as="li"
                                          className="d-flex justify-content-between align-items-start"
                                        >
                                          <ListGroup.Item as="li" style={{ width: '60%' }}>{link.mealName}</ListGroup.Item>
                                          <div className="ms-2 me-auto">
                                            <div className="fw-bold" style={{ fontWeight: 'bold' }}>{link.mealType}</div>
                                            {link.mealCategory}
                                          </div>
                                        </ListGroup.Item>
                                      </>
                                    )}
                                  </ListGroup>
                                  :
                                  <p style={{ fontWeight: 'bold', margin: '0px' }}>Click the button below to Add Meal</p>
                              }
                            </Card.Body>
                            <Card.Footer>
                              <Link to={'/createMeal/' + budgetID + '/' + ISODate} style={{ background: '#00C2DE', marginLeft: '4%', paddingTop: '1%', color: 'white', width: '50%' }} className="btn "><i className="fa fa-plus" style={{ fontSize: 20, color: 'white' }}></i>
                                Add Meals
                                <Badge bg="secondary" pill style={{ right: '-5%' }}>
                                  {mealInBudget.length}
                                </Badge>
                              </Link>

                            </Card.Footer>
                          </Card>
                        </div>

                      </div>

                      :

                      <>
                        <div style={{ textAlign: 'center' }}>
                          <p style={{ color: 'red', fontWeight: 'bold' }}>Please Add Budget First!</p>
                          <Link to={'/budgetMeal/' + ISODate} style={{ background: '#00C2DE', marginLeft: '4%', paddingTop: '1%', color: 'white', width: '50%' }} className="btn "><i className="fa fa-plus" style={{ fontSize: 20, color: 'white', marginRight: '4%' }}></i>Create Budget</Link>
                        </div>
                      </>
                  }
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
              </Modal.Footer>
            </Modal>
            <div style={{}}>
              <FullCalendar
                schedulerLicenseKey="GPL-My-Project-Is-Open-Source"
                ref={calendarComponentRef}
                initialView="dayGridMonth"
                dateClick={handleDateClick}
                displayEventTime={true}
                headerToolbar={{
                  left: "today",
                  center: "title",
                  right: "prev,next"
                }}
                selectable={true}
                plugins={[
                  dayGridPlugin,
                  interactionPlugin,
                  timeGridPlugin,
                  resourceTimeGridPlugin
                ]}
                eventClick={event => {
                  // handleDateClick()
                  console.log(event);
                }}
                events={data}
                select={handleSelectedDates}
                // eventLimit={2}
                showNonCurrentDates={false}
                weekends={false}
                fixedWeekCount={false}
              />
            </div>
          </>
      }
    </div>


  );
}

export default Calendar2;