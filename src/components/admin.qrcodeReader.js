import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import QrReader from "react-qr-reader";
import {
  Button,
  Modal
} from "react-bootstrap";
export default class CreateEmp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      result: "No result",
      time: new Date(),
      capturedDate: new Date(),
      firstname: '',
      lastname: '',
      address: '',
      age: '',
      email: '',
      cpnumber: '',
      id: '',
      toShow: false,
    };

    this.handleScan = this.handleScan.bind(this);
    this.handleError = this.handleError.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.addTimeIn = this.addTimeIn.bind(this);

  }



  handleScan = data => {
    if (data) {
      this.setState({
        result: data
      });

      axios.get(process.env.REACT_APP_BASE_URL + '/employee/' + data)
        .then(res => {
          this.setState({
            firstname: res.data.firstname,
            lastname: res.data.lastname,
            address: res.data.address,
            age: res.data.age,
            email: res.data.email,
            cpnumber: res.data.cpnumber,
            id: res.data._id,
            capturedDate: this.state.time.toString()
          })
        })
        .catch(error => {
          console.log(error);
        })
    }
  };
  handleError = err => {
    console.error(err);
  };

  handleClose() {
    this.setState({ result: 'No result' });
  }

  addTimeIn = (result, time, e) => {
    const attendance = {};

    let theTimeIn = '';
    let theTimeOut = '';
    attendance['user'] = this.state.id;
    attendance['dateRecorded'] = new Date().toLocaleDateString();
    if (e.target.id === 'timeIn') {
      theTimeIn = time;
      attendance['timeIn'] = time;
    } else {
      theTimeOut = time
      attendance['timeOut'] = time;
    }
  }

  render() {
    setInterval(() => this.setState({ time: new Date() }), 1000);
    return (
      <div className="App" style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
        <h2> {this.state.time.toString()}</h2>;
        <QrReader
          delay={300}
          onError={this.handleError}
          onScan={this.handleScan}
          style={{ width: "40%" }}
        />
        <p>{this.state.result}</p>
        <p>{this.state.firstname}</p>
        {
          this.state.result !== 'No result' ?
            <Modal show={this.state.result !== 'No result'} onHide={this.handleClose} animation={false}>
              <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                <Modal.Title>Attendance Monitoring</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {/* <canvas id="canvas" width={600} height={500} style={{maxWidth: '100%', zIndex: 999}} /> */}
                <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                  <Button id='timeIn' onClick={(e) => { this.addTimeIn(this.state.result, new Date().toLocaleTimeString(), e) }} variant="outline-primary">Time IN</Button>{' '}
                  <Button variant="outline-danger">Time OUT</Button>{' '}
                </div>

              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={this.handleClose}>
                  Close
                </Button>
              </Modal.Footer>
            </Modal>
            :
            <p>Scan your QR Code to take attendance</p>
        }

      </div>
    )
  }


}