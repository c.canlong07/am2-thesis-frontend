import React, { Component, useEffect, useState } from 'react';
// import {Link} from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import { height } from '@mui/material';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import TextareaAutosize from 'react-textarea-autosize';
import { Table, Modal, Button } from "react-bootstrap";
import moment from 'moment';
import { BudgetCards } from '../styles/RecommendationStyle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faPlusCircle, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import Chip from '@mui/material/Chip';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Swal from 'sweetalert2';
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { MainLayout, CustomDatePickDiv } from '../styles/DashboardLayout';

function CreateEmp() {

  const [budget, setbudget] = useState('');
  const [budget2, setbudget2] = useState(0);
  const [budget2AddMinus, setbudget2AddMinus] = useState('');
  const [budgetTotal, setbudgetTotal] = useState(0);
  const [addremove, setaddremove] = useState(false);
  const [budgetNote, setbudgetNote] = useState('');
  const [budgetDate, setbudgetDate] = useState(new Date(window.location.pathname.split('/')[2]));
  const [totalEmployees, settotalEmployees] = useState(0);
  const [allDate, setallDate] = useState([]);
  const [budgetThisWeek, setbudgetThisWeek] = useState([]);
  const [budgetAndMeals, setbudgetAndMeals] = useState([]);
  const [modalShow2, setmodalShow2] = useState(false);
  const [opening, setopening] = useState([]);
  const [allMeals, setallMeals] = useState([]);
  const [addThisBudget, setaddThisBudget] = useState(0);
  const [addThisBudgetArray, setaddThisBudgetArray] = useState([]);
  const [fixedRemainingBudget, setfixedRemainingBudget] = useState(0);
  const [AverageData, setAverageData] = useState([]);
  const [borrowed, setborrowed] = useState([]);
  const [loadOnce, setloadOnce] = useState(false);

  const [startDate, setDate] = React.useState(moment()
    .startOf("day")
    .subtract(14, "day")._d);
  const [endDate, setEndDate] = React.useState(new Date);

  let startOfWeek = moment().startOf('week').toDate();
  let endOfWeek = moment().endOf('week').toDate();


  const setDateRange = (d) => {
    setEndDate(d);
  }

  const selectDateHandler2 = (d) => {
    setDate(d)
  }

  const CustomInput = React.forwardRef((props, ref) => {
    return (
      <CustomDatePickDiv>
        <label className="btn-label" onClick={props.onClick} ref={ref}>
          {props.value || props.placeholder}
        </label>
        <FontAwesomeIcon style={{ height: '16px' }} icon={faCalendarAlt} onClick={props.onClick} />
      </CustomDatePickDiv>
    );
  });

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget')
      .then(res => {
        let theBudgets = res.data.data.filter(function (weeklyDate) {
          return moment(new Date(weeklyDate.mealDate)).isAfter(new Date(moment(startDate).subtract("days", 1).format("MM-DD-YYYY"))) &&
            moment(new Date(weeklyDate.mealDate)).isBefore(new Date(moment(endDate).add("days", 1).format("MM-DD-YYYY")));
        });

        setbudgetThisWeek(theBudgets);
      })
      .catch(error => {
        console.log(error);
      })

    // axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/mealTodayWeekly', {
    //   params: {
    //     startDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear(),
    //     endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
    //   }
    // })
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
      .then(res => {
        let theMeals = res.data.filter(function (weeklyDate) {
          return moment(new Date(weeklyDate.mealDate)).isAfter(new Date(moment(startDate).subtract("days", 1).format("MM-DD-YYYY"))) &&
            moment(new Date(weeklyDate.mealDate)).isBefore(new Date(moment(endDate).add("days", 1).format("MM-DD-YYYY")));
        });
        setallMeals(theMeals);

        let weeklyData = {};
        let ratingData = theMeals;
        for (let element of ratingData) {
          if (weeklyData[moment(element.mealDate).format('LL')]) {
            weeklyData[moment(element.mealDate).format('LL')].weekly = weeklyData[moment(element.mealDate).format('LL')].weekly + (element.rating !== undefined ? element.rating : 0);
            weeklyData[moment(element.mealDate).format('LL')].n++;
            weeklyData[moment(element.mealDate).format('LL')].mealType += '#' + element.mealType;
            weeklyData[moment(element.mealDate).format('LL')].mealName += '#' + element.mealName;
            weeklyData[moment(element.mealDate).format('LL')].mealDesc += '#' + element.mealDesc;
            weeklyData[moment(element.mealDate).format('LL')].mealIng += '#' + element.mealIng;
            weeklyData[moment(element.mealDate).format('LL')].mealServedEst += '#' + element.mealServedEst;
            weeklyData[moment(element.mealDate).format('LL')].mealTotalPrice += element.mealTotalPrice;
            weeklyData[moment(element.mealDate).format('LL')].priceBreakdown += '#' + element.mealTotalPrice;
            weeklyData[moment(element.mealDate).format('LL')].date = element.mealDate;
            weeklyData[moment(element.mealDate).format('LL')].day = element.mealDate;
            weeklyData[moment(element.mealDate).format('LL')]._id = element._id;
          } else {
            weeklyData[moment(element.mealDate).format('LL')] = {
              weekly: element.rating ? element.rating : 0,
              n: 1,
              mealType: element.mealType,
              mealName: element.mealName,
              mealDesc: element.mealDesc,
              mealIng: element.mealIng,
              mealServedEst: element.mealServedEst,
              priceBreakdown: element.mealTotalPrice,
              mealTotalPrice: element.mealTotalPrice,
              date: element.mealDate,
              day: element.mealDate,
              _id: element._id
            };
          }
        }

        let averageData = [];

        for (let element of Object.keys(weeklyData)) {
          averageData.push({
            mealDate: element.split('+')[0],
            users: weeklyData[element].n,
            rating: weeklyData[element].weekly / weeklyData[element].n,
            mealType: weeklyData[element].mealType,
            mealName: weeklyData[element].mealName,
            mealIng: weeklyData[element].mealIng,
            mealDesc: weeklyData[element].mealDesc,
            mealServedEst: weeklyData[element].mealServedEst,
            priceBreakdown: weeklyData[element].priceBreakdown,
            mealTotalPrice: weeklyData[element].mealTotalPrice,
            date: moment(weeklyData[element].date).format('ddd, MMM DD YYYY'),
            day: moment(weeklyData[element].date).format('dddd'),
            // _id: weeklyData[element]._id,
          });
        }

        // let theBudgetWithMeals = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1).map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
        let sortedBudget = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);
        let sortedMeals = averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);

        const result = sortedBudget.map(v => ({ ...v, ...sortedMeals.find(sp => sp.mealDate.toLowerCase() === v.mealDate.toLowerCase()) }));
        let theBudgetWithMeals = result;
        console.log('^^^^^^^^^^^^^^', res.data, theMeals, theBudgetWithMeals, sortedMeals, sortedBudget)
        setbudgetAndMeals(theBudgetWithMeals);
        setbudget2(theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[1]).format('LL') === moment(budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0));

      })
      .catch(error => {
        console.log(error);
      })

    axios.get(process.env.REACT_APP_BASE_URL + '/employee/allbudgets')
      .then(res => {
        let dateGather = [];
        const mapping = res.data.map(year => dateGather.push(moment(year.budgetDate).format('MMMM DD YYYY')));
        // this.setState({ allDate: dateGather });
        setallDate(dateGather);
      })
      .catch(error => {
        console.log(error);
      })
  }, [startDate, endDate])

  useEffect(() => {
    // console.log('whyyy', allMeals.length !== 0 && budgetAndMeals.length === 0 , addThisBudgetArray.length , budgetAndMeals.length !== AverageData.length,
    // budgetAndMeals , AverageData, loadOnce)
    //  console.log('theBudgetWithMeals',budgetAndMeals.filter(meal => meal.mealName !== undefined))

    let sortedBudget = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);
    let sortedMeals = AverageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);

    // const result = sortedBudget.map(v => ({ ...v, ...sortedMeals.find(sp => sp.mealDate.toLowerCase() === v.mealDate.toLowerCase()) }));

    // console.log('cccccccccccc', sortedMeals, AverageData, sortedBudget, budgetThisWeek,)

    if (allMeals.length !== 0 && budgetAndMeals.length === 0 && addThisBudgetArray.length || budgetAndMeals.length !== AverageData.length && loadOnce === false) {
      axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget', {
        params: {
          startDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear(),
          endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
        }
      })
        .then(res => {
          setbudgetThisWeek(res.data.data);
        })
        .catch(error => {
          console.log(error);
        })

      // axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/mealTodayWeekly', {
      //   params: {
      //     startDate: (startDate.getMonth() + 1) + '/' + (startDate.getDate() + 1) + '/' + startDate.getFullYear(),
      //     endDate: (endDate.getMonth() + 1) + '/' + (endDate.getDate() + 1) + '/' + endDate.getFullYear()
      //   }
      // })
      axios.get(process.env.REACT_APP_BASE_URL + '/employee/allmeals')
        .then(res => {
          let theMeals = res.data.filter(function (weeklyDate) {
            return moment(new Date(weeklyDate.mealDate)).isAfter(new Date(moment(startDate).subtract("days", 1).format("MM-DD-YYYY"))) &&
              moment(new Date(weeklyDate.mealDate)).isBefore(new Date(moment(endDate).add("days", 1).format("MM-DD-YYYY")));
          });
          setallMeals(theMeals);

          let weeklyData = {};
          let ratingData = theMeals;
          for (let element of ratingData) {
            if (weeklyData[moment(element.mealDate).format('LL')]) {
              weeklyData[moment(element.mealDate).format('LL')].weekly = weeklyData[moment(element.mealDate).format('LL')].weekly + (element.rating !== undefined ? element.rating : 0);
              weeklyData[moment(element.mealDate).format('LL')].n++;
              weeklyData[moment(element.mealDate).format('LL')].mealType += '#' + element.mealType;
              weeklyData[moment(element.mealDate).format('LL')].mealName += '#' + element.mealName;
              weeklyData[moment(element.mealDate).format('LL')].mealDesc += '#' + element.mealDesc;
              weeklyData[moment(element.mealDate).format('LL')].mealIng += '#' + element.mealIng;
              weeklyData[moment(element.mealDate).format('LL')].mealServedEst += '#' + element.mealServedEst;
              weeklyData[moment(element.mealDate).format('LL')].mealTotalPrice += element.mealTotalPrice;
              weeklyData[moment(element.mealDate).format('LL')].priceBreakdown += '#' + element.mealTotalPrice;
              weeklyData[moment(element.mealDate).format('LL')].date = element.mealDate;
              weeklyData[moment(element.mealDate).format('LL')].day = element.mealDate;
              weeklyData[moment(element.mealDate).format('LL')]._id = element._id;
            } else {
              weeklyData[moment(element.mealDate).format('LL')] = {
                weekly: element.rating ? element.rating : 0,
                n: 1,
                mealType: element.mealType,
                mealName: element.mealName,
                mealDesc: element.mealDesc,
                mealIng: element.mealIng,
                mealServedEst: element.mealServedEst,
                priceBreakdown: element.mealTotalPrice,
                mealTotalPrice: element.mealTotalPrice,
                date: element.mealDate,
                day: element.mealDate,
                _id: element._id
              };
            }
          }

          let averageData = [];

          for (let element of Object.keys(weeklyData)) {
            averageData.push({
              mealDate: element.split('+')[0],
              users: weeklyData[element].n,
              rating: weeklyData[element].weekly / weeklyData[element].n,
              mealType: weeklyData[element].mealType,
              mealName: weeklyData[element].mealName,
              mealIng: weeklyData[element].mealIng,
              mealDesc: weeklyData[element].mealDesc,
              mealServedEst: weeklyData[element].mealServedEst,
              priceBreakdown: weeklyData[element].priceBreakdown,
              mealTotalPrice: weeklyData[element].mealTotalPrice,
              date: moment(weeklyData[element].date).format('ddd, MMM DD YYYY'),
              day: moment(weeklyData[element].date).format('dddd'),
              // _id: weeklyData[element]._id,
            });
          }
          setAverageData(averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1));
          // let mergeBudgetMeal = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1).map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
          // let theBudgetWithMeals = mergeBudgetMeal;
          // let theBudgetWithMeals = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1).map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
          let sortedBudget = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);
          let sortedMeals = averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);

          const result = sortedBudget.map(v => ({ ...v, ...sortedMeals.find(sp => sp.mealDate.toLowerCase() === v.mealDate.toLowerCase()) }));
          let theBudgetWithMeals = result;
          setbudgetAndMeals(theBudgetWithMeals);
          setbudget2(theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[1]).format('LL') === moment(budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0));
          // setbudget(Number(budget))
        })
        .catch(error => {
          console.log(error);
        })

      setloadOnce(true);
    }


  })

  useEffect(() => {
    axios.get(process.env.REACT_APP_BASE_URL + '/employee/check/weeklybudget')
      .then(res => {
        let theBudgets = res.data.data.filter(function (weeklyDate) {
          return moment(new Date(weeklyDate.mealDate)).isAfter(new Date(moment(startDate).subtract("days", 1).format("MM-DD-YYYY"))) &&
            moment(new Date(weeklyDate.mealDate)).isBefore(new Date(moment(endDate).add("days", 1).format("MM-DD-YYYY")));
        });
        setbudgetThisWeek(theBudgets);
        // let mergeBudgetMeal = theBudgets.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1).map((item, i) => Object.assign({}, item, AverageData[i]));
        // let theBudgetWithMeals = mergeBudgetMeal;
        // let theBudgetWithMeals = budgetThisWeek.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1).map((item, i) => Object.assign({}, item, averageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1)[i]));
        let sortedBudget = theBudgets.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);
        let sortedMeals = AverageData.sort((a, b) => a.mealDate.toLowerCase() > b.mealDate.toLowerCase() ? 1 : -1);

        const result = sortedBudget.map(v => ({ ...v, ...sortedMeals.find(sp => sp.mealDate.toLowerCase() === v.mealDate.toLowerCase()) }));
        let theBudgetWithMeals = result;
        setbudgetAndMeals(theBudgetWithMeals);
        // setbudget(Number(budget))
        setbudget2(theBudgetWithMeals.filter(x => x.usedBudget?.length === 1 && moment(x.usedBudget?.[0].split('/')[1]).format('LL') === moment(budgetDate).format('LL')).reduce((n, { usedBudget }) => n + Number(usedBudget[0].split('/')[0]), 0));
      })
      .catch(error => {
        console.log(error);
      })

  }, [addremove, budgetDate, startDate, endDate])

  const onValueChange = (e, other) => {
    if (e.target.dataset.name === 'budget') {
      setbudget(e.target.value);
    } else if (e.target.dataset.name === 'budgetNote') {
      setbudgetNote(e.target.value);
    }
  }

  const selectDateHandler = (value) => {
    setbudgetDate(value);
    setaddThisBudget(0);
    setaddThisBudgetArray([]);
  }

  const onSubmit = (e) => {
    e.preventDefault();

    const meals = {
      budget: Number(budget) + budget2,
      budgetNote: budgetNote,
      budgetDate: moment(budgetDate).format('MM-DD-YYYY'),
      shared: budget2 ? true : false,
      sharedBudget: budget2,
    }

    axios.post(process.env.REACT_APP_BASE_URL + '/employee/add/budget', meals)
      .then(res =>
        Swal.fire({
          title: 'Budget has been Created',
          text: "You can now add meal to this specific Budget.",
          icon: 'success',
          confirmButtonText: 'Okay'
        }).then((result) => {
          if (result.isConfirmed) {
            window.location = "/meals"
          }
        })
      )
      .catch(err => console.log('Error :' + err));
  }

  const minusAdd = ((value, operation) => {
    if (operation == 'add') {
      setbudget2AddMinus(`add/${value}`)

      setbudget2(budget2 + value)
      // setbudget(Number(budget) + Number(value))
    } else {
      setbudget2AddMinus(`minus/${value}`)
      setbudget2(budget2 > value ? budget2 - value : value - budget2)
      // setbudget(budget > value ? (Number(budget) - Number(value)) : (Number(value) - Number(budget)))
    }

    // operation === 'add' ? setbudget(budget + value) : setbudget(budget > value ? budget - value : value - budget)
  })





  const addRemainingBudget = (station) => {
    Swal.fire({
      title: station.budget - station.mealTotalPrice,
      // text: `Add the remaining budget of ${moment(station.budgetDate).format('LL')}?`,
      html:
        `<h4>Add the remaining budget of ${moment(station.budgetDate).format('LL')} to ${moment(budgetDate).format('LL')}?</h4>` +
        '<strong style="color: red; white-space: pre-wrap;">NOTE:</strong> ' +
        '<span style="color: red;">Please make sure you want to add the Remaining Budget to the Date Selected.</span>',
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.isConfirmed) {
        // setaddThisBudget(station.budget - station.mealTotalPrice);
        setaddThisBudgetArray([...addThisBudgetArray, `${!isNaN(Number(station.budget - station.mealTotalPrice)) ? Number(station.budget - station.mealTotalPrice)
          : station.budget}/${moment(station.budgetDate).format('LL')}`]);

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/add/usedremainingbudget',
          {
            params: {
              id: station._id, remainingBudget: `${!isNaN(Number(station.budget - station.mealTotalPrice)) ? Number(station.budget - station.mealTotalPrice)
                : station.budget}/${moment(budgetDate).format('LL')}`
            }
          })
          .then(res => {
            Swal.fire(
              'Added!',
              'Budget has been added.',
              'success'
            );
            setaddremove(!addremove);
            minusAdd(!isNaN(Number(station.budget - station.mealTotalPrice)) ? Number(station.budget - station.mealTotalPrice)
              : Number(station.budget), 'add')

            // window.location.href = "/meals";
          })
          .catch(error => {
            console.log(error);
          })
      }
    })

  }

  const removeRemainingBudget = (station) => {

    Swal.fire({
      title: station.budget - station.mealTotalPrice,
      text: `Deduct the remaining budget of ${moment(station.budgetDate).format('LL')}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove it!'
    }).then((result) => {
      if (result.isConfirmed) {
        // setbudget(budget > station.mealTotalPrice ? budget - (station.budget - station.mealTotalPrice) : (station.budget - station.mealTotalPrice) - budget);
        setaddThisBudgetArray(addThisBudgetArray.filter(item => item !== `${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`))

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/remove/removedremainingbudget',
          {
            params: {
              id: station._id, remainingBudget: `${!isNaN(station.budget - station.mealTotalPrice) ? station.budget - station.mealTotalPrice
                : station.budget}/${moment(budgetDate).format('LL')}`
            }
          })
          .then(res => {
            Swal.fire(
              'Deducted!',
              'Budget has been deducted.',
              'success'
            );
            setaddremove(!addremove);
            minusAdd(!isNaN(Number(station.budget - station.mealTotalPrice)) ? Number(station.budget - station.mealTotalPrice)
              : Number(station.budget), 'minus')
          })
          .catch(error => {
            console.log(error);
          })
      }
    })

  }

  return (
    <LayoutForm>
      <MainLayout>
        <div className="date-con" id="date-con" style={{ marginLeft: '46%', boxShadow: 'none', marginTop: '-6%', position: 'absolute' }}>
          <>
            <div id="btn" className="flex-container2" style={{ paddingLeft: '3%' }}>
              <div><label className="start-date wordings" style={{ padding: '0' }}>Start Date:  </label> </div>
              <div className="start-date" style={{ width: '11vw' }}> <DatePicker
                className=""
                dateFormat="yyyy/MM/dd"
                selected={startDate}
                onChange={selectDateHandler2}
                maxDate={endDate}
                todayButton={"Today"}
                customInput={<CustomInput />}
              /></div>
              <div><label className="end-date wordings" style={{ padding: '0', paddingTop: '0' }}>End Date:  </label> </div>
              <div className="start-date" style={{ width: '8vw' }}>
                <DatePicker
                  className="form-control"
                  dateFormat="yyyy/MM/dd"
                  selected={endDate}
                  onChange={setDateRange}
                  minDate={startDate}
                  todayButton={"Today"}
                  customInput={<CustomInput />}
                />

              </div>

            </div>
          </>
        </div>
      </MainLayout>
      <Modal
        show={modalShow2}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        scrollable='true'
      >
        <Modal.Header style={{ background: 'rgb(32, 51, 84)', color: 'white', }} >
          <Modal.Title id="contained-modal-title-vcenter">
            Budget Details
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Divider style={{ marginBottom: '3%' }}>
            <Chip label="Budget Details" />
          </Divider>
          <BudgetCards>
            {/* <p>Request to ADD: <h6>{opening.toString()}</h6> </p> */}
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
              <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar alt="B" src="/static/images/avatar/1.jpg" />
                </ListItemAvatar>
                <ListItemText
                  primary={opening}
                  secondary={

                    budgetAndMeals.filter(v => moment(v.budgetDate).format('LL') === opening).map((station, i) =>
                      <React.Fragment>
                        <Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          Budget Note: &nbsp;
                        </Typography>
                        {station.budgetNote}<br></br>

                        <Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          Budget: &nbsp;
                        </Typography>
                        {station.budget} <br></br>

                        <Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          Used Budget: &nbsp;
                        </Typography>
                        {!isNaN(station.mealTotalPrice) ? station.mealTotalPrice : 'None'} <br></br>

                        <Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          Shared Budget To: &nbsp;
                        </Typography>
                        {station.usedBudget?.length ? station.usedBudget[0].split('/')[1] + " (" + station.usedBudget[0].split('/')[0] + ") " : 'None'} <br></br>

                        <Typography
                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          Remaining Budget: &nbsp;
                        </Typography>
                        {!isNaN(station.budget - station.mealTotalPrice - station.usedBudget2) ?
                          station.budget - station.mealTotalPrice - station.usedBudget2 : station.budget} <br></br>
                      </React.Fragment>

                    )
                  }
                />
              </ListItem>
              <Divider variant="inset" component="li" />
            </List>
          </BudgetCards>

          <Divider style={{ marginBottom: '3%' }}>
            <Chip label="Meal within this Budget" />
          </Divider>


          <BudgetCards>
            {/* <p>Request to ADD: <h6>{opening.toString()}</h6> </p> */}
            {
              allMeals.filter(v => moment(v.mealDate).format('LL') === opening).length !== 0 ?
                allMeals.filter(v => moment(v.mealDate).format('LL') === opening).map((station, index) =>
                  <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                    <ListItem alignItems="flex-start">
                      <ListItemAvatar>
                        <Avatar alt={(index + 1).toString()} src="/static/images/avatar/1.jpg" />
                      </ListItemAvatar>
                      <ListItemText
                        primary={moment(station.mealDate).format('LL') + ' (' + station.mealTotalPrice + ')'}
                        secondary={

                          <React.Fragment>
                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Meal Name: &nbsp;
                            </Typography>
                            {station.mealName}<br></br>

                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Meal Type: &nbsp;
                            </Typography>
                            {station.mealType}<br></br>
                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Meal Description: &nbsp;
                            </Typography>
                            {station.mealDesc}<br></br>

                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Meal Ingredients: &nbsp;
                            </Typography>
                            {station.mealIng}<br></br>

                            <Typography
                              sx={{ display: 'inline' }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Meal Serve Estimation: &nbsp;
                            </Typography>
                            {station.mealServedEst}<br></br>
                          </React.Fragment>

                        }
                      />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                  </List>

                )
                : <p style={{ textAlign: 'center', color: 'gray', padding: '2%' }}>There is no Meal for this Budget</p>
            }

          </BudgetCards>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => [setmodalShow2(false), setopening([])]}>Close</Button>
        </Modal.Footer>
      </Modal>
      <div className="container" style={{ marginTop: '7%' }}>
        <h1>Budget</h1>

        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Budget Date</th>
              <th>Budget Day</th>
              <th>Total Budget</th>
              <th>Excess Budget</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody >
            {budgetAndMeals.sort((a, b) => -a.budgetDate.localeCompare(b.budgetDate)).map((station, i) =>
              <tr>
                <td>{i + 1}</td>
                <td>{moment(station.budgetDate).format('LL')}</td>
                <td>{moment(station.budgetDate).format('dddd')}</td>
                <td>{station.budget}</td>
                <td style={{ zIndex: 9999 }} >{
                  !isNaN(station.budget - station.mealTotalPrice - station.usedBudget2) ?
                    station.budget - station.mealTotalPrice - station.usedBudget2 : station.budget
                } </td>
                <td>
                  <FontAwesomeIcon
                    icon={faInfoCircle} size="lg" style={{ color: 'gray', marginRight: '8%', height: '20px' }}
                    onClick={() => [setmodalShow2(true), setopening(moment(station.budgetDate).format('LL'))]}
                  />

                  {
                    addThisBudgetArray.includes(`${station.budget - station.mealTotalPrice}/${moment(station.budgetDate).format('LL')}`)
                      ||
                      station.usedBudget?.filter(s => s.includes(moment(budgetDate).format('LL'))).length === 1
                      ?
                      <FontAwesomeIcon
                        icon={faMinusCircle} size="lg" style={allDate.includes(moment(budgetDate).format('MMMM DD YYYY')) ?
                          { color: 'red', marginRight: '8%', height: '20px', pointerEvents: 'none' } :
                          { color: 'red', marginRight: '8%', height: '20px' }}
                        onClick={() => removeRemainingBudget(station)}
                      /> :
                      station.budget - station.mealTotalPrice - station.usedBudget2 !== 0 ?
                        <FontAwesomeIcon
                          icon={faPlusCircle} size="lg" style={allDate.includes(moment(budgetDate).format('MMMM DD YYYY')) ?
                            { color: 'green', marginRight: '8%', height: '20px', pointerEvents: 'none' } :
                            { color: 'green', marginRight: '8%', height: '20px' }}
                          onClick={() => addRemainingBudget(station)}
                        /> : null
                  }


                </td>
              </tr>
            )
            }
          </tbody>
        </Table>

        <form onSubmit={onSubmit}>
          <div className="wholeForm">
            <div className="form">

              <div className="leftSide">
                <div className="form-group" style={{ padding: '0%' }}>
                  <label>Date
                    {/* {addThisBudgetArray.length !== 0 ? <span style={{ color: 'red', textTransform: 'capitalize' }}> *Disabled</span> : null} */}
                  </label>
                  <DatePicker
                    id="budgetDate"
                    className="form-control"
                    dateFormat="yyyy/MM/dd"
                    selected={budgetDate}
                    onChange={(value) => selectDateHandler(value)}
                    // minDate={today}
                    // readOnly={addThisBudgetArray.length === 0 ? false : true}
                    todayButton={"Today"} />
                </div>
                <Divider style={{ marginBottom: '3%' }}>
                </Divider>
                <div className="form-group">
                  <label>Input Budget</label>
                  <input style={{ width: '50%', float: 'right' }} type="number" min="0" className="form-control" data-name="budget" value={budget} required={Number(budget) + budget2 <= 0 ? true : false} onChange={(e) => onValueChange(e)} />
                </div>
                <div className="form-group">
                  <label>Borrowed Budget</label>
                  <input style={{ width: '50%', float: 'right' }} readOnly type="number" className="form-control" data-name="budget" value={budget2} />
                </div>

                <div className="form-group">
                  <h2 style={{ textAlign: 'center', color: 'green', fontWeight: 'bold' }}>{Number(budget) + budget2}</h2>
                  <p style={{ textAlign: 'center', textTransform: 'uppercase', marginBottom: '0px', color: 'gray' }}>Overall Budget</p>

                </div>
                <Divider style={{ marginBottom: '3%' }}>
                </Divider>
                <div className="form-group">
                  <label>Budget Note</label>
                  <TextareaAutosize
                    className="textArea form-control" data-name="budgetNote" value={budgetNote} required onChange={(e) => onValueChange(e)}
                    minRows={3}
                    maxRows={6}
                    placeholder="Description..."
                  />
                </div>


              </div>
            </div>

            <Buttons>
              <button type="submit" style={allDate.includes(moment(budgetDate).format('MMMM DD YYYY')) ? { marginRight: 10, pointerEvents: 'none', color: 'white', background: 'red', border: 'red' } : { marginRight: 10 }} className="btn btn-primary">
                {allDate.includes(moment(budgetDate).format('MMMM DD YYYY')) ? 'Budget for Selected Date Already Exist' : 'Save Budget'}
              </button>
              <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
            </Buttons>

          </div>
        </form>
      </div>
    </LayoutForm>

  )
}

export default CreateEmp;
