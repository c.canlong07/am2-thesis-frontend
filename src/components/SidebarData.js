import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import QrCodeIcon from '@mui/icons-material/QrCode';
import DashboardIcon from '@mui/icons-material/Dashboard';
import GroupIcon from '@mui/icons-material/Group';
import RecommendIcon from '@mui/icons-material/Recommend';
import SettingsIcon from '@material-ui/icons/Settings';

export const AdminSidebarData = [
  {
    id: 1,
    title: "Dashboard",
    cName: "sidebar-item",
    icon: <DashboardIcon />,
    path: "/dashboard"
  },
  {
    id: 2,
    title: "Attendance",
    cName: "sidebar-item",
    icon: <AssignmentIndIcon />,
    path: "/attendance"
  },
  {
    id: 3,
    title: "Meals & Budgets",
    cName: "sidebar-item",
    icon: <FastfoodIcon />,
    path: "/meals"
  },
  {
    id: 4,
    title: "Employees",
    cName: "sidebar-item",
    icon: <AccountCircleIcon />,
    path: "/employees"
  },
  {
    id: 5,
    title: "QR Code",
    cName: "sidebar-item",
    icon: <QrCodeIcon />,
    path: "/qrcode"
  },
  // {
  //   id: 6,
  //   title: "Users",
  //   cName: "sidebar-item",
  //   icon: <GroupIcon />,
  //   path: "/users"
  // },
  {
    id: 6,
    title: "Meal Records",
    cName: "sidebar-item",
    icon: <GroupIcon />,
    path: "/mealrecords"
  },
  {
    id: 7,
    title: "Recommendation",
    cName: "sidebar-item",
    icon: <RecommendIcon />,
    path: "/recommendation"
  },
  {
    id: 8,
    title: "Settings",
    cName: "sidebar-item",
    icon: <SettingsIcon />,
    path: "/admin"
  },
];

export const EmployeeSidebarData = [
  {
    id: 1,
    title: "Meals",
    cName: "sidebar-item",
    icon: <FastfoodIcon />,
    path: "/meals"
  },
  {
    id: 2,
    title: "User",
    cName: "sidebar-item",
    icon: <GroupIcon />,
    path: "/users"
  },
];

export const FrontdeskSidebarData = [
  {
    id: 5,
    title: "QR Code",
    cName: "sidebar-item",
    icon: <QrCodeIcon />,
    path: "/qrcode"
  },
];

export const CanteenSidebarData = [
  {
    id: 1,
    title: "Dashboard",
    cName: "sidebar-item",
    icon: <DashboardIcon />,
    path: "/dashboard"
  },
  {
    id: 2,
    title: "Attendance",
    cName: "sidebar-item",
    icon: <AssignmentIndIcon />,
    path: "/attendance"
  },
  {
    id: 3,
    title: "Meals & Budgets",
    cName: "sidebar-item",
    icon: <FastfoodIcon />,
    path: "/meals"
  },
  // {
  //   id: 4,
  //   title: "Employees",
  //   cName: "sidebar-item",
  //   icon: <AccountCircleIcon />,
  //   path: "/employees"
  // },
  // {
  //   id: 5,
  //   title: "QR Code",
  //   cName: "sidebar-item",
  //   icon: <QrCodeIcon />,
  //   path: "/qrcode"
  // },
  {
    id: 6,
    title: "Meal Records",
    cName: "sidebar-item",
    icon: <GroupIcon />,
    path: "/mealrecords"
  },
  {
    id: 7,
    title: "Recommendation",
    cName: "sidebar-item",
    icon: <RecommendIcon />,
    path: "/recommendation"
  },
  // {
  //   id: 8,
  //   title: "Settings",
  //   cName: "sidebar-item",
  //   icon: <GroupIcon />,
  //   path: "/admin"
  // },
];
