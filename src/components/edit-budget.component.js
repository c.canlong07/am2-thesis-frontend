import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Buttons, LayoutForm } from '../styles/LayoutForm';
import axios from 'axios';
import TextareaAutosize from 'react-textarea-autosize';
import Swal from 'sweetalert2';
export default class EditEmp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            budgetNote: '',
            budget: '',
            sharedBudget: '',
            overallBudget: 0
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.DefaultChange = this.DefaultChange.bind(this);

    }



    componentDidMount() {
        axios.get(process.env.REACT_APP_BASE_URL + '/employee/getBudget/' + this.props.match.params.id)
            .then(res => {
                this.setState({
                    budgetNote: res.data.budgetNote,
                    budget: res.data.budget > res.data.sharedBudget ? res.data.budget - res.data.sharedBudget : res.data.sharedBudget - res.data.budget,
                    sharedBudget: res.data.sharedBudget ? res.data.sharedBudget : 0,
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

    onValueChange(e) {
        this.setState({
            [e.target.dataset.name]: e.target.value
        })
    }

    onSelectChange(e) {
        this.setState({
            ['mealType']: e.target.value
        })
    }

    DefaultChange(e) {
        this.setState({
            ['defaultMeal']: e.target.checked
        })
    }

    onSubmit(e) {
        e.preventDefault();

        const employee = {
            budgetNote: this.state.budgetNote,
            budget: Number(this.state.budget) + Number(this.state.sharedBudget),
        }

        axios.post(process.env.REACT_APP_BASE_URL + '/employee/updateBudget/' + this.props.match.params.id, employee)
            .then(res =>
                Swal.fire({
                    title: 'Budget has been Updated',
                    text: "Successfully Update Budget",
                    icon: 'success',
                    confirmButtonText: 'Okay'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = "/meals"
                    }
                })
            )
            .catch(err => console.log('Error :' + err));
    }

    render() {
        return (
            <LayoutForm>
                <div className="container">
                    <h1>Update Budget info</h1>
                    <form onSubmit={this.onSubmit}>
                        <div className="wholeForm">
                            <div className="form">

                                <div className="leftSide">
                                    <div className="form-group">
                                        <label>Budget Note</label>
                                        <TextareaAutosize type="text" className="form-control" data-name="budgetNote" required onChange={this.onValueChange} value={this.state.budgetNote} />
                                    </div>
                                    {/* <div className="form-group">
                                        <label>Budget</label>
                                        <input type="number" className="form-control" data-name="budget" required onChange={this.onValueChange} value={this.state.budget} />
                                    </div> */}

                                    <div className="form-group">
                                        <label>Input Budget</label>
                                        <input style={{ width: '50%', float: 'right' }} type="number" min="0" className="form-control" data-name="budget" onChange={this.onValueChange} value={this.state.budget} />
                                    </div>
                                    <div className="form-group">
                                        <label>Borrowed Budget</label>
                                        <input style={{ width: '50%', float: 'right' }} readOnly type="number" className="form-control" data-name="sharedBudget" value={this.state.sharedBudget} />
                                    </div>
                                    <div className="form-group">
                                        <h2 style={{ textAlign: 'center', color: 'green', fontWeight: 'bold' }}>{Number(this.state.budget) + Number(this.state.sharedBudget)}</h2>
                                        <p style={{ textAlign: 'center', textTransform: 'uppercase', marginBottom: '0px', color: 'gray' }}>Overall Budget</p>

                                    </div>
                                </div>



                                {/* <div className="form-group">
                            <label>Meal Date</label>
                            <input style={{width: '50%'}} type="text"  className="form-control" data-name="mealDate"  required onChange={this.onValueChange} value={this.state.mealDate} />
                        </div> */}


                            </div>

                            <Buttons>
                                <button type="submit" style={{ marginRight: 10 }} className="btn btn-primary">Save Budget</button>
                                <Link to='/meals' style={{ color: '#203354', borderColor: '#203354' }} className="btn btn-outline-dark">Back</Link>
                            </Buttons>

                        </div>
                    </form>
                </div>
            </LayoutForm>
        )
    }


}