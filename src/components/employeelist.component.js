import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import QRCode from 'react-qr-code';
import QRCode2 from "qrcode.react";
import QRCode from 'qrcode'
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import {
    Button,
    Modal,
} from "react-bootstrap";
const Employee = props => {
    return (
        <tr>
            <td>{props.employee.firstname}</td>
            <td>{props.employee.lastname}</td>
            <td>{props.employee.address}</td>
            <td>{props.employee.department}</td>
            <td>{props.employee.email}</td>
            <td>{props.employee.cpnumber}</td>
            <td className="text-center">
                <Link to={'/edit/' + props.employee._id} className="btn btn-sm btn-primary">Edit</Link>
                <a href="#" onClick={() => { props.deleteEmployee(props.employee._id) }} className="btn btn-sm btn-danger">Delete</a>
                <a href="#" onClick={() => { props.generateQr(props.employee._id) }
                } className="btn btn-sm btn-info">Generate</a>
            </td>
        </tr>
    )
}

export default class EmpList extends Component {

    constructor(props) {
        super(props)

        this.deleteEmployee = this.deleteEmployee.bind(this);
        this.generateQr = this.generateQr.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.download = this.download.bind(this);
        this.state = { employee: [], toShow: false, toGenerate: '' }
    }

    componentDidMount() {

        axios.get(process.env.REACT_APP_BASE_URL + '/employee/')
            .then(res => {
                this.setState({ employee: res.data })
            })
            .catch(error => {
                console.log(error);
            })


    }

    deleteEmployee(id) {
        axios.delete(process.env.REACT_APP_BASE_URL + '/employee/' + id)
            .then(res => console.log(res.data))
        this.setState({
            employee: this.state.employee.filter(el => el._id !== id)
        })
    }



    employeeDeclarations() {
        return this.state.employee.map(currentEmployee => {
            return <Employee employee={currentEmployee} deleteEmployee={this.deleteEmployee} generateQr={this.generateQr} key={currentEmployee._id} />
        })
    }

    generateQr(id) {
        this.setState({ toShow: true, toGenerate: id })
    }
    handleClose() {
        this.setState({ toShow: false });
    }
    download = function () {
        const link = document.createElement("a");
        link.download = "filename.png";
        link.href = document.getElementById("canvas").toDataURL();
        link.click();
    };


    render() {
        return (
            <div className="container">
                <h1>Employee List</h1>
                <table className="table table-bordered table-hover">
                    <thead className="thead-dark">
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Address</th>
                            <th>Department</th>
                            <th>Email</th>
                            <th>Cellphone Number</th>
                            <th className="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.employeeDeclarations()}
                    </tbody>
                </table>
                <div align="center">
                    {/* <canvas  id="canvas" width={600} height={500} style={{maxWidth: '100%'}}/> */}
                    {/* <QRCode2 value={this.state.toGenerate} id="canvas" /> */}

                    <Modal show={this.state.toShow} onHide={this.handleClose} animation={false}>
                        <Modal.Header style={{ background: ' rgb(32, 51, 84)', color: 'white' }}>
                            <Modal.Title>QR Code Generator</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {/* <canvas id="canvas" width={600} height={500} style={{maxWidth: '100%', zIndex: 999}} /> */}
                            <div style={{ margin: 'auto', display: 'block', textAlign: 'center' }}>
                                <p>{this.state.employee}2222</p>
                                <QRCode2 style={{ width: '40%', height: '40%', }} value={this.state.toGenerate} id="canvas" />
                                <br /><br />
                                <Button type="button" className="mt-30" onClick={this.download}>
                                    Download QR
                                </Button>
                            </div>

                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    {/* <button onClick={this.generateQr}>
                        Generate QR!
                    </button> */}
                </div>
            </div>
        )
    }


}