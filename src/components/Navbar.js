import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { AdminSidebarData, EmployeeSidebarData, FrontdeskSidebarData, CanteenSidebarData } from "./SidebarData";
import styled from 'styled-components';
import logo from '../img/Logo1.png';
import admin from '../img/admin.png';
import { Dropdown, DropdownButton } from "react-bootstrap";





function Navbar() {
  const [sidebar, setSidebar] = useState();
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  let theUser = EmployeeSidebarData;

  const showSidebar = () => {
    setSidebar(sidebar);
  };

  if (user?.role === 'Admin' && localStorage.getItem("manage") === null) {
    theUser = AdminSidebarData;
  } else if (user?.role === 'Frontdesk' && localStorage.getItem("manage") === null) {
    theUser = FrontdeskSidebarData;
    // document.getElementById('sidebar-container').style.display = 'none';
    // setSidebar(false);
  } else if (user?.role === 'Canteen' && localStorage.getItem("manage") === null) {
    theUser = CanteenSidebarData;
  } else {
    theUser = EmployeeSidebarData;
  }

  const Logout = () => {
    window.location.href = '/';
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("user");
    localStorage.removeItem("manage");
  }

  console.log('window.location.pathname',window.location.pathname)


  return (
    <NavbarStyled>

      <div className="ham-burger-menu">

        <div className="logo">
          <img src={logo} alt="logo" />
        </div>

        <div className="admin">
          <span style={{ textTransform: 'capitalize', marginRight: 20 }}>{user?.firstname} {user?.lastname}</span>
          {/* <img src={admin} alt="admin" /> */}
          <DropdownButton id="dropdown-basic-button" title="">
            <Dropdown.Item onClick={Logout}>Logout</Dropdown.Item>
          </DropdownButton>
        </div>

      </div>



      <nav className={sidebar ? "sidebar-container active" : "sidebar-container"}
        style={user?.role === 'Frontdesk' && window.location.pathname === '/qrcode' ? { display: 'none' } : { display: 'block' }}
        id="sidebar-container">
        <ul className="sidebar-list">
          {theUser.map((sidebaritem) => {
            return (
              <li
                key={sidebaritem.id}
                className={sidebaritem.cName}
                onClick={showSidebar}

              >
                <Link className="row" to={sidebaritem.path} id="active">
                  <div id="icon">{sidebaritem.icon}</div>
                  <div id="title">{sidebaritem.title}</div>
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>

    </NavbarStyled>
  );
}
const NavbarStyled = styled.main`
      margin: 0;
      padding: 0;
  
    .ham-burger-menu {
      height: 10vh;    
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: white;
      box-shadow: 0px 2px 10px rgb(0,0,0, .20);
    
     }

    .logo{
      width:50%;
      display:flex;
      @media (max-width: 450px){
          width:40%;
          display:flex;
                 
        }
    }

    .logo img{
      flex-direction: center;
      margin-left: 10%;
      width: 13%;
    }

    .admin{
      color: black;
      align-items: right;
      justify-content: right;
      display: flex;
      margin-right: 2%;
      width:50%;
    }

   
    .admin img{
     margin-left: 10px;
      width: 15%;
    
    }

    .admin h2{
      margin-left:10%;
    }

    .nav-menu-icon {
      margin-left: 1rem;
      color: white;
      max-height: 2rem;
      background: none;
    }
    

    .sidebar-container {
      background-color: #203354;
      width: 15.8rem;
      height: 90vh;
      margin-top:10vh;
      border-radius: 0px 50px 0px 0px;
      display: flex;
      justify-content: left;
      position: fixed;
      top: 0;
      left: -16.6%;
      transition: 1s;
      z-index: 999;
    }

    .sidebar-container.active{
      left: 0;
      transition: 500ms;
      
    }
    .sidebar-container:hover {
      left: 0%;
      transition: 500ms;
    }

    .sidebar-item {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      height: 3rem;
      padding: 8px 0px 20px 16px;
    }

    .sidebar-list{
      padding-top: 20%;
      width: 100%;
    }

    .sidebar-item a {
      margin-top: 5rem;
      text-decoration: none;
      color: white;
      font-size: 1rem;
      height: 100%;
      width: 100%;
      display: flex;
      align-items: center;
      padding: 0 2rem;
      border-radius: 4px;
    }

    .sidebar-item a:hover {
      color:#203354;
      height: 150%;
      border-radius: 20px 0px 0px 20px;
      width: 106%;
      background-color:white;
    }


    .sidebar-items {
      margin-bottom: 2%;
      width: 150%;
    }

    .sidebar-container .sidebar-toggle {
      display: flex;
      height: 60px;
      justify-content: flex-start;
      align-items: center;
      margin-bottom: 0.7rem;
    }
    span {
    padding-left: 0.3rem;
  }

  .sidebar-item #icon{
    margin-right: 5%;
  }

  @media screen and (max-width:450px){
     .sidebar-container{
       left: -60%;
     }
    
    .logo img{
      width: 50%;
    }
     
  }

  @media screen and (max-width:400px){
    .sidebar-container{
      left: -60%;
    }
   
   .logo img{
     width: 50%;
   }
    
 }

  `;


export default Navbar;