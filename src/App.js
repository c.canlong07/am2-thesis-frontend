// import {BrowserRouter as Router, Route} from 'react-router-dom';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'font-awesome/css/font-awesome.min.css';
import '@fortawesome/fontawesome-svg-core/styles.css'
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store';
import jwt_decode from "jwt-decode";
import setAuthToken from "./util/setAuthToken";
import { setCurrentUser, logoutUser } from "./action/authAction";
import dotenv from 'dotenv'
// import Navbar from './components/navbar.component';
import EmpList from './components/employeelist.component';
import EditEmp from './components/edit-employee.component';
import EmpEdit from './components/employee-edit.component';
import EditMeal from './components/edit-meal.component';
import EditMealRecommendation from './components/edit-meal.component.recommendation';
import CreateEmp from './components/create-employee.component';
import QrCodReader from './components/admin.qrcodeReader';
import styled from 'styled-components';
import AttendancePage from './pages/AttendancePage';
import MealPage from './pages/MealPage';
import EmployeesPage from './pages/EmployeesPage';
import QrcodePage from './pages/QrcodePage';
import Navbar from "./components/Navbar";
import DashboardPage from './pages/DashboardPage';
import UsersPage from './pages/UsersPage';
import AdminPage from './pages/AdminPage';
import LoginPage from './pages/LoginPage';
import MealRecordsPage from './pages/MealRecordsPage';
import Recommendation from './pages/Recommendation';

//Meals
import CreateMeal from './components/create-meal.component2';
import BudgetMeal from './components/create-budget.component2';
import EditBudget from './components/edit-budget.component';
import { useEffect } from 'react';

if (localStorage.jwtToken) {

  const token = localStorage.jwtToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    // window.location.href = "./login";
  }
}
let theLocalStorageUser = JSON.parse(localStorage.getItem('user'));
let invalidUser = localStorage.getItem('invalid');

function App() {

  useEffect(() => {
    if (!theLocalStorageUser && !invalidUser && window.location.pathname !== '/') {
      localStorage.setItem('invalid', 'invalid')
      window.location.href = "/";
    }

    if (invalidUser) {
      console.log('theLocalStorageUser', theLocalStorageUser, window.location.pathname)
      window.location.href = "/";
      localStorage.removeItem("invalid");
    }
  })

  return (
    <>
      <Provider store={store} >
        <MainContentStyled>
          <BrowserRouter>
            <div className="ham-burger-menu" style={window.location.href === process.env.REACT_APP_FRONT_URL || window.location.href === 'https://awesome-goldberg-7d7538.netlify.app/' ||
              window.location.href === 'https://am2.netlify.app/' ? { display: 'none' } : { display: 'fixed' }} >
              <Navbar />
            </div>

            <Switch>
              <Route path="/dashboard" exact>
                <DashboardPage />
              </Route>
              <Route path="/recommendation" exact>
                <Recommendation />
              </Route>
              <Route path="/attendance" exact>
                <AttendancePage />
              </Route>
              <Route path="/meals" exact>
                <MealPage />
              </Route>
              {/* <Route path="/employees" exact component={EmpList} /> */}
              <Route path="/edit/:id" component={EditEmp} />
              <Route path="/editemp/:id" component={EmpEdit} />
              <Route path="/editMeal/:id" component={EditMeal} />
              <Route path="/editMealRecommendation/:id" component={EditMealRecommendation} />
              <Route path="/create" component={CreateEmp} />
              <Route path="/qrcodeReader" component={QrCodReader} />
              <Route path="/employees" exact>
                <EmployeesPage />
              </Route>
              <Route path="/qrcode" exact>
                <QrcodePage />
              </Route>
              <Route path="/users" exact>
                <UsersPage />
              </Route>
              <Route path="/admin" exact>
                <AdminPage />
              </Route>
              <Route path="/" exact>
                <LoginPage />
              </Route>

              <Route path="/createmeal" component={CreateMeal} />
              <Route path="/budgetmeal" component={BudgetMeal} />
              <Route path="/editBudget/:id" component={EditBudget} />
              <Route path="/mealrecords" exact>
                <MealRecordsPage />
              </Route>
            </Switch>
          </BrowserRouter>

        </MainContentStyled>
      </Provider>
    </>
  );
}
const MainContentStyled = styled.main`
    margin: 0;
    padding: 0;
    height: auto;
  
   
    

    @media screen and (max-width:1200px){
    margin-left: 0;
  }
  
  
`;

export default App;
